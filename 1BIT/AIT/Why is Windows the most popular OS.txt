[AIT] How Windows became so popular?
-----------------------------------------

- Introduction:
    - MS-DOS
    - Windows
    - Right decisions
    
- History of MS-DOS:
    - In the early PC market, Microsoft had established itself as the largest producer of computer programming languages, notably with an interpreted version of the BASIC language that had become the default standard on just about every major PC to date.
    - August 21 1980 – Licencing Microsoft langueges for IBM PC
    - Microsoft bought QDOS from Seattle Computer Products
    - 1981 – Project Chess - PC DOS
    - Although MS-DOS and PC DOS were initially developed in parallel by Microsoft and IBM, in subsequent years, the two products diverged, with recognizable differences in compatibility, syntax, and capabilities.

- History of Windows ( hitting the sweetspot ):
    - 1985 - Windows 1.0
    - At 9.35am on March 13, 1986, Microsoft’s stock went public to strong retail demand.
    - Windows continued to evolve, and in 1988, helped make Microsoft the world’s largest PC software company based on sales.
    - Windows 3.0 (1990) and 3.1 (1992) sold 10 million copies in their first two years, which was the highest number of any Windows version to date.
    - Windows 95 sold seven million copies in its first five weeks, and brought built-in support for a then little-known feature called the internet.
    - Windows 95 drove Windows into the mainstream, and thanks to the blossoming of the internet, drove PC sales. Microsoft’s stock price peaked at $58.72 a share on December 27, 1999, valuing the company at $50 billion.
    - $70 billion today

- Right decisions
    - Aggressive business tactics
        DOS spread thanks to Microsoft’s aggressive business tactics. 
        Salesmans kept comming to same person every day until they finaly bought MS-DOS. 
        For Microsoft, it was more important to get MS-DOS used on as many computers as possible rather than making a big amount of money from any one deal.
    - Miscrosoft didn't do the selling
        Bill Gates made their money by convincing computer hardware manufacturers (at first IBM, but soon others) to pre-load DOS and later Windows on their computers.
        Thus when people bought a computer, they invariably bought DOS or Windows.
        - Easy user interface
    - Price / Funcionality
    - Hardware-Software Compatibility
    - Games

- Windows and games:
    - 1980s – Macs had a reputation for having best games a PCs were more work related but that was the way companies wanted to go
    - 1983 - Apple hired a former president of Pepsi John Scully
    - they wanted to be taken more seriously and not just as machines for learning and fun... People were asking for macs in offices, but they've been rejected, because masc had a reputation
    - Apple threatned to pull their coverage from several magasines if they don't cut on games ads (Macworld)
    - they wanted to minimalize importance of games in Apple culture
    - but what Microsoft and IBM reliesied was that office software is't really HW intanse, so if you buy a PC for work, you don't have to buy a new one for a while
    - offices are always behind on HW and SW
    - Apple left gap in the market -> games
    - but Windows had limited access to HW from programers end -> DirectX was born in Win95
    - Games became big reason PCs got into our homes -> parent for productivity and children for games
    - so why there is so much games on PCs?

- Fun fact:
    "Gates earns roughly $114 a second, meaning that if he dropped a 100-dollar bill, it wouldn’t be worth his time stopping to pick it up."

- Sources:
    - http://forwardthinking.pcmag.com/software/286148-the-rise-of-dos-how-microsoft-got-the-ibm-pc-os-contract
    - http://home.bt.com/tech-gadgets/computing/history-of-microsoft-how-it-became-valuable-and-successful-11364045249599
    - https://www.quora.com/Why-is-Microsoft-Windows-so-popular
    - https://www.quora.com/Why-did-Microsoft-Windows-become-worlds-most-popular-Operating-System
    - https://youtu.be/GBq18kpPVSQ?list=WL

Thank You for Your attention.