// bit_array.h
// Reseni IJC-DU1, priklad A), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... makra a inline funkce pro praci s bitovym polem

#ifndef BIT_ARRAY_H_
#define BIT_ARRAY_H_

#include "error.h"


typedef unsigned long bit_array_t[];


#define size_of_array(array)\
            (sizeof((array)[0]) * 8)

#define ba_bit_to_byte(size)\
            ((size) % (sizeof(unsigned long) * 8) ?\
                (size) / (sizeof(unsigned long) * 8) + 1 :\
                (size) / (sizeof(unsigned long) * 8))

#define ba_mask(shift) (1lu << (shift))

#define ba_bounds(array, index)\
            ((array) != NULL && (array)[0] >= (index) && (index) >= 0 ? 1 : 0)

#define BA_SET_BIT(array, index, value)\
            if ((value)) {\
                (array)[(index) / size_of_array((array)) + 1] |= ba_mask((index) % size_of_array((array))); }\
            else {\
                (array)[(index) / size_of_array((array)) + 1] &= ~ba_mask((index) % size_of_array((array))); }

#define BA_GET_BIT(array, index)\
            (ba_mask((index) % size_of_array((array))) & (array)[(index) / size_of_array((array)) + 1] ?\
                    1 :\
                    0)

#define ba_create(array, size)\
            if ((size) <= 0) {\
                error_msg("Velikost pole musi byt vetsi nez 0!\n"); }\
            unsigned long (array)[ba_bit_to_byte((size)) + 1] = {(size), 0}

#ifndef USE_INLINE

#define ba_size(array)\
                (array)[0]

#define ba_set_bit(array, index, value)\
            if (!ba_bounds((array), (index))) {\
                error_msg("Index %lu mimo rozsah 0..%lu\n", (unsigned long)(index), (unsigned long)ba_size(array)); }\
            BA_SET_BIT((array), (index), (value))

#define ba_get_bit(array, index)\
            (!ba_bounds((array), (index)) ?\
                error_msg("Index %lu mimo rozsah 0..%lu\n", (unsigned long)(index), (unsigned long)ba_size((array))), 0 :\
                BA_GET_BIT((array), (index)))

#else

// Inline funkce

static inline unsigned long ba_size(bit_array_t array) {
    
    if (array != NULL)
        return array[0];
    error_msg("Pole == NULL\n");
    return 0;   
}

static inline void ba_set_bit(bit_array_t array, unsigned long index, unsigned value) {

    if (ba_bounds(array, index)) {
        BA_SET_BIT(array, index, value);
    }
    else {
        error_msg("Index %lu mimo rozsah 0..%lu\n", (unsigned long)index, (unsigned long)ba_size(array));
    }
}

static inline unsigned ba_get_bit(bit_array_t array, unsigned long index) {

    if (ba_bounds(array, index)) {
        return BA_GET_BIT(array, index);
    }
    else {
        error_msg("Index %lu mimo rozsah 0..%lu\n", (unsigned long)index, (unsigned long)ba_size(array));
        return 0;
    }
}

#endif // USE_INLINE

#endif // BIT_ARRAY_H_
