// eratosthenes.c
// Reseni IJC-DU1, priklad A), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... Eratosthenovo sito

#include "eratosthenes.h"

void Eratosthenes(bit_array_t primes) {
    
    unsigned long array_size = ba_size(primes);
    unsigned long array_sqrt = sqrt(array_size);
    ba_set_bit(primes, 0, 1);
    ba_set_bit(primes, 1, 1);

    for (unsigned long i = 2; i <= array_sqrt; i++) {
        if (!ba_get_bit(primes, i)) {
            for (unsigned long j = 2*i; j <= array_size; j += i) {
                ba_set_bit(primes, j, 1);
            }
        }
    }
    return;
}
