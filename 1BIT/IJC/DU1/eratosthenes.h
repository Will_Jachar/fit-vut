// eratosthenes.h
// Reseni IJC-DU1, priklad A), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... Eratosthenovo sito

#ifndef ERATOSTHENES_H_
#define ERATOSTHENES_H_

#include <math.h>
#include "bit_array.h"

void Eratosthenes(bit_array_t primes);

#endif
