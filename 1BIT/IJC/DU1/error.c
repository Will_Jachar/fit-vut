// error.c
// Reseni IJC-DU1, priklad B), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... funkce pro vypis chybovych hlaseni

#include "error.h"

void warning_msg(const char *fmt, ...) {

    va_list arglist;
    va_start(arglist, fmt);
    vfprintf(stderr, fmt, arglist);
    va_end(arglist);
}

void error_msg(const char *fmt, ...) {
    
    va_list arglist;
    va_start(arglist, fmt);
    vfprintf(stderr, fmt, arglist);
    va_end(arglist);
    exit(1);
}
