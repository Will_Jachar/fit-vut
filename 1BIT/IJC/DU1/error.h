// error.h
// Reseni IJC-DU1, priklad B), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... rozhrani pro soubor error.c

#ifndef ERROR_H_
#define ERROR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void warning_msg(const char *fmt, ...);

void error_msg(const char *fmt, ...);

#endif // ERROR_H_
