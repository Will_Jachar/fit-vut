// ppm.c
// Reseni IJC-DU1, priklad B), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... funkce pro praci s PPM souborem

#include "ppm.h"

struct ppm * ppm_read(const char * filename) {

    FILE *fr;
    struct ppm *picture;
    unsigned xsize;
    unsigned ysize;
    unsigned color_depth;
    unsigned long data_size;

    fr = fopen(filename, "rb");
    if (fr == NULL) {
        warning_msg("Nepovedlo se otevrit soubor %s!\n", filename);
        return NULL;
    }

    if (fscanf(fr, "P6 %u %u %u%*c", &xsize, &ysize, &color_depth) != 3) {
        warning_msg("Neodpovidajici format obrazku!\n");
        fclose(fr);
        return NULL;
    }

    if (xsize > MAX_RES || ysize > MAX_RES || color_depth != 255) {
        warning_msg("Nepodporovane rozmery obrazku!\n");
        fclose(fr);
        return NULL;
    }

    data_size = sizeof(char) * xsize * ysize * 3;

    if ((picture = (struct ppm *)malloc(sizeof(struct ppm) + data_size)) == NULL) {
        warning_msg("Nepovedlo se allocovat dostatek pameti!\n");
        fclose(fr);
        return NULL;
    }

    picture->xsize = xsize;
    picture->ysize = ysize;

    fread(picture->data, sizeof(char), data_size, fr);

    fclose(fr);
    return picture;
}

int ppm_write(struct ppm *p, const char * filename) {

    FILE *fw;
    unsigned long data_size = sizeof(char) * p->xsize * p->ysize * 3;

    fw = fopen(filename, "wb");
    if (fw == NULL) {
        warning_msg("Nepovedlo se otevrit soubor %s!\n", filename);
        return -1;
    }

    fprintf(fw, "P6\n%u %u\n%u\n", p->xsize, p->ysize, 255);
    fwrite(p->data, sizeof(char), data_size, fw);

    if (fclose(fw) == EOF) {
        warning_msg("Nepovedlo se uzavrit soubor %s!\n", filename);
        return -1;
    }

    return 0;
}
