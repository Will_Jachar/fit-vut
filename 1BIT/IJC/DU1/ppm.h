// ppm.h
// Reseni IJC-DU1, priklad B), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... rozhrani pro soubor ppm.c

#ifndef PPM_H_
#define PPM_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"

#define MAX_RES 1000

struct ppm {
    unsigned xsize;
    unsigned ysize;
    char data[];    // RGB bajty, celkem 3*xsize*ysize
};

struct ppm * ppm_read(const char * filename);

int ppm_write(struct ppm *p, const char * filename);

#endif // PPM_H_
