// primes.c
// Reseni IJC-DU1, priklad A), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... zdrojovy text programu pro vypocet prvocisel

#include <stdio.h>
#include <stdlib.h>
#include "bit_array.h"
#include "error.h"
#include "eratosthenes.h"

#define  N  303000000

int main() {

    ba_create(primes, N);
    unsigned long last_primes[10];
    
    Eratosthenes(primes);
    
    for (int i = 10, j = ba_size(primes); i > 0 && j > 0; j--) {
        if (!ba_get_bit(primes, j)) {
            last_primes[i-1] = j;
            i--;
        }
    }
    
    for (int i = 0; i < 10; i++) {
        printf("%d\n", last_primes[i]);
    }
    
    return 0;
}
