// steg-decode.c
// Reseni IJC-DU1, priklad B), 27.3.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4
// ... zdrojovy text programu pro dekodovani "tajne zpravy" z obrazku

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "ppm.h"
#include "bit_array.h"
#include "eratosthenes.h"

int main(int argc, const char* argv[]) {

    if (argc != 2) {
        error_msg("Nespravny pocet argumentu!\n");
    }

    struct ppm *picture;
    if ((picture = ppm_read(argv[1])) == NULL) {
        error_msg("Chyba pri cteni souboru!\n");
    }

    unsigned long data_size = sizeof(char) * picture->xsize * picture->ysize * 3 + 1;

    unsigned long *primes;
    if ((primes = (unsigned long *)malloc(sizeof(unsigned long) * ba_bit_to_byte(data_size))) == NULL) {
        free(picture);
        error_msg("Nepovedlo se allocovat dostatek pameti!\n");
    }

    primes[0] = data_size;

    for (unsigned long i = 1; i < ba_bit_to_byte(data_size); i++) {
        primes[i] = 0;
    }

    Eratosthenes(primes);

    unsigned char byte = 0;
    for (unsigned long i = 0, j = 0; i < data_size; i++) {

        if (!ba_get_bit(primes, i)) {

            if (picture->data[i] & 1) {
                byte |= ba_mask(j);
            }
            else {
                byte &= ~ba_mask(j);
            }

            j++;

            if (j == 8) {
                if (byte == '\0')
                    break;
                putchar(byte);
                j = 0;
            }
        }
    }

    putchar('\n');

    free(picture);
    free(primes);

    return 0;
}
