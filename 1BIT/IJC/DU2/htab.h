// htab.h
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#ifndef STDLIB
#define STDLIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

// Struktura polozek ulozenych v hashovací tabulce
struct htab_listitem {
    char *key;
    unsigned data;
    struct htab_listitem *next;
};

// Struktura hashovací tabulky
typedef struct {
    size_t arr_size;
    size_t n;
    struct htab_listitem *map[];
} htab_t;

unsigned int hash_function(const char *str);    // Hashovaci funkce
size_t htab_bucket_count(htab_t *t);            // Vrati pocet prvku pole (arr_size)
void htab_clear(htab_t *t);                     // Zruseni vsech polozek, tabulka zustane prazdna
struct htab_listitem *htab_find(htab_t *t, const char *key);          // Vyhledavani - pokud nenalezne prati NULL
void htab_foreach(htab_t *t, void (*func)(const char*, unsigned*));   // Volani funkce func pro kazdy prvek
void htab_free(htab_t *t);                      // Destruktor: zruseni tabulky (vola htab_clear())
htab_t *htab_init(int size);                    // Konstruktor: vytvoreni a inicializace tabulky
struct htab_listitem *htab_lookup_add(htab_t *t, const char *key);    // Vyhledavani - pokud nenalezne, prida novou polozku a vrati ukazatel
htab_t *htab_move(int newsize, htab_t *t2);     // Move konstruktor: vytvoreni a inicializace tabulky daty z tabulky t2, t2 nakonec zustane prazdna a alokovana
bool htab_remove(htab_t *t, const char *key);   // Vyhledani a zruseni zadane polozky vraci b==false pokud neexistuje
size_t htab_size(htab_t *t);                    // Vrati pocet prvku tabulky (n)

#endif