// htab_bucket_count.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Vrati pocet prvku pole (arr_size)
size_t htab_bucket_count(htab_t *t) {
    if (t == NULL) {    // Pokud tabulka neexistuje
        fprintf(stderr, "WARNING: Pointer to NULL!\n");
        return 0;
    }

    return t->arr_size;
}