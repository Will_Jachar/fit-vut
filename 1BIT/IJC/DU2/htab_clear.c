// htab_clear.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Zruseni vsech polozek, tabulka zustane prazdna
void htab_clear(htab_t *t) {
    if (t == NULL) return;      // Pokud tabulka neexistuje

    for (size_t i = 0; i < t->arr_size; i++) {
        if (t->map[i] == NULL) continue;

        struct htab_listitem *next_item;
        while (t->map[i] != NULL) {         // Mazani vazaneho seznamu
            next_item = t->map[i]->next;
            free(t->map[i]->key);
            free(t->map[i]);
            t->map[i] = next_item;
        }
    }
}