// htab_find.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Vyhledavani - pokud nenalezne prati NULL
struct htab_listitem *htab_find(htab_t *t, const char *key) {
    if (t == NULL) return NULL;     // Pokud tabulka neexistuje

    unsigned h_key = hash_function(key) % t->arr_size;
    struct htab_listitem *item = t->map[h_key];     // Dostane se na spravny index

    while (item != NULL) {      // Prochazi vazany seznam
        if (!strcmp(key, item->key)) {
            break;
        }

        item = item->next;
    }

    return item;
}