// htab_foreach.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Volani funkce func pro kazdy prvek
void htab_foreach(htab_t *t, void (*func)(const char*, unsigned*)) {
    if (t == NULL) return;      // Pokud tabulka neexistuje

    for (unsigned long i = 0; i < t->arr_size; i++) {   // Pro kazdy prvek pole v tabulce
        if (t->map[i] == NULL) continue;

        struct htab_listitem *item = t->map[i];
        while (item != NULL) {      // Pro kazdou polozku ve vazanem seznamu
            (*func)(item->key, &item->data);
            item = item->next;
        }
    }
}