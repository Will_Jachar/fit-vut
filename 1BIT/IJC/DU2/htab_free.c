// htab_free.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Destruktor: zruseni tabulky (vola htab_clear())
void htab_free(htab_t *t) {
    if (t == NULL) return;      // Pokud tabulka neexistuje

    htab_clear(t);      // Vycisti tabulku
    free(t);        // Uvolni tabulku

    return;
}