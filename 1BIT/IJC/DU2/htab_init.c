// htab_init.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Konstruktor: vytvoreni a inicializace tabulky
htab_t *htab_init(int size) {
    if (size < 0) {     // Pokud je velikost zaporna
        fprintf(stderr, "ERROR: 'size' cannot be negative!\n");
        return NULL;
    }

    htab_t *t;
    if ((t = malloc(sizeof(htab_t) + (size * sizeof(struct htab_listitem*)))) == NULL) {    // Allocace mista
        fprintf(stderr, "ERROR: Cannot allocate memory!\n");
        return NULL;
    }

    t->arr_size = size;
    t->n = 0;

    memset(t->map, 0, size * sizeof(struct htab_listitem*));    // Vynulovani

    return t;
}