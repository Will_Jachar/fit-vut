// htab_lookup_add.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Vyhledavani - pokud nenalezne, prida novou polozku a vrati ukazatel
struct htab_listitem *htab_lookup_add(htab_t *t, const char *key) {
    if (t == NULL) return NULL;     // Pokud tabulka neexistuje

    unsigned h_key = hash_function(key) % t->arr_size;      // Dostane se na spravny index
    struct htab_listitem *prev = t->map[h_key], *item = t->map[h_key];

    while (item != NULL) {      // Hleda jestli tam uz polozka je
        if (!strcmp(key, item->key)) {
            break;
        }

        prev = item;
        item = item->next;
    }

    if (item == NULL) {     // Pokud neni
        if ((item = malloc(sizeof(struct htab_listitem))) == NULL) {    // Allocuje pro polozku
            fprintf(stderr, "ERROR: malloc() error:\n");
            return NULL;
        }
        if ((item->key = malloc((strlen(key) + 1) * sizeof(char))) == NULL) {   // Allocuje pro key
            fprintf(stderr, "ERROR: malloc() error:\n");
            return NULL;
        }

        (t->n)++;   // Zvysi pocet polozek

        strcpy(item->key, key);     // Kopiruje key
        item->data = 0;
        item->next = NULL;

        if (t->map[h_key] == NULL) {    // Pokud je to prvni prvek v seznamu
            t->map[h_key] = item;
        }
        else {
            prev->next = item;
        }
    }

    return item;
}