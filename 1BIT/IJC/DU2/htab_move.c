// htab_move.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Move konstruktor: vytvoreni a inicializace tabulky daty z tabulky t2, t2 nakonec zustane prazdna a alokovana
htab_t *htab_move(int newsize, htab_t *t2) {
    if (newsize < (long)t2->arr_size) {     // Pokud je nova velikost mensi nez stavajici
        fprintf(stderr, "ERROR: 'new size' cannot be lower then original size!\n");
        return NULL;
    }

    htab_t *t = htab_init(newsize);     // Vytvori novou tabulku

    if (t == NULL) return NULL;     // Pokud se neco nepovedlo

    memcpy(t, t2, sizeof(htab_t) + (t2->arr_size * sizeof(struct htab_listitem*)));     // Zkopiruje vse
    t2->n = 0;
    memset(t2->map, 0, t2->arr_size * sizeof(struct htab_listitem*));   // Vynuluje puvodni tabulku

    return t;
}