// htab_remove.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Vyhledani a zruseni zadane polozky vraci b==false pokud neexistuje
bool htab_remove(htab_t *t, const char *key) {
    if (t == NULL) return false;    // Pokud tabulka neexistuje

    unsigned h_key = hash_function(key) % t->arr_size;
    struct htab_listitem *prev = t->map[h_key], *item = t->map[h_key];  // Dostane se na spravny index

    while (item != NULL) {      // Prohledava vazany seznam
        if (!strcmp(key, item->key)) {      // Pokud najde
            if (item == t->map[h_key]) {    // Pokud je to prvni prvek v seznamu
                t->map[h_key] = item->next;
                free(item->key);
                free(item);
                return true;
            }
            else {
                prev->next = item->next;            
                free(item->key);
                free(item);
                return true;
            }
        }

        prev = item;
        item = item->next;
    }

    return false;
}