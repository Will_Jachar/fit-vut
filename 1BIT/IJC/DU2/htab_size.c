// htab_size.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "htab.h"

// Vrati pocet prvku tabulky (n)
size_t htab_size(htab_t *t) {
    if (t == NULL) {    // Pokud tabulka neexistuje
        fprintf(stderr, "WARNING: Pointer to NULL!\n");
        return 0;
    }

    return t->n;
}