// io.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "io.h"

// Funce pro nacteni jednoho slova ze souboru
int get_word(char *s, int max, FILE *f) {
    static bool first_warning = true;
    int c;
    
    while (isspace(c = fgetc(f)))   // Preskoceni bilych znaku na zacatku
        ;

    int i = 0;
    while (!isspace(c) && c != EOF) {   // Cteni slova po znacich
        if (i < max) {
            s[i] = c;
            i++;
        }
        else if (first_warning) {       // Prekroceni max delky slova
            fprintf(stderr, "WARNING: Too long word!\n");
            first_warning = false;
        }

        c = fgetc(f);
    }

    s[i] = '\0';

    if (i) return i;
    else return EOF;
}