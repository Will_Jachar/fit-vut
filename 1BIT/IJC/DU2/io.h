// io.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

// Funce pro nacteni jednoho slova ze souboru
int get_word(char *s, int max, FILE *f);