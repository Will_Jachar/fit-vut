// tail.c
// Reseni IJC-DU2, priklad 1, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "tail.h"

int main(int argc, char *argv[]) {  
    FILE *fr;
    Args_t args = get_args(argc, argv);     // Struktura ve ktere jsou ulozeny argumenty programu

    if (!args.valid) {      // Pokud jsou nespravne argumenty
        fprintf(stderr, "Invalid argument!\n");
        return 1;
    }

    if (args.filename_index != -1) {    // Otevirani souboru
        if ((fr = fopen(argv[args.filename_index], "r")) == NULL) {
            fprintf(stderr, "Can't open the %s file!\n", argv[args.filename_index]);
            return 1;
        }
    }

    else {      // Pokud se ma cist ze stdin
        fr = stdin;
    }

    Last_lines_t *last_lines;   // Jeden radek
    if ((last_lines = calloc(args.n, sizeof(Last_lines_t))) == NULL) {  // N radku
        fprintf(stderr, "Unable to allocate memory!\n");

        if (args.filename_index != -1) {
            fclose(fr);
        }
        return 1;
    }

    int first_of = 1;
    char last[LINE_BUFFER] = {0};
    while (fgets(last, LINE_BUFFER, fr)) {      // Dokud nejsme na konci

        for (int i = 1; i < args.n; i++) {      // Posuneme vsechny radky o jedno a vlozime posledni
            memset(&last_lines[i-1].line[0], 0, LINE_BUFFER);
            memcpy(last_lines[i-1].line, last_lines[i].line, LINE_BUFFER);
        }

        memset(&last_lines[args.n-1].line[0], 0, LINE_BUFFER);
        memcpy(last_lines[args.n-1].line, last, LINE_BUFFER);

        if (last[LINE_BUFFER-2] != '\0' && last[LINE_BUFFER-2] != '\n') {   // Pokud je prekrocena max delka jednoho radku
            if (first_of) {
                fprintf(stderr, "Number of characters on one line is too big!\n");
                first_of = 0;
            }

            int c;
            while ((c = getc(fr)) != '\n' && c != EOF) {    // Preskocime zbytek radku
                ;
            }

            memset(&last[0], 0, LINE_BUFFER);
        }
    }

    for (int i = 0; i < args.n; i++) {      // Vypis poslednich N radku
        printf("%s", last_lines[i].line);

        if (last_lines[i].line[LINE_BUFFER-2] != '\0' && last_lines[i].line[LINE_BUFFER-2] != '\n') {
            putchar('\n');
        }
    }

    free(last_lines);
    if (args.filename_index != -1) {
        fclose(fr);
    }
    return 0;
}

// Funkce pro zpracovani argumentu
Args_t get_args(int argc, char *argv[]) {
    Args_t args;
    args.valid = 1;
    args.filename_index = -1;
    args.n = 10;

    if (argc > 4) {     // Nespravny pocet argumentu
        args.valid = 0;
    }

    else if (argc == 1) {   // Zadny argument nebyl zadan
        args.filename_index = -1;
    }

    else if (argc == 2) {   // Zadan filename
        if (!strcmp(argv[1], "-n")) {
            args.valid = 0;
        }
        else {
            args.filename_index = 1;
        }
    }

    else if (argc == 3) {   // Zadano -n N
        char *ptr = NULL;

        if (!strcmp(argv[1], "-n")) {
            args.n = strtol(argv[2], &ptr, 10);
            if (*ptr != 0 || args.n < 0) {
                args.valid = 0;
            }
        }
        else {
            args.valid = 0;
        }
    }

    else if (argc == 4) {   // Zadano -n N filename
        char *ptr = NULL;

        if (!strcmp(argv[1], "-n")) {
            args.n = strtol(argv[2], &ptr, 10);
            if (*ptr != 0) {
                args.valid = 0;
            }
        }
        else {
            args.valid = 0;
        }

        args.filename_index = 3;
    }

    return args;
}