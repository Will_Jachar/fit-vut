// tail.h
// Reseni IJC-DU2, priklad 1, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_BUFFER 1025    // Maximalni delka jedne radky

// Struktura ve ktere jsou ulozeny argumenty programu
typedef struct {
    int valid;
    int filename_index;
    int n;
} Args_t;

// Struktura ve ktere je jeda radka textu
typedef struct {
    char line[LINE_BUFFER];
} Last_lines_t;

// Funkce pro zpracovani argumentu
Args_t get_args(int argc, char *argv[]);
