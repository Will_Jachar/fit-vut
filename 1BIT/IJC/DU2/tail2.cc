// tail2.c
// Reseni IJC-DU2, priklad 1, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "tail2.h"

using namespace std;

int main(int argc, char *argv[]) {
    istream *in;
    ifstream f;
    Args_t args = get_args(argc, argv);     // Struktura ve ktere jsou ulozeny argumenty programu

    if (!args.valid) {      // Pokud jsou nespravne argumenty
        cerr << "ERROR: Invalid argument!" << endl;
        return 1;
    }

    if (args.filename_index != -1) {    // Otevirani souboru
        f.open(argv[args.filename_index], fstream::in);
        if (!f) {
            cerr << "ERROR: Cannot open file" << argv[args.filename_index] << "!" << endl;
            return 1;
        }
        in = &f;
    }
    else {      // Pokud se ma cist ze cin
        in = &cin;
    }

    ios::sync_with_stdio(false);

    string line;    // Jeden radek
    queue<string> lines;    // Fronta radku

    while (!(*in).eof()) {      // Dokud nejsme na konci
        getline(*in, line);     // Nacte jeden radek
        lines.push(line);       // Ulozi do fronty
        
        if (lines.size() > (unsigned)args.n + 1) {      // Pokud je ve fronte vice jak N radku jeden popene
            lines.pop();
        }
    }

    while (!lines.empty()) {    // Vypis poslednich N radku
        cout << lines.front();
        lines.pop();

        if (!lines.empty()) {
            cout << endl;
        }
    }

    return 0;
}

// Funkce pro zpracovani argumentu
Args_t get_args(int argc, char *argv[]) {
    Args_t args;
    args.valid = 1;
    args.filename_index = -1;
    args.n = 10;

    if (argc > 4) {     // Nespravny pocet argumentu
        args.valid = 0;
    }

    else if (argc == 1) {   // Zadny argument nebyl zadan
        args.filename_index = -1;
    }

    else if (argc == 2) {   // Zadan filename
        if (!string(argv[1]).compare("-n")) {
            args.valid = 0;
        }
        else {
            args.filename_index = 1;
        }
    }

    else if (argc == 3) {   // Zadano -n N
        char *ptr = NULL;

        if (!string(argv[1]).compare("-n")) {
            args.n = strtol(argv[2], &ptr, 10);
            if (*ptr != 0 || args.n < 0) {
                args.valid = 0;
            }
        }
        else {
            args.valid = 0;
        }
    }

    else if (argc == 4) {   // Zadano -n N filename
        char *ptr = NULL;

        if (!string(argv[1]).compare("-n")) {
            args.n = strtol(argv[2], &ptr, 10);
            if (*ptr != 0) {
                args.valid = 0;
            }
        }
        else {
            args.valid = 0;
        }

        args.filename_index = 3;
    }

    return args;
}