// tail2.h
// Reseni IJC-DU2, priklad 1, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include <iostream>
#include <fstream>
#include <string>
#include <queue>

// Struktura ve ktere jsou ulozeny argumenty programu
typedef struct {
    int valid;
    int filename_index;
    int n;
} Args_t;

// Funkce pro zpracovani argumentu
Args_t get_args(int argc, char *argv[]);