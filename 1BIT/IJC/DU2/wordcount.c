// wordcount.c
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include "wordcount.h"

int main() {

    htab_t *t;
    if ((t = htab_init(HTAB_SIZE)) == NULL) {   // Vytvoreni hash table
        fprintf(stderr, "ERROR: Cannot initialize hash table!\n");
        return 1;
    }

    char word[WORD_BUFFER + 1];     // Buffer pro jedno slovo

    //int count = 0;
    while (get_word(word, WORD_BUFFER, stdin) != EOF) {     // Cteni slov az do konce
        (htab_lookup_add(t, word)->data)++;     // Pocitani vyskytu slov
        //count++;
    }

    htab_foreach(t, &print_table);      // Tisknuti slov

    /*printf("There is %d words in this book and %lu of them are unique.\n", count, t->n);

    freopen("/dev/tty", "rw", stdin);

    struct htab_listitem *item;
    while (get_word(word, WORD_BUFFER, stdin) != EOF) {
        if ((item = htab_find(t, word)) == NULL) {
            printf("%s : 0\n", word);
        }
        else {
            printf("%s : %u\n", word, item->data);
        }
    }*/

    htab_free(t);       // Uvolneni pameti

    return 0;
}

// Pomocna funkce pro sisknuti prvku z hashovaci tabulky.
void print_table(const char *key, unsigned int *value) {
    printf("%s\t%d\n", key, *value);
}