// wordcount.h
// Reseni IJC-DU2, priklad 2, 26.04.2017
// Autor: Peter Uhrin, FIT
// Prelozeno: gcc 5.4.0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "htab.h"
#include "io.h"

/* Podle https://en.oxforddictionaries.com/ obsahuje Oxford English Dictionary
 * 171 476 slov a 47 156 archaismu. Dohromady je to tedy cca 220 000 slov.
 * Cas potrebny k linearnimu vyhledavani ve vazanem seznamu o velikosti dejme
 * tomu 500 záznamu je zanedbatelny, takze pokud bude kazdy ukazatel z hashovaci
 * tabulky ukozaval na seznam 500 prvcich, tak abychom pokrili 220 000 slov,
 * potrebujeme jen hashovaci tabulku o velikosti 440.
 * Pro velikost hashovaci tabulky je ale vhodne pouzivat prvocisla, protoze
 * minimalizuji clustrovani na stejnych indexech, proto teda 449. */

#define HTAB_SIZE 449
#define WORD_BUFFER 127

// Pomocna funkce pro sisknuti prvku z hashovaci tabulky.
void print_table(const char *key, unsigned int *value);