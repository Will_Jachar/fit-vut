-- fsm.vhd: Finite State Machine
-- Author(s): Peter Uhrin (xuhrin02)
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (KEY12_1, KEY12_2, KEY12_3, KEY1_4, KEY1_5, KEY1_6, KEY1_7, KEY1_8, KEY1_9, KEY1_10, KEY1_11, KEY2_4, KEY2_5, KEY2_6, KEY2_7, KEY2_8, KEY2_9, KEY2_10, SUCCESS, FAILURE, ERROR, ENTER, RESTART);
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= KEY12_1;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   -- First two numbers are the same in both codes --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when KEY12_1 =>
      if (KEY(1) = '1') then
         next_state <= KEY12_2;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
          next_state <= ERROR;
      else
         next_state <= KEY12_1;
      end if;

   when KEY12_2 =>
      if (KEY(4) = '1') then
         next_state <= KEY12_3;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
          next_state <= ERROR;
      else
         next_state <= KEY12_2;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   -- Third number desides what code are we typing --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when KEY12_3 =>
      if (KEY(3) = '1') then
         next_state <= KEY1_4;
      elsif (KEY(8) = '1') then
         next_state <= KEY2_4;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY12_3;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --              Testing first code              --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when KEY1_4 =>
      if (KEY(4) = '1') then
         next_state <= KEY1_5;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_4;
      end if;

   when KEY1_5 =>
      if (KEY(9) = '1') then
         next_state <= KEY1_6;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_5;
      end if;

   when KEY1_6 =>
      if (KEY(7) = '1') then
         next_state <= KEY1_7;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_6;
      end if;

   when KEY1_7 =>
      if (KEY(9) = '1') then
         next_state <= KEY1_8;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_7;
      end if;

   when KEY1_8 =>
      if (KEY(9) = '1') then
         next_state <= KEY1_9;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_8;
      end if;

   when KEY1_9 =>
      if (KEY(2) = '1') then
         next_state <= KEY1_10;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_9;
      end if;

   when KEY1_10 =>
      if (KEY(4) = '1') then
         next_state <= KEY1_11;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_10;
      end if;

   when KEY1_11 =>
      if (KEY(8) = '1') then
         next_state <= ENTER;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY1_11;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --             Testing second code              --
   -- --- --- --- --- --- --- --- --- --- --- --- --- 

   when KEY2_4 =>
      if (KEY(7) = '1') then
         next_state <= KEY2_5;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_4;
      end if;

   when KEY2_5 =>
      if (KEY(4) = '1') then
         next_state <= KEY2_6;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_5;
      end if;

   when KEY2_6 =>
      if (KEY(4) = '1') then
         next_state <= KEY2_7;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_6;
      end if;

   when KEY2_7 =>
      if (KEY(9) = '1') then
         next_state <= KEY2_8;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_7;
      end if;

   when KEY2_8 =>
      if (KEY(8) = '1') then
         next_state <= KEY2_9;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_8;
      end if;

   when KEY2_9 =>
      if (KEY(1) = '1') then
         next_state <= KEY2_10;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_9;
      end if;

   when KEY2_10 =>
      if (KEY(2) = '1') then
         next_state <= ENTER;
      elsif (KEY(15) = '1') then
         next_state <= FAILURE;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= KEY2_10;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --     User presed # at the end of the code     --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when ENTER =>
      if (KEY(15) = '1') then
         next_state <= SUCCESS;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= ERROR;
      else
         next_state <= ENTER;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --         Incorrect number was entered         --
   -- --- --- --- --- --- --- --- --- --- --- --- --- 

   when ERROR =>
      if (KEY(15) = '1') then
         next_state <= FAILURE;
      else
         next_state <= ERROR;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --                Print message                 --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when SUCCESS =>
      if (CNT_OF = '1') then
         next_state <= RESTART;
      else
         next_state <= SUCCESS;
      end if;

   when FAILURE =>
      if (CNT_OF = '1') then
         next_state <= RESTART;
      else
         next_state <= FAILURE;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --                   Restart                    --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when RESTART =>
      if (KEY(15) = '1') then
         next_state <= KEY12_1;
      else
         next_state <= RESTART;
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when others =>
      next_state <= KEY12_1;
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --           Print "Pristup povolen"            --
   -- --- --- --- --- --- --- --- --- --- --- --- --- 

   when SUCCESS =>
      FSM_MX_MEM     <= '1';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --           Print "Pristup odepren"            --
   -- --- --- --- --- --- --- --- --- --- --- --- --- 

   when FAILURE =>
      FSM_MX_MEM     <= '0';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --                Clear screen                  --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when RESTART =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- ---
   --                   Print *                    --
   -- --- --- --- --- --- --- --- --- --- --- --- ---

   when others =>
      if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;

   -- --- --- --- --- --- --- --- --- --- --- --- --- 

   end case;
end process output_logic;

end architecture behavioral;

