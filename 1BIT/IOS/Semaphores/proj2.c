// proj2.c
// Project IOS-2, 27.4.2017
// Author: Peter Uhrin, FIT
// Compilator: gcc 5.4
// ... program for syncing multiple processes using semaphores

#include "proj2.h"

// Output
FILE *output;

// Variables used by all processes
int variables_ID;
Shared_t *variables;

// Semaphores
sem_t *sem_1, *sem_2, *mutex, *start, *end;

/*############################################################################*/
/*############################################################################*/

int main(int argc, char *argv[]) {

    signal(SIGINT, end_program);
    signal(SIGTERM, end_program);

    Args_t args;    // Variable for holding arguments of the program.
    if (get_args(argc, argv, &args) == -1) {    // Getting arguments.
        fprintf(stderr, "Invalid arguments!\n");
        exit(1);
    }

    if (shared_variables() == -1) {     // Creating variables in shared memory and semaphores.
        fprintf(stderr, "Error when creating shared variable!\n");
        clean_variables();
        exit(2);
    }

    srand(time(0));     // Generate random numbers based on time.

    pid_t adult_spawn_PID = fork();     // Creating new process for creating "Adult" processes.
    if (adult_spawn_PID == 0) {
        // Adult
        sem_wait(start);    // Sync for both processes to start at the same time.
        adult_spawn(&args);
        exit(0);
    }
    else if (adult_spawn_PID > 0) {

        pid_t child_spawn_PID = fork();     // Creating new process for creating "Child" processes.
        if (child_spawn_PID == 0) {
            // Child
            sem_post(start);    // Sync for both processes to start at the same time.
            child_spawn(&args);
            exit(0);
        }
        else if (child_spawn_PID < 0) {
            fprintf(stderr, "Error when calling fork()\n");
            kill(adult_spawn_PID, SIGKILL);
            exit(2);
        }
    }
    else {
        fprintf(stderr, "Error when calling fork()\n");
        exit(2);
    }

    if (wait(NULL) != 0 || wait(NULL) != 0) {   // Waiting for both adult_spawn and child_spawn to end.
        clean_variables();
        return 2;
    }   

    //fprintf(output, "Exiting...\n");

    clean_variables();      // Clean variables in sharem memory and semaphores.

    return 0;
}

/*############################################################################*/
/*############################################################################*/

// Function for proccesing arguments passed to the program.
int get_args(int argc, char *argv[], Args_t *args) {

    if (argc != 7) {    // There must be precisely 6 arguments.
        return -1;
    }

    char *ptr;

    // Convert values from argument into intigers.
    args->A = strtol(argv[1], &ptr, 10);
    args->C = strtol(argv[2], &ptr, 10);
    args->AGT = strtol(argv[3], &ptr, 10);
    args->CGT = strtol(argv[4], &ptr, 10);
    args->AWT = strtol(argv[5], &ptr, 10);
    args->CWT = strtol(argv[6], &ptr, 10);

    // Test if all numbers were intigers and within the acceptable range.
    if (*ptr != 0 ||
        args->A <= 0 || args->C <= 0 ||
        args->AGT < 0 || args->AGT > 5000 ||
        args->CGT < 0 || args->CGT > 5000 ||
        args->AWT < 0 || args->AWT > 5000 ||
        args->CWT < 0 || args->CWT > 5000) {
        return -1;
    }

    return 0;
}

/*############################################################################*/

// Function for creating new "Adult" processes.
void adult_spawn(Args_t *args) {
    int i = 0;
    pid_t adults_PIDs[args->A];

    // Creating A adults in interval <0, AGT>
    for (; i < args->A; i++) {
        usleep((rand() % (args->AGT + 1)) * 1000);  // Waiting to create new adult process.

        pid_t adult_PID = fork();   // Creating new adult process.
        if (adult_PID == 0) {
            adult(args, i + 1);         // Calling function representing "Adult" behavior in child center.
            exit(0);
        }
        else if (adult_PID > 0) {
            adults_PIDs[i] = adult_PID;
        }
        else {
            fprintf(stderr, "Error when calling fork()\n");
            kill_them_all(i, adults_PIDs);
            variables->adults_left = args->A;
            for (int j = variables->child_waiting; j; j--) {    // So no deadlock can happen.
                sem_post(sem_1);
            }
            for (int j = args->C; j; j--) {
                sem_post(end);
            }
            exit(2);
        }
    }

    for (int i = 0; i < args->A; i++) wait(NULL);   // Waiting for all adult processes to end.

    return;
}

/*############################################################################*/

// Function for creating new "Child" processes.
void child_spawn(Args_t *args) {
    int i = 0;
    pid_t children_PIDs[args->C];

    // Creating C children in interval <0, CGT>
    for (; i < args->C; i++) {
        usleep((rand() % (args->CGT + 1)) * 1000);  // Waiting to create new child process.

        pid_t child_PID = fork();   // Creating new child process
        if (child_PID == 0) {
            child(args, i + 1);         // Calling function representing "Child" behavior in child center.
            exit(0);
        }
        else if (child_PID > 0) {
            children_PIDs[i] = child_PID;
        }
        else {
            fprintf(stderr, "Error when calling fork()\n");
            kill_them_all(i, children_PIDs);
            for (int j = variables->adult_waiting; j; j--) {    // So no deadlock can happen.
                sem_post(sem_2);
            }
            for (int j = args->A; j; j--) {
                sem_post(end);
            }
            exit(2);
        }
    }

    for (int i = 0; i < args->C; i++) wait(NULL);   // Waiting for all child processes to end.

    return;
}

/*############################################################################*/

// Represents one "Adult" process describing basic interactions between center, adult and child.
void adult(Args_t *args, int ID) {

    sem_wait(mutex);
        fprintf(output, "%d\t: A %d\t: started\n", ++(variables->shared_counter), ID);
    sem_post(mutex);

    sem_wait(mutex);
        fprintf(output, "%d\t: A %d\t: enter\n", ++(variables->shared_counter), ID);

        (variables->adults)++;

        // If there are children waiting to enter, lets them enter (max 3).
        if (variables->child_waiting > 0) {
            for (int j = 0; j < 3 && j < variables->child_waiting; j++) sem_post(sem_1);
        }
        // If there is an adult waiting to leave, lets him/her leave.
        else if (variables->adult_waiting > 0 && 
            //variables->child_waiting == 0 && 
            (((variables->adults) - 1) * 3 >= (variables->children))) {
            sem_post(sem_2);
        }
    sem_post(mutex);

    usleep(((rand() % (args->AWT + 1)) * 1000) + 1);   // child sleep -> +10^-7s for better sync if CWT is 0s

    sem_wait(mutex);
        fprintf(output, "%d\t: A %d\t: trying to leave\n", ++(variables->shared_counter), ID);

        // Adult can leave if there are so little to none children to be managed by other adult.
        if ((variables->adults - 1) * 3 < (variables->children)) {
            fprintf(output, "%d\t: A %d\t: waiting : %d : %d\n", ++(variables->shared_counter), ID, variables->adults, variables->children);

            (variables->adult_waiting)++;
            sem_post(mutex);

            sem_wait(sem_2);    // Waiting for chance to leave.
            sem_wait(mutex);
            (variables->adult_waiting)--;
        }

        (variables->adults)--;

        // Checks if THIS is the last "Adult" process leaving. If yes allow children throught.
        if (++(variables->adults_left) == args->A) {
            for (int i = variables->child_waiting; i; i--) sem_post(sem_1);
        }

        fprintf(output, "%d\t: A %d\t: leave\n", ++(variables->shared_counter), ID);

        // Checks if THIS is the last process ending. If yes allow others to finish.
        if (++(variables->ended) == (args->A + args->C)) {
            for (int j = variables->ended; j; j--) sem_post(end);
        }
    sem_post(mutex);
    
    sem_wait(end);
    sem_wait(mutex);
        fprintf(output, "%d\t: A %d\t: finished\n", ++(variables->shared_counter), ID);
    sem_post(mutex);

    return;
}

/*############################################################################*/

// Represents one "Child" process describing basic interactions between center, adult and child.
void child(Args_t *args, int ID) {

    sem_wait(mutex);
        fprintf(output, "%d\t: C %d\t: started\n", ++(variables->shared_counter), ID);
        
        // Child can enter if there is free space (1 adult = 3 places) or if no more adults will enter.
        if (!(variables->adults_left == args->A) && 
            ((variables->adults) * 3 < (variables->children) + 1)) {
            fprintf(output, "%d\t: C %d\t: waiting : %d : %d\n", ++(variables->shared_counter), ID, variables->adults, variables->children);

            (variables->child_waiting)++;
            sem_post(mutex);

            sem_wait(sem_1);    // Waiting for free space.
            sem_wait(mutex);
            (variables->child_waiting)--;
        }
        else {
            sem_post(mutex);

            sem_wait(mutex);
        }

        (variables->children)++;
        fprintf(output, "%d\t: C %d\t: enter\n", ++(variables->shared_counter), ID);
    sem_post(mutex);

    usleep(((rand() % (args->CWT + 1)) * 1000) + 1);   // child sleep -> +10^-7s for better sync if CWT is 0s

    sem_wait(mutex);
        fprintf(output, "%d\t: C %d\t: trying to leave\n", ++(variables->shared_counter), ID);
    sem_post(mutex);

    sem_wait(mutex);
        fprintf(output, "%d\t: C %d\t: leave\n", ++(variables->shared_counter), ID);

        // If there is another child waiting for entry lets him/her in.
        // In project forum this condition was marked as wrong, but to me it seem logical.
            /*if (variables->child_waiting > 0) {
                sem_post(sem_1);
            }*/

        // If there is adult waiting to leave (checks if he can leave and lets him/her out).
        if (variables->adult_waiting > 0 && (((variables->adults) - 1) * 3 >= (variables->children) - 1)) {
            sem_post(sem_2);
        } 

        (variables->children)--;
    
        // Checks if THIS is the last process ending. If yes allow others to finish.
        if (++(variables->ended) == (args->A + args->C)) {
            for (int j = variables->ended; j; j--) sem_post(end);
        }
    sem_post(mutex);
    
    sem_wait(end);
    sem_wait(mutex);
        fprintf(output, "%d\t: C %d\t: finished\n", ++(variables->shared_counter), ID);
    sem_post(mutex);

    return;
}

/*############################################################################*/

// Function for allocating space for variables in shared memory and creating semaphores.
int shared_variables(void) {

    // Output
    output = NULL;
    if ((output = fopen("proj2.out", "w")) == NULL) return -1;

    // Shared memory
    if ((variables_ID = shm_open("/xuhrin02_variables", O_CREAT | O_EXCL | O_RDWR, 0666)) == -1) return -1;
    ftruncate(variables_ID, sizeof(Shared_t));
    if ((variables = mmap(NULL, sizeof(Shared_t), PROT_READ | PROT_WRITE, MAP_SHARED, variables_ID, 0)) == NULL) return -1;

    variables->shared_counter = 0;
    variables->adults = 0;
    variables->children = 0;
    variables->adult_waiting = 0;
    variables->child_waiting = 0;
    variables->adults_left = 0;
    variables->ended = 0;

    // Semaphores
    if ((sem_1 = sem_open("/xuhrin02_sem_1", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED) return -1;
    if ((sem_2 = sem_open("/xuhrin02_sem_2", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED) return -1;
    if ((mutex = sem_open("/xuhrin02_mutex", O_CREAT | O_EXCL, 0666, 1)) == SEM_FAILED) return -1;
    if ((start = sem_open("/xuhrin02_start", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED) return -1;
    if ((end = sem_open("/xuhrin02_end", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED) return -1;

    setbuf(output, NULL);

    return 0;
}

/*############################################################################*/

// Function for freeing allocated space and destoying semaphores.
void clean_variables(void) {

    // Output
    fclose(output);

    // Shared memory
    munmap(variables, sizeof(Shared_t));
    close(variables_ID);
    shm_unlink("/xuhrin02_variables");

    // Semaphores
    sem_close(sem_1);
    sem_close(sem_2);
    sem_close(mutex);
    sem_close(start);
    sem_close(end);

    sem_unlink("/xuhrin02_sem_1");
    sem_unlink("/xuhrin02_sem_2");
    sem_unlink("/xuhrin02_mutex");
    sem_unlink("/xuhrin02_start");
    sem_unlink("/xuhrin02_end");
}

// Function for killing child processes when error occurs.
void kill_them_all(int count, pid_t *members) {
    for (int i = 0; i < count; i++) {
        kill(members[i], SIGKILL);
    }
}

// Cleans and ends the program when interupt.
void end_program(int sig) {
    clean_variables();
    exit(2);
}