// proj2.h
// Project IOS-2, 27.4.2017
// Author: Peter Uhrin, FIT
// Compilator: gcc 5.4
// ... declaration of structures and functions used in proj2.c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <signal.h>
#include <fcntl.h>

// Structure for holding program arguments.
typedef struct {
    int A;
    int C;
    int AGT;
    int CGT;
    int AWT;
    int CWT;
} Args_t;

// Structure for holding variables in shared memory.
typedef struct {
    int shared_counter;
    int adults;
    int children;
    int adult_waiting;
    int child_waiting;
    int adults_left;
    int ended;
} Shared_t;

// Function for proccesing arguments passed to the program.
int get_args(int argc, char *argv[], Args_t *args);
// Function for creating new "Adult" processes.
void adult_spawn(Args_t *args);
// Function for creating new "Child" processes.
void child_spawn(Args_t *args);
// Represents one "Adult" process describing basic interactions between center, adult and child.
void adult(Args_t *args, int ID);
// Represents one "Child" process describing basic interactions between center, adult and child.
void child(Args_t *args, int ID);
// Function for allocating space for variables in shared memory and creating semaphores.
int shared_variables(void);
// Function for freeing allocated space and destoying semaphores.
void clean_variables(void);
// Function for killing child processes when error occurs.
void kill_them_all(int count, pid_t *members);
// Cleans and ends the program when interupt.
void end_program(int sig);