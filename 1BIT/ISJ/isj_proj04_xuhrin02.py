#!/usr/bin/env python3

"""
This module contains three functions. All of them are described separately below

Functions: balanced-paren(parenstr)
           caesar_list(word, key)
           caesar_varnumkey(word, *keys)
"""

import re
import itertools


def balanced_paren(parenstr):

    """
    Function that chacks if the given string has correct bracketing

    Args:
        parenstr: string in which we want to check correct bracketing
    Returns:
        True: if bracketing is correct
        False: if bracketing is not correct

    """

    openPar = 0
    nextExpected = []
    parPairs = {'(':')', '[':']', '{':'}', '<':'>'}     # parenthases pairs

    for char in parenstr:
        if char in [')', ']', '}', '>']:    # if closing bracket is found
            if openPar == 0:                # if there is't any bracket to be closed
                return False
            if char != nextExpected[0]:     # if it's not a bracket we expected
                return False
            else:                           # found valid closing bracket
                openPar -= 1
                nextExpected = nextExpected[1:]

        if char in ['(', '[', '{', '<']:    # new opening bracket
            openPar += 1
            nextExpected.insert(0, parPairs[char])
    if openPar != 0:                        # if there are still some brackets open
        return False

    return True


def caesar_list(word, key=[1,2,3]):

    """
    Fuction that shifts individual letters in given string by a key value

    Args:
        word: string of only a-z characters that we want to encipher
        key: list of values ([1,2,3] by default) which is cyclicly iterated throught and individual values are used
             to shift letters
    Returns:
        outWord: modified string
    """

    if not re.match('^[a-z]+$', word):      # check for valid string
        raise ValueError("Characters other than [a-z] not allowed")

    outWord = ""
    keyCycle = itertools.cycle(key)

    for char in word:
        outWord += chr((ord(char) - ord('a') + next(keyCycle)) % 26 + ord('a'))     # shift letters

    return outWord


def caesar_varnumkey(word, *keys):

    """
    Fuction that shifts individual letters in given string by a key value

    Args:
        word: string of only a-z characters that we want to encipher
        keys: varialbe number of values ([1,2,3] by default) passed as individual arguments which are cyclicly
              iterated throught and individual values are used to shift letters
    Returns:
        outWord: modified string
    """

    if not re.match('^[a-z]+$', word):      # check for valid string
        raise ValueError("Characters other than [a-z] not allowed")

    if len(keys) == 0:
        keys = [1,2,3]

    outWord = ""
    keyCycle = itertools.cycle(keys)

    for char in word:
        outWord += chr((ord(char) - ord('a') + next(keyCycle)) % 26 + ord('a'))     # shift letters

    return outWord
