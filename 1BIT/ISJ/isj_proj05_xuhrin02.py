#!/usr/bin/env python3

"""
Module contains class Polynomial and function test() described below.
"""


class Polynomial:
    """
    Class for working with polynomials.
    All of the functions are described separately below.

    Methods:
        __init__(self, *args, **kwargs)
        __str__(self)
        __eq__(self, other)
        __add__(self, other)
        __pow__(self, n)
        derivative(self)
        at_value(self, *vals)
    """

    def __init__(self, *args, **kwargs):

        """
        Constructor of the class. Creates a list of given values.

        Args:
            *args: whole list of polynoms as first argument,
                   or tuple of arguments, one polynom at a time
            **kwargs: dictionary of polynoms in format {position : value}
        """

        self.pol = []                           # list representing polynom

        if len(args) == 0:                      # if there are no regular arguments
            if len(kwargs) != 0:                # if there are some kwargs
                max_index = max(kwargs.keys(), key=lambda x: int(x[1:]))    # highest index in kwargs
                for i in range(int(max_index[1:]) + 1):
                    self.pol.append(kwargs.get("x" + str(i), 0))            # assigning value to coresponding list index
            else:
                self.pol = []
        elif isinstance(args[0], list):         # if whole list was given as argument
            self.pol = args[0]
        else:
            self.pol = list(args)               # if evary value was given individualy

#####################################################################################

    def __str__(self):

        """
            Method for printing polynom in correct format.

        Returns:
            string representation of polynoms in correct format
        """

        if len(self.pol) == 0:                      # if list is empty
            return "0"

        if all(n == 0 for n in self.pol):           # if every element in the list is 0
            return "0"

        pol_tmp = self.pol

        while not pol_tmp[-1]:                      # delete all zeros from the end of the list
            pol_tmp.pop()

        pol_str = ""

        if pol_tmp[0] > 0:                          # if first element is bigger then 0
            if len(pol_tmp) > 1:                    # if there is other elemnet behind it
                pol_str = " + " + str(pol_tmp[0])   # put ' + %d' to the string
            else:
                return str(pol_tmp[0])              # put just '%d'
        elif pol_tmp[0] < 0:                        # if first element is lower then 0
            if len(pol_tmp) > 1:                    # if there is other elemnet behind it
                pol_str = " - " + str(-pol_tmp[0])  # put ' - %d' to the string
            else:
                return "- " + str(-pol_tmp[0])      # put '- %d' to the string

        for n in range(1, len(pol_tmp)-1):
            if pol_tmp[n] == 0:                     # if element is 0, skip
                continue
            if pol_tmp[n] > 0:                      # various variations for converting element to string
                pol_str = " + " + (str(pol_tmp[n]) if pol_tmp[n] > 1 else "") + \
                          ("x^" + str(n) if n > 1 else "x") + pol_str               # 1 is not writing down in front of 'x' or as an exponent
            else:
                pol_str = " - " + (str(-pol_tmp[n]) if -pol_tmp[n] > 1 else "") + \
                          ("x^" + str(n) if n > 1 else "x") + pol_str               # 1 is not writing down in front of 'x' or as an exponent

        if pol_tmp[len(pol_tmp)-1] > 0:             # final element (first in string)
            return (str(pol_tmp[len(pol_tmp)-1]) if pol_tmp[len(pol_tmp)-1] > 1 else "") + \
                   ("x^" + str(len(pol_tmp)-1) if (len(pol_tmp)-1) > 1 else "x") + pol_str      # 1 is not writing down in front of 'x' or as an exponent
        else:
            return "- " + (str(-pol_tmp[len(pol_tmp)-1]) if -pol_tmp[len(pol_tmp)-1] > 1 else "") + \
                   ("x^" + str(len(pol_tmp)-1) if (len(pol_tmp)-1) > 1 else "x") + pol_str      # 1 is not writing down in front of 'x' or as an exponent

#####################################################################################

    def __eq__(self, other):

        """
        Method for comparing two polynoms.

        Returns:
            True: if polynoms are alike
            False: if polynoms are different
        """

        pol_tmp1 = self.pol
        pol_tmp2 = other.pol

        if len(self.pol) != 0 and not all(n == 0 for n in self.pol):    # if there are some ono zero numbers in the list
            while not pol_tmp1[-1]:                                     # delete all zeros from the end of the list
                pol_tmp1.pop()
        else:
            pol_tmp1 = []

        if len(other.pol) != 0 and not all(n == 0 for n in other.pol):  # if there are some ono zero numbers in the list
            while not pol_tmp2[-1]:                                     # delete all zeros from the end of the list
                pol_tmp2.pop()
        else:
            pol_tmp2 = []

        return pol_tmp1 == pol_tmp2                                     # compare two lists

#####################################################################################

    def __add__(self, other):

        """
        Method for adding two polynoms.

        Returns:
            res: object of class Polynomial containing result of addition
        """

        res = []

        for a, b in zip(self.pol, other.pol):    # add up elements in both lists
            res.append(a + b)                   # stop when shorter of two lists ends

        if len(self.pol) > len(other.pol):      # if was the first list longer
            res += self.pol[len(res):]          # add rest of the elements to result
        elif len(other.pol) > len(self.pol):    # if was the second list longer
            res += other.pol[len(res):]         # add rest of the elements to result

        return Polynomial(res)

#####################################################################################

    def __pow__(self, n):

        """
        Method for polynom to the power of n.

        Args:
            n: value of power
        Returns:
            res: object of class Polynomial containing result of power operation
        """

        if n == 0:                                          # if it's x^0 it's always 1
            return 1
        if n == 1:                                          # if its x^1 it's x
            return Polynomial(self.pol)

        pol_tmp = self.pol                                  # variable for holding temporary result
        res = []

        for i in range(n-1):                                # number of multiplications
            res = [0] * (len(pol_tmp) + len(self.pol) - 1)  # list with number of elements matching highest polynom

            for a in range(len(pol_tmp)):
                for b in range(len(self.pol)):
                    res[a + b] += (pol_tmp[a] * self.pol[b])    # add result to coresponding list element

            pol_tmp = res

        return Polynomial(res)

#####################################################################################

    def derivative(self):

        """
        Method for derivating the polynom.

        Returns:
            der: object of class Polynomial containing result of derivation
        """

        der = []

        for n in range(len(self.pol)):      # for every element in list
            der.append(self.pol[n] * n)     # derivate

        return Polynomial(der[1:])          # remove constant (it's 0)

#####################################################################################

    def at_value(self, *vals):

        """
        Method for calculating value of the polynom.

        Args:
            *vals: one or two numbers for calculating value of polynom
        Returns:
            res: number representing value of the polynom
        """

        if len(self.pol) == 0:                      # if list is empty
            return 0

        res = self.pol[0]                           # add constant

        for n in range(1, len(self.pol)):
            res += (self.pol[n] * (vals[0] ** n))   # add value of each element at a time

        if len(vals) == 2:                          # if there are two value arguments
            return self.at_value(vals[1]) - res     # recursive call for computing second value
        else:
            return res

#####################################################################################

def test():
    """
    Function for testing methods of class Polynomial.
    """
    assert str(Polynomial(0,1,0,-1,4,-2,0,1,3,0)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial([-5,1,0,-1,4,-2,0,1,3,0])) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x - 5"
    assert str(Polynomial(x7=1, x4=4, x8=3, x9=0, x0=0, x5=-2, x3= -1, x1=1)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial(x2=0)) == "0"
    assert str(Polynomial(x0=0)) == "0"
    assert Polynomial(x0=2, x1=0, x3=0, x2=3) == Polynomial(2,0,3)
    assert Polynomial(x2=0) == Polynomial(x0=0)
    assert str(Polynomial(x0=1)+Polynomial(x1=1)) == "x + 1"
    assert str(Polynomial([-1,1,1,0])+Polynomial(1,-1,1)) == "2x^2"
    pol1 = Polynomial(x2=3, x0=1)
    pol2 = Polynomial(x1=1, x3=0)
    assert str(pol1+pol2) == "3x^2 + x + 1"
    assert str(pol1+pol2) == "3x^2 + x + 1"
    assert str(Polynomial(x0=-1,x1=1)**1) == "x - 1"
    assert str(Polynomial(x0=-1,x1=1)**2) == "x^2 - 2x + 1"
    pol3 = Polynomial(x0=-1,x1=1)
    assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(Polynomial(x0=2).derivative()) == "0"
    assert str(Polynomial(x3=2,x1=3,x0=2).derivative()) == "6x^2 + 3"
    assert str(Polynomial(x3=2,x1=3,x0=2).derivative().derivative()) == "12x"
    pol4 = Polynomial(x3=2,x1=3,x0=2)
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert Polynomial(-2,3,4,-5).at_value(0) == -2
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3) == 20
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3,5) == 44
    pol5 = Polynomial([1,0,-2])
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-1,3.6) == -23.92
    assert pol5.at_value(-1,3.6) == -23.92

if __name__ == '__main__':
    test()
