#!/usr/bin/env python3

import itertools

def first_nonrepeating(nonrepstr):
    """
    Function for finding first nonrepeating character in string.

    Args:
        nonrepstr: string in which we want to find first nonrepeating character
    Returns:
        first non repeating character in string
    """

    order = []
    counts = {}
    for c in nonrepstr:
        if c in counts:
            counts[c] += 1
        else:
            counts[c] = 1
            order.append(c)
    for c in order:
        if counts[c] == 1:
            return c
    return None


def brackets(num, op):
    """
    Auxiliary function used generate possible combinations of bracketing in
    expression of four numbers and three operands.

    Args:
        num: list of numbers in expression
        op: list of operands in expression
    Returns:
        list of all possible combinations of bracketing in expression
    """

    subset = []

    subset.append(num[0] + op[0] + num[1] + op[1] + num[2] + op[2] + num[3])
    subset.append("(" + num[0] + op[0] + num[1] + ")" + op[1] + num[2] + op[2] + num[3])
    subset.append(num[0] + op[0] + num[1] + op[1] + "(" + num[2] + op[2] + num[3] + ")")
    subset.append("(" + num[0] + op[0] + num[1] + ")" + op[1] + "(" + num[2] + op[2] + num[3] + ")")
    subset.append("(" + num[0] + op[0] + num[1] + op[1] + num[2] + ")" + op[2] + num[3])
    subset.append("(" + "(" + num[0] + op[0] + num[1] + ")" + op[1] + num[2] + ")" + op[2] + num[3])
    subset.append("(" + num[0] + op[0] + "(" + num[1] + op[1] + num[2] + ")" + ")" + op[2] + num[3])
    subset.append(num[0] + op[0] + "(" + num[1] + op[1] + num[2] + op[2] + num[3] + ")")
    subset.append(num[0] + op[0] + "(" + "(" + num[1] + op[1] + num[2] + ")" + op[2] + num[3] + ")")
    subset.append(num[0] + op[0] + "(" + num[1] + op[1] + "(" + num[2] + op[2] + num[3] + ")" + ")")

    return subset


def combine4(combine, res):
    """
    Functrion tryes to combine four given numbers with four basic math operations
    to produce given result.

    Args:
        combine: list of four numbers we want to combine
        res: expected result
    Returns:
        list of all valid operations that produce given result
    """

    operations = []

    combine = [str(n) for n in combine]

    for subset_num in itertools.permutations(combine):
        for subset_op in itertools.product("+-*/", repeat=3):
            for subset_final in brackets(subset_num, subset_op):
                try:
                    if eval(subset_final) == res:
                        operations.append(subset_final)
                except ZeroDivisionError:
                    pass

    return operations


print(combine4([2, 3, 6, 4], 120))
