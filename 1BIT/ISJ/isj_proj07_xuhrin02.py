#!/usr/bin/env python3

"""
This module contains:
    class TooManyCallsError
    class Log
    decorator function limit_calls
    function ordered_merge
"""

class TooManyCallsError(Exception):
    """
    Custom exeption for function limit_calls
    """
    pass

def limit_calls(max_calls = 2, error_message_tail = 'called too often'):
    """
    Decorator function that limits number of function calls
    """
    def decorator(func):
        """
        Decorator
        """
        def wrapper(*args, **kwargs):
            """
            Wrapper
            """
            wrapper.called += 1

            if wrapper.called > max_calls:
                raise TooManyCallsError('function \"' + func.__name__ + '\" - ' + error_message_tail)

            return func(*args, **kwargs)
        wrapper.called = 0
        return wrapper
    return decorator

##############################################################

def ordered_merge(*args, **kwargs):
    """
    Function for iterating throught number of iterators based on 'selector'
    """

    try:
        selector = kwargs["selector"]
    except KeyError:
        return []

    iter_objs = []
    for obj in args:
        iter_objs.append(iter(obj))

    res = []
    for obj in selector:
        res.append(next(iter_objs[obj]))

    return res

##############################################################

class Log:
    """
    Class for logging containing methods:
        __init__
        __enter__
        logging
        __exit__
    """

    def __init__(self, filename):
        """
        Constructor saves filename to instance variable
        """
        self.filename = filename

    def __enter__(self):
        """
        Function opens the file for writing logs
        """
        self.file = open(self.filename, 'w')
        self.file.write('Begin\n')
        return self

    def logging(self, string):
        """
        Function for writing logs into the file
        """
        self.file.write(string + '\n')

    def __exit__(self, type, value, traceback):
        """
        Function closes the file after an exeption happens or after end of block of commands
        """
        self.file.write('End\n')
        self.file.close()

##############################################################
