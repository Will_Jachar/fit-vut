#!/usr/bin/env python3

import fileinput

words = {w.rstrip() for w in fileinput.input()}

result = [w for w in words if w != w[::-1] and w[::-1] in words]

print(sorted(result))