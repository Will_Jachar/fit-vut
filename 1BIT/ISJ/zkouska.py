def ordered_merge(*args, **kwargs):

    iter_objs = [iter(o) for o in args]

    for i in kwargs['selector']:
        yield next(iter_objs[i])


def decorator(orig_func):

    def wrapper(*args, **kwargs):
        try:
            wrapper.all_args[repr(args) + repr(kwargs)] += 1
        except KeyError:
            wrapper.all_args[repr(args) + repr(kwargs)] = 1
        return orig_func(*args, **kwargs)

    wrapper.all_args = {}
    return wrapper


@decorator
def funct(*args, **kwargs):
    print('ahoj')


def pythagoras(limit):
    for b in range(2, limit + 1):
        for a in range(b - 1, limit + 1):
            for c in range(b + 1, limit + 1):
                if a * a + b * b == c * c:
                    yield (a, b, c)


class SparseVector:
    counter = 0

    def __init__(self):
        self.vector = {}
        SparseVector.counter += 1

    def __setitem__(self, index, value):
        self.vector[index] = value

    def __getitem__(self, index):
        return self.vector[index]

    def __len__(self):
        return max(self.vector) + 1

    def __iter__(self):
        for x in range(len(self)):
            try:
                yield self.vector[x]
            except KeyError:
                yield 0

    def __mul__(self, other):
        res = 0
        bigger = self if len(self) > len(other) else other

        for i in range(len(bigger)):
            try:
                res += self[i] * other[i]
            except (KeyError, IndexError):
                pass

        return res

v = SparseVector()
v[34] = 5

print(len(v))