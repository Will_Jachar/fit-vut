//======== Copyright (c) 2017, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Red-Black Tree - public interface tests
//
// $NoKeywords: $ivs_project_1 $black_box_tests.cpp
// $Author:     PETER UHRIN <xuhrin02@stud.fit.vutbr.cz>
// $Date:       $2017-03-06
//============================================================================//
/**
 * @file black_box_tests.cpp
 * @author PETER UHRIN
 * 
 * @brief Implementace testu binarniho stromu.
 */

#include <vector>

#include "gtest/gtest.h"

#include "red_black_tree.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy Red-Black Tree, testujte nasledujici:
// 1. Verejne rozhrani stromu
//    - InsertNode/DeleteNode_a FindNode
//    - Chovani techto metod testuje pro prazdny i neprazdny strom.
// 2. Axiomy (tedy vzdy platne vlastnosti) Red-Black Tree:
//    - Vsechny listove uzly stromu jsou *VZDY* cerne.
//    - Kazdy cerveny uzel muze mit *POUZE* cerne potomky.
//    - Vsechny cesty od kazdeho listoveho uzlu ke koreni stromu obsahuji
//      *STEJNY* pocet cernych uzlu.
//============================================================================//

class EmptyTree : public ::testing::Test
{
protected:
    BinaryTree tree;
};

class NonEmptyTree : public ::testing::Test
{
protected:
    virtual void SetUp() {
        int values[] = { 10, 85, 15, 70, 20, 60, 30, 50, 65, 80, 90, 40, 5, 55 };

        for(int i = 0; i < 14; ++i)
            tree.InsertNode(values[i]);
    }

    BinaryTree tree;
};

//============================================================================//

TEST_F(EmptyTree, CreatingEmptyTree)
{
    EXPECT_TRUE(tree.GetRoot() == NULL);
}

TEST_F(EmptyTree, InsertNode)
{
    EXPECT_TRUE(tree.GetRoot() == NULL);

    std::pair<bool, BinaryTree::Node_t *> tmp = tree.InsertNode(0);
    int key = tmp.second->key;
    ASSERT_TRUE(tree.GetRoot() != NULL);
    EXPECT_TRUE(tmp.first);
    EXPECT_EQ(key, 0);

    std::pair<bool, BinaryTree::Node_t *> tmp2 = tree.InsertNode(100);
    int key2 = tmp2.second->key;
    ASSERT_TRUE(tree.GetRoot() != NULL);
    EXPECT_TRUE(tmp2.first);
    EXPECT_EQ(key2, 100);
}

TEST_F(EmptyTree, DeleteNode)
{
    EXPECT_FALSE(tree.DeleteNode(0));
}

TEST_F(EmptyTree, FindNode)
{
    EXPECT_TRUE(tree.FindNode(0) == NULL);
}

//============================================================================//

TEST_F(NonEmptyTree, InsertNode)
{
    ASSERT_TRUE(tree.GetRoot() != NULL);
    std::pair<bool, BinaryTree::Node_t *> tmp = tree.InsertNode(0);
    int key = tmp.second->key;
    EXPECT_TRUE(tmp.first);
    EXPECT_EQ(key, 0);
}

TEST_F(NonEmptyTree, InsertExistingNode)
{
    ASSERT_TRUE(tree.GetRoot() != NULL);
    std::pair<bool, BinaryTree::Node_t *> tmp = tree.InsertNode(10);
    int key = tmp.second->key;
    EXPECT_FALSE(tmp.first);
    EXPECT_EQ(key, 10);
}

TEST_F(NonEmptyTree, DeleteAllForward)
{
    int values[] = { 5, 10, 15, 20, 30, 40, 50, 55, 60, 65, 70, 80, 85, 90 };
    for(int i = 0; i < 13; ++i)
    {
        EXPECT_TRUE(tree.DeleteNode(values[i]));
    }

    tree.DeleteNode(90);
    EXPECT_TRUE(tree.GetRoot() == NULL);
}

TEST_F(NonEmptyTree, DeleteAllBackward)
{
    int values[] = { 90, 85, 80, 70, 65, 60, 55, 50, 40, 30, 20, 15, 10, 5 };
    for(int i = 0; i < 13; ++i)
    {
        EXPECT_TRUE(tree.DeleteNode(values[i]));
    }

    tree.DeleteNode(5);
    EXPECT_TRUE(tree.GetRoot() == NULL);
}

TEST_F(NonEmptyTree, DeleteNonExistingNode)
{
    EXPECT_FALSE(tree.DeleteNode(0));
}

TEST_F(NonEmptyTree, FindNode)
{
    int values[] = { 5, 10, 15, 20, 30, 40, 50, 55, 60, 65, 70, 80, 85, 90 };
    for(int i = 0; i < 14; ++i)
    {
        BinaryTree::Node_t *tmp = tree.FindNode(values[i]);
        ASSERT_TRUE(tmp != NULL);
        EXPECT_EQ(tmp->key, values[i]);
    }
}

TEST_F(NonEmptyTree, FindNonExistingNode)
{
    EXPECT_TRUE(tree.FindNode(0) == NULL);
}

//============================================================================//

TEST_F(NonEmptyTree, Axiom_1)
{
    std::vector<BinaryTree::Node_t *> leafs;
    tree.GetLeafNodes(leafs);

    for (int i = 0; leafs[i] != NULL; ++i) {
        ASSERT_TRUE(leafs[i]->color == 1);
    }
}

TEST_F(NonEmptyTree, Axiom_2)
{
    int values[] = { 5, 10, 15, 20, 30, 40, 50, 55, 60, 65, 70, 80, 85, 90 };
    for(int i = 0; i < 14; ++i)
    {
        BinaryTree::Node_t *tmp = tree.FindNode(values[i]);
        if (tmp->color == 0) {
            if (tmp->pLeft != NULL) {
                EXPECT_TRUE(tmp->pLeft->color == 1);
            }
            if (tmp->pRight != NULL) {
                EXPECT_TRUE(tmp->pRight->color == 1);
            }
        }
    }

}

TEST_F(NonEmptyTree, Axiom_3)
{
    std::vector<BinaryTree::Node_t *> leafs;
    tree.GetLeafNodes(leafs);

    BinaryTree::Node_t *root = tree.GetRoot();
    BinaryTree::Node_t *tmp = leafs[0];
    int reference_count = 0;

    while (tmp->pParent != root) {
        if (tmp->color == 1) {
            reference_count++;
        }
        tmp = tmp->pParent;
    }

    for (int i = 1; leafs[i] != NULL; ++i) {
        BinaryTree::Node_t *tmp = leafs[i];
        int count = 0;

        while (tmp->pParent != root) {
            if (tmp->color == 1) {
                count++;
            }
            tmp = tmp->pParent;
        }

        EXPECT_EQ(reference_count, count);
    }
}

/*** Konec souboru black_box_tests.cpp ***/
