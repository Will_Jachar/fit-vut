//======== Copyright (c) 2017, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Test Driven Development - priority queue code
//
// $NoKeywords: $ivs_project_1 $tdd_code.cpp
// $Author:     PETER UHRIN <xuhrin02@stud.fit.vutbr.cz>
// $Date:       $2017-03-06
//============================================================================//
/**
 * @file tdd_code.cpp
 * @author PETER UHRIN
 * 
 * @brief Implementace metod tridy prioritni fronty.
 */

#include <stdlib.h>
#include <stdio.h>

#include "tdd_code.h"

//============================================================================//
// ** ZDE DOPLNTE IMPLEMENTACI **
//
// Zde doplnte implementaci verejneho rozhrani prioritni fronty (Priority Queue)
// 1. Verejne rozhrani fronty specifikovane v: tdd_code.h (sekce "public:")
//    - Konstruktor (PriorityQueue()), Destruktor (~PriorityQueue())
//    - Metody Insert/Remove/Find a GetHead
//    - Pripadne vase metody definovane v tdd_code.h (sekce "protected:")
//
// Cilem je dosahnout plne funkcni implementace prioritni fronty implementovane
// pomoci tzv. "double-linked list", ktera bude splnovat dodane testy 
// (tdd_tests.cpp).
//============================================================================//

PriorityQueue::PriorityQueue()
{
}

PriorityQueue::~PriorityQueue()
{
    Element_t *tmp = root;

    while (tmp != NULL) {
        Element_t *next_element = tmp->pNext;
        delete tmp;
        tmp = next_element;
    }

    delete tmp;
}

void PriorityQueue::Insert(int value)
{
    Element_t *tmp = root;
    Element_t *next_element = new Element_t();
    next_element->value = value;

    if (root == NULL) {
        next_element->pPrev = NULL;
        next_element->pNext = NULL;
        root = next_element;
        return;
    }

    while (tmp->pNext != NULL) {
        if (tmp->value >= value) {

            if (tmp == root) {
                next_element->pPrev = NULL;
                next_element->pNext = tmp;

                root = next_element;
                tmp->pPrev = next_element;
            }
            else {
                next_element->pPrev = tmp->pPrev;
                next_element->pNext = tmp;

                tmp->pPrev->pNext = next_element;
                tmp->pPrev = next_element;
            }
            return;
        }
        tmp = tmp->pNext;
    }

    if (tmp->value >= value) {
        if (tmp == root) {
            next_element->pPrev = NULL;
            next_element->pNext = tmp;

            root = next_element;
            tmp->pPrev = next_element;
        }
        else {
            next_element->pPrev = tmp->pPrev;
            next_element->pNext = tmp;

            tmp->pPrev->pNext = next_element;
            tmp->pPrev = next_element;
        }
    }
    else {
        next_element->pPrev = tmp;
        next_element->pNext = NULL;
        tmp->pNext = next_element;
    }
}

bool PriorityQueue::Remove(int value)
{
    Element_t *tmp = Find(value);

    if (tmp == NULL) {
        return false;
    }

    if (tmp->pPrev == NULL) {
            root = tmp->pNext;
    }
    else {
        tmp->pPrev->pNext = tmp->pNext;
    }

    if (tmp->pNext != NULL) {
        tmp->pNext->pPrev = tmp->pPrev;
    }

    delete tmp;

    return true;
}

PriorityQueue::Element_t *PriorityQueue::Find(int value)
{
    Element_t *tmp = root;

    while (tmp != NULL) {
        if (tmp->value == value) {
            return tmp;
        }
        tmp = tmp->pNext;
    }

    return NULL;
}

PriorityQueue::Element_t *PriorityQueue::GetHead()
{
    return root;
}

/*** Konec souboru tdd_code.cpp ***/
