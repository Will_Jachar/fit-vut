//======== Copyright (c) 2017, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     White Box - Tests suite
//
// $NoKeywords: $ivs_project_1 $white_box_code.cpp
// $Author:     PETER UHRIN <xuhrin02@stud.fit.vutbr.cz>
// $Date:       $2017-03-06
//============================================================================//
/**
 * @file white_box_tests.cpp
 * @author PETER UHRIN
 * 
 * @brief Implementace testu prace s maticemi.
 */

#include "gtest/gtest.h"
#include "white_box_code.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy operaci nad maticemi. Cilem testovani je:
// 1. Dosahnout maximalniho pokryti kodu (white_box_code.cpp) testy.
// 2. Overit spravne chovani operaci nad maticemi v zavislosti na rozmerech 
//    matic.
//============================================================================//

class ZeroMatrix : public ::testing::Test
{
protected:
    Matrix matrix = Matrix(4,4);
};

class SameSizeMatrix : public ::testing::Test
{
protected:
    virtual void SetUp() {
        std::vector<std::vector< double > > values = {{1, 2, 3, 4},
                                                      {5, 6, 7, 8},
                                                      {8, 7, 6, 5},
                                                      {4, 3, 2, 1}};

        std::vector<std::vector< double > > res_plus = {{2, 4, 6, 8},
                                                      {10, 12, 14, 16},
                                                      {16, 14, 12, 10},
                                                      {8, 6, 4, 2}};

        std::vector<std::vector< double > > res_mul = {{51, 47, 43, 39},
                                                       {123, 119, 115, 111},
                                                       {111, 115, 119, 123},
                                                       {39, 43, 47, 51}};

        matrix.set(values);
        matrix2.set(values);
        matrix_res_plus.set(res_plus);
        matrix_res_mul.set(res_mul);
    }

    Matrix matrix = Matrix(4,4);
    Matrix matrix2 = Matrix(4,4);
    Matrix matrix_res_plus = Matrix(4,4);
    Matrix matrix_res_mul = Matrix(4,4);
};

class DifferentSizeMatrix : public ::testing::Test
{
protected:
    virtual void SetUp() {
        std::vector<std::vector< double > > values = {{1, 2, 3, 4},
                                                      {5, 6, 7, 8},
                                                      {8, 7, 6, 5},
                                                      {4, 3, 2, 1}};

        std::vector<std::vector< double > > values2 = {{1, 2, 3, 4, 4},
                                                       {5, 6, 7, 8, 8},
                                                       {8, 7, 6, 5, 5},
                                                       {4, 3, 2, 1, 1}};

        std::vector<std::vector< double > > res_mul = {{51, 47, 43, 39, 39},
                                                       {123, 119, 115, 111, 111},
                                                       {111, 115, 119, 123, 123},
                                                       {39, 43, 47, 51, 51}};

        matrix.set(values);
        matrix2.set(values2);
        matrix_res_mul.set(res_mul);
    }

    Matrix matrix = Matrix(4,4);
    Matrix matrix2 = Matrix(4,5);
    Matrix matrix_res_mul = Matrix(4,5);
};

class SolveEquation :public ::testing::Test
{
protected:
    virtual void SetUp() {
        std::vector<std::vector< double > > values = {{1, 2, 0},
                                                      {0, 1, -3},
                                                      {3, 0, -1}};

        matrix.set(values);
        
    }

    Matrix matrix = Matrix(3,3);
};

TEST(DefaultMatrix, DefaultConstructor)
{
    Matrix matrix;
    EXPECT_EQ(matrix.get(0,0), 0);
//###########################################################
    EXPECT_FALSE(matrix.get(1,1) == std::numeric_limits<double>::quiet_NaN());
}

TEST_F(ZeroMatrix, Constructor)
{
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            EXPECT_EQ(matrix.get(i,j), 0);
        }
    }
}

TEST_F(ZeroMatrix, ConstructorThrow)
{
    EXPECT_ANY_THROW(Matrix matrix2 = Matrix(1,0));
}

TEST_F(ZeroMatrix, SetValue)
{
    EXPECT_TRUE(matrix.set(0, 0, 42));
    EXPECT_EQ(matrix.get(0,0), 42);

    EXPECT_FALSE(matrix.set(-1, 0, 42));
    EXPECT_FALSE(matrix.set(0, -1, 42));
}

TEST_F(ZeroMatrix, SetVector)
{
    std::vector<std::vector< double > > values = {{1, 2, 3, 4},
                                                  {5, 6, 7, 8},
                                                  {8, 7, 6, 5},
                                                  {4, 3, 2, 1}};

    std::vector<std::vector< double > > values2 = {{1, 2, 3},
                                                   {5, 6, 7},
                                                   {8, 7, 6},
                                                   {4, 3, 2}};
    EXPECT_TRUE(matrix.set(values));
    EXPECT_FALSE(matrix.set(values2));
}

TEST_F(ZeroMatrix, Get)
{
    EXPECT_EQ(matrix.get(0,0), 0);
//###########################################################
    EXPECT_FALSE(matrix.get(-1, 1) == std::numeric_limits<double>::quiet_NaN());
}

TEST_F(SameSizeMatrix, oper_ekv)
{
    EXPECT_TRUE(matrix == matrix2);

    matrix2.set(2, 2, 42);
    EXPECT_FALSE(matrix == matrix2);
}

TEST_F(DifferentSizeMatrix, oper_ekv)
{
    EXPECT_ANY_THROW(matrix == matrix2);
}

TEST_F(SameSizeMatrix, oper_plus)
{
    EXPECT_TRUE((matrix + matrix2) == matrix_res_plus);
}

TEST_F(DifferentSizeMatrix, oper_plus)
{
    EXPECT_ANY_THROW(matrix + matrix2);
}

TEST_F(SameSizeMatrix, oper_mul)
{
    EXPECT_TRUE((matrix * matrix2) == matrix_res_mul);

    EXPECT_EQ((matrix * 2).get(0,0), 2);
}

TEST_F(DifferentSizeMatrix, oper_mul)
{
    EXPECT_TRUE((matrix * matrix2) == matrix_res_mul);
    EXPECT_ANY_THROW(matrix2 * matrix);
}

TEST_F(SolveEquation, Matrix)
{
    std::vector< double > jeden = {5};
    std::vector< double > dva = {5, 4};
    std::vector< double > tri = {5, 5, 4};
    std::vector< double > ctyri = {5, 5, 4, 4};
    std::vector< double > vysledky = {1, 2, -1};

    Matrix sing_matrix = Matrix();
    Matrix double_matrix = Matrix(2,2);
    double_matrix.set(0, 0, 1);
    Matrix quad_matrix = Matrix(4,4);
    quad_matrix.set(0, 0, 1);
    Matrix rect_matrix = Matrix(3,4);
    rect_matrix.set(0, 0, 1);

    EXPECT_TRUE(matrix.solveEquation(tri) == vysledky);
    EXPECT_ANY_THROW(matrix.solveEquation(ctyri));
    EXPECT_ANY_THROW(rect_matrix.solveEquation(tri));
    EXPECT_ANY_THROW(rect_matrix.solveEquation(ctyri));
    EXPECT_ANY_THROW(sing_matrix.solveEquation(jeden));
    EXPECT_ANY_THROW(double_matrix.solveEquation(dva));
    EXPECT_ANY_THROW(quad_matrix.solveEquation(ctyri));
}

/*TEST(SolveEquation, RectangularMatrix)
{
}*/

/*** Konec souboru white_box_tests.cpp ***/
