int get_args(int pocet_argumentu, char *argv[]) {
  int s = 0;
  int n = 0;
  int S = 0;
  if (pocet_argumentu > 4) {
    napoveda();
    return 0;
  }
  switch (pocet_argumentu) {
    case 0 :
      vypis_bez_argumentu(s, n);
      return 0;
    case 1 :
      if (my_strcmp("-s", argv[1]) || my_strcmp("-n", argv[1]) || my_strcmp("-S", argv[1])) {
        printf("Zadejte upresnujici argument!\n");
        return 1;
      }
      else if (my_strcmp("-x", argv[1])) {
        vypis_x();
        return 0;
      }
      else if (my_strcmp("-r", argv[1])) {
        vypis_r();
        return 0;
      }
      napoveda();
      return 0;
    case 2 :
      if (my_strcmp("-s", argv[1])) {
        s = my_atoi(argv[2]);
        if (s == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        vypis_bez_argumentu(s, n);
        return 0;
      }
      if (my_strcmp("-n", argv[1])) {
        n = my_atoi(argv[2]);
        if (n == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        vypis_bez_argumentu(s, n);
        return 0;
      }
      if (my_strcmp("-S", argv[1])) {
        S = my_atoi(argv[2]);
        if (S == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        vypis_S(S);
        return 0;
      }
      napoveda();
      return 0;
    case 3 :
      if (my_strcmp("-s", argv[1])) {
        s = my_atoi(argv[2]);
        if (s == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        if (my_strcmp("-n", argv[3])) {
          vypis_bez_argumentu(s, n);
          return 0;
        }
        napoveda();
        return 0;
      }
      if (my_strcmp("-n", argv[1])) {
        s = my_atoi(argv[2]);
        if (n == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        if (my_strcmp("-s", argv[3])) {
          vypis_bez_argumentu(s, n);
          return 0;
        }
        napoveda();
        return 0;
      }
      napoveda();
      return 0;
    case 4 :
      if (my_strcmp("-s", argv[1])) {
        s = my_atoi(argv[2]);
        if (s == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        if (my_strcmp("-n", argv[3])) {
          n = my_atoi(argv[4]);
          if (n == -1) {
          MIMO_ROZSAH;
            return 1;
          }
          vypis_bez_argumentu(s, n);
          return 0;
        }
        napoveda();
        return 0;
      }
      if (my_strcmp("-n", argv[1])) {
        n = my_atoi(argv[2]);
        if (n == -1) {
          MIMO_ROZSAH;
          return 1;
        }
        if (my_strcmp("-s", argv[3])) {
          s = my_atoi(argv[4]);
          if (s == -1) {
          MIMO_ROZSAH;
            return 1;
          }
          vypis_bez_argumentu(s, n);
          return 0;
        }
        napoveda();
        return 0;
      }
      napoveda();
      return 0;
  }
  return 0;
}