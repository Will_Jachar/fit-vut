ARGS get_args(int argc, *argv[]) {
  ARGS argumenty = {
    argumenty.s = FALSE,
    argumenty.s_num = 0,
    argumenty.n = FALSE,
    argumenty.n_num = 0,
    argumenty.S = FALSE,
    argumenty.S_num = 0,
    argumenty.x = FALSE,
    argumenty.r = FALSE,
    argumenty.stav = 0};

  for(int i = 1; i < argc; i++) {
    if (my_strcmp("-s", argv[i]) && argumenty.s == FALSE) {
      argumenty.s = TRUE;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1) {
        argumenty.s_num = my_atoi(argv[i + 1]);
      }
      else
        argumenty.stav = 2;
    }
    else if (my_strcmp("-n", argv[i]) && argumenty.n == FALSE) {
      argumenty.n = TRUE;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1) {
        argumenty.n_num = my_atoi(argv[i + 1]);
      }
      else
        argumenty.stav = 2;
    }
    else if (my_strcmp("-S", argv[i]) && argumenty.S == FALSE) {
      argumenty.S = TRUE;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1) {
        argumenty.S_num = my_atoi(argv[i + 1]);
      }
      else
        argumenty.stav = 2;
    }
    else if (my_strcmp("-x", argv[i]) && argumenty.x == FALSE) {
      argumenty.x = TRUE;
    }
    else if (my_strcmp("-r", argv[i]) && argumenty.r == FALSE) {
      argumenty.r = TRUE;
    }
    else {
      argumenty.stav = 1;
    }
  }
  return argumenty;
}