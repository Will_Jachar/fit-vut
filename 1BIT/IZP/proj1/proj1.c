/*
 * Soubor:  proj1.c
 * Datum:   13.10.2016
 * Autor:   Peter Uhrin © (xuhrin02)
 * Projekt: Projekt 1 - Prace s textem
 * Popis:   Program, ktery bud binarni data formatuje do textove podoby
 *          nebo textovou podobu dat prevadi do binarni podoby.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#define MAX 200
#define JEDEN_RADEK 16

typedef struct {     /* Struktura slouzici k uchovani nactenych argumentu. */
  int s;
  long int s_num;
  int n;
  long int n_num;
  int S;
  int S_num;
  int x;
  int r;
  int stav;
} t_args;

int my_strcmp(char *s_prvni, char *s_druhy);
int my_atoi(char *s_cislo);
int my_atohex(char *s_cislo);
t_args get_args(int argc, char *argv[]);
void napoveda(void);
void vypis_bez_argumentu(long int s, long int n);
void vypis_x(void);
void vypis_S(int n);
int vypis_r(void);

int main(int argc, char *argv[]) {
  t_args args = get_args(argc, argv);   /* Nebyl zadan zadny argument. */
  if (argc == 1) {
    vypis_bez_argumentu(args.s_num, args.n_num);
  }
  else if (args.stav == 1) {   /* Chybovy stav 1 (viz get_args()). */
    fprintf(stderr, ">Byl zadan neznamy argument, nebo stejny argument vice krat.\n");
    napoveda();
    return EXIT_FAILURE;
  }
  else if (args.stav == 2) {   /* Chybovy stav 2 (viz get_args()). */
    fprintf(stderr, ">Chybne zadan rozsirujici argument.\n");
    napoveda();
    return EXIT_FAILURE;
  }
  else if ((args.s == true || args.n == true) && args.S == false &&
      args.x == false && args.r == false) {   /* Byl zadan prepinac -s nebo -n. */
    vypis_bez_argumentu(args.s_num, args.n_num);
  }
  else if (args.s == false && args.n == false && args.S == true &&
      args.x == false && args.r == false) {   /* Byl zadan argument -S. */
    if (args.S_num > 200) {    /* Prekrocena maximalni velikost rozsirujiciho argumentu. */
      fprintf(stderr, ">Zadana hodnota je mimo rozah.\n");
      return EXIT_FAILURE;
    }
    vypis_S(args.S_num);
  }
  else if (args.s == false && args.n == false && args.S == false &&
      args.x == true && args.r == false) {    /* Byl zadan argument -x. */
    vypis_x();
  }
  else if (args.s == false && args.n == false && args.S == false &&
      args.x == false && args.r == true) {    /* Byl zadan argument -r. */
    if (vypis_r()) {    /* Neocekavany znak na vstupu. */
      fprintf(stderr, ">Neocekavany znak na vstupu.\n");
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

/* Porovnava dva retezce. Vraci true, nebo false. */
int my_strcmp(char *s_prvni, char *s_druhy) {
  int i = 0;
  while (s_prvni[i] != '\0') {
    if (s_prvni[i] != s_druhy[i])   /* Porovnava znaky a zaroven kontroluje, jestli je s_druhy kratsi. */
      return false;
    i++;
  }
  if (s_druhy[i] != '\0')   /* Kontroluje, jestli není s_druhy delsi nez s_prvni. */
    return false;
  return true;
}

/* Prevadi retezec s cislicemi na jeho skutecnout ciselnou hodnotu. */
int my_atoi(char *s_cislo) {
  int i = 0;
  int hodnota = 0;
  while (s_cislo[i] != '\0') {
    if (s_cislo[i] >= '0' && s_cislo[i] <= '9') {
      hodnota = hodnota * 10 + (s_cislo[i] - '0');    /* Nasobeni radem a přicitani dalsi cifry. */
    }
    else
      return -1;
    i++;
  }
  return hodnota;
}

/* Prevadi retezec na jeho skutecnout ciselnou hodnotu. v hexadecimalni soustave. */
int my_atohex(char *s_cislo) {
  int i = 0;
  int hodnota = 0;
  while (s_cislo[i] != '\0') {
    if (s_cislo[i] >= '0' && s_cislo[i] <= '9') {   /* Kontrola vstupnich dat. */
      hodnota = hodnota * 16 + (s_cislo[i] - '0');
    }
    else if (s_cislo[i] >= 'a' && s_cislo[i] <= 'f') {
      hodnota = hodnota * 16 + (s_cislo[i] - 'a' + 10);
    }
    else if (s_cislo[i] >= 'A' && s_cislo[i] <= 'F') {
      hodnota = hodnota * 16 + (s_cislo[i] - 'A' + 10);
    }
    else
      return -1;
    i++;
  }
  return hodnota;
}

/* Kontroluje zadane argumenty. */
t_args get_args(int argc, char *argv[]) {
  t_args argumenty = {
    argumenty.s =  false,
    argumenty.s_num =  0,
    argumenty.n =  false,
    argumenty.n_num = -1,
    argumenty.S =  false,
    argumenty.S_num =  0,
    argumenty.x =  false,
    argumenty.r =  false,
    argumenty.stav =   0};

  for(int i = 1; i < argc; i++) {
    if (my_strcmp("-s", argv[i]) && argumenty.s == false) {   /* prepinac -s */
      argumenty.s = true;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1)   /* Kontrola, zda je za prepinacem ciselny argument. */
        argumenty.s_num = my_atoi(argv[i + 1]);
      else
        argumenty.stav = 2;   /* Pokud nebyl nalezen ciselny argument nastavi chybovy stav. */
      i++;
    }
    else if (my_strcmp("-n", argv[i]) && argumenty.n == false) {    /* prepinac -n */
      argumenty.n = true;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1)   /* Kontrola, zda je za prepinacem ciselny argument. */
        argumenty.n_num = my_atoi(argv[i + 1]);
      else
        argumenty.stav = 2;   /* Pokud nebyl nalezen ciselny argument nastavi chybovy stav. */
      i++;
    }
    else if (my_strcmp("-S", argv[i]) && argumenty.S == false) {    /* argument -S */
      argumenty.S = true;
      if ((i + 1) < argc && (my_atoi(argv[i + 1])) != -1)   /* Kontrola, zda je za prepinacem ciselny argument. */
        argumenty.S_num = my_atoi(argv[i + 1]);
      else
        argumenty.stav = 2;   /* Pokud nebyl nalezen ciselny argument nastavi chybovy stav. */
      i++;
    }
    else if (my_strcmp("-x", argv[i]) && argumenty.x == false) {    /* argument -x */
      argumenty.x = true;
    }
    else if (my_strcmp("-r", argv[i]) && argumenty.r == false) {    /* argument -r */
      argumenty.r = true;
    }
    else {    /* Pokud nebyl nalezen validni argument nastavi chybovy stav. */
      argumenty.stav = 1;
    }
  }
  return argumenty;
}

/* Provadi vypis informaci o programu a napovedy. */
void napoveda(void) {
  printf("\nProgram: Projekt 1 - Prace s textem\n"
    "Autor:   Peter Uhrin (xuhrin02) © 2016\n"
    "Popis:   Program, ktery bud binarni data formatuje do textové podoby,\n"
    "         nebo textovou podobu dat prevadi do binarni podoby.\n\n"
    "Napoveda/Doplnujici argumenty programu:\n\n"
    "  -x     Vstupni data budou prevedena do hexadecimalni podoby na jeden radek.\n"
    "  -S     Program bude tisknout pouze takové posloupnosti v binarnim vstupu\n"
    "         ktere vypadaji jako textovy retezec.\n"
    "  -r     Ocekava na vstupu sekvenci hexadecimalnich cislic\n"
    "         a tyto prevadi do binarniho formatu.\n"
    "  -s M   Prepinac ktery definuje kolik znaku se ma ze vstupu ignorovat.\n"
    "         (M je cele nezaporne cislo)\n"
    "  -n N   Prepinac ktery definuje maximalni delku vstupnich bajtu ke zpracovani.\n"
    "         (N je kladne cislo)\n");
}

/* Provadi vypis znaku v hexadecimalni podobe.
 * s - od kolikateho znaku se vypisuje, n - kolik znaku se vypisuje */
void vypis_bez_argumentu(long int s, long int n) {
  unsigned int adresa = s;
  int dalsi_radek = true;
  for (unsigned long int i = s; i > 0; i--) {   /* Preskoceni s znaku. */
    if (getchar() == EOF)
      return;
  }
  while (dalsi_radek) {
    int znak;
    char s_radek[JEDEN_RADEK + 1] = {'\0'};
    printf("%08x  ", adresa);    /* Vypsani adresy prvniho bytu. */
    if (n < 0)    /* Pokud uzivatel nezada -n, pak se vypisuje do EOF. */
      n = -1;
    for(int pocet_bytu = 0; pocet_bytu < JEDEN_RADEK; pocet_bytu++, n--) {    /* Cyklus pro jeden radek (16 bytu). */
      if ((znak = getchar()) == EOF || n == 0) {
        dalsi_radek = false;
        if (pocet_bytu < (JEDEN_RADEK / 2))   /* Kompenzace mezery mezi 8. a 9. bytem. */
          putchar(' ');
        for (; pocet_bytu < JEDEN_RADEK; pocet_bytu++) {   /* Zarovnani pomoci mezer. */
          s_radek[pocet_bytu] = ' ';
          printf("   ");
        }
        break;
      }
      if (isprint(znak))    /* Ulozeni nacteneho znaku. */
        s_radek[pocet_bytu] = znak;
      else
        s_radek[pocet_bytu] = '.';
      printf("%02x ", znak);    /* Vypsani ordinalni hodnoty znaku. */
      if (pocet_bytu + 1 == (JEDEN_RADEK / 2))    /* Mezera mezi 8. a 9. bytem. */
        putchar(' ');
    }
    printf(" |%s|\n", s_radek);   /* Vypsani 16ti znaku. */
    if ((znak = getchar()) != EOF && n != 0)    /* Pokud je prvni znak dalsiho radku EOF. */
      ungetc(znak, stdin);
    else
      dalsi_radek = false;
    adresa += JEDEN_RADEK;
  }
}

/* Provadi vypis znaku v hexadecimalni podobe na jeden radek. */
void vypis_x(void) {
  int znak;
  int i = 0;
  while ((znak = getchar()) != EOF) {
    printf("%02x", znak);
    i++;
  }
  putchar('\n');
}

/* Provadi vypis pouze takovych posloupnosti, 
 * ktere vypadaji jako textovy retezec vetsi nez n. */
void vypis_S(int n) {
  char s_slovo[MAX + 1];    /* Maximalni delka jednoho retezce. */
  int znak;
  int delka = 0;
  while ((znak = getchar()) != EOF) {
    if (isprint(znak) || isblank(znak)) {
      if (delka > MAX) {   /* Kontrola maximalni delky retezce. */
        putchar(znak);
        continue;
      }
      s_slovo[delka] = znak;
      delka++;
    }
    else {
      s_slovo[delka] = '\0';
      if (delka >= n) {
        printf("%s\n", s_slovo);    /* Vypsani retezce. */
        delka = 0;
      }
      else
        delka = 0;
    }
  }
  s_slovo[delka] = '\0';
  if (delka >= n)
    printf("%s\n", s_slovo);    /* Vypsani posledniho retezce. */
}

/* Sekvenci hexadecimalnich cislic prevadi do binarniho formatu. */
int vypis_r(void) {
  int znak;
  int cislo;
  int konec = false;
  while (!konec) {
    char s_cislo[3] = {'\0'};
    while (true) {
      if ((znak = getchar()) == EOF)
        return 0;
      if (!isspace(znak)) {   /* Vynechani bilych znaku. */
        s_cislo[0] = znak;
        break;
      }
    }
    while (true) {
      if ((znak = getchar()) == EOF) {
        konec = true;
        break;
      }
      if (!isspace(znak)) {   /* Vynechani bilych znaku. */
        s_cislo[1] = znak;
        break;
      }
    }
    cislo = my_atohex(s_cislo);
    if (cislo == -1)    /* Kontrola chybneho vstupu. */
      return 1;
    printf("%c", cislo);
  }
  return 0;
}