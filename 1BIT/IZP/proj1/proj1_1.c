#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define TRUE 1
#define FALSE 0

void vypis_bez_argumentu(int s, int n) {
  int adresa = s;
  int dalsi_radek = TRUE;
  int znak;
  char s_radek[17];
  s_radek[16] = '\0';
  for (int i = s; i > 0; i--) {   /* Preskoceni s znaku. */
    getchar();
  }
  while (dalsi_radek) {
    int pocet_bytu = 0;
    int n_tmp = 0;
    if (n == 0)
      n_tmp = 1;
    printf("%08x  ", adresa);
    while (n_tmp != n) {
      if ((znak = getchar()) == EOF) {
        dalsi_radek = FALSE;
        break;
      }
      if ((n_tmp + 1) == n)
        dalsi_radek = FALSE;
      printf("%02x ", znak);
      if (isprint(znak))
        s_radek[pocet_bytu] = znak;
      else
        s_radek[pocet_bytu] = '.';
      n_tmp++;
      s++;
      pocet_bytu++;
      if (pocet_bytu == 8) {    /* Vlozeni mezery po 8mi bytech. */
        putchar(' ');
      }
      else if (pocet_bytu == 16)    /* Konec radku po 16ti bytech. */
        break;
    }
    if (pocet_bytu <= 8)    /* Kompenzace mezery pokud se nevypisuje vic jak 8 bytu. */
        putchar(' ');
    while (pocet_bytu < 16) {   /* Zarovnani radku pomoci mezer. */
      printf("   ");
      s_radek[pocet_bytu] = ' ';
      pocet_bytu++;
    }
    printf(" |%s|\n", s_radek);
    adresa += 16; 
  }
}

int main(void) {
  vypis_bez_argumentu(0, 0);
  return 0;
}

