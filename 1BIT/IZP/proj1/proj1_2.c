void vypis_bez_argumentu(int s, int n) {
  int adresa = s;
  int dalsi_radek = TRUE;
  for (int i = s; i > 0; i--) {   /* Preskoceni s znaku. */
    getchar();
  }
  while (dalsi_radek) {
    int znak;
    char s_radek[17] = {'\0'};
    printf("%08x  ", adresa);    /* Vypsani adresy prvniho bytu. */
    if (n < 0)    /* Pokud uzivatel nezada -n, pak se vypisuje do EOF. */
      n = -1;
    for(int pocet_bytu = 0; pocet_bytu < 16; pocet_bytu++, n--) {    /* Cyklus pro jeden radek (16 bytu). */
      if ((znak = getchar()) == EOF || n == 0) {
        dalsi_radek = FALSE;
        if (pocet_bytu < 8)   /* Kompenzace mezery mezi 8. a 9. bytem. */
          putchar(' ');
        for (; pocet_bytu < 16; pocet_bytu++) {   /* Zarovnani pomoci mezer. */
          s_radek[pocet_bytu] = ' ';
          printf("   ");
        }
        break;
      }
      if (isprint(znak))    /* Ulozeni nacteneho znaku. */
        s_radek[pocet_bytu] = znak;
      else
        s_radek[pocet_bytu] = '.';
      printf("%02x ", znak);    /* Vypsani ordinalni hodnoty znaku. */
      if (pocet_bytu + 1 == 8)    /* Mezera mezi 8. a 9. bytem. */
        putchar(' ');
    }
    printf(" |%s|\n", s_radek);   /* Vypsani 16ti znaku. */
    if ((znak = getchar()) != EOF && n != 0)    /* Pokud je prvni znak dalsiho radku EOF. */
      ungetc(znak, stdin);
    else
      dalsi_radek = FALSE;
    adresa += 16;
  }
}