/*
 * File:        proj2.c
 * Date:        27.11.2016
 * Author:      Peter Uhrin � (xuhrin02)
 * Project:     Project 2 - Iteration computing
 * Description: Program for calculating the natural logarithm and exponential growth
 *              using Taylor series and continuous fraction.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#define EPS   1e-9    // Precision of approximation in functions mylog() ang mypow()
#define LOG2  0.69314718056   // Value of log(2)

void hint(void);
double my_abs(double x);
double taylor_log(double x, unsigned int n);
double cfrac_log(double x, unsigned int n);
double taylor_pow(double x, double y, unsigned int n);
double taylorcf_pow(double x, double y, unsigned int n);
double mylog(double x);
double mypow(double x, double y);

/*###################################################################################*/

int main (int argc, char *argv[]) {
  if (argc == 1) {
    fprintf(stderr, ">> Too few arguments.\n");
    hint();
    return EXIT_FAILURE;
  }
  else if (!strcmp("--log", argv[1])) {    // test for argument --log
    if (argc != 4) {    // there always must be x and n
      fprintf(stderr, ">> Invalid number of arguments.\n");
      hint();
      return EXIT_FAILURE;
    }
    else {
      char *str_x, *str_n;
      double x = strtod(argv[2], &str_x);
      long int n = strtol(argv[3], &str_n, 10);
      if (*str_x == '\0' && *str_n == '\0') {   // test if there are valid numbers inside argv[]
        if (n <= 0) {
          fprintf(stderr, ">> Number of iterations is not valid.");
          hint();
          return EXIT_FAILURE;
        }
        printf("       log(%g) = %.12g\n", x, log(x));
        printf(" cfrac_log(%g) = %.12g\n", x, cfrac_log(x, n));
        printf("taylor_log(%g) = %.12g\n", x, taylor_log(x, n));
      }
      else {
        fprintf(stderr, ">> Invalid argument.\n");
        hint();
        return EXIT_FAILURE;
      }
    }
  }
  else if (!strcmp("--pow", argv[1])) {   // test for argument --pow
    if (argc != 5) {    // there always must be x, y and n
      fprintf(stderr, ">> Invalid number of arguments.\n");
      hint();
      return EXIT_FAILURE;
    }
    else {
      char *str_x, *str_y, *str_n;
      double x = strtod(argv[2], &str_x);
      double y = strtod(argv[3], &str_y);
      long int n = strtol(argv[4], &str_n, 10);
      if (*str_x == '\0' && *str_y == '\0' && *str_n == '\0') {    // test if there are valid numbers inside argv[]
        if (n <= 0) {
          fprintf(stderr, ">> Number of iterations is not valid");
          hint();
          return EXIT_FAILURE;
        }
        printf("         pow(%g,%g) = %.12g\n", x, y, pow(x, y));
        printf("  taylor_pow(%g,%g) = %.12g\n", x, y, taylor_pow(x, y, n));
        printf("taylorcf_pow(%g,%g) = %.12g\n", x, y, taylorcf_pow(x, y, n));
      }
      else {
        fprintf(stderr, ">> Invalid argument.\n");
        hint();
        return EXIT_FAILURE;
      }
    }
  }
  else if (!strcmp("--mylog", argv[1])) {   // test for argument --mylog
    if (argc != 3) {    // there always must be x
      fprintf(stderr, ">> Invalid number of arguments.\n");
      hint();
      return EXIT_FAILURE;
    }
    else {
      char *str_x;
      double x = strtod(argv[2], &str_x);
      if (*str_x == '\0') {    // test if there are valid numbers inside argv[]
        printf("   log(%g) = %.7e\n", x, log(x));
        printf(" mylog(%g) = %.7e\n", x, mylog(x));
      }
      else {
        fprintf(stderr, ">> Invalid argument.\n");
        hint();
        return EXIT_FAILURE;
      }
    }
  }
  else if (!strcmp("--mypow", argv[1])) {   // test for argument --mypow
    if (argc != 4) {    // there always must be x and y
      fprintf(stderr, ">> Invalid number of arguments.\n");
      hint();
      return EXIT_FAILURE;
    }
    else {
      char *str_x, *str_y;
      double x = strtod(argv[2], &str_x);
      double y = strtod(argv[3], &str_y);
      if (*str_x == '\0' && *str_y == '\0') {    // test if there are valid numbers inside argv[]
        printf("   pow(%g,%g) = %.7e\n", x, y, pow(x, y));
        printf(" mypow(%g,%g) = %.7e\n", x, y, mypow(x, y));
      }
      else {
        fprintf(stderr, ">> Invalid argument.\n");
        hint();
        return EXIT_FAILURE;
      }
    }
  }
  else {    // no valid argument was found
    fprintf(stderr, ">> Invalid argument.\n");
    hint();
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/*###################################################################################*/

/* Hint. */
void hint(void) {
  printf("\nProgram:       Project 2 - Iteration computing\n"
    "Author:        Peter Uhrin (xuhrin02) � 2016\n"
    "Description:   Program for calculating the natural logarithm and\n"
    "               exponential growth\n"
    "               using Taylor series and continuous fraction.\n\n"
    "Hint:\n\n"
    "  --log X N    Calculates the natural logarithm.\n"
    "       X       ln(X)\n"
    "       N       Number of iterations\n"
    "  --pow X Y N  Calculates the exponential growth.\n"
    "      X Y      X^Y\n"
    "       N       Number of iterations\n");
}

/* Reternes the apsolute value of x. */
double my_abs(double x) {
  return x < 0.0 ? -x : x;
}

/* Calculates the natural logarithm using Taylor's series. */
double taylor_log(double x, unsigned int n) {
  double sum = 0;
  double numerator = 1.0;
  if (x == INFINITY)
    return INFINITY;
  if (x == 0)
    return -INFINITY;
  if (x < 0)
    return NAN;
  if (x < 1) {
    double y = 1 - x;
    for (unsigned int i = 1; i <= n; i++) {    // Taylor's series for natural logarithm, 0 < x <= 1
      sum -= (numerator *= y)/i;
    }
  }
  else {
    for (unsigned int i = 1; i <= n; i++) {    // Taylor's series for natural logarithm, x > 1
      sum += (numerator *= ((x - 1)/x))/i;
    }
  }
  return sum;
}

/* Calculates the natural logarithm using continuous fraction. */
double cfrac_log(double x, unsigned int n) {
  if (x == INFINITY)
    return INFINITY;
  if (x == 0)
    return -INFINITY;
  if (x < 0)
    return NAN;
  else {
    double fraction = 2 * n - 1;    // deepest fraction (+-0.5)
    double z = (x - 1)/(x + 1);   // x = (1+z)/(1-z) ==> z = (x-1)/(x+1)
    double square_z = z * z;
    for (; n > 0; n--) {    // continuous fraction
      fraction = (2 * n - 1) - ((n * n * square_z)/fraction);
    }
    return (2 * z)/fraction;
  }
}

/* Calculates the exponential growth using Taylors series. */
double taylor_pow(double x, double y, unsigned int n) {
  double sum = 0;
  double previous = 1.0;
  double current = 0.0;
  if (isnan(x) || isnan(y) || x <= 0)
    return NAN;
  if (y == -INFINITY) {
    if (x > 0 && x < 1)
      return INFINITY;
    if (x == 1)
      return NAN;
    return 0;
  }
  if (y == INFINITY) {
    if (x > 0 && x < 1)
      return 0;
    if (x == 1)
      return 1;
    return INFINITY;
  }
  if (isinf(x) && y < 0)
    return 0;
  if (y == 0)
    return 1;

  double abs_y = my_abs(y);

  double numerator = abs_y * cfrac_log(x, n);

  for (unsigned int i = 1; i <= n; i++) {   // Taylor's series for exponential growth
    current = numerator / i;
    sum += previous * current;
    previous *= current;
  }
  if (y < 0)
    return 1/(1 + sum);
  return 1 + sum;
}

/* Calculates the exponential growth using continuous fraction. */
double taylorcf_pow(double x, double y, unsigned int n) {
  double sum = 0;
  double previous = 1.0;
  double current = 0.0;
  if (isnan(x) || isnan(y) || x <= 0)
    return NAN;
  if (y == -INFINITY) {
    if (x > 0 && x < 1)
      return INFINITY;
    if (x == 1)
      return NAN;
    return 0;
  }
  if (y == INFINITY) {
    if (x > 0 && x < 1)
      return 0;
    if (x == 1)
      return 1;
    return INFINITY;
  }
  if (isinf(x) && y < 0)
    return 0;
  if (y == 0)
    return 1;

  double abs_y = my_abs(y);

  double numerator = abs_y * cfrac_log(x, n);

  for (unsigned int i = 1; i <= n; i++) {   // Taylor's series for exponential growth
    current = numerator / i;
    sum += previous * current;
    previous *= current;
  }
  if (y < 0)
    return 1/(1 + sum);
  return 1 + sum;
}

/* Calculates the natural logarithm using Euler's continued fraction formula. */
double mylog(double x) {
  int less_than_one = x <= 1 ? true : false;

  if (x == INFINITY)
    return INFINITY;
  if (x == 0)
    return -INFINITY;
  if (x < 0)
    return NAN;
  else {
    double sum = 0.0;
    double dif;
    unsigned int count = 0;

    /* Heuristic for natural logarithm. This function modifies the input value of
     * ln() by adjusting value towards 1.0 for gaining the best rate of convergence.
     * Return value represents the quantity of how many times the input value was
     * modified. */

    if (less_than_one) {    // for 0 < x <= 1
      for (; x < 1; count++) {   // Shifts floating point behind first digit.
        x *= 2;
      }
    }
    else {    // for x > 1
      for (; x > 2; count++) {   // Shifts floating point behind first digit.
        x /= 2;
      }
    }

    double z = (x - 1)/(x + 1);    // x = (1+z)/(1-z) ==> z = (x-1)/(x+1)
    double square_z = z * z;
    double numerator = 1.0;

    unsigned int k = 0;
    for (; (dif = (numerator)/(2*k+1)) > EPS; k++) {   // Euler's continued fraction formula.
      sum += dif;
      numerator *= square_z;
    }

    //printf("\n Number of iterations: %d\n", k);
    if (less_than_one)
      return count * -LOG2 + 2 * z * sum;   // Compensation for shifting the floating point.
    else
      return count * LOG2 + 2 * z * sum;   // Compensation for shifting the floating point.
  }
}

/* Calculates the exponential growth using Taylor's series
 * and natural logarithm from function mylog(). */
double mypow(double x, double y) {
  double sum = 0;
  double previous = 1;
  double current = 0.0;
  if (isnan(x) || isnan(y))
    return NAN;
  if (y == -INFINITY) {
    if (x == 0)
      return INFINITY;
    if (x > 0 && x < 1)
      return INFINITY;
    if (x == 1)
      return NAN;
    return 0;
  }
  if (y == INFINITY) {
    if (x > 0 && x < 1)
      return 0;
    if (x == 1)
      return 1;
    return INFINITY;
  }
  if (isinf(x) && y < 0)
    return 0;
  if (x == 0)
    return 0;
  if (y == 0)
    return 1;
  int is_negative = x < 0 ? true : false;   // test if x is negative
  if (is_negative && y == (int)y)   // if x is negative and y is whole number, x = -x (log(x), x < 0 = nan)
    x = -x;

  double numerator = y * mylog(x);

  unsigned int i = 1;
  for (; my_abs(previous) > EPS; i++) {   // Taylor's series for exponential growth
    current = numerator / i;
    if (isinf(sum += previous * current))   // if sum is close to infinity
      return INFINITY;
    previous *= current;
  }
  //printf("\n Number of iterations: %d\n", i);
  if (is_negative && (int)y%2)    // if x was negative and y is odd
    return -(1 + sum);
  else
    return 1 + sum;
}
