/**
 * Kostra hlavickoveho souboru 3. projekt IZP 2015/16
 * a pro dokumentaci Javadoc.
 */

/**
 * @file proj3.h
 */

/**
 * @struct obj_t
 * Stores information about an object.
 */
struct obj_t {
    /*@{*/
    int id;  /**< The ID of an object. */
    float x; /**< The X coordinate. */
    float y; /**< The Y coordinate. */
    /*@}*/
};

/**
 * @struct cluster_t
 * Stores information about a cluster.
 */
struct cluster_t {
    /*@{*/
    int size;          /**< Number of objects stored in an array of objects. */
    int capacity;      /**< Number of objects for which is allocated memory in an array of objects. */
    struct obj_t *obj; /**< Array of objects. */
    /*@}*/
};

/**
 * @defgroup aid Functions for auxiliary purpose
 * @{
 */

/**
 * Hint function prints the hint.
 */
void hint(void);

/**
 * Function my_fabs
 * @param x is a number whose absolute value i want to know.
 * @return If x is lesser then 0 returnes -x, otherwise returnes x.
 */
float my_fabs(float x);

/**
 * @}
 */

/**
 * @defgroup modify Functions for modifying clusters
 * @{
 */

/**
 * Function init_cluster initializes one cluster, allocates memory for array of objects,
 * and sets the capacity variable in that structure to the amount of allocated memory.
 * @param *c points to the cluster i want to initialize.
 * @param cap is amount of objects i want to allocate memory for.
 * @pre c != NULL
 * @pre cap >= 0
 * @post Memory for an array of objects will be allocated,
 * c->size will be set to 0 and c->capacity will be set to cap.
 */
void init_cluster(struct cluster_t *c, int cap);

/**
 * Function clear_cluster frees memory allocated for array of objects in a cluster
 * and sets size and capacity variable in cluster to 0.
 * @param *c points to the cluster i want to clear.
 * @post Memory for an array of objects in cluster c will be freed and
 * c-> size and c->capacity will be set to 0.
 */
void clear_cluster(struct cluster_t *c);

/**
 * @}
 */

/**
 * Chunk of cluster objects. Value recommended for reallocation.
 */
extern const int CLUSTER_CHUNK;

/**
 * @addtogroup modify
 * @{
 */

/**
 * Function resize_cluster resizes given cluster to a new capacity.
 * @param *c points to the cluster i want to resize.
 * @param new_cap is number of objects for which i want to have enought allocated memory.
 * @pre c != NULL
 * @pre c->capacity >= 0
 * @pre new_cap >= 0
 * @return Pointer to cluster c of NULL if error has occured.
 * @post An amount of allocated memory for an array of objects will be increased and
 * and c->capacity will be set to new value.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap);

/**
 * Function append_clusters adds new object to the end of array of objects in the cluster.
 * Expands the array of objects if there isn't enought space.
 * @param *c points to the cluster into which i want to add a new object.
 * @param obj is an object i want to add to the cluster.
 * @post An object obj will be added to an array of objects in cluster c.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj);

/**
 * Function merge_clusters copies objects stored in cluster c2 to cluster c1.
 * Cluster c1 will be resized if needed.
 * In the end all objects (old and new) in cluster c1 will be sorted in ascending order.
 * @param *c1 points to the first cluster into which i want to copy objects form cluster c2.
 * @param *c2 points to the second cluster from which i want to copy objects to cluster c1.
 * @pre c1 != NULL
 * @pre c2 != NULL
 * @post Cluster at adress c1 will contain objects form both clusters c1 and c2.
 * Objects will be sorted in ascending order.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2);

/**
 * Function remove_cluster removes given cluster and frees allocated memory for its array of objects.
 * @param *carr is a pointer to an array of clusters.
 * @param narr is a number of clusters in the array of clusters *carr.
 * @param idx is an index of a cluster in the array of clusters *carr i want to remove.
 * @pre idx < narr
 * @pre narr > 0
 * @return New number of clusters in the array of clusters *carr.
 * @post All clusters behind the one we want to remove will be shifted one index forward and cluster
 * in the last index will be cleared.
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx);

/**
 * @}
 */

/**
 * @defgroup distance Functions for calculating distances
 * @{
 */

/**
 * Function obj_distance calculates a distance between two objects in Euclidean space.
 * @param *o1 points to the first object for calculating distance.
 * @param *o2 points to the second object for calculating distance.
 * @pre o1 != NULL
 * @pre o2 != NULL
 * @return Distance between two given objects.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2);

/**
 * Function cluster_distance calculates a distance between two clusters using complete linkage method.
 * @param *c1 points to the first cluster for calculating distance.
 * @param *c2 points to the second cluster for calculating distance.
 * @pre c1 != NULL
 * @pre c1->size > 0
 * @pre c2 != NULL
 * @pre c2->size > 0
 * @return Distance between two given clusters.
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2);

/**
 * Function find_neighbours finds two closest clusters to each other.
 * @param *carr is an array of clusters in which i want to find two closest neighbours.
 * @param narr is a number of clusters in an array of clusters *carr.
 * @param *c1 points to the variable in which will be index of first of two neighbours saved.
 * @param *c2 points to the variable in which will be index of second of two neighbours saved.
 * @pre narr > 0
 * @post At adress on which c1 is pointing at, an index of the first of two neigbours will be stored.
 * @post At adress on which c2 is pointing at, an index of the second of two neigbours will be stored.
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2);

/**
 * @}
 */

/**
 * @addtogroup modify
 * @{
 */

/**
 * Function sort_custer sorts objects in an array of objects in given cluster in ascending order.
 * @param *c points to a cluster in which i want to sort objects.
 * @post Objects in an array of objects in cluster at adress on which c is pointing at will be sorted in ascending order.
 */
void sort_cluster(struct cluster_t *c);

/**
 * @}
 */

/**
 * @defgroup print Functions for printing clusters
 * @{
 */

/**
 * Function print_cluster prints all objects from one given cluster in specific format.
 * @param *c points to the cluster from which i want to print objects.
 */
void print_cluster(struct cluster_t *c);

/**
 * @}
 */

/**
 * @addtogroup modify
 * @{
 */

/**
 * Function load_clusters allocates memory for as many clusters as there are objects in given file
 * from which is function loading objects and saving them separately into each cluster.
 * @param *filename is a path to a file from which i want to load objects.
 * @param **arr is adress of a pointer to array of clusters.
 * @pre arr != NULL
 * @return Number of created clusters or -1 if an error has occured.
 * @post *arr will be pointing to an array of clusters or will be NULL if an error has occured.
 */
int load_clusters(char *filename, struct cluster_t **arr);

/**
 * @}
 */

/**
 * @addtogroup print
 * @{
 */

/**
 * Function print_clusters prints all objects from all clusters to stdout.
 * Every cluster starting on new line.
 * @param *carr is an array of clusters which i want to print to stdout.
 * @param narr is a number of clusters in an array of clusters *carr.
 */
void print_clusters(struct cluster_t *carr, int narr);

/**
 * @}
 */