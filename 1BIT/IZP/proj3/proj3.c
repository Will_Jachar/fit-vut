/**
@file
*/

/**
 * Kostra programu pro 3. projekt IZP 2015/16
 *
 * Jednoducha shlukova analyza
 * Complete linkage
 * http://is.muni.cz/th/172767/fi_b/5739129/web/web/clsrov.html
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *      #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - "__FILE__":%u: "s"\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *      pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */

struct obj_t {
    int id;
    float x;
    float y;
};

struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/*
 Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 Ukazatel NULL u pole objektu znamena kapacitu 0.
*/

void init_cluster(struct cluster_t *c, int cap)
{
    assert(c != NULL);
    assert(cap >= 0);

    c->capacity = cap;
    c->size = 0;

    if ((c->obj = (struct obj_t *) malloc(sizeof(struct obj_t) * cap)) == NULL) {
        fprintf(stderr, ">> Nepodarilo se allocovat dostatek pameti.\n");
        c->capacity = 0;
    }
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/*
 Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 */
void clear_cluster(struct cluster_t *c)
{
    free(c->obj);
    c->size = 0;
    c->capacity = 0;
}

/*
 Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
    // TUTO FUNKCI NEMENTE
    assert(c);
    assert(c->capacity >= 0);
    assert(new_cap >= 0);

    if (c->capacity >= new_cap)
        return c;

    size_t size = sizeof(struct obj_t) * new_cap;

    void *arr = realloc(c->obj, size);
    if (arr == NULL)
        return NULL;

    c->obj = arr;
    c->capacity = new_cap;
    return c;
}

/*
 Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 nevejde.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
    if (c->size <= c->capacity) {
        if (resize_cluster(c, CLUSTER_CHUNK) == NULL) {
            fprintf(stderr, "Nepodarilo se allokovat dostatek mista.\n");
            return;
        }
    }

    // Kopirovani objektu
    int size = c->size;
    c->obj[size].id = obj.id;
    c->obj[size].x = obj.x;
    c->obj[size].y = obj.y;
    c->size++;
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);

/*
 Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 Objekty ve shluku 'c1' budou serazny vzestupne podle identifikacniho cisla.
 Shluk 'c2' bude nezmenen.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c2 != NULL);

    int new_cap = c1->size + c2->size;
    if (resize_cluster(c1, new_cap) == NULL) {      // Allokace nove pameti
        fprintf(stderr, "Nepodarilo se allokovat dostatek pameti.\n");
        return;
    }

    for (int i = 0; i < c2->size; i++) {        // Pridani novych objektu
        append_cluster(c1, c2->obj[i]);
    }

    sort_cluster(c1);
}

/**********************************************************************/
/* Prace s polem shluku */

/*
 Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 pocet shluku v poli.
*/
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
    assert(idx < narr);
    assert(narr > 0);

    clear_cluster(&carr[idx]);
    for (int i = idx; i < narr-1; i++) {      // Posunuti zbyvajicich shluku o jedno dozadu
        carr[i].size = carr[i+1].size;
        carr[i].capacity = carr[i+1].capacity;
        carr[i].obj = carr[i+1].obj;
    }

    return narr - 1;
}

/*
 Pocita Euklidovskou vzdalenost mezi dvema objekty.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
    assert(o1 != NULL);
    assert(o2 != NULL);

    float distance_x = o1->x - o2->x;
    float distance_y = o1->y - o2->y;

    float distance = sqrtf((distance_x*distance_x) + (distance_y*distance_y)); // Vzdalenost dvou objektu.

    return distance;
}

/*
 Pocita vzdalenost dvou shluku.
*/
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c1->size > 0);
    assert(c2 != NULL);
    assert(c2->size > 0);

    float max_distance = 0;
    float distance;

    for (int i = 0; i < c1->size; i++) {        // Porovnava kazdy s kazdym
        for (int j = 0; j < c2->size; j++) {
            distance = obj_distance(&c1->obj[i], &c2->obj[j]);
            if (distance > max_distance)
                max_distance = distance;
        }
    }

    return max_distance;
}

/*
 Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 hleda dva nejblizsi shluky. Nalezene shluky identifikuje jejich indexy v poli
 'carr'. Funkce nalezene shluky (indexy do pole 'carr') uklada do pameti na
 adresu 'c1' resp. 'c2'.
*/
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
    assert(narr > 0);

    float min_distance = cluster_distance(&carr[0], &carr[1]);      // Referenci vzdalenost
    float distance;

    for (int i = 0; i < narr; i++) {        // Porovnava kazdy s kazdym
        for (int j = i+1; j < narr; j++) {
            distance = cluster_distance(&carr[i], &carr[j]);
            if (distance <= min_distance) {
                *c1 = i;
                *c2 = j;
                min_distance = distance;
            }
        }
    }
}

// pomocna funkce pro razeni shluku
static int obj_sort_compar(const void *a, const void *b)
{
    // TUTO FUNKCI NEMENTE
    const struct obj_t *o1 = a;
    const struct obj_t *o2 = b;
    if (o1->id < o2->id) return -1;
    if (o1->id > o2->id) return 1;
    return 0;
}

/*
 Razeni objektu ve shluku vzestupne podle jejich identifikatoru.
*/
void sort_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/*
 Tisk shluku 'c' na stdout.
*/
void print_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    for (int i = 0; i < c->size; i++)
    {
        if (i) putchar(' ');
        printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
    }
    putchar('\n');
}

/*
 Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
*/

#define BUFFER 64
int load_clusters(char *filename, struct cluster_t **arr)
{
    assert(arr != NULL);

    FILE *fr;
    if ((fr = fopen(filename, "r")) == NULL) {      // Otevreni souboru
        fprintf(stderr, ">> Nepodarilo se otevrit soubor %s.\n", filename);
        *arr = NULL;
        return -1;
    }

    int obj_count;
    if (fscanf(fr, "count=%d\n", &obj_count) != 1) {
      fprintf(stderr, ">> Chyba v souboru.\n");
      *arr = NULL;
      fclose(fr);
      return -1;
    }

    // Allokace mista pro shluky
    if ((*arr = (struct cluster_t *) malloc(sizeof(struct cluster_t) * obj_count)) == NULL) {
        fprintf(stderr, ">> Nepodarilo se allocovat dostatek pameti.\n");
        *arr = NULL;
        fclose(fr);
        return -1;
    }

    for (int i = 0; i < obj_count; i++) {
        init_cluster(&(*arr)[i], CLUSTER_CHUNK);     // Allokace mista pro jednotlive objekty
        if ((*arr)[i].obj == NULL) {
            for (; i >= 0; i--) {       // Uklizim po sobe
                clear_cluster(&(*arr)[i]);
            }
            free(*arr);
            *arr = NULL;
            fclose(fr);
            return -1;
        }

        struct obj_t obj;
        char radek[BUFFER];
        char tail[BUFFER];

        // Nacteni objektu ze souboru do struktury
        fgets(radek, BUFFER, fr);
        if (sscanf(radek, "%d %f %f %s\n", &obj.id, &obj.x, &obj.y, tail) != 3) {
            fprintf(stderr, ">> Chyba v souboru.\n");
            for (; i >= 0; i--) {       // Uklizim po sobe
                clear_cluster(&(*arr)[i]);
            }
            free(*arr);
            *arr = NULL;
            fclose(fr);
            return -1;
        }
        if (obj.x < 0 || obj.x > 1000 || obj.y < 0 || obj.y > 1000 || obj.x != (int)obj.x || obj.y != (int)obj.y) {
            fprintf(stderr, ">> Chyba v souboru.\n");
            for (; i >= 0; i--) {       // Uklizim po sobe
                clear_cluster(&(*arr)[i]);
            }
            free(*arr);
            *arr = NULL;
            fclose(fr);
            return -1;
        }
        append_cluster(&(*arr)[i], obj);    // Pridani objektu do shluku
    }

    for (int i = 0; i < obj_count; i++) {       // Kontrola duplicity OBJID
        for (int j = i+1; j < obj_count; j++) {
            if ((*arr)[i].obj->id == (*arr)[j].obj->id) {
                fprintf(stderr, ">> Duplicitni OBJID.\n");
                for (; obj_count > 0; obj_count--) {       // Uklizim po sobe
                    clear_cluster(&(*arr)[obj_count-1]);
                }
                free(*arr);
                *arr = NULL;
                fclose(fr);
                return -1;
            }
        }
    }

    fclose(fr);
    return obj_count;       // Pocet nactenych shluku
}

/*
 Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 Tiskne se prvnich 'narr' shluku.
*/
void print_clusters(struct cluster_t *carr, int narr)
{
    printf("Clusters:\n");
    for (int i = 0; i < narr; i++)
    {
        printf("cluster %d: ", i);
        print_cluster(&carr[i]);
    }
}

int main(int argc, char *argv[])
{
    struct cluster_t *clusters;
    int cluster_count;
    int final_cluster_count = 1;       // Pozadovany pocet shluku

    if (argc == 1 || argc > 3) {        // Test na spravny pocet argumentu
      fprintf(stderr, ">> Nespravny pocet argumentu.\n");
      return 1;
    }
    if (argc == 3) {
      char *tail;
      final_cluster_count = strtol(argv[2], &tail, 10);     // Volitelny argument [N]
      if (tail[0] != '\0' || final_cluster_count <= 0) {
        fprintf(stderr, ">> Nespravny argument.\n");
        return 1;
      }
    }

    cluster_count = load_clusters(argv[1], &clusters);      // Nacteni objektu ze souboru
    if (cluster_count < 0) {
        return 1;
    }
    if (final_cluster_count > cluster_count) {
        fprintf(stderr, ">> Nepravny argument.\n");
        for (int i = 0; i < cluster_count; i++) {       // Uklizim po sobe
          clear_cluster(&clusters[i]);
        }
        free(clusters);
        return 1;
    }

    int c1, c2;

    while (cluster_count > final_cluster_count) {       // Smycka pro hledani a slucovani shluku
        find_neighbours(clusters, cluster_count, &c1, &c2);
        merge_clusters(&clusters[c1], &clusters[c2]);
        cluster_count = remove_cluster(clusters, cluster_count, c2);
    }

    print_clusters(clusters, cluster_count);

    for (int i = 0; i < cluster_count; i++) {       // Uklizim po sobe
      clear_cluster(&clusters[i]);
    }
    free(clusters);

    return 0;
}
