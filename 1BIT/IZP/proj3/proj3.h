/**
 * Kostra hlavickoveho souboru 3. projekt IZP 2015/16
 * a pro dokumentaci Javadoc.
 */

/**
 * @author Peter Uhrin
 */

/**
 * STRUCT obj_t
 * Stores information about an object.
 */
struct obj_t {
    int id;
    float x;
    float y;
};

/**
 * STRUCT cluster_t
 * Stores information about a cluster.
 */
struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/**
 * Hint function prints the hint.
 */
void hint(void);

/**
 * Function my_fabs
 * @param x is a number whose absolute value i want to know.
 * @return If x is lesser then 0 returnes -x, otherwise returnes x.
 */
float my_fabs(float x);

/**
 * Function init_cluster initializes one cluster, allocates memory for array of objects,
 * and sets the capacity variable in that structure to the amount of allocated memory.
 * @param *c points to the cluster i want to initialize.
 * @pre c != NULL
 * @param cap is amount of objects i want to allocate memory for.
 * @pre cap >= 0
 */
void init_cluster(struct cluster_t *c, int cap);

/**
 * Function clear_cluster frees memory allocated for array of objects in a cluster
 * and sets size and capacity variable in cluster to 0.
 * @param *c points to the cluster i want to clear.
 */
void clear_cluster(struct cluster_t *c);

/**
 * Chunk of cluster objects. Value recommended for reallocation.
 */
extern const int CLUSTER_CHUNK;

/**
 * Function resize_cluster resizes given cluster to a new capacity.
 * @param *c points to the cluster i want to resize.
 * @param new_cap is number of objects for which i want to have enought allocated memory.
 * @return Pointer to cluster c.
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap);

/**
 * Function append_clusters adds new object to the end of array of objects in the cluster.
 * Expands the array of objects if there isn't enought space.
 * @param *c points to the cluster into which i want to add a new object.
 * @param obj is a object i want to add to the cluster.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj);

/**
 * Function merge_clusters copies objects stored in cluster c2 to cluster c1.
 * Cluster c1 will be resized if needed.
 * In the end all objects (old and new) in cluster c1 will be sorted in ascending order.
 * @param *c1 points to the first cluster into which i want to copy objects form cluster c2.
 * @param *c2 points to the second cluster from which i want to copy objects to cluster c1.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2);

/**
 * Function remove_cluster removes given cluster and frees allocated memory for its array of objects.
 * @param *carr is a pointer to an array of clusters.
 * @param narr is a number of clusters in the array of clusters *carr.
 * @param idx is an index of a cluster in the array of clusters *carr i want to remove.
 * @return New number of clusters in the array of clusters *carr.
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx);

/**
 * Function obj_distance calculates a distance between two objects in Euclidean space.
 * @param *o1 points to the first object for calculating distance.
 * @param *o2 points to the second object for calculating distance.
 * @return Distance between two given objects.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2);

/**
 * Function cluster_distance calculates a distance between two clusters using complete linkage method.
 * @param *c1 points to the first cluster for calculating distance.
 * @param *c2 points to the second cluster for calculating distance.
 * @return Distance between two given clusters.
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2);

/**
 * Function find_neighbours finds two closest clusters to each other.
 * @param *carr is an array of clusters in which i want to find two closest neighbours.
 * @param narr is a number of clusters in an array of clusters *carr.
 * @param *c1 points to the variable in which will be index of first of two neighbours saved.
 * @param *c2 points to the variable in which will be index of second of two neighbours saved.
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2);

/**
 * Function sort_custer sorts objects in an array of objects in given cluster in ascending order.
 * @param *c points to a cluster in which i want to sort objects.
 */
void sort_cluster(struct cluster_t *c);

/**
 * Function print_cluster
 * @param
 */
void print_cluster(struct cluster_t *c);

/**
 * Function load_clusters 
 * @param 
 * @param
 */
int load_clusters(char *filename, struct cluster_t **arr);

/**
 * Function print_clusters prints all objects from all clusters to stdout.
 * Every cluster starting on new line.
 * @param *carr is an array of clusters which i want to print to stdout.
 * @param narr is a number of clusters in an array of clusters *carr.
 */
void print_clusters(struct cluster_t *carr, int narr);