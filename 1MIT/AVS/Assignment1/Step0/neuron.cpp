/*
 * Architektury výpočetních systémů (AVS 2019)
 * Projekt c. 1 (ANN)
 * Login: xuhrin02
 */

#include <cstdlib>
#include "neuron.h"

float evalNeuron(
  size_t inputSize,
  size_t neuronCount,
  const float* input,
  const float* weights,
  float bias,
  size_t neuronId
)
{
  float sum = bias;

  for (int i = 0; i < inputSize; i++) {
    sum += input[i] * weights[i * neuronCount + neuronId];
  }

  return sum > 0.0f ? sum : 0.0f;
}
