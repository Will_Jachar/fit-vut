/*
 * Architektury výpočetních systémů (AVS 2019)
 * Projekt c. 1 (ANN)
 * Login: xuhrin02
 */

#include <cstdlib>
#include "neuron.h"

float evalNeuron(
  size_t inputSize,
  const float* input,
  const float* weights,
  float bias
)
{
  float sum = bias;

  #pragma omp simd aligned(weights) reduction(+: sum)
  for (int i = 0; i < inputSize; i++) {
    sum += input[i] * weights[i];
  }

  return sum > 0.0f ? sum : 0.0f;
}
