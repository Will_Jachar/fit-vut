/**
 * @file    loop_mesh_builder.cpp
 *
 * @author  Peter Uhrín <xuhrin02@stud.fit.vutbr.cz>
 *
 * @brief   Parallel Marching Cubes implementation using OpenMP loops
 *
 * @date    9.12.2019
 **/

#include <iostream>
#include <limits>
#include <math.h>
#include <omp.h>

#include "loop_mesh_builder.h"

LoopMeshBuilder::LoopMeshBuilder(unsigned gridEdgeSize)
    : BaseMeshBuilder(gridEdgeSize, "OpenMP Loop")
{
    mThreadTriangles.resize(omp_get_max_threads());
}

unsigned LoopMeshBuilder::marchCubes(const ParametricScalarField &field)
{
    size_t totalCubesCount = mGridSize * mGridSize * mGridSize;

    unsigned totalTriangles = 0;

    #pragma omp parallel for schedule(static) reduction(+: totalTriangles)
    for (size_t i = 0; i < totalCubesCount; ++i)
    {
        Vec3_t<float> cubeOffset(
            i % mGridSize,
            (i / mGridSize) % mGridSize,
            i / (mGridSize * mGridSize));

        totalTriangles += buildCube(cubeOffset, field);
    }

    return totalTriangles;
}

float LoopMeshBuilder::evaluateFieldAt(const Vec3_t<float> &pos, const ParametricScalarField &field)
{

    const Vec3_t<float> *pPoints = field.getPoints().data();
    const unsigned count = unsigned(field.getPoints().size());

    float value = std::numeric_limits<float>::max();

    for (unsigned i = 0; i < count; ++i)
    {
        float distanceSquared = (pos.x - pPoints[i].x) * (pos.x - pPoints[i].x);
        distanceSquared += (pos.y - pPoints[i].y) * (pos.y - pPoints[i].y);
        distanceSquared += (pos.z - pPoints[i].z) * (pos.z - pPoints[i].z);

        value = std::min(value, distanceSquared);
    }

    return sqrt(value);
}

void LoopMeshBuilder::emitTriangle(const Triangle_t &triangle)
{
    mThreadTriangles[omp_get_thread_num()].push_back(triangle);
}

const BaseMeshBuilder::Triangle_t *LoopMeshBuilder::getTrianglesArray() const
{
    std::vector<Triangle_t> mTriangles;

    for(auto const& triangles: mThreadTriangles) {
        mTriangles.insert(std::end(mTriangles), std::begin(triangles), std::end(triangles));
    }

    return mTriangles.data();
}
