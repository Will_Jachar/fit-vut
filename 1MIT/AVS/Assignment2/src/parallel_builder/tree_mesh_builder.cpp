/**
 * @file    tree_mesh_builder.cpp
 *
 * @author  Peter Uhrín <xuhrin02@stud.fit.vutbr.cz>
 *
 * @brief   Parallel Marching Cubes implementation using OpenMP tasks + octree early elimination
 *
 * @date    9.12.2019
 **/

#include <iostream>
#include <limits>
#include <math.h>

#include "tree_mesh_builder.h"

#define SQRT_3_OVER_2 0.86602540378f

TreeMeshBuilder::TreeMeshBuilder(unsigned gridEdgeSize)
    : BaseMeshBuilder(gridEdgeSize, "Octree")
{
}

unsigned TreeMeshBuilder::marchSubcube(const Vec3_t<float> &pos, unsigned edgeSize, const ParametricScalarField &field)
{
    // End of recursion condition -> when edge of the cube is <= 1 build cube
    if (edgeSize <= 1)
    {
        return buildCube(pos, field);
    }

    unsigned halfSize = edgeSize / 2;

    Vec3_t<float> middlePoint(
        (pos.x + halfSize) * mGridResolution,
        (pos.y + halfSize) * mGridResolution,
        (pos.z + halfSize) * mGridResolution);

    // Check if cube is empty
    if (evaluateFieldAt(middlePoint, field) > (mIsoLevel + SQRT_3_OVER_2 * edgeSize * mGridResolution))
    {
        return 0;
    }

    unsigned totalTriangles = 0;

    // Recursively check all subcubes
    #pragma omp parallel reduction(+: totalTriangles) if (edgeSize >= 4)
    {
        #pragma omp single
        {
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(pos, halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x + halfSize, pos.y, pos.z), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x, pos.y + halfSize, pos.z), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x + halfSize, pos.y + halfSize, pos.z), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x, pos.y, pos.z + halfSize), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x + halfSize, pos.y, pos.z + halfSize), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x, pos.y + halfSize, pos.z + halfSize), halfSize, field);
            #pragma omp task shared(totalTriangles)
            totalTriangles += marchSubcube(
                Vec3_t<float>(pos.x + halfSize, pos.y + halfSize, pos.z + halfSize), halfSize, field);
        }
    }

    return totalTriangles;
}

unsigned TreeMeshBuilder::marchCubes(const ParametricScalarField &field)
{
    return marchSubcube(Vec3_t<float>(), mGridSize, field);
}

float TreeMeshBuilder::evaluateFieldAt(const Vec3_t<float> &pos, const ParametricScalarField &field)
{
    const Vec3_t<float> *pPoints = field.getPoints().data();
    const unsigned count = unsigned(field.getPoints().size());

    float value = std::numeric_limits<float>::max();

    for (unsigned i = 0; i < count; ++i)
    {
        float distanceSquared = (pos.x - pPoints[i].x) * (pos.x - pPoints[i].x);
        distanceSquared += (pos.y - pPoints[i].y) * (pos.y - pPoints[i].y);
        distanceSquared += (pos.z - pPoints[i].z) * (pos.z - pPoints[i].z);

        value = std::min(value, distanceSquared);
    }

    return sqrt(value);
}

void TreeMeshBuilder::emitTriangle(const BaseMeshBuilder::Triangle_t &triangle)
{
    #pragma omp critical
    {
        mTriangles.push_back(triangle);
    }
}
