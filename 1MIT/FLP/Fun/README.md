---
title: FLP - Funkcionální projekt
author: Peter Uhrín (xuhrin02)
date: 5.4.2020
---



[toc]

# PLG-2-NKA

### Implementace

#### Datové struktury

V projektu jsem využil několika datových struktur, hlavně pro zlepšení čitelnosti programu, ale v některých případech také na zjednodušení závěrečného výpisu.

Některé typy jsem pouze kvůli přehlednosti přejmenoval. To se týká typů `Nonterminal`, `Terminal`, `State`, `Symbol`. Struktury `Grammar` a `FiniteAutomaton` odpovídají struktuře těchto entit z formální definice. Struktury `Rule` a `Transition` mají navíc implementované vlastní chování pro funkci `show`, tak aby se vypisovali korektně.

#### Funkce `main`

Funkce `main` řídí chod celého programu. Její průběh je přímočarý. Nejprve pomocí funkce `getArgs` načte argumenty, které následně zpracuje. Podle zadaných argumentů načte vstup ze souboru, nebo ze standardního vstupu. Následně načtený vstup zpracuje do interní struktury gramatiky, tu zjednoduší a nakonec převede na konečný automat. Nakonec podle zadaného argumentu vypíše požadovanou strukturu.

#### Zpracování argumentů

V projektu jsem při zpracování projektu vycházel z formátu uvedeném v zadání, tedy:

```./plg-2-nka option [filename]```

Program tedy předpokládá, že případná cesta k souboru je uvedená jako poslední argument a před ním může, ale nemusí být uveden jeden argument pro výběr možnosti výpisu. Více možností výpisů najednou není možné uvest. Pakliže nejsou programu zadány žádné argumenty, cčte ze standardního vstupu a vypisuje na konci konečný automat (tedy možnost výpisu `-2`).

#### Zpracování vstupu

Pokud je při zpracování argumentů načtena i cesta k souboru, program se pokusí tento soubor otevřít a načíst vstup z něj. V opačné případě se vstup načítá ze standardního vstupu. Po načtení je řetězec rozdělen do pole podle jednotlivých řádků, přičemž prázdné řádky jsou ignorovány. Z každého řádku jsou následně smazány bílé znaky.

Následně probíhá zpracování vstupu do interní struktury pro gramatiku. Proběhne kontrola, zda byly zadány alespoň čtyři neprázdné řádky (neterminály, terminály, počáteční neterminál a alespoň jedno pravidlo) a následně jsou tyto řádky zpracovány.

- Zpracování neterminálů a terminálů probíhá jednoduchým rozdělením podle čárek.
- Při zpracování počátečního neterminálu je pouze provedena kontrola, zda se opravdu jedná o neterminál uvedený na prvním řádku vstupu.
- Při zpracování pravidel je každé pravidlo rozděleno na levou a pravou část podle řetězce "->". Na levé části se zkontroluje, zda se jedná o platný neterminál. Na pravé části se kontroluje, zda odpovídá formátu pravé lineární gramatiky. Pravá část tedy může obsahovat buď epsilon, nebo posloupnost terminálů, nebo posloupnost terminálů zakončenou jedním neterminálem. Nesplnění tohoto formátu ukončí program s chybovou hláškou.

#### Zjednodušení gramatiky

Ve fázi zjednodušování gramatiky se pracuje pouze s pravidly a neterminály. Každé pravidlo rozgenerováno na jedno, nebo více pravidel, pokud nesplňuje formát, že na pravé straně pravidla se nachází buď epsilon, nebo právě jeden terminál a jeden neterminál. Rozgenerování pravidla se řídí následovně:

```
if A->#   => [A->#]
if A->a   => [A->aA1, A1->#]
if A->aB  => [A->aB]
if A->a.. => [A->aA1, A1->..]
```

Na konci zjednodušování pravidel je v gramatice aktualizován taky seznam neterminálů, který obsahuje také nově vygenerované neterminály.

#### Převod gramatiky na konečný automat

Po zjednodušení původní gramatiky je možné gramatiku převést na konečný automat.

- Převod neterminálů na stavy je prováděn jednoduše jako namapování každého neterminálu na jedno číslo, protože počet neterminálů odpovídá počtu stavů.
- Terminály jsou ekvivalentní jako abeceda konečného automatu.
- Počáteční stav je odvozen z počátečního neterminálu a to za pomoci vyhledání příslušného stavu  ve slovníku vytvořeném jako `zip nonterminals states`.
- Převod pravidel gramatiky na přechody konečného automatu je díky předchozímu zjednodušení gramatiky také přímočarý. Pro každé pravidlo, které nemá na pravé straně epsilon se vytvoří přechod tak, že jako původní stav se použije stav odpovídající neterminálu na levé straně, jako cílový stav se použije stav odpovídající neterminálu na konci pravé strany a jako symbol je použit terminál na začátku pravé strany.
- Koncové stavy jsou opět nalezeny za pomocí pravidel, kde se vyhledají všechna pravidla, která mají na pravé straně epsilon. Stav odpovídající neterminálu na levé straně takového pravidla je následně vložen do množiny koncových stavů.

#### Výpis výsledků

Pro výpis gramatiky a výpis konečného automatu v požadovaném formátu jsou vytvořeny funkce `printGrammar` a `printFiniteAutomaton`. Většina formátování výstupu se děje právě v těchto funkcích.

- Neterminály, terminály, stavy a koncové stavy jsou jednoduše spojeny znakem čárky.
- Počáteční neterminál a počáteční stav není potřeba pře výpisem nijak upravovat.
- Pravidla a přechody mají implementováno chování pro funkci `show`. Pro výpis pravidla je tedy možné použít ji.

### Využité moduly

V projektu jsou využity funkce z několika modulů.

- **`System.Enviroment`** - použití funkce `getArgs` pro načtení argumentů programu a použití funkcí `getContents` a `readFile` pro načtení vstupních dat.
- **`Data.List`** - použití funkce `sort` pro výsledné seřazení neterminálů a pravidel po zjednodušení gramatiky a funkce `intercalate` pro spojování řetězců při výsledném výpisu.
- **`Data.List.Split`** - použití funkce `splitOn` pro jednoduché rozdělování řetězců ve fázi zpracování vstupu.

### Ukázkové vstupy

V odevzdaném archivu se nachází dva ukázkové vstupy. V souboru _test/test-1.in_ se nachází ukázkový vstup uvedený v zadání projektu. V souboru _test/test-2.in_ se následně nachází ukázkový vstup s gramatikou, popsanou v příkladu 3.9 ve skriptech do předmětu TIN.

