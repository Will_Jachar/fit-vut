-- FLP (2020): Functional project
-- Project variant: PLG-2-NKA
-- Author: Peter Uhrín (xuhrin02)


import Data.List
import Data.List.Split
import System.Environment


-- Grammar data types -----------------------------------------------------

type Nonterminal = String
type Terminal = String

data Rule = Rule
    { left :: Nonterminal
    , right :: [String]
    } deriving (Eq, Ord)

-- Show Rule in correct format
instance Show Rule where
    show (Rule left right) = left ++ "->" ++ (foldr (++) "" right)

data Grammar = Grammar
    { nonterminals :: [Nonterminal]
    , terminals :: [Terminal]
    , rules :: [Rule]
    , initial_nonterminal :: Nonterminal
    } deriving (Show)


-- Finite Automaton data types --------------------------------------------

type State = Integer
type Symbol = String

data Transition = Transition { origin :: State
                             , symbol :: Symbol
                             , destination :: State
                             }

-- Show Transition in correct format
instance Show Transition where
    show (Transition origin symbol destination) =
        (show origin) ++ "," ++ symbol ++ "," ++ (show destination)

data FiniteAutomaton = FiniteAutomaton { states :: [State]
                                       , alphabet :: [Symbol]
                                       , transitions :: [Transition]
                                       , initial_state :: State
                                       , end_states :: [State]
                                       }


-- Main function ==========================================================

main :: IO ()
main = do
    args <- getArgs
    let (option, filename) = parseArgs args

    raw_input <-
        if
            filename == ""
        then
            getContents
        else
            readFile filename

    let grammar = parseGrammar (prepareInput raw_input)
    let simplified_grammar = simplifyGrammar grammar
    let finite_automaton = convertGrammar simplified_grammar
    
    case option of
        "-i" -> printGrammar grammar
        "-1" -> printGrammar simplified_grammar
        "-2" -> printFiniteAutomaton finite_automaton

    return ()


-- Utility functions ------------------------------------------------------

-- Parse input arguments into option and filename
parseArgs :: [String] -> (String, String)
parseArgs [] = ("-2", "")
parseArgs (arg:[])
    | elem arg ["-i", "-1", "-2"] = (arg, "")
    | otherwise = ("-2", arg)
parseArgs (option:filename:[])
    | elem option ["-i", "-1", "-2"] = (option, filename)
parseArgs _ = error "Invalid argument!"


-- Prepare input by spliting it by lines, removing empty lines and white characters
prepareInput :: String -> [String]
prepareInput raw_input = map removeWhitespaces (filter (/= "") (lines raw_input))


-- Remove white characters from string
removeWhitespaces :: String -> String
removeWhitespaces input = filter (`notElem` [' ', '\t']) input


-- Parse grammar functions ------------------------------------------------

-- Parse raw input into grammar data structure
parseGrammar :: [String] -> Grammar
parseGrammar raw_input
    | (length raw_input) >= 4 =
        let
            (raw_nonterminals:raw_terminals:raw_initial_nonterminal:raw_rules) = raw_input
            nonterminals = parseNonterminals raw_nonterminals
            terminals = parseTerminals raw_terminals
            initial_nonterminal = parseInitialNonterminal nonterminals raw_initial_nonterminal
            rules = parseRules nonterminals terminals raw_rules
        in
            (Grammar nonterminals terminals rules initial_nonterminal)
    | otherwise = error "Missing information in grammar definition!"


-- Parse non-terminals by splitting them by colon
parseNonterminals :: String -> [Nonterminal]
parseNonterminals nonterminals = splitOn "," nonterminals


-- Parse terminals by splitting them by colon
parseTerminals :: String -> [Terminal]
parseTerminals terminals = splitOn "," terminals


-- Parse initial non-terminal and check if it's a valid non-terminal
parseInitialNonterminal :: [Nonterminal] -> String -> Nonterminal
parseInitialNonterminal all_nonterminals nonterminal =
    if
        elem nonterminal all_nonterminals
    then
        nonterminal
    else
        error "Invalid literal for inital non-terminal!"


-- Parse rules and check if thay are in correct format
parseRules :: [Nonterminal] -> [Terminal] -> [String] -> [Rule]
parseRules _ _ [] = []
parseRules all_nonterminals all_terminals (raw_rule:raw_rules) =
    let
        rule = splitOn "->" raw_rule
        left = head rule
        right = [[symbol] | symbol <- last rule]
    in
        if -- Check if there is non-terminal on the left and right side is valid
            (elem left all_nonterminals) &&
            (notElem (head right) all_nonterminals) && -- Check for rules starting with non-terminals
            ((right == ["#"]) || (checkRuleRight right))
        then
            [(Rule left right)] ++ parseRules all_nonterminals all_terminals raw_rules
        else
            error "Invalid rule format!"
        where -- Check if symbols on the right side are terminals or non-terminals
            checkRuleRight [] = False
            checkRuleRight (symbol:[]) =
                (elem symbol all_nonterminals) || (elem symbol all_terminals)
            checkRuleRight (symbol:symbols) =
                if
                    elem symbol all_terminals
                then
                    checkRuleRight symbols
                else
                    False


-- Simplify grammar functions ---------------------------------------------

-- Simplifies grammar by generating new nonterminals and rules so every rule
-- has a form A->aB or A->#
simplifyGrammar :: Grammar -> Grammar
simplifyGrammar (Grammar nonterminals terminals rules initial_nonterminal) =
    Grammar new_nonterminals terminals new_rules initial_nonterminal
    where
        new_rules = sort (simplifyRules nonterminals rules)
        new_nonterminals = sort (updateNonterminalsFromRules nonterminals new_rules)


-- Gets all neterminal from provided rules
updateNonterminalsFromRules :: [Nonterminal] -> [Rule] -> [Nonterminal]
updateNonterminalsFromRules nonterminals [] = nonterminals
updateNonterminalsFromRules nonterminals ((Rule left right):rules)
    | elem left all_nonterminals = all_nonterminals -- Every nonterminal has to be on the left side at least once
    | otherwise = [left] ++ all_nonterminals -- Only non-duplicate nonterminals are added
    where
        all_nonterminals = updateNonterminalsFromRules nonterminals rules


-- Simplifies all grammar rules by simlifing one rule at a time
simplifyRules :: [Nonterminal] -> [Rule] -> [Rule]
simplifyRules all_nonterminals [] = []
simplifyRules all_nonterminals (rule:rules) =
    let
        new_rules = simplifyRule all_nonterminals rule
        new_nonterminals = updateNonterminalsFromRules all_nonterminals (new_rules ++ rules)
    in
        new_rules ++ simplifyRules new_nonterminals rules


-- Simplifies one rule by generating other rules if necessary as shown below:
-- if A->#   => [A->#]
-- if A->a   => [A->aA1, A1->#]
-- if A->aB  => [A->aB]
-- if A->a.. => [A->aA1, A1->..]
simplifyRule :: [Nonterminal] -> Rule -> [Rule]
simplifyRule all_nonterminals (Rule left (symbol:[]))
    | symbol == "#" = [Rule left [symbol]]                                            -- A->#
    | otherwise =                                                                     -- A->a
        let
            new_left = generateNonterminal all_nonterminals left
        in
            [(Rule left [symbol, new_left]), (Rule new_left ["#"])]
simplifyRule all_nonterminals (Rule left (first_symbol:second_symbol:[]))
    | elem second_symbol all_nonterminals = [Rule left [first_symbol, second_symbol]] -- A->aB
simplifyRule all_nonterminals (Rule left (symbol:symbols)) =                          -- A->a..
    let
        new_left = generateNonterminal all_nonterminals left
    in
        [Rule left [symbol, new_left]]
        ++ simplifyRule (all_nonterminals ++ [new_left]) (Rule new_left symbols)


-- Generates new non-duplicate nonterminal by appending number to it
generateNonterminal :: [Nonterminal] -> Nonterminal -> Nonterminal
generateNonterminal all_nonterminals nonterminal = generator 2
    where
        generator index
            | elem new_nonterminal all_nonterminals = generator (index + 1)
            | otherwise = new_nonterminal
            where
                new_nonterminal = [head nonterminal] ++ show index


-- Convert grammar functions ----------------------------------------------

-- Convert simplified grammar into finite automaton
convertGrammar :: Grammar -> FiniteAutomaton
convertGrammar (Grammar nonterminals terminals rules initial_nonterminal) =
    (FiniteAutomaton states alphabet transitions initial_state end_states)
    where
        states = convertNonterminals nonterminals
        alphabet = convertTerminals terminals
        nonterminals_states_map = zip nonterminals states
        transitions = convertRules nonterminals_states_map rules
        initial_state = convertInitialNonterminal nonterminals_states_map initial_nonterminal
        end_states = convertEpsilonRules nonterminals_states_map rules


-- Convert nonterminals from grammar into states from finite automaton
convertNonterminals :: [Nonterminal] -> [State]
convertNonterminals all_nonterminals = generate all_nonterminals 1
    where -- States are generated as [1..(lenght all_nonterminals)]
        generate [] _ = []
        generate (_:nonterminals) index = [index] ++
            generate nonterminals (index + 1)


-- Convert nonterminals from grammar into alphabet from finite automaton
convertTerminals :: [Terminal] -> [Symbol]
convertTerminals all_terminals = all_terminals


-- Convert rules from grammar into transitiona from finite automaton
convertRules :: [(Nonterminal, State)] -> [Rule] -> [Transition]
convertRules _ [] = []
convertRules nonterminals_states_map ((Rule left right):rules)
    | last right == "#" = convertRules nonterminals_states_map rules -- Skip epsilon rules
    | otherwise = [(Transition origin symbol destination)] ++
        convertRules nonterminals_states_map rules
    where -- Simplified rule will allways be in form A->aB
        symbol = head right
        origin = getStateFromNonterminal nonterminals_states_map left
        destination = getStateFromNonterminal nonterminals_states_map (last right)


-- Convert initial nonterminal from grammar into initial state from finite automaton
convertInitialNonterminal :: [(Nonterminal, State)] -> Nonterminal -> State
convertInitialNonterminal nonterminals_states_map initial_nonterminal =
    getStateFromNonterminal nonterminals_states_map initial_nonterminal


-- Convert epsilon rules from grammar into list of end states from finite automaton
convertEpsilonRules :: [(Nonterminal, State)] -> [Rule] -> [State]
convertEpsilonRules _ [] = []
convertEpsilonRules nonterminals_states_map ((Rule left right):rules)
    | last right == "#" = [getStateFromNonterminal nonterminals_states_map left] ++
        convertEpsilonRules nonterminals_states_map rules
    | otherwise = convertEpsilonRules nonterminals_states_map rules


-- Gets state identifier coresponding to the privided nonterminal
getStateFromNonterminal :: [(Nonterminal, State)] -> Nonterminal -> State
getStateFromNonterminal [] _ = error "Non-terminal not in list of all non-terminals!"
getStateFromNonterminal ((nonterminal, state):nonterminals_states_map) wanted_nonterminal
    | wanted_nonterminal == nonterminal = state
    | otherwise = getStateFromNonterminal nonterminals_states_map wanted_nonterminal


-- Print functions --------------------------------------------------------

-- Print grammar in correct format
printGrammar :: Grammar -> IO ()
printGrammar (Grammar nonterminals terminals rules initial_nonterminal) =
    do
        putStrLn (intercalate "," nonterminals)
        putStrLn (intercalate "," terminals)
        putStrLn (initial_nonterminal)
        putStrLn (unlines (map show rules))


-- Print finite automaton in correct format
printFiniteAutomaton :: FiniteAutomaton -> IO ()
printFiniteAutomaton (FiniteAutomaton states alphabet transitions initial_state end_states) =
    do
        putStrLn (intercalate "," (map show states))
        putStrLn (show initial_state)
        putStrLn (intercalate "," (map show end_states))
        putStrLn (unlines (map show transitions))


---------------------------------------------------------------------------
