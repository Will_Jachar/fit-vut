---
title: FLP - Logický projekt
author: Peter Uhrín (xuhrin02)
date: 26.4.2020
---



[toc]

# Rubikova kostka

### Implementace

#### Datové struktury

V projektu jsem využil několika datových struktur, hlavně pro zlepšení čitelnosti programu, ale v některých případech také na zjednodušení práce s objekty a závěrečného výpisu.

Datová struktura `cube` je složená z šesti struktur `side` a reprezentuje hlavolam v nějakém legálním stavu. Struktura `side` následně obsahuje devět čísel, kde každé reprezentuje barvu jedné z devíti ploch na straně hlavolamu.

#### Funkce `main`

Funkce `main` řídí chod celého programu. Její průběh je přímočarý. Nejprve pomocí funkce `load_cube` načte hlavolam ze standardního vstupu do struktury `cube`. Následně na standardní výstup vypíše reprezentaci původního stavu hlavolamu funkcí `write_cube`, načež zahájí hledání řešení pomocí funkce `solve_cube`. Po nalezení řešení je toto řešení vypsáno na standardní výstup funkcí `write_solution` a program je ukončen.

#### Zpracování vstupu

O načtení dat ze standardního vstupu se starají funkce `read_line`, `isEOFEOL`, `read_lines`, `split_line` a `split_lines` převzaté ze souboru _input2.pl_, který byl poskytnut v souborech k předmětu FLP.

Po načtení dat do vícerozměrného pole jsou data transformována do objektu typu `cube` ve funkci `parse_cube`.

#### Reprezentace akcí nad hlavolamem

Pro to, aby mohl Prolog najít správné řešení Rubikovi kostky je potřeba nejen reprezentovat hlavolam nějakým stavem, ale také definovat množinu operací, pomocí kterých lze dosáhnout stavu dalšího. Tato množina operací je definována funkcí `rotate`.

Funkce `rotate` definuje, že existují operace `rotate_side` a každá taková operace je také inverzní, tedy pokud otočím pravou stranou po směru hodinových ručiček, mohu otočit pravou stranou také proti směru hodinových ručiček a dostanu se do původního stavu. Díky této funkci není potřeba definovat zvlášť operace pro otáčení o a proti směru hodinových ručiček.

Funkce `rotate_side` transformuje stav hlavolamu způsobem simulujícím otočení požadované strany hlavolamu.

#### Hledání řešení

Hledání řešení Rubikovy kostky je implementováno ve funkci `solve_cube`. Zde je naplno využita síla programovacího jazyka Prolog, protože celé hledání řešení se děje rekurzivně, přičemž Prolog sám zajistí prohledávání stavového prostoru metodou backtracking.

#### Výpis výsledků

Výpis objektu typu `cube` zajišťuje funkce `write_cube`. Výpis řešení hlavolamu ve funkci `write_solution` pouze opakovaně volá funkci `write_cube`.

### Ukázkové vstupy

V odevzdaném archivu se nachází několik ukázkových vstupů, přičemž nad každým vstupem jsem měřil čas výpočtu.

- Jeden krok do vyřešení (_input_1.txt_):	0.02s 
- Tři kroky do vyřešení (_input_3.txt_):	0.03s
- Pět kroků do vyřešení (_input_5.txt_):	0.04s
- Sedm kroků do vyřešení (_input_7.txt_):	20.02s

Jak je vidět z výsledků, s rostoucím počtem kroků potřebných na vyřešení hlavolamu roste čas výpočtu velmi výrazně.

