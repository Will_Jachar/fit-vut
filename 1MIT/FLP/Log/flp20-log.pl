% FLP 2020
% Zadani: Rubikova kostka
% Autor:  Peter Uhrin
% Login: xuhrin02


% Main ---------------------------------------------------------------------------------

start :-
    load_cube(Cube),
    write_cube(Cube),
    solve_cube(Cube, Solution),
    write_solution(Solution),
    halt.


% Nacteni vstupu -----------------------------------------------------------------------

% Nacte data ze vstupu a transformuje do reprezentace pouzivane v programu
load_cube(Cube) :-
    prompt(_, ''),
    read_lines(RawInput),
    split_lines(RawInput, ParsedInput),
    parse_cube(ParsedInput, Cube).


% Data nactena ze vstupu transformuje z vicerozmerneho pole do objektu Cube
parse_cube(
    [
        [[U1, U2, U3]],
        [[U4, U5, U6]],
        [[U7, U8, U9]],
        [[F1, F2, F3], [R1, R2, R3], [B1, B2, B3], [L1, L2, L3]],
        [[F4, F5, F6], [R4, R5, R6], [B4, B5, B6], [L4, L5, L6]],
        [[F7, F8, F9], [R7, R8, R9], [B7, B8, B9], [L7, L8, L9]],
        [[D1, D2, D3]],
        [[D4, D5, D6]],
        [[D7, D8, D9]]
    ],
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    )
).


% Funkce z input2.pl -------------------------------------------------------------------

read_line(L, C) :-
    get_char(C),
    (isEOFEOL(C), L = [], !;
        read_line(LL,_),
        [C | LL] = L).


isEOFEOL(C) :-
    C == end_of_file;
    (char_code(C, Code), Code == 10).


read_lines(Ls) :-
    read_line(L, C),
    (C == end_of_file, Ls = []; read_lines(LLs), Ls = [L|LLs]).


split_line([], [[]]) :- !.
split_line([' ' | T], [[] | S1]) :- !, split_line(T, S1).
split_line([32|T], [[]|S1]) :- !, split_line(T, S1).
split_line([H | T], [[H | G] | S1]) :- split_line(T, [G | S1]).


split_lines([], []).
split_lines([L | Ls], [H | T]) :- split_lines(Ls, T), split_line(L, H).


% Algoritmus pro nalezeni reseni -------------------------------------------------------

% Hlavolam je vyreseny prave kdyz je na kazde strane kosty pouze jedna barva
solved(
    cube(
        side(F, F, F,   F, F, F,   F, F, F),
        side(R, R, R,   R, R, R,   R, R, R),
        side(B, B, B,   B, B, B,   B, B, B),
        side(L, L, L,   L, L, L,   L, L, L),
        side(U, U, U,   U, U, U,   U, U, U),
        side(D, D, D,   D, D, D,   D, D, D)
    )
).

% Rekurzivni hledani reseni hlavolamu
solve_cube(Cube, []) :- solved(Cube).
solve_cube(Cube, [NewState | PrevStates]) :-
    solve_cube(NewState, PrevStates),
    rotate(_, Cube, NewState).


% Operace nad kostkou ------------------------------------------------------------------

% Kazda rotace můze byt provedena po smeru, nebo proti smeru hodinovych rucicek
rotate(+Side, OldState, NewState) :-
    rotate_side(Side, OldState, NewState).
rotate(-Side, OldState, NewState) :-
    rotate_side(Side, NewState, OldState).


% Rotace predni strany po smeru hodinovych rucicek
rotate_side(
    front,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(F7, F4, F1,   F8, F5, F2,   F9, F6, F3),
        side(U7, R2, R3,   U8, R5, R6,   U9, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, D1,   L4, L5, D2,   L7, L8, D3),
        side(U1, U2, U3,   U4, U5, U6,   L9, L6, L3),
        side(R7, R4, R1,   D4, D5, D6,   D7, D8, D9)
    )
).

% Rotace prave strany po smeru hodinovych rucicek
rotate_side(
    right,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(F1, F2, D3,   F4, F5, D6,   F7, F8, D9),
        side(R7, R4, R1,   R8, R5, R2,   R9, R6, R3),
        side(U9, B2, B3,   U6, B5, B6,   U3, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, F3,   U4, U5, F6,   U7, U8, F9),
        side(D1, D2, B7,   D4, D5, B4,   D7, D8, B1)
    )
).

% Rotace zadni strany po smeru hodinovych rucicek
rotate_side(
    back,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, D9,   R4, R5, D8,   R7, R8, D7),
        side(B7, B4, B1,   B8, B5, B2,   B9, B6, B3),
        side(U3, L2, L3,   U2, L5, L6,   U1, L8, L9),
        side(R3, R6, R9,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   L1, L4, L7)
    )
).

% Rotace leve strany po smeru hodinovych rucicek
rotate_side(
    left,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(U1, F2, F3,   U4, F5, F6,   U7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, D7,   B4, B5, D4,   B7, B8, D1),
        side(L7, L4, L1,   L8, L5, L2,   L9, L6, L3),
        side(B9, U2, U3,   B6, U5, U6,   B3, U8, U9),
        side(F1, D2, D3,   F4, D5, D6,   F7, D8, D9)
    )
).

% Rotace horni strany po smeru hodinovych rucicek
rotate_side(
    up,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(R1, R2, R3,   F4, F5, F6,   F7, F8, F9),
        side(B1, B2, B3,   R4, R5, R6,   R7, R8, R9),
        side(L1, L2, L3,   B4, B5, B6,   B7, B8, B9),
        side(F1, F2, F3,   L4, L5, L6,   L7, L8, L9),
        side(U7, U4, U1,   U8, U5, U2,   U9, U6, U3),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    )
).

% Rotace spodni strany po smeru hodinovych rucicek
rotate_side(
    down,
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    ),
    cube(
        side(F1, F2, F3,   F4, F5, F6,   L7, L8, L9),
        side(R1, R2, R3,   R4, R5, R6,   F7, F8, F9),
        side(B1, B2, B3,   B4, B5, B6,   R7, R8, R9),
        side(L1, L2, L3,   L4, L5, L6,   B7, B8, B9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D7, D4, D1,   D8, D5, D2,   D9, D6, D3)
    )
).


% Vypsani vystupu ----------------------------------------------------------------------

% Vypsany kazde kosty z reseni
write_solution([]).
write_solution([Cube | Cubes]) :-
    nl,
    write_cube(Cube),
    write_solution(Cubes).


% Vypsani kostky na standardni vystup
write_cube(
    cube(
        side(F1, F2, F3,   F4, F5, F6,   F7, F8, F9),
        side(R1, R2, R3,   R4, R5, R6,   R7, R8, R9),
        side(B1, B2, B3,   B4, B5, B6,   B7, B8, B9),
        side(L1, L2, L3,   L4, L5, L6,   L7, L8, L9),
        side(U1, U2, U3,   U4, U5, U6,   U7, U8, U9),
        side(D1, D2, D3,   D4, D5, D6,   D7, D8, D9)
    )
) :-
    writef('%w%w%w\n', [U1, U2, U3]),
    writef('%w%w%w\n', [U4, U5, U6]),
    writef('%w%w%w\n', [U7, U8, U9]),
    writef(
        '%w%w%w %w%w%w %w%w%w %w%w%w\n',
        [F1, F2, F3, R1, R2, R3, B1, B2, B3, L1, L2, L3]
    ),
    writef(
        '%w%w%w %w%w%w %w%w%w %w%w%w\n',
        [F4, F5, F6, R4, R5, R6, B4, B5, B6, L4, L5, L6]
    ),
    writef(
        '%w%w%w %w%w%w %w%w%w %w%w%w\n',
        [F7, F8, F9, R7, R8, R9, B7, B8, B9, L7, L8, L9]
    ),
    writef('%w%w%w\n', [D1, D2, D3]),
    writef('%w%w%w\n', [D4, D5, D6]),
    writef('%w%w%w\n', [D7, D8, D9]).
