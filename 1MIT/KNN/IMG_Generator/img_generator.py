"""Script for generating traning data for convolutional neural network"""

import os

import click
import cv2
import numpy as np


# Image dirs
BG_IMG_DIR = "bg_imgs"
LEAF_IMG_DIR = "leaf_imgs"
RES_IMG_DIR = "res_imgs"

# Number of background images and images of leaves
NO_BG_IMGS = 15
NO_FG_IMGS = 387

# Static size of background image and leaf image
BG_WIDTH, BG_HEIGHT = 1024, 768
FG_WIDTH, FG_HEGHT = 255, 255

# Size of minimum distance between leaves
MIN_LEAF_BOUNDARY = 255

# -------------------------------------------------------------------------


@click.command()
@click.option(
    "--bg_dir",
    type=str,
    default=BG_IMG_DIR,
    help="Path to folder with background images.",
)
@click.option(
    "--leaf_dir",
    type=str,
    default=LEAF_IMG_DIR,
    help="Path to folder with images of leaves.",
)
@click.option(
    "--res_dir",
    type=str,
    default=RES_IMG_DIR,
    help="Path to folder where to save generated images.",
)
@click.option(
    "--count_per_bg",
    type=int,
    required=True,
    default=1,
    help="How many images to generate for each background image.",
)
def main(bg_dir, leaf_dir, res_dir, count_per_bg):
    bg_filenames, leaf_filenames = load_img_filenames(
        bg_img_dir=bg_dir, leaf_img_dir=leaf_dir
    )

    res_index = 1

    for bg_filename in bg_filenames:
        # Load background image
        bg_src_img = cv2.imread(os.path.join(bg_dir, bg_filename))

        for _ in range(count_per_bg):
            # Random number of leaves in image
            leaf_count = np.random.randint(1, 10)

            # Random leaf images
            leaf_imgs = [
                cv2.imread(os.path.join(leaf_dir, leaf_filename))
                for leaf_filename in np.random.choice(leaf_filenames, leaf_count)
            ]
            # Generate image
            out_img, out_img_seg = generate_img(
                bg_img=bg_src_img.copy(), leaf_imgs=leaf_imgs
            )

            cv2.imwrite(os.path.join(res_dir, f"leaf_{res_index}_img.png"), out_img)
            cv2.imwrite(os.path.join(res_dir, f"leaf_{res_index}_seg.png"), out_img_seg)

            res_index += 1


# -------------------------------------------------------------------------


def load_img_filenames(bg_img_dir, leaf_img_dir):
    _, _, filenames = next(os.walk(bg_img_dir))
    bg_img_files = [filename for filename in filenames if filename.endswith(".jpg")]

    _, _, filenames = next(os.walk(leaf_img_dir))
    fg_img_files = [filename for filename in filenames if filename.endswith(".jpg")]

    return bg_img_files, fg_img_files


def generate_img(bg_img, leaf_imgs):
    occupied_positions = set()

    height, width, _ = bg_img.shape
    bg_img_seg = np.zeros((height, width), dtype="uint8")

    # Inprint every leaf into final image
    for seg_index, leaf_img in enumerate(leaf_imgs, start=1):
        angle = np.random.randint(0, 360)
        scale = np.random.random() + 0.5

        # Rotate and scale image at random
        leaf_img = rotate_img(img=leaf_img, angle=angle)
        leaf_img = scale_img(img=leaf_img, scale=scale)

        # Create mask for bitwise operations
        mask, mask_inv = create_mask(img=leaf_img)

        # Calculate possible position for leaf
        position_boundary = random_position_boundary(
            fg_img=leaf_img, bg_img=bg_img, occupied_positions=occupied_positions
        )

        # Inprint leaf into image
        out_img = inprint(
            bg_img=bg_img,
            fg_img=leaf_img,
            position=position_boundary,
            mask=mask,
            mask_inv=mask_inv,
        )

        # Convert leaf image to segment
        leaf_img_seg = cv2.cvtColor(leaf_img, cv2.COLOR_BGR2GRAY)
        leaf_img_seg[leaf_img_seg > 0] = seg_index * 25

        # Inprint leaf into segmented image
        out_img_seg = inprint(
            bg_img=bg_img_seg,
            fg_img=leaf_img_seg,
            position=position_boundary,
            mask=mask,
            mask_inv=mask_inv,
        )

    return out_img, out_img_seg


def random_position_boundary(fg_img, bg_img, occupied_positions):
    fg_img_height, fg_img_width, _ = fg_img.shape
    bg_img_height, bg_img_width, _ = bg_img.shape

    # Calculate max x, y coordinates for leaf position
    max_x = bg_img_width - fg_img_width
    max_y = bg_img_height - fg_img_height

    while True:
        x1, y1 = (np.random.randint(0, max_x + 1), np.random.randint(0, max_y + 1))

        grid_position = (x1 % MIN_LEAF_BOUNDARY, y1 % MIN_LEAF_BOUNDARY)
        if grid_position not in occupied_positions:
            occupied_positions.add(grid_position)
            break

    x2, y2 = (x1 + fg_img_width, y1 + fg_img_height)

    return (x1, y1, x2, y2)


def rotate_img(img, angle):
    height, width, _ = img.shape
    center = (width / 2, height / 2)

    rotation_matrix = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1)

    rotated_img = cv2.warpAffine(img, rotation_matrix, (width, height))

    return rotated_img


def scale_img(img, scale):
    height, width, _ = img.shape

    return cv2.resize(img, (int(width * scale), int(height * scale)))


def create_mask(img):
    bw_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Now create a mask of leaf and create its inverse mask also
    _, mask = cv2.threshold(bw_img, 25, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)

    return mask, mask_inv


def inprint(bg_img, fg_img, position, mask, mask_inv):
    # Take reagon of interest from background image
    x1, y1, x2, y2 = position
    roi = bg_img[y1:y2, x1:x2]

    # Now black-out the area of reagon of interest
    bg_img_masked = cv2.bitwise_and(roi, roi, mask=mask_inv)

    # Mask foreground image
    fg_img_masked = cv2.bitwise_and(fg_img, fg_img, mask=mask)

    # Inprint foreground image into reagon of interest of backgroud image
    inprint_img = cv2.add(bg_img_masked, fg_img_masked)
    bg_img[y1:y2, x1:x2] = inprint_img

    return bg_img


# -------------------------------------------------------------------------

if __name__ == "__main__":
    main()
