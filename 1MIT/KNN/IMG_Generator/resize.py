import cv2
import numpy as np


for bg_index in range(15):
    filename = f"./bg_imgs/bg_{bg_index + 1}.jpg"
    
    srcVYF_img = cv2.imread(f"./bg_imgs/bg_{bg_index + 1}.jpg")
    
    out_img = cv2.resize(src_img, (1024, 768))
    out_img = cv2.GaussianBlur(out_img, (7, 7), cv2.BORDER_DEFAULT)
    
#    cv2.imshow("res", out_img)
#    cv2.waitKey(0)
#    cv2.destroyAllWindows()
    
    cv2.imwrite(f"./bg_imgs/bg_{bg_index + 1}.jpg", out_img)