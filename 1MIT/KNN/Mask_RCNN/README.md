# Training script for Mask R-CNN for leaf detection and segmentation

## Implementation
- situated in leaf/leaf.py

### Run script
```
leaf.py [-h] [--dataset /path/to/leaf/dataset/] --weights
            /path/to/weights.h5 [--logs /path/to/logs/]
            [--image path or URL to image]
            <command>
```
meaning:
```
positional arguments:
  <command>             'train' or 'detect'

optional arguments:
  -h, --help            show this help message and exit
  --dataset /path/to/leaf/dataset/
                        Directory of the Leaf dataset
  --weights /path/to/weights.h5
                        Path to weights .h5 file or 'coco'
  --logs /path/to/logs/
                        Logs and checkpoints directory (default=logs/)
  --image path or URL to image
                        Image for detection
```
## Requirements
- for training unzip dataset (https://www.egr.msu.edu/denseleaves/Data/DenseLeaves.zip) to folder ```images``` (should lokk like:  ```images/train```)

- download trained weights for coco dataset (run code with --weights=coco) 

## Documents

repo: https://github.com/matterport/Mask_RCNN
paper for repo: https://engineering.matterport.com/splash-of-color-instance-segmentation-with-mask-r-cnn-and-tensorflow-7c761e238b46
