import os
import sys
import json
import datetime
import numpy as np
import cv2

# Root directory of the project
ROOT_DIR = os.path.abspath("../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

############################################################
#  Configurations
############################################################


class LeafConfig(Config):
    """Configuration for training on the toy  dataset.
    Derives from the base Config class and overrides some values.
    """
    # Give the configuration a recognizable name
    NAME = "leaf"

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + leaf

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 33

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.9


############################################################
#  Dataset
############################################################

class LeafDataset(utils.Dataset):

    def load_leafs(self, dataset_dir, subset):
        #---add name of class
        self.add_class("leaf", 1, "leaf")

        #---select subset (train / val /test)
        assert subset in ["train", "val", "test"]
        dataset_dir = os.path.join(dataset_dir, subset)

        #---load images from folder
        # get img directory from image path
        for (_, _, filenames) in os.walk(dataset_dir):
            for filename in filenames:
                if filename.endswith('img.png'): 
                    image_id = filename.replace('img.png','')
                    self.add_image(
                        "leaf",
                        image_id=image_id,
                        path=os.path.join(dataset_dir,filename)
                    )
                              
    def load_mask(self, image_id):
        #get mask path
        info = self.image_info[image_id]
        mask_img = info['path'].replace('img.png','seg.png')
        #mask to gray
        #print(mask_img)
        img = cv2.imread(mask_img)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        height, width = gray.shape
        #init mask startingpoint
        mask = []
        colors = np.unique(gray)
        colors = np.delete(colors, np.where(colors==0))
        for instance in colors:
            #print(instance)
            m = np.where(gray == instance,True,False)
            mask.append(m)
        mask = np.stack(mask, axis=-1)
        return mask, np.ones([mask.shape[-1]], dtype=np.int32)  

    def image_reference(self, image_id):
        #Return the path of the image.
        info = self.image_info[image_id]
        if info["source"] == "leaf":
            return info["id"]
        else:
            super(self.__class__, self).image_reference(image_id)

############################################################
#  Training
############################################################
def train(model, dataset_dir):
    #prepare training dataset
    dataset_train =  LeafDataset()
    dataset_train.load_leafs(dataset_dir, "train")
    dataset_train.prepare()

    #prepare validation dataset
    dataset_val =  LeafDataset()
    dataset_val.load_leafs(dataset_dir, "val")
    dataset_val.prepare()

    # *** This training schedule is an example. Update to your needs ***
    # Since we're using a very small dataset, and starting from
    # COCO trained weights, we don't need to train too long. Also,
    # no need to train all layers, just the heads should do it.
    print("Training network heads")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=30,
                layers='heads')

############################################################
#  Detect
############################################################
def detect(model, image_path=None):
    # Run model detection and generate the color splash effect
    print("Running on {}".format(image_path))
    # Read image
    image = cv2.imread(image_path)
    # Detect objects
    return model.detect([image], verbose=1)[0]

############################################################
#  Evaluate
############################################################
def evaluate(model, dataset_folder):
    
    from mrcnn.utils import compute_ap_range, extract_bboxes, compute_recall, compute_matches
    
    # Initializing dataset.
    dataset_test= LeafDataset()
    dataset_test.load_leafs(dataset_folder, "test")
    dataset_test.prepare()

    for id in dataset_test.image_ids[0:1]:
        
        # Loading image with annotation.
        image = dataset_test.load_image(id)
        gt_masks, gt_class_ids = dataset_test.load_mask(id)
        gt_boxes = extract_bboxes(gt_masks)

        # Predicting leaves.
        res = model.detect([image],verbose=1)[0]
        pred_boxes = res["rois"]
        pred_class_ids = res["class_ids"]
        pred_scores = res["scores"]
        pred_masks = res["masks"]

        # Calculating evaluation.
        step = 0.05
        for iou in np.arange(0.0, 1.0 + step, step):
            gt_match, pred_match, _ = compute_matches(  gt_boxes, gt_class_ids, gt_masks,
                                                        pred_boxes, pred_class_ids, pred_scores, pred_masks,
                                                        iou_threshold=iou,score_threshold=0.0)
                                                        
            tp = np.sum(np.where(pred_match != -1, 1 ,0),keepdims=False)
            fp = np.sum(np.where(pred_match == -1, 1 ,0),keepdims=False)
            fn = np.sum(np.where(gt_match == -1, 1 ,0),keepdims=False)

            p = tp/(tp+fp)
            r = tp/(tp+fn)

            print("Precision : ",round(p,2), " Recall : ", round(r,2), " IoU : ",round(iou,2))
    
    # Computing final precision-recall curve.
    # precisions = np.mean(np.stack(precisions,axis=0),axis=0)
    # print(precisions)

def show_detection(img_path, mask):
    image = cv2.imread(img_path)
    print ( mask.shape[-1])
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        result = np.where(mask, image, (0,0,0)).astype(np.uint8)
    else:
        result = image
    
    cv2.imshow("Detection result", result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect leafs.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train', 'detect' or 'eval'")
    parser.add_argument('--dataset', required=False,
                        metavar="/path/to/leaf/dataset/",
                        help='Directory of the Leaf dataset')
    parser.add_argument('--weights', required=True,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5 file or 'coco'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image for detection')

    args = parser.parse_args()

    # Validate arguments
    if args.command == "train":
        assert args.dataset, "Argument --dataset is required for training"
    elif args.command == "detect":
        assert args.image, "Provide --image for detection"

    print("Weights: ", args.weights)
    print("Dataset: ", args.dataset)
    print("Logs: ", args.logs)

    # Configurations
    if args.command == "train":
        config = LeafConfig()
    else:
        class InferenceConfig(LeafConfig):
            # Set batch size to 1 since we'll be running inference on
            # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
            GPU_COUNT = 1
            IMAGES_PER_GPU = 1
        config = InferenceConfig()
    config.display()

    # Create model
    if args.command == "train":
        model = modellib.MaskRCNN(mode="training", config=config,
                                  model_dir=args.logs)
    else:
        model = modellib.MaskRCNN(mode="inference", config=config,
                                  model_dir=args.logs)
    

    # Select weights file to load
    if args.weights.lower() == "coco":
        weights_path = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
        # Download weights file
        if not os.path.exists(weights_path):
            utils.download_trained_weights(weights_path)
    else:
        weights_path = args.weights

    # Load weights
    print("Loading weights ", weights_path)
    if args.weights.lower() == "coco":
        # Exclude the last layers because they require a matching
        # number of classes
        model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])
    else:
        model.load_weights(weights_path, by_name=True)

    # Train or detect
    if args.command == "train":
        train(model, args.dataset)
    
    elif args.command == "detect":
        res = detect(model, image_path=args.image)
        show_detection(args.image, res['masks'])
    
    elif args.command == "eval":
        evaluate(model, args.dataset)

    else:
        print("'{}' is not recognized. "
              "Use 'train' or 'detect'".format(args.command))


