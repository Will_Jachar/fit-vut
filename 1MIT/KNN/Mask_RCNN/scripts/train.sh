#!/bin/sh

#go to source
cd /storage/brno2/home/xbazou00/LeafInstanceSegmentation/Mask_RCNN/leaf/

#import python and enable custom packages
module add python36-modules-gcc
export PYTHONUSERBASE=/storage/praha1/home/${USER}/.local
export PYTHONPATH=$PYTHONUSERBASE/lib/python3.6/site-packages:$PYTHONPATH/
export PATH=$PYTHONUSERBASE/bin:$PATH

#install packeges
pip3 install tensorflow==1.9.0 --user
pip3 install numpy --user
pip3 install opencv-python --user
pip3 install scikit-image --user
pip3 install Keras==2.2.4 --user

#run training
python3 leaf.py --weights=../leaf/mask_rcnn_leaf.h5 --dataset=../images train
