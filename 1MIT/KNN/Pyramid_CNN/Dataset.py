import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from tensorflow import keras

# random gen setup
import random
seed = 2020
random.seed = seed

class Dataset(keras.utils.Sequence):
    
    def __init__ (self, dataset_path, subfolder, image_width, image_height, batch_size=8):
        self.image_width = image_width
        self.image_height = image_height
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        #load training images paths
        self.images_paths = self.load_images_paths(subfolder)
        #loas validation data
        #self.validation_images, self.validation_masks = self.load_images_and_masks('val')

    def __getitem__(self, index):
        batch_size = self.batch_size
        if (self.batch_size+1)*index > len(self.images_paths):
            batch_size = len(self.images_paths) - index*self.batch_size

        images_paths = self.images_paths[index*self.batch_size : (index+1)*self.batch_size]
        #load images and masks
        images = []
        masks = []
        for image_path in images_paths:
            image = cv2.imread(image_path)
            resized_image = cv2.resize(image, (self.image_width, self.image_height))
            images.append(resized_image)
            mask = self.load_mask(image_path)
            masks.append(mask)

        return np.array(images), np.array(masks)

    def __len__(self):
        return int(np.ceil(len(self.images_paths)/float(self.batch_size)))     

    def load_images_paths(self, subfolder):
        # select subset (train / val /test)
        dataset_dir = os.path.join(self.dataset_path, subfolder)

        # image paths
        image_paths = []
        for (_, _, filenames) in os.walk(dataset_dir):
            for filename in filenames:
                if filename.endswith('img.png'): 
                    # load image and resize it to self.image_width..
                    image_paths.append(os.path.join(dataset_dir,filename))
        #shuffle paths
        #random.shuffle(image_paths)
        return image_paths

    def load_mask(self, image_path):
        mask_path = image_path.replace('img.png','seg.png')
        return self.get_edges_from_mask(mask_path) 

    # Get from color mask just edges
    def get_edges_from_mask(self, mask_path):
        #load mask
        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
        mask = cv2.resize(mask, (self.image_width, self.image_height))
        
        #sobel
        sobelx = cv2.Sobel(mask,cv2.CV_64F,1,0,ksize=3)
        sobely = cv2.Sobel(mask,cv2.CV_64F,0,1,ksize=3)
        sobel = np.sqrt(sobelx**2+sobely**2)
        #trashold
        trashold = np.where(np.abs(sobel) > 0,1,0).astype(np.uint8)
        trashold = trashold.reshape(trashold.shape +(1,))
        return trashold

    def show_random_mask_with_picture(self):
        #random index
        random_index = random.randint(0, len(self.images_paths)-1)

        image = cv2.imread(self.images_paths[random_index])
        image = cv2.resize(image, (self.image_width, self.image_height))
        mask = self.load_mask(self.images_paths[random_index]).reshape(self.image_width, self.image_height)     
        
        #show the result
        plt.subplot(1,2,1),plt.imshow(image)
        plt.title('Image'), plt.xticks([]), plt.yticks([])

        plt.subplot(1,2,2),plt.imshow(mask, cmap = 'gray')
        plt.title('Mask'), plt.xticks([]), plt.yticks([])
        plt.show()
