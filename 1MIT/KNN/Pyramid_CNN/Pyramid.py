from tensorflow import keras
import numpy as np
import cv2
from Dataset import Dataset

class Pyramid:
    def __init__ (self, dataset_path=None, image_width=224, image_height=224, epochs_count=30, weights=None):
        self.dataset_path = dataset_path
        self.image_width = image_width
        self.image_height = image_height
        self.epochs_count = epochs_count
        self.weights = weights

    def build_cnn(self, half_pyramid_count = 5):
        
        # get input sizes of pyramid layers
        scale = lambda x: 2**(x+4)#int(np.log2(image_width / 4)))
        tmp = np.arange(0,half_pyramid_count)
        layers_output_sizes = scale(tmp)
        #input layer
        layers = []
        layers.append(keras.layers.Input((self.image_width,  self.image_height, 3)))

        # downsizing layers
        for x in range(0, half_pyramid_count*2-2, 2):
            layers.append(None)
            layers.append(None)
            layers[x+1], layers[x+2] = self.downsize_block(layers[x], layers_output_sizes[int(x/2)])
        
        #bottleneck
        conv1 = keras.layers.Conv2D(filters=layers_output_sizes[half_pyramid_count-1], kernel_size=(3,3), padding="same", activation="relu", strides=1)(layers[half_pyramid_count*2-2])
        layers.append(keras.layers.Conv2D(filters=layers_output_sizes[half_pyramid_count-1], kernel_size=(3,3), padding="same", activation="relu", strides=1)(conv1))
        
        # upsizing layers
        start = len(layers)-3
        for x in range(0, half_pyramid_count-1, 1):
            layers.append(self.upsize_block(layers[len(layers)-1], layers[start-x*2], layers_output_sizes[half_pyramid_count-2-x]))

        
        layers.append(keras.layers.Conv2D(1,(1,1), padding="same", activation="sigmoid")(layers[len(layers)-1]))
        self.model = keras.models.Model(layers[0], layers[len(layers)-1])
        self.model.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])
        self.model.summary()
        #load weights
        if self.weights != None: self.model.load_weights(self.weights)
    
    def downsize_block(self, input_layer, output_dimensions, kernel_size = (3,3), padding = "same", activation="relu", strides = 1):
        conv1 = keras.layers.Conv2D(filters=output_dimensions, kernel_size=kernel_size, padding=padding, activation=activation, strides=strides)(input_layer)
        conv2 = keras.layers.Conv2D(filters=output_dimensions, kernel_size=kernel_size, padding=padding, activation=activation, strides=strides)(conv1)
        pool = keras.layers.MaxPool2D()(conv2)
        return conv2, pool

    def upsize_block(self, input_layer, skip_layer, output_dimensions, kernel_size = (3,3), padding = "same", activation="relu", strides = 1):
        up_sampl = keras.layers.UpSampling2D()(input_layer)
        concat = keras.layers.Concatenate()([up_sampl, skip_layer])
        conv1 = keras.layers.Conv2D(filters=output_dimensions, kernel_size=kernel_size, padding=padding, activation=activation, strides=strides)(concat)
        conv2 = keras.layers.Conv2D(filters=output_dimensions, kernel_size=kernel_size, padding=padding, activation=activation, strides=strides)(conv1)
        return conv2


    def train(self, batch_size=8):
        training_data = Dataset(self.dataset_path, 'train', self.image_width, self.image_height)

        #training_data.show_random_mask_with_picture()
        #exit()

        validation_data = Dataset(self.dataset_path, 'val', self.image_width, self.image_height)
        training_steps = len(training_data.images_paths)//batch_size
        validation_steps = len(validation_data.images_paths)//batch_size

        # Training checkpoint to save best model.
        filepath = "weights/pyramid_weights-{epoch:02d}-{val_accuracy:.3f}.hdf5"
        checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=False, mode='max')
        callbacks_list = [checkpoint]
        
        self.model.fit_generator(training_data,
                                validation_data=validation_data,
                                steps_per_epoch=training_steps,
                                validation_steps=validation_steps,
                                epochs=self.epochs_count,
                                callbacks=callbacks_list)

        self.model.save_weights("pyramid_weights.h5")

    def detect(self, image_path):
        # open and resize the image
        image = cv2.imread(image_path)
        resized_image = cv2.resize(image, (self.image_width, self.image_height))
        reshaped_image = resized_image.reshape((1,) + resized_image.shape)
        return self.model.predict(reshaped_image)

        