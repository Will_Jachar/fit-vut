# Pyramid
Suits for segmenting leaves in image

## Run training
    python3 leaf.py --dataset path/to/dataset/folder --weights path/to/weights train

When --weights are not specified, trainig begins with inicial weights from keras
weights of each epoch are saved to folder weights


## Run detection
    python3 leaf.py --weights path/to/weights --image path/to/image detect

### Network changes
 - For changing input image size / count of epochs / bach size just change parametres in `leaf.py` in main under comment UNet settings
 - For changing Unet deph just change `half_pyramid_count` at the same place, but then you have to train new weights

## Run training on metacentrum
For this case scripts/train.sh is prepaired. So go to scripts and run:

    qsub -q gpu -l select=1:ngpus=1:ncpus=6:mem=32gb -l walltime=24:00:00 train.sh

interactive:

    qsub -q gpu -l select=1:ngpus=1:ncpus=6:mem=32gb -l walltime=3:00:00 -I
