import argparse
import numpy as np
import cv2

from Dataset import Dataset
from Pyramid import Pyramid

def get_arguments():
    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Pyramid CNN to detect leafs.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'detect'")
    parser.add_argument('--dataset', required=False,
                        metavar="/path/to/leaf/dataset/",
                        help='Directory of the Leaf dataset')
    parser.add_argument('--weights', required=False,
                        metavar="/path/to/weights.h5",
                        help="Path to weights .h5")
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image for detection')

    args = parser.parse_args()
    #controll dependencies for detection
    if args.command == "detect" and (args.image == None or args.weights == None):
        print("\nFor detection you need to specify --weights and --image \n")
        parser.print_help()
        exit(42)
    
    if args.command == "train" and args.dataset == None:
        print("\nFor training you need to specify at least dataset\n")
        parser.print_help()
        exit(42)

    return args

def extract_masks(prediction):
    result = np.where(prediction > 0.5,0,1).astype(np.uint8)
    kernel = np.ones((3,3),np.uint8)
    result = cv2.morphologyEx(result,cv2.MORPH_OPEN,kernel, iterations = 2).astype(np.uint8)
    markers = (cv2.connectedComponents(result)[1] - 1).astype(np.uint8)
    markers = markers * result

    instances = np.unique(markers)
    mask = []
    for instance in instances:
        if instance == 0:
            continue
        m = np.where(markers == instance, True, False)
        mask.append(m)

    mask = np.stack(mask, axis=-1)
    return mask, np.ones([mask.shape[-1]], dtype=np.int32)

def show_detection(img_path, mask):
    image = cv2.imread(img_path)
    image = cv2.resize(image, mask.shape[0:2])
    print ( mask.shape[-1])
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        result = np.where(mask, image, (0,0,0)).astype(np.uint8)
    else:
        result = image
    
    cv2.imshow("Detection result", result)
    while cv2.waitKey(1) != ord('q'):
        pass
    cv2.destroyAllWindows()

if __name__ == '__main__':
    # parse aguments
    args = get_arguments()

    # UNet settings
    image_width = 224
    image_height = 224
    epochs_count = 100
    batch_size = 8
    half_pyramid_count = 5

    # Model training.
    if args.command == "train":
        cnn = Pyramid(args.dataset, image_width, image_height, epochs_count, args.weights)
        cnn.build_cnn(half_pyramid_count)
        cnn.train(batch_size)
    
    # Detection and visualisation.
    elif args.command == "detect": 
        cnn = Pyramid(args.dataset, image_width, image_height, weights=args.weights)
        cnn.build_cnn(half_pyramid_count)
        prediction = cnn.detect(args.image)[0]
        mask, ids = extract_masks(prediction)
        show_detection(args.image,mask)
    
