#!/bin/sh
module add tensorflow-2.0.0-gpu-python3

cd /storage/brno2/home/xbazou00/LeafInstanceSegmentation/Pyramid_CNN

python3 leaf.py --dataset ../Mask_RCNN/images/ --weights pyramid_weights.hdf5 train

exit