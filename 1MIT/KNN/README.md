[toc]

# Leaf Instance Segmentation

Pracovní dokument : https://docs.google.com/document/d/1qUu3UyazKGtnkVsuc0cUCoVkkidmq49iZ468DydZW_8/edit#

## img_generator.py

#### Install

- Create virtual enviroment: `python3 -m venv knn_venv`
- Activate venv: `source knn_venv/bin/activate`
- Install dependences: `pip install requirements.txt`

#### Usage

```
Usage: img_generator.py [OPTIONS]

Options:
  --bg_dir TEXT           Path to folder with background images.
  --leaf_dir TEXT         Path to folder with images of leaves.
  --res_dir TEXT          Path to folder where to save generated images.
  --count_per_bg INTEGER  How many images to generate for each background
                          image.  [required]

  --help                  Show this message and exit.
```
