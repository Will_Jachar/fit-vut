/*
 * author: Peter Uhrin (xuhrin02)
 *
 * Inspirovano algoritmem poskytnutym jako vzorovy priklad do predmetu PRL:
 * http://www.fit.vutbr.cz/~ikalmar/PRL/odd-even-trans/odd-even.cpp
 */

#include <fstream>
#include <iostream>
#include <mpi.h>

using namespace std;

#define TAG 0

void load_numbers(void);
void odd_even_sort(int num_procs, int my_id, int *my_number, int *neigh_number, MPI_Status *stat);

int main(int argc, char *argv[])
{
    int num_procs;    // pocet procesoru
    int my_id;        // muj rank
    int my_number;    // moje hodnota
    int neigh_number; // hodnota souseda
    MPI_Status stat;  // struct -> obsahuje kod, source, tag, error

    double start, end; // promenne pro mereni casu

    // MPI INIT ----------------------------------------------------------------
    MPI_Init(&argc, &argv);                    // inicializace MPI
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs); // zjistíme, kolik procesů běží­
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);     // zjistíme id svého procesu

    int *final = new int[num_procs];

    // NACTENI SOUBORU ---------------------------------------------------------
    if (my_id == 0)
    {
        load_numbers();
    }

    MPI_Recv(&my_number, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); // prijeti vlastniho cisla

    // ODD-EVEN SORT -----------------------------------------------------------
    start = MPI_Wtime();
    odd_even_sort(num_procs, my_id, &my_number, &neigh_number, &stat);
    end = MPI_Wtime();

    // FINALNI SEZBIRANI DAT Z CHILD PROCESU -----------------------------------
    if (my_id == 0)
    {
        final[0] = my_number;

        // prijem vysledku od jednotlivych procesu
        for (int i = 1; i < num_procs; i++)
        {
            MPI_Recv(&neigh_number, 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &stat);
            final[i] = neigh_number;
        }

        // vypis serazeneho pole
        for (int i = 0; i < num_procs; i++)
        {
            cout << final[i] << endl;
        }

        // cout << "Razeni trvalo: " << end - start << "s" << endl;
    }
    else
    {
        // finalni distribuce vysledku do master procesu
        for (int i = 1; i < num_procs; i++)
        {
            if (my_id == i)
            {
                MPI_Send(&my_number, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD);
            }
        }
    }

    MPI_Finalize();

    return 0;
}

void load_numbers()
{
    char input[] = "numbers"; // jmeno souboru

    ifstream f_in;
    f_in.open(input, ios::in | ios::binary);

    for (int i = 0, number = 0; number = f_in.get(), f_in.good(); i++)
    {
        MPI_Send(&number, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);

        cout << number << " "; // vypis neserazeneho pole na jeden radek
    }
    cout << endl;

    f_in.close();
}

void odd_even_sort(int num_procs, int my_id, int *my_number, int *neigh_number, MPI_Status *stat)
{
    // LIMIT PRO INDEXY --------------------------------------------------------
    int odd_limit = 2 * (num_procs / 2) - 1;    // limity pro sude
    int even_limit = 2 * ((num_procs - 1) / 2); // limity pro liche
    int half_cycles = num_procs / 2;

    // RAZENI ------------------------------------------------------------------
    for (int i = 1; i <= half_cycles; i++)
    {
        // sude procesy
        if ((!(my_id % 2) || my_id == 0) && (my_id < odd_limit))
        {
            MPI_Send(my_number, 1, MPI_INT, my_id + 1, TAG, MPI_COMM_WORLD);
            MPI_Recv(my_number, 1, MPI_INT, my_id + 1, TAG, MPI_COMM_WORLD, stat);
        }
        else if (my_id <= odd_limit)
        {
            MPI_Recv(neigh_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD, stat);

            if (*neigh_number > *my_number)
            {
                MPI_Send(my_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD);
                *my_number = *neigh_number;
            }
            else
            {
                MPI_Send(neigh_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD);
            }
        }

        // liche procesy
        if ((my_id % 2) && (my_id < even_limit))
        {
            MPI_Send(my_number, 1, MPI_INT, my_id + 1, TAG, MPI_COMM_WORLD);
            MPI_Recv(my_number, 1, MPI_INT, my_id + 1, TAG, MPI_COMM_WORLD, stat);
        }
        else if (my_id <= even_limit && my_id != 0)
        {
            MPI_Recv(neigh_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD, stat);

            if (*neigh_number > *my_number)
            {
                MPI_Send(my_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD);
                *my_number = *neigh_number;
            }
            else
            {
                MPI_Send(neigh_number, 1, MPI_INT, my_id - 1, TAG, MPI_COMM_WORLD);
            }
        }
    }
}
