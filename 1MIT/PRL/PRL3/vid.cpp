/*
 * author: Peter Uhrin (xuhrin02)
 */

#include <iostream>
#include <new>
#include <sstream>
#include <vector>
#include <cmath>
#include <mpi.h>

using namespace std;

#define TAG 0
#define MAX_INPUT_SIZE 32

#define HALF_PI M_PI / 2

int load_altitudes(string raw_altitudes, int* altitudes, int *ref_altitude);
int next_pow2(int num);
double* calculate_angles(int ref_altitude, int* altitudes, int offset, int limit);
char* calculate_visibility(double* angles, double* max_prev_angles, int limit);
double* max_prescan(double* angles, int rank, int global_limit, int local_limit);
void local_up_sweep(double* max_prev_angles, int limit);
void local_down_sweep(double* max_prev_angles, int limit);


int main(int argc, char *argv[])
{
    int procs_count;   // pocet procesoru
    int rank;          // muj rank

    double start, end; // promenne pro mereni casu

    // MPI INIT ----------------------------------------------------------------
    MPI_Init(&argc, &argv);                      // inicializace MPI
    MPI_Comm_size(MPI_COMM_WORLD, &procs_count); // zjistíme, kolik procesů běží­
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);        // zjistíme id svého procesu

    int ref_altitude;
    int global_count;
    int* global_altitudes;
    char* global_visibility;

    int chunk_size;

    // NACTENI VSTUPU ----------------------------------------------------------
    if (rank == 0) {
        global_altitudes = new int[MAX_INPUT_SIZE]{-1};
        global_visibility = new char[MAX_INPUT_SIZE]{'\0'};

        global_count = load_altitudes(argv[1], global_altitudes, &ref_altitude);

        chunk_size = next_pow2(global_count) / procs_count;
    }

    // Sdileni dulezitych hodnot
    MPI_Bcast(&ref_altitude, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&chunk_size, 1, MPI_INT, 0, MPI_COMM_WORLD);

    int* altitudes = new int[chunk_size];

    // Rozposlani nadmorskych vysek jednotlivym procesum
    MPI_Scatter(global_altitudes, chunk_size, MPI_INT, altitudes, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);

    // VISIBILITY --------------------------------------------------------------
    start = MPI_Wtime();

    double* angles = calculate_angles(ref_altitude, altitudes, (rank * chunk_size + 1), chunk_size);
    double* max_prev_angles = max_prescan(angles, rank, procs_count, chunk_size);
    char* visibility = calculate_visibility(angles, max_prev_angles, chunk_size);

    end = MPI_Wtime();

    // Sesbirani informaci o viditelnosti
    MPI_Gather(visibility, chunk_size, MPI_BYTE, global_visibility, chunk_size, MPI_BYTE, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        // Vypis vysledku
        cout << "_";
        for (int i = 0; i < global_count; i++) {
            cout << "," << global_visibility[i];
        }
        cout << endl;

        delete[] global_altitudes;
        delete[] global_visibility;
    }

    delete[] angles;
    delete[] max_prev_angles;
    delete[] visibility;

    MPI_Finalize();

    return 0;
}

int load_altitudes(string raw_altitudes, int* altitudes, int *ref_altitude)
{
    stringstream raw_altitudes_stream(raw_altitudes);

    // Nacteni referencni vysky
    raw_altitudes_stream >> *ref_altitude;

    if (raw_altitudes_stream.peek() == ',') {
        raw_altitudes_stream.ignore();
    }

    // Nacitani merenych nadmorskych vysek
    int count = 0;
    for (int altitude; count < MAX_INPUT_SIZE && raw_altitudes_stream >> altitude; count++) {
        altitudes[count] = altitude;

        if (raw_altitudes_stream.peek() == ',') {
            raw_altitudes_stream.ignore();
        }
    }

    return count;
}


int next_pow2(int num)
{
    int next_pow = 1;
    while (next_pow < num) {
        next_pow <<= 1;
    }

    return next_pow;
}


double* calculate_angles(int ref_altitude, int* altitudes, int offset, int limit)
{
    double* angles = new double[limit];

    for (int i = 0; i < limit; i++) {
        if (altitudes[i] == -1) {
            angles[i] = 0;
        }
        else {
            angles[i] = atan(static_cast<double>(altitudes[i] - ref_altitude) / (offset + i)) + HALF_PI;
        }
    }

    return angles;
}


char* calculate_visibility(double* angles, double* max_prev_angles, int limit)
{
    char* visibility = new char[limit];

    for (int i = 0; i < limit; i++) {
        visibility[i] = angles[i] > max_prev_angles[i] ? 'v' : 'u';
    }

    return visibility;
}


double* max_prescan(double* angles, int rank, int global_limit, int local_limit)
{
    MPI_Status stat;

    double* max_prev_angles = new double[local_limit];
    memcpy(max_prev_angles, angles, sizeof(double) * local_limit);

    local_up_sweep(max_prev_angles, local_limit);

    // Up-sweep between processes
    for (int step = 1; step < (global_limit / 2); step <<= 1) {
        for (int i = (step - 1); i < global_limit; i += (2 * step)) {
            if (rank == i) {
                MPI_Send(&max_prev_angles[local_limit - 1], 1, MPI_DOUBLE, (i + step), TAG, MPI_COMM_WORLD);
            }
            else if (rank == (i + step)) {
                double child_value;
                MPI_Recv(&child_value, 1, MPI_DOUBLE, i, TAG, MPI_COMM_WORLD, &stat);

                max_prev_angles[local_limit - 1] = (
                    child_value > max_prev_angles[local_limit - 1]
                    ? child_value
                    : max_prev_angles[local_limit - 1]
                );
            }
        }
    }

    // Clear
    if (rank == global_limit - 1) {
        max_prev_angles[local_limit - 1] = 0;
    }

    // Down-sweep between processes
    for (int step = (global_limit / 2); step > 0; step >>= 1) {
        for (int i = (step - 1); i < global_limit; i += (2 * step)) {
            if (rank == i) {
                MPI_Send(&max_prev_angles[local_limit - 1], 1, MPI_DOUBLE, (i + step), TAG, MPI_COMM_WORLD);
                MPI_Recv(&max_prev_angles[local_limit - 1], 1, MPI_DOUBLE, (i + step), TAG, MPI_COMM_WORLD, &stat);
            }
            else if (rank == (i + step)) {
                double child_value;
                MPI_Recv(&child_value, 1, MPI_DOUBLE, i, TAG, MPI_COMM_WORLD, &stat);
                MPI_Send(&max_prev_angles[local_limit - 1], 1, MPI_DOUBLE, i, TAG, MPI_COMM_WORLD);

                max_prev_angles[local_limit - 1] = (
                    child_value > max_prev_angles[local_limit - 1]
                    ? child_value
                    : max_prev_angles[local_limit - 1]
                );
            }
        }
    }

    local_down_sweep(max_prev_angles, local_limit);

    return max_prev_angles;
}


void local_up_sweep(double* max_prev_angles, int limit)
{
    for (int step = 1; step < (limit / 2); step <<= 1) {
        for (int i = (step - 1); i < limit; i += (2 * step)) {
            max_prev_angles[i + step] = (
                max_prev_angles[i] > max_prev_angles[i + step]
                ? max_prev_angles[i]
                : max_prev_angles[i + step]
            );
        }
    }
}


void local_down_sweep(double* max_prev_angles, int limit)
{
    for (int step = (limit / 2); step > 0; step >>= 1) {
        for (int i = (step - 1); i < limit; i += (2 * step)) {
            double tmp_max = (
                max_prev_angles[i] > max_prev_angles[i + step]
                ? max_prev_angles[i]
                : max_prev_angles[i + step]
            );

            max_prev_angles[i] = max_prev_angles[i + step];
            max_prev_angles[i + step] = tmp_max;
        }
    }
}