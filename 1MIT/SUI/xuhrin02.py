import copy
import random
import logging

from dicewars.ai.utils import (
    probability_of_holding_area,
    probability_of_successful_attack,
)
from dicewars.ai.utils import possible_attacks

from dicewars.client.ai_driver import BattleCommand, EndTurnCommand


class AI:
    """Naive player agent

    This agent performs all possible moves in random order
    """

    def __init__(self, player_name, board, players_order):
        """
        Parameters
        ----------
        game : Game
        """

        self.logger = logging.getLogger("AI")
        self.logger.setLevel(10)

        nb_players = board.nb_players_alive()
        self.logger.info("Setting up for {}-player game".format(nb_players))
        self.logger.debug(players_order)

        self.player_name = player_name
        self.players_order = players_order

        self.treshold = 0.4
        self.score_weight = 2

    def ai_turn(self, board, nb_moves_this_turn, nb_turns_this_game, time_left):
        """AI agent's turn

        Get a random area. If it has a possible move, the agent will do it.
        If there are no more moves, the agent ends its turn.
        """

        player_index = self.players_order.index(self.player_name)
        opponents_order = (
            self.players_order[player_index + 1 :] + self.players_order[:player_index]
        )

        largest_reagon = self.get_largest_region(board, self.player_name)

        evaluated_turns = []

        for attacker, deffender, *_ in self.relevant_turns(
            board, self.player_name, largest_reagon
        ):
            simulated_board = self.simulate_turn(
                copy.deepcopy(board), attacker, deffender
            )

            simulated_board = self.simulate_opponents(
                opponents_order[0], opponents_order[1:], simulated_board
            )
            new_score = self.get_largest_region_size(simulated_board, self.player_name)

            evaluated_turns.append((new_score, (attacker, deffender)))

        # Sort by highest score
        evaluated_turns.sort(key=lambda x: x[0], reverse=True)

        simulated_board = self.simulate_opponents(
            opponents_order[0], opponents_order[1:], copy.deepcopy(board)
        )
        pass_score = self.get_largest_region_size(simulated_board, self.player_name)

        if not evaluated_turns or pass_score > evaluated_turns[0][0]:
            self.logger.debug("No more possible turns.")
            return EndTurnCommand()

        turn = evaluated_turns[0][1]
        return BattleCommand(turn[0], turn[1])

    def relevant_turns(self, board, player_name, largest_region):
        """Find possible turns with hold higher hold probability than treshold

        This method returns list of all moves with probability of holding the area
        higher than the treshold or areas with 8 dice. In addition, it includes
        the preference of these moves. The list is sorted in descending order with
        respect to preference * hold probability.
        """
        turns = []
        for source, target in possible_attacks(board, player_name):
            atk_power = source.get_dice()
            atk_prob = probability_of_successful_attack(
                board, source.get_name(), target.get_name()
            )
            hold_prob = atk_prob * probability_of_holding_area(
                board, target.get_name(), atk_power - 1, player_name
            )
            if hold_prob >= self.treshold or atk_power == 8:
                preference = hold_prob
                if source.get_name() in largest_region:
                    preference *= self.score_weight
                turns.append(
                    (source.get_name(), target.get_name(), preference, hold_prob)
                )

        return sorted(turns, key=lambda turn: turn[2], reverse=True)

    def get_largest_region_size(self, board, player_name):
        """Get size of the largest region of specified player,
        including the areas within.
        """
        return max(len(region) for region in board.get_players_regions(player_name))

    def get_largest_region(self, board, player_name):
        """Get the largest region of specified player, including the areas within."""
        largest_region = []

        players_regions = board.get_players_regions(player_name)
        max_region_size = max(len(region) for region in players_regions)
        max_sized_regions = [
            region for region in players_regions if len(region) == max_region_size
        ]

        largest_region = max_sized_regions[0]
        return largest_region

    def simulate_opponents(self, player_name, opponents, board):
        """Simulate opponents turns."""
        while True:
            largest_reagon = self.get_largest_region(board, player_name)
            turns = self.relevant_turns(board, player_name, largest_reagon)

            if turns:
                board = self.simulate_turn(board, turns[0][0], turns[0][1])
            else:
                break

        if opponents:
            return self.simulate_opponents(opponents[0], opponents[1:], board)
        return board

    def simulate_turn(self, board, attacker_name, deffender_name):
        """Simulate change on board when battle is commanded."""
        attacker = board.get_area(attacker_name)
        deffender = board.get_area(deffender_name)

        if probability_of_successful_attack(board, attacker_name, deffender_name) > 0.5:
            deffender.set_owner(attacker.get_owner_name())
            deffender.set_dice(attacker.get_dice())

        attacker.set_dice(1)

        return board
