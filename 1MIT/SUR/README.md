# VoiceAndFaceRec
Person recognition system using voice record and face picture.

Authors: David Bažout (), Peter Uhrín (), Roman Janík (xjanik20)

### Requirements
System is implemented by Python scripts and its libraries described in requirements.txt. Version of Python is 3.8. To install requirements, run:

pip3 install -r requirements.txt

### Content
Face classifier is implemented in face_rec.py and takes following arguments: PCA dimensions, number of parameters in 1st layer, number of parameters in 2nd layer
Example of training:  
**face_rec.py 128 1024 256**

Voice classifier is implemented in voice_rec.py.  
Example of training:  
**voice_rec.py**

Training of 2 face classifiers and 1 voice classifier is implemented in evaluate.py. Classifiers are then evaluated and results are printed into directory results. Additional result of mixed face and voice classifier is printed too.

### Documentation
Documentation will be submitted later.