import face_rec
import voice_rec
import os
import glob
import cv2
import numpy as np
import copy


def load_eval_data(folder):
    segment_names = []
    images = []

    for file in sorted(glob.glob(folder + '/*.png')):
        # Getting segment name.
        segment = os.path.basename(file).replace('.png', '')
        segment_names.append(segment)

        # Reading image from file.
        img = cv2.imread(file, True).astype(np.float64) / 255
        tmp_img = [img]
        for x in tmp_img:
            images.append(x.flatten())

    return segment_names, np.array(images)


def evaluate__face_model(model_name):
    # Loading classifier.
    rec = face_rec.Classifier()
    rec.load('models/' + model_name)

    # Load data for evaluation.
    segment_names, images = load_eval_data("data/evaluate")

    # Computing predictions.
    predict_probas = rec.predict_proba(images)
    predicts = rec.predict(images)

    # Print to file.
    with open('results/' + 'image_' + model_name.replace('.model', '') + '.txt', 'w') as file:
        for segment_name, predict_proba, predict in zip(
            segment_names, np.around(predict_probas, 3), predicts
        ):
            file.write(f"{segment_name} {predict_proba} {predict}\n")


def evaluate__voice_model(model_name):
    # Loading classifier.
    rec = voice_rec.Classifier.load('models/' + model_name)

    # Load data for evaluation.
    segment_names, features = voice_rec.load_from_folder("data/evaluate")

    # Computing predictions.
    predict_probas = rec.predict_proba(features)
    predicts = rec.predict(features)

    # Print to file.
    with open('results/' + 'audio_' + model_name.replace('.model', '') + '.txt', 'w') as file:
        for segment_name, predict_proba, predict in zip(
            segment_names, np.around(predict_probas, 3), predicts
        ):
            file.write(f"{segment_name} {predict_proba} {predict}\n")


def evaluate__mixed_model(face_model_name, voice_model_name):
    # Loading classifiers.
    face_classifier = face_rec.Classifier()
    face_classifier.load('models/' + face_model_name)
    voice_classifier = voice_rec.Classifier.load('models/' + voice_model_name)

    # Load data for evaluation.
    segment_names, images = load_eval_data("data/evaluate")
    features = voice_rec.load_from_folder("data/evaluate")[1]

    # Computing predictions.
    predict_probas = face_classifier.predict_proba(images)
    predict_probas = np.add(predict_probas, voice_classifier.predict_proba(features))
    predict_probas = np.divide(predict_probas, 2.0)
    predicts = predict_mixed(predict_probas)

    # Print to file.
    with open('results/' + 'mixed_' + face_model_name.replace('.model', '_') + voice_model_name.replace('.model', '') + '.txt', 'w') as file:
        for segment_name, predict_proba, predict in zip(
            segment_names, np.around(predict_probas, 3), predicts
        ):
            file.write(f"{segment_name} {predict_proba} {predict}\n")


def predict_mixed(x):
    decisions = copy.deepcopy(x)
    decisions[decisions >= 0.5] = 1
    decisions[decisions < 0.5] = 0

    return decisions.astype(int)


def main():
    # Create models directory if not exist to save models.
    if not os.path.exists("models"):
        os.makedirs("models")

    # Create results directory if not exist to save results.
    if not os.path.exists("results"):
        os.makedirs("results")


    # Train and save face recognition models. Arguments are: PCA dimensions, number of parameters in 1st layer, number of parameters in 2nd layer
    # The models will be saved in models directory. Plot of each model will be shown.
    os.system("python face_rec.py 128 1024 256")
    os.system("python face_rec.py 128 512 256")

    # Train and save voice recognition model.
    # The model will be saved in models directory. Plot of model will be shown.
    print("Training voice model may take some time...")
    os.system("python voice_rec.py")

    # Evaluate face recognition models.
    evaluate__face_model("PCA 128-dim + CNN  512  256.model")
    evaluate__face_model("PCA 128-dim + CNN  1024  256.model")

    # Evaluate voice recognition models.
    evaluate__voice_model("RandomForests.model")

    # Evaluate mixed model of face and voice recognition.
    evaluate__mixed_model("PCA 128-dim + CNN  1024  256.model", "RandomForests.model")


if __name__ == "__main__":
    main()