import glob
import pickle
import cv2
import os
import sys
import random
import skimage.transform
import skimage.util
import numpy as np
from sklearn.utils import shuffle
import sklearn.model_selection
import sklearn.decomposition
import sklearn.neural_network
from modules.evaluation import evaluate_method

def load_from_folder(folder,augmentation=False):
    
    images = []
    
    for file in glob.glob(folder + '/*.png'):
        
        # Reading image from file.
        img = cv2.imread(file, True).astype(np.float64)/255
        
        # Data augmentation.
        aug_images = data_augmentation(img) + [img] if augmentation else [img]
        for aug_img in aug_images:
            images.append(aug_img.flatten())

    return np.array(images)

def load_training_data(folder):
    
    # Loading target images.
    target = load_from_folder(folder + '/target_train',augmentation=True)
    target_label = np.ones((target.shape[0],))

    # Loading non-target images.
    non_target = load_from_folder(folder + '/non_target_train',augmentation=True)
    non_target_label = np.zeros((non_target.shape[0],))

    # Stacking data together.
    x = np.concatenate((target,non_target))
    y = np.concatenate((target_label, non_target_label))
    
    # Data shuffle.
    x,y = shuffle(x,y)

    return x, y

def load_testing_data(folder):
    
    # Loading target images.
    target = load_from_folder(folder + '/target_dev')
    target_label = np.ones((target.shape[0],))

    # Loading non-target images.
    non_target = load_from_folder(folder + '/non_target_dev')
    non_target_label = np.zeros((non_target.shape[0],))

    # Stacking data together.
    x = np.concatenate((target,non_target))
    y = np.concatenate((target_label, non_target_label))

    return x, y

class Classifier:
    
    def __init__(self, threshold=0.5):
        self.pca_dims = None
        self.layers = None
        self.pca = None
        self.network = None
        self.threshold = threshold

    def get_name(self):
        return "PCA " + str(self.pca_dims) + "-dim + CNN " + str(self.layers).replace('(', ' ').replace(',', ' ').replace(')', '') + ".model"
    
    def fit(self, x, y, pca_dims, layers):
        
        # Saving dimension settings.
        self.pca_dims = pca_dims
        self.layers = layers

        # Reducing dimensionality with PCA.
        self.pca = sklearn.decomposition.PCA(n_components=self.pca_dims,whiten=True,random_state=1)
        self.pca.fit(x)
        x = self.pca.transform(x)

        # Training simple neural classifier.
        self.network = sklearn.neural_network.MLPClassifier(hidden_layer_sizes=self.layers, 
                                                        batch_size=32,
                                                        verbose=True,
                                                        early_stopping=True,
                                                        max_iter=200,
                                                        validation_fraction=0.2,
                                                        activation='relu',
                                                        shuffle=True,
                                                        random_state=1
                                                        )
        self.network.fit(x, y)
    
    def predict_proba(self, x):
        x = self.pca.transform(x)
        score = self.network.predict_proba(x)[:,1]
        return score

    def predict(self, x):
        x = self.pca.transform(x)
        decisions = self.network.predict_proba(x)[:, 1]

        decisions[decisions >= self.threshold] = 1
        decisions[decisions < self.threshold] = 0

        return decisions.astype(int)

    def save(self, filename):
        file = open(filename, "wb")
        pickle.dump((self.pca, self.network, self.pca_dims, self.layers),file)
        file.close()
    
    def load(self, filename):
        file = open(filename, "rb")
        self.pca, self.network, self.pca_dims, self.layers = pickle.load(file)
        file.close()

def data_augmentation(img):

    images = []

    # Horizontal flip.
    images.append(img[:, ::-1].copy())

    # Gaussian blur.
    images.append(cv2.GaussianBlur(img,(3,3),1))

    # Rotation.
    images.append(skimage.transform.rotate(img,-20))
    images.append(skimage.transform.rotate(img,-10))
    images.append(skimage.transform.rotate(img,10))
    images.append(skimage.transform.rotate(img,20))

    # Random noise.
    images.append(skimage.util.random_noise(img))

    return images


def main():
    # Loading training data from file.
    x, y = load_training_data("data")

    # Training classifier.
    rec = Classifier()
    dims = int(sys.argv[1])
    layers = (int(sys.argv[2]), int(sys.argv[3]))
    rec.fit(x, y, dims, layers)
    rec.save("models/" + rec.get_name())
    # rec.load(sys.argv[1])

    # Evaluating classifier.
    x, y = load_testing_data("data")
    evaluate_method(x, y, rec)

if __name__ == "__main__":
    main()
    


