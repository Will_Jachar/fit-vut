import numpy as np
import matplotlib.pyplot as plt


def compute_precision_recall(sample, gtruth):
    
    tp = np.sum(sample * gtruth, keepdims=False)
    fp = np.sum(np.where((gtruth - sample) == -1, 1, 0),keepdims=False)
    fn = np.sum(np.where((gtruth - sample) == 1, 1, 0), keepdims=False)

    try:
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        return precision, recall
    
    except:
        return np.nan, np.nan


def evaluate_method(x, y, method, step=0.01):

    # Computing predictions.
    scores = method.predict_proba(x)

    # Creating precision-recall curve.
    p, r = [], []
    for threshold in np.arange(0.0, 1.0 + step, step):

        decisions = np.where(scores > threshold, 1, 0)
        prec, rec = compute_precision_recall(decisions, y)
        p.append(prec)
        r.append(rec)

    # Visualising results.
    fig = plt.figure()
    fig.canvas.set_window_title(method.get_name())
    plt.plot(r,p,label=method.get_name())
    plt.ylabel('Precision')
    plt.xlabel('Recall')
    plt.legend(loc='best')
    plt.show()
