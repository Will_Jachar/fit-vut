"""Classifier for voice recordings"""

import pickle
import os

from glob import glob

import librosa
import numpy as np
import matplotlib.pyplot as plt
import soundfile

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, plot_precision_recall_curve
from sklearn.utils import resample, shuffle


def extract_features(filepath):
    with soundfile.SoundFile(filepath) as file:
        recording = file.read(dtype="float32")
        sample_rate = file.samplerate

        stft = np.abs(librosa.stft(recording))

        mfccs = np.mean(
            librosa.feature.mfcc(y=recording, sr=sample_rate, n_mfcc=40).T, axis=0
        )
        chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
        mel = np.mean(
            librosa.feature.melspectrogram(recording, sr=sample_rate).T, axis=0
        )
        contrast = np.mean(
            librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0
        )
        tonnetz = np.mean(
            librosa.feature.tonnetz(
                y=librosa.effects.harmonic(recording), sr=sample_rate
            ).T,
            axis=0,
        )

    return np.hstack((mfccs, chroma, mel, contrast, tonnetz))


def load_from_folder(folder):
    segment_names = []
    features = []
    for file in sorted(glob(folder + "/*.wav")):
        print("Processing file: ", file)

        segment = os.path.basename(file).replace('.wav', '')
        segment_names.append(segment)

        features.append(extract_features(file))

    return segment_names, np.array(features)


def load_dataset(target_folder, non_target_folder, balance=False):
    # Loading target recordings
    _, target_features = load_from_folder(target_folder)
    target_count = len(target_features)

    # Loading non-target recordings
    _, non_target_features = load_from_folder(non_target_folder)
    non_target_count = len(non_target_features)

    # Balancing samples so both categories has equal number of samples
    if balance:
        if target_count < non_target_count:
            target_features = resample(
                target_features, n_samples=non_target_count, replace=True
            )
            target_count = non_target_count
        elif non_target_count < target_count:
            non_target_features = resample(
                non_target_features, n_samples=target_count, replace=True
            )
            non_target_count = target_count

    # Generating labels
    target_labels = np.ones((target_count,))
    non_target_labels = np.zeros((non_target_count,))

    # Stacking data together
    features = np.concatenate((target_features, non_target_features))
    labels = np.concatenate((target_labels, non_target_labels))

    # Shuffle data
    features, labels = shuffle(features, labels)

    return features, labels


class Classifier:
    def __init__(self, model=None, threshold=0.2):
        self.model = model or RandomForestClassifier()
        self.threshold = threshold

    def save(self, filename):
        with open(filename, "wb") as file:
            pickle.dump((self.model, self.threshold), file)

    @classmethod
    def load(cls, filename):
        with open(filename, "rb") as file:
            model, threshold = pickle.load(file)

        return cls(model, threshold)

    def fit(self, x, y):
        self.model.fit(x, y)

    def score(self, x, y):
        return accuracy_score(y, self.predict(x))

    def predict_proba(self, x):
        return self.model.predict_proba(x)[:, 1]

    def predict(self, x):
        decisions = self.model.predict_proba(x)[:, 1]

        decisions[decisions >= self.threshold] = 1
        decisions[decisions < self.threshold] = 0

        return decisions.astype(int)


def main():
    # Loading training data from file
    training_features, training_labels = load_dataset(
        target_folder="data/target_train",
        non_target_folder="data/non_target_train",
        balance=True,
    )

    # Loading testing data from file
    testing_features, testing_labels = load_dataset(
        target_folder="data/target_dev",
        non_target_folder="data/non_target_dev",
        balance=False,
    )

    # # Loading preprocessed data
    # with open("data/audio_train.data", "rb") as file:
    #     training_features, training_labels = pickle.load(file)
    # with open("data/audio_test.data", "rb") as file:
    #     testing_features, testing_labels = pickle.load(file)

    # Training classifier
    classifier = Classifier()
    classifier.fit(training_features, training_labels)

    # Save classifier
    classifier.save("models/RandomForests.model")

    # # Loading pretrained classifier
    # classifier = Classifier.load("model_to_test/RF_1.model")

    # # Load data for evaluation
    # segment_names, evaluate_features = load_from_folder("data/evaluate")

    # with open("data/audio_evaluate.data", "wb") as file:
    #     pickle.dump((segment_names, evaluate_features), file)

    # # Loading preprocessed data
    # with open("data/audio_evaluate.data", "rb") as file:
    #     segment_names, evaluate_features = pickle.load(file)
    #
    # predict_probas = classifier.predict_proba(evaluate_features)
    # predicts = classifier.predict(evaluate_features)
    #
    # with open("result/audio_RF.txt", "w") as file:
    #     for segment_name, predict_proba, predict in zip(
    #         segment_names, predict_probas, predicts
    #     ):
    #         file.write(f"{segment_name} {predict_proba} {predict}\n")

    # Calculate precision recall curve
    plot_precision_recall_curve(classifier.model, testing_features, testing_labels)
    plt.show()


if __name__ == "__main__":
    main()
