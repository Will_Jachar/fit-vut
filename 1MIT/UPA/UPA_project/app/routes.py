import json
import cx_Oracle

from flask import render_template, request
from app import app
from app.modules.helper import (
    getConnection,
    parse_oracle_object,
    GEO_TYPE,
    GEO_TYPE_SQL,
)


@app.route("/")
@app.route("/index")
def index():
    return render_template("main.html")


@app.route("/get_objects")
def get_objects():
    connection = getConnection()
    cursor = connection.cursor()

    res = cursor.execute("SELECT id, shape, surface_id FROM object")

    geo_json = []
    for row in res:
        geometry = parse_oracle_object(row[1])
        geometry_type = GEO_TYPE[geometry["SDO_GTYPE"]]

        geo_object = {
            "type": "Feature",
            "geometry": {
                "type": geometry_type["name"],
                "coordinates": geometry[geometry_type["key"]],
            },
            "properties": {
                "id": row[0],
                "surface_id": row[2],
                "surface_color": "rgba(255, 165, 0, 0.5)",
            },
        }

        geo_json.append(geo_object)

    return json.dumps(geo_json)


@app.route("/create_object", methods=["POST"])
def create_object():
    data = request.json

    connection = getConnection()
    cursor = connection.cursor()

    geometry_type = connection.gettype("MDSYS.SDO_GEOMETRY")
    element_info_type = connection.gettype("MDSYS.SDO_ELEM_INFO_ARRAY")
    ordinate_array_type = connection.gettype("MDSYS.SDO_ORDINATE_ARRAY")
    new_geometry = geometry_type.newobject()
    new_geometry.SDO_GTYPE = GEO_TYPE_SQL[data["object_type"]]["code"]
    new_geometry.SDO_ELEM_INFO = element_info_type.newobject()
    new_geometry.SDO_ELEM_INFO.extend(GEO_TYPE_SQL[data["object_type"]]["info"])
    new_geometry.SDO_ORDINATES = ordinate_array_type.newobject()
    new_geometry.SDO_ORDINATES.extend(data["coordinates"])

    new_id = cursor.var(cx_Oracle.NUMBER)

    sql = (
        "INSERT INTO object VALUES "
        f"(NULL, :new_geometry, :surface_id) "
        "RETURNING ID INTO :new_id"
    )
    res = cursor.execute(
        sql, new_geometry=new_geometry, surface_id=data["surface_id"], new_id=new_id
    )

    cursor.close()
    connection.commit()

    return json.dumps({"id": new_id.getvalue()[0]})


@app.route("/delete_object", methods=["POST"])
def delete_object():
    data = request.json

    object_id = data["object_id"]

    connection = getConnection()
    cursor = connection.cursor()

    res = cursor.execute(
        "DELETE FROM object WHERE id = :object_id", object_id=object_id
    )

    cursor.close()
    connection.commit()

    return {}


@app.route("/update_object", methods=["POST"])
def update_object():
    data = request.json

    connection = getConnection()
    cursor = connection.cursor()

    geometry_type = connection.gettype("MDSYS.SDO_GEOMETRY")
    element_info_type = connection.gettype("MDSYS.SDO_ELEM_INFO_ARRAY")
    ordinate_array_type = connection.gettype("MDSYS.SDO_ORDINATE_ARRAY")
    new_geometry = geometry_type.newobject()
    new_geometry.SDO_GTYPE = GEO_TYPE_SQL[data["object_type"]]["code"]
    new_geometry.SDO_ELEM_INFO = element_info_type.newobject()
    new_geometry.SDO_ELEM_INFO.extend(GEO_TYPE_SQL[data["object_type"]]["info"])
    new_geometry.SDO_ORDINATES = ordinate_array_type.newobject()
    new_geometry.SDO_ORDINATES.extend(data["coordinates"])

    res = cursor.execute(
        (
            f"UPDATE object SET shape = :new_geometry, "
            "surface_id = :surface_id WHERE id = :object_id"
        ),
        new_geometry=new_geometry,
        surface_id=data["surface_id"],
        object_id=data["object_id"],
    )

    connection.commit()
    cursor.close()

    return {}


def intersect():
    data = {}
    data["project"] = "0"
    data["object1"] = "0"
    data["object2"] = "1"

    object1 = data["object1"]
    object2 = data["object2"]
    project = data["project"]

    connection = getConnection()
    cursor = connection.cursor()

    res = cursor.execute("SELECT shape,surface_id FROM OBJECT WHERE id = " + object1)
    item = res.fetchone()
    geomstr1 = item[0]
    surface_id = item[1]

    geometry1 = parse_oracle_object(geomstr1)
    geometry_type1 = GEO_TYPE[geometry1["SDO_GTYPE"]]
    coordinates1 = geometry1[geometry_type1["key"]]

    res = cursor.execute("SELECT shape FROM OBJECT WHERE id = " + object2)
    item = res.fetchone()
    geomstr2 = item[0]

    geometry2 = parse_oracle_object(geomstr2)
    geometry_type2 = GEO_TYPE[geometry2["SDO_GTYPE"]]
    coordinates2 = geometry2[geometry_type2["key"]]

    # res = cursor.execute("SELECT shape FROM OBJECT WHERE id = " + object2)
    # geometry2 = res.fetchone()[0]

    geom1 = str(
        "MDSYS.SDO_GEOMETRY(2003, NULL, NULL, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003, "
        + object1
        + "), MDSYS.SDO_ORDINATE_ARRAY("
        + str(coordinates1)
        + "))"
    )
    geom2 = str(
        "MDSYS.SDO_GEOMETRY(2003, NULL, NULL, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003, "
        + object2
        + "), MDSYS.SDO_ORDINATE_ARRAY("
        + str(coordinates2)
        + "))"
    )

    # res = cursor.execute(
    #     "INSERT INTO OBJECT VALUES ("
    #     "OBJECT_SEQ.CURRVAL,"
    #     "SDO_GEOM.SDO_INTERSECTION(" + str(geom1) + ", " + str(geom2) + ", 0.5) , " +
    #     str(surface_id) + ", " + str(project) + ")"
    # )

    cursor.close()
    connection.commit()
