class DrawableObject {
  constructor(id, coordinates, surface, color) {
    this.id = id;
    this.type = undefined;
    this.coordinates = coordinates;
    this.surface = surface;
    this.color = color;
    this.moveOffsetX = 0;
    this.moveOffsetY = 0;
    this.finished = false;
    this.path = undefined;
    this.end = {
      "x": this.coordinates[this.coordinates.length - 2],
      "y": this.coordinates[this.coordinates.length - 1]
    }
  }

  finish() {
    this.finished = true;

    this.generatePath2D();
  }

  updatePoint(endX, endY) {
    this.end.x = endX;
    this.end.y = endY;

    this.generatePath2D();
  }

  savePoint(endX, endY) {
    this.coordinates.push(endX);
    this.coordinates.push(endY);
    this.finished = true;

    this.updatePoint(endX, endY);
  }

  updateMove(offsetX, offsetY) {
    this.moveOffsetX = offsetX;
    this.moveOffsetY = offsetY;
  }

  confirmMove(offsetX, offsetY) {
    this.moveOffsetX = 0;
    this.moveOffsetY = 0;

    for (let i = 0; i < this.coordinates.length; i += 2) {
      this.coordinates[i] += offsetX;
      this.coordinates[i + 1] += offsetY;
    }

    this.generatePath2D();
  }

  generatePath2D() {
    this.path = new Path2D();
    this.path.moveTo(this.coordinates[0], this.coordinates[1]);

    for (let i = 2; i < this.coordinates.length; i += 2) {
      this.path.lineTo(this.coordinates[i], this.coordinates[i + 1]);
    }

    if (!this.finished) {
      this.path.lineTo(this.end.x, this.end.y);
    }
  }

  draw(ctx) {
    const originalFillStyle = ctx.fillStyle;
    const originalStrokeStyle = ctx.strokeStyle;
    ctx.fillStyle = this.color;
    ctx.strokeStyle = this.color;

    ctx.translate(this.moveOffsetX, this.moveOffsetY);
    if (!this.finished || this.type == "Point" || this.type == "LineString") {
      ctx.stroke(this.path);
    }
    else {
      ctx.fill(this.path);
    }
    ctx.resetTransform();

    ctx.fillStyle = originalFillStyle;
    ctx.strokeStyle = originalStrokeStyle;
  }
}

//--------------------------------------------------------------------------------------

class DrawablePoint extends DrawableObject {
  constructor(id, coordinates, surface, color) {
    super(id, coordinates, surface, color);
    this.type = "Point";
    this.generatePath2D();
    this.finished = true;
  }

  generatePath2D() {
    super.generatePath2D();
    this.path.rect(
      this.coordinates[0] - 1 + this.moveOffsetX,
      this.coordinates[1] - 1 + this.moveOffsetY,
      2,
      2
    );
  }
}

//--------------------------------------------------------------------------------------

class DrawableLine extends DrawableObject {
  constructor(id, coordinates, surface, color) {
    super(id, coordinates, surface, color);
    this.type = "LineString";
  }
}

//--------------------------------------------------------------------------------------

class DrawablePolygon extends DrawableObject {
  constructor(id, coordinates, surface, color) {
    super(id, coordinates, surface, color);
    this.type = "Polygon";
  }

  savePoint(endX, endY) {
    if (Math.sqrt(Math.pow((endX - this.coordinates[0]), 2) + Math.pow((endY - this.coordinates[1]), 2)) < 4) {
      this.updatePoint(endX, endY);
      this.finished = true;
    }
    else {
      this.coordinates.push(endX);
      this.coordinates.push(endY);
    }
  }

  generatePath2D() {
    super.generatePath2D();

    if (this.finished) {
      this.path.closePath();
    }
  }
}
