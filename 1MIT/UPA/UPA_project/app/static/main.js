function setMode(event) {
  app.mode = { "action": event.target.dataset.mode, "object": event.target.dataset.object };

  for (const button of document.getElementsByClassName("mode-button")) {
    button.classList.remove("active");
  }

  event.target.classList.add("active");
}

function mouseDown(event) {
  switch (app.mode.action) {
    case "MOVE":
      if (!app.drag) {
        for (let i = app.drawableObjects.length - 1; i >= 0; i--) {
          if (
            app.ctx.isPointInPath(app.drawableObjects[i].path, event.offsetX, event.offsetY) ||
            app.ctx.isPointInStroke(app.drawableObjects[i].path, event.offsetX, event.offsetY)
          ) {
            if (!event.shiftKey) {
              app.activeObjects.splice(0, app.activeObjects.length);
            }
            app.activeObjects.push(app.drawableObjects[i]);
            app.moving.object = app.drawableObjects[i];
            app.moving.origin.x = event.offsetX;
            app.moving.origin.y = event.offsetY;
            app.drag = true;

            draw();
            return;
          }
        }

        app.activeObjects.splice(0, app.activeObjects.length);

        draw();
      }
      break;
    case "REMOVE":
      if (!app.drag) {
        for (let i = app.drawableObjects.length - 1; i >= 0; i--) {
          if (
            app.ctx.isPointInPath(app.drawableObjects[i].path, event.offsetX, event.offsetY) ||
            app.ctx.isPointInStroke(app.drawableObjects[i].path, event.offsetX, event.offsetY)
          ) {
            deleteObjectInDB(app.drawableObjects[i]);
            app.drawableObjects.splice(i, 1);
            break;
          }
        }

        app.activeObjects.splice(0, app.activeObjects.length);
        draw();
      }
      break;
    case "ADD":
      if (app.drag) {
        app.drawing.savePoint(event.offsetX, event.offsetY);

        if (app.drawing.finished) {
          createObjectInDB(app.drawing);

          app.drawableObjects.push(app.drawing);
          app.drawing = undefined;
          app.drag = false;
        }

        draw();
      }
      else {
        switch (app.mode.object) {
          case "POINT":
            app.drawing = new DrawablePoint(null, [event.offsetX, event.offsetY], 1, "rgba(255, 165, 0, 0.5)");

            createObjectInDB(app.drawing);

            app.drawableObjects.push(app.drawing);
            draw();
            break;
          case "LINE":
            app.drawing = new DrawableLine(null, [event.offsetX, event.offsetY], 1, "rgba(155, 165, 0, 0.5)");
            app.drag = true;
            break;
          case "POLYGON":
            app.drawing = new DrawablePolygon(null, [event.offsetX, event.offsetY], 1, "rgba(0, 165, 0, 0.5)");
            app.drag = true;
            break;
        }

        app.activeObjects.splice(0, app.activeObjects.length);
        app.activeObjects.push(app.drawing);
      }
  }
}

function mouseUp(event) {
  if (app.drag && app.mode.action == "MOVE") {
    app.moving.object.confirmMove(
      event.offsetX - app.moving.origin.x,
      event.offsetY - app.moving.origin.y
    );

    updateObjectInDB(app.moving.object);

    app.moving.object = undefined;
    app.drag = false;
  }
}

function mouseMove(event) {
  if (app.drag) {
    if (app.mode.action == "MOVE") {
      app.moving.object.updateMove(
        event.offsetX - app.moving.origin.x,
        event.offsetY - app.moving.origin.y
      );
    }
    else {
      app.drawing.updatePoint(event.offsetX, event.offsetY);
    }

    draw();
  }
}

function draw() {
  app.ctx.clearRect(0, 0, app.canvas.width, app.canvas.height);

  for (const drawableObject of app.drawableObjects) {
    drawableObject.draw(app.ctx);
  }

  if (app.drawing !== undefined) {
    app.drawing.draw(app.ctx);
  }
  else if (app.activeObjects.length > 0) {
    for (const activeObject of app.activeObjects) {
      let strokeStyle = app.ctx.strokeStyle;
      app.ctx.strokeStyle = "rgba(255, 165, 0, 1)";
      app.ctx.translate(activeObject.moveOffsetX, activeObject.moveOffsetY);
      app.ctx.stroke(activeObject.path);
      app.ctx.resetTransform();
      app.ctx.strokeStyle = strokeStyle;
    }
  }
}

//=========================================================================

function loadGeoJson() {
  fetch("/get_objects").then(response => response.json()).then(geoJson => {
    for (const geoObject of geoJson) {
      id = geoObject.properties.id;
      coordinates = geoObject.geometry.coordinates;
      surface = geoObject.properties.surface_id;
      color = geoObject.properties.surface_color;

      switch (geoObject.geometry.type) {
        case "Point":
          newDrawable = new DrawablePoint(id, coordinates, surface, color);
          break;
        case "LineString":
          newDrawable = new DrawableLine(id, coordinates, surface, color);
          break;
        case "Polygon":
          newDrawable = new DrawablePolygon(id, coordinates, surface, color);
          break;
      }

      newDrawable.finish();
      app.drawableObjects.push(newDrawable);
    }

    draw();
  }).catch((error) => {
    console.log(error);
  });
}

function createObjectInDB(newObject) {
  fetch("/create_object", {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify({
      "object_type": newObject.type,
      "coordinates": newObject.coordinates,
      "surface_id": newObject.surface
    })
  }).then(response => response.json()).then(response_json => {
    newObject.id = response_json.id;
  }).catch((error) => {
    console.log(error);
  });
}

function deleteObjectInDB(deletedObject) {
  fetch("/delete_object", {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify({
      "object_id": deletedObject.id
    })
  }).catch((error) => {
    console.log(error);
  });
}

function updateObjectInDB(updatedObject) {
  fetch("/update_object", {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify({
      "object_id": updatedObject.id,
      "object_type": updatedObject.type,
      "coordinates": updatedObject.coordinates,
      "surface_id": updatedObject.surface
    })
  }).catch((error) => {
    console.log(error);
  });
}

//=========================================================================

document.addEventListener("DOMContentLoaded", function (event) {
  window.app = {
    "canvas": document.getElementById("canvas"),
    "ctx": document.getElementById("canvas").getContext("2d"),
    "drawableObjects": [],
    "mode": { "action": undefined, "object": undefined },
    "drag": false,
    "drawing": undefined,
    "moving": { "object": undefined, "origin": { "x": undefined, "y": undefined } },
    "activeObjects": [],
  }

  //-----------------------------------------------------------------------

  for (const button of document.getElementsByClassName("mode-button")) {
    button.addEventListener("click", setMode, false);
  }
  app.canvas.addEventListener("mousedown", mouseDown, false);
  app.canvas.addEventListener("mouseup", mouseUp, false);
  app.canvas.addEventListener("mousemove", mouseMove, false);

  app.ctx.fillStyle = "rgba(255, 165, 0, 0.5)";
  app.ctx.strokeStyle = "rgba(255, 165, 0, 0.5)";
  app.ctx.lineWidth = 5;

  loadGeoJson();
});
