// Variables that represet state of the app
// Things like framerate or how manz past states for undo is stored
// can be set here
var app = {
    "framerate": 30,
    "maxHistoryStates": 10,
    "canvas": undefined,
    "currentState": {
        "attractors": [],
        "movers": []
    },
    "historyStates": [],
    "dragging": false,
    "dragOrigin": undefined,
    "dragEnd": undefined
};

// ------------------------------------------------------------------------

// Function for initial setup
function setup() {
    app.canvas = createCanvas(500, 500);
    
    // Mouse click handler will save original mouse coordinates
    // and start dragging process
    app.canvas.mousePressed(function() {
        if (mouseButton !== LEFT) {
            return;
        }

        const moverType = $("input[name=shapes]:checked").data("shape");

        // If mover type is attractor it will be created without
        // the dragging proccess because it has no velocity
        if (moverType === "ATTRACTOR") {
            saveState();
            createMover(moverType, mouseX, mouseY);
        }
        else {
            app.dragging = true;

            app.dragOrigin = createVector(mouseX, mouseY);
            app.dragEnd = app.dragOrigin.copy();
        }
    });

    // Mouse release handler will save current state (for undo) stops
    // the dragging process and will create new object of selected type
    app.canvas.mouseReleased(function() {
        if (!app.dragging) {
            return;
        }

        saveState();

        app.dragging = false;
        
        const moverType = $("input[name=shapes]:checked").data("shape");
        // Velocity is calculated based on dragged distance and framerate
        // as dragged distance traveled in one second
        const velocity = p5.Vector.div(
            p5.Vector.sub(app.dragOrigin, app.dragEnd), app.framerate
        );
        createMover(moverType, mouseX, mouseY, velocity);
    });

    app.canvas.parent("canvas-parent");
    
    frameRate(app.framerate);
    background(255);

    randomMover();
} 

// Draw function is the main loop and its called every frame
function draw() {
    background(255);

    // Draw all attractors
    for (const attractor of app.currentState.attractors) {
        attractor.draw();
    }

    // Every mover is firstly attracted to every attractor and then
    // to every other mover
    for (const mover of app.currentState.movers) {
        for (const attractor of app.currentState.attractors) {
            attractor.attract(mover);
        }

        for (const peerAttractor of app.currentState.movers) {
            if (peerAttractor !== mover) {
                peerAttractor.attract(mover);
            }
        }

        // Update movers velocity and location and draw it to the canvas
        mover.update();
        mover.draw();
    }

    // Draw arrow if user is dragging
    if (app.dragging) {
        app.dragEnd.x = mouseX;
        app.dragEnd.y = mouseY;

        drawArrow(app.dragOrigin, app.dragEnd);
    }
}

// ------------------------------------------------------------------------

// Helper function for drawing arrow
function drawArrow(origin, end) {
    stroke(color(255, 0, 0));
    fill(color(255, 0, 0));
    
    line(origin.x, origin.y, end.x, end.y);
    
    p5.Vector.sub(origin, end)

    push();

    translate(origin.x, origin.y);
    rotate(p5.Vector.sub(origin, end).heading());
    triangle(0, 2, 0, -2, 4, 0);

    pop();
}

// ------------------------------------------------------------------------

// Function for creating mover or attractor object of desired type
// with desired initial conditions
function createMover(moverType, x, y, velocity) {
    const maxTrailLength = parseInt($("#trail-range").val(), 10);

    switch (moverType) {
        case "ATTRACTOR":
            app.currentState.attractors.push(new Attractor(x, y, 250));
            break;
        case "CIRCLE":
            app.currentState.movers.push(
                new CircleMover(x, y, 25, maxTrailLength, velocity)
            );
            break;
        case "RECT":
            app.currentState.movers.push(
                new RectMover(x, y, 25, maxTrailLength, velocity)
            );
            break;
    }
}

// Clears all attractors and movers
function clearMovers() {
    app.currentState.attractors = [];
    app.currentState.movers = [];
}

// Clears all attractors and movers and then created one attractor
// in the middle and random mover
function randomMover() {
    clearMovers();

    createMover("ATTRACTOR", width / 2, height / 2);
    createMover(
        random(["CIRCLE", "RECT"]),
        random(width),
        random(height),
        createVector(random(-5, 5), random(-5, 5))
    );
}

// Sets max trail length for every mover
function setMaxTrailLenght() {
    const newMaxTrailLength = parseInt($("#trail-range").val(), 10);

    for (const mover of app.currentState.movers) {
        mover.maxTrailLength = newMaxTrailLength;
    }
}

// Saves state (attractors and movers) for undo
function saveState() {
    const newHistoryState = {
        "attractors": [],
        "movers": []
    };

    // Deep copy of every attractor
    for (const attractor of app.currentState.attractors) {
        newHistoryState.attractors.push(attractor.copy());
    }

    // Deep copy of every mover
    for (const mover of app.currentState.movers) {
        newHistoryState.movers.push(mover.copy());
    }

    app.historyStates.push(newHistoryState);

    // Kepp maximum hystory states at some maximum
    if (app.historyStates.length > app.maxHistoryStates) {
        app.historyStates.shift();
    }

    // Set undo button enabled (because there is and state to undo to)
    $("#undo-button").prop("disabled", false);
}

// Reverts state (attractors and movers) when user wants to undo
function revertState() {
    const revertedState = app.historyStates.pop();

    // Reverts to previous state and updates max trail lenght
    if (revertedState) {
        app.currentState = revertedState;

        setMaxTrailLenght();
    }

    // If there is no more history states disable undo button
    if (app.historyStates.length === 0) {
        $("#undo-button").prop("disabled", true);
    }
}

// ------------------------------------------------------------------------

// Action button press handler calls appropriate function based
// on which button was pressed
$(".action-button").click(function() {
    switch($(this).data("action")) {
        case "RANDOM":
            saveState();
            randomMover();
            break;
        case "CLEAR":
            saveState();
            clearMovers();
            break;
        case "UNDO":
            revertState();
            break;
        case "SAVE":
            saveCanvas(app.canvas, "gravity_art", "jpg");
            break;
    }
});

// Max trail lenght handler updated max trail lenght upon changing the value
$("#trail-range").change(function() {
    setMaxTrailLenght();
});
