var G = 0.4;


// Attractor class represents unmovable object of high mass that will be
// attracting other objects (something like an black hole but unmovable)
class Attractor {
    constructor(x, y, mass) {
        this.location = createVector(x, y);
        this.mass = mass;
    }
    
    // Function that will return deep copy of this instance
    copy() {
        return new this.constructor(
            this.location.x,
            this.location.y,
            this.mass
        );
    }

    // Attract given mover
    attract(mover) {
        let distanceVector = p5.Vector.sub(this.location, mover.location);
        // Some constraints so the mover will be allwas attracted no matter
        // how far away it is
        const distanceSq = constrain(distanceVector.magSq(), 150, 1500);

        // Calculate attraction force vector with recpect to mass of both
        // objects and distance between them
        mover.applyForce(
            distanceVector.normalize().mult(
                G * ((this.mass * mover.mass) / distanceSq)
        ));
    }

    draw() {
        stroke(0);
        fill(0);
        ellipseMode(RADIUS);
        ellipse(this.location.x, this.location.y, 10);
    }
}


// Mover is an abstract class that represents objects that can attract other
// object and can be attracted to other objects.
class Mover extends Attractor {
    constructor(x, y, mass, maxTrailLength, velocity) {
        if (new.target === Mover) {
            throw new Error("Can't create instance of an abstract class Mover!");
        }
        
        super(x, y, mass);
        
        this.maxTrailLength = maxTrailLength;
        this.trail = [];

        this.velocity = velocity.copy() || createVector(0, 0);
        this.acceleration = createVector(0, 0);
    }

    // Function that will return deep copy of this instance
    copy() {
        const objectCopy = new this.constructor(
            this.location.x,
            this.location.y,
            this.mass,
            this.maxTrailLength,
            this.velocity,
        );

        objectCopy.acceleration = this.acceleration.copy();

        for (const element of this.trail) {
            objectCopy.trail.push(element.copy());
        }

        return objectCopy;
    }

    // Function that updates acceleration of this object based on
    // force vector and its mass
    applyForce(force) {
        this.acceleration.add(p5.Vector.div(force, this.mass));
    }

    // Updates velocity and location of this object based on its acceleration
    update() {
        // Save past position as trail behind the object
        this.trail.push(this.location.copy());

        // If length of trail is bigger than max lenght the rest of the trail
        // is cut off
        if (this.trail.length > this.maxTrailLength) {
            this.trail.splice(0, this.trail.length - this.maxTrailLength);
        }

        this.velocity.add(this.acceleration);
        this.location.add(this.velocity);

        this.acceleration.mult(0);
    }
    
    drawShape(x, y) {
        throw new Error("This is an abstract method!");
    };
    
    draw() {// Fill color is based on objects mass (higher mass, darker color)
        const fillColor = 255 - this.mass * 3;
        // AlphaMultiplyer is used to fade away the objects trail
        const alphaMultiplyer = 255 / this.trail.length;

        // Draw trail
        for (let i = 0; i < this.trail.length; i++) {
            const alpha = i * alphaMultiplyer;

            stroke(0, alpha);
            fill(fillColor, alpha);

            this.drawShape(
                this.trail[i].x,
                this.trail[i].y,
                this.mass - this.trail.length + i
            );
        }

        stroke(0);
        fill(fillColor);

        // Draw object itself
        this.drawShape(this.location.x, this.location.y, this.mass);
    }
}


// Class used to render Circle movers
class CircleMover extends Mover {
    drawShape(x, y, size) {
        ellipseMode(RADIUS);
        ellipse(x, y, size / 2);
    }
}


// Class used to render Square movers
class RectMover extends Mover {
    drawShape(x, y, size) {
        rectMode(CENTER);
        rect(x, y, size, size);
    }
}