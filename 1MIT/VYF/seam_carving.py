import warnings

import click
import cv2 as cv
import numpy as np

from numba import jit
from progress.bar import Bar

# Ignore numba compilation warnings
warnings.filterwarnings("ignore")

# -------------------------------------------------------------------------


@click.command()
@click.option(
    "--src",
    type=str,
    required=True,
    help="Path to source image file."
)
@click.option(
    "--dst",
    type=str,
    required=False,
    help="Path to destination image file."
)
@click.option(
    "--rows",
    type=int,
    default=0,
    help="Amount of rows do add/remove from image."
)
@click.option(
    "--cols",
    type=int,
    default=0,
    help="Amount of columns do add/remove from image."
)
def main(src, dst, rows, cols):
    img = cv.imread(src)
    if img is None:
        print("Unable to load source image")
        return

    # Handle vertical seam carving
    if rows > 0:
        img = add_rows(img=img, amount=rows)
    elif rows < 0:
        img = remove_rows(img=img, amount=abs(rows))

    # Handle horizontal seam carving
    if cols > 0:
        img = add_cols(img=img, amount=cols)
    elif cols < 0:
        img = remove_cols(img=img, amount=abs(cols))

    cv.imwrite(dst, img)


# -------------------------------------------------------------------------


def add_rows(img, amount):
    """
    Inserts rows to an image.

    Args:
        img (np.array): Image data
        amount (int): nubmer of rows to insert into image

    Returns:
        np.array: Image with inserted rows

    """
    # Adding rows is the same thing as adding columns if we rotate the image 90°
    img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    img = add_cols(img=img, amount=amount)
    img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)

    return img


def remove_rows(img, amount):
    """
    Removes rows from an image.

    Args:
        img (np.array): Image data
        amount (int): nubmer of rows to remove from image

    Returns:
        np.array: Image with removed rows

    """
    # Removing rows is the same thing as removing columns if we rotate the image 90°
    img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    img = remove_cols(img=img, amount=amount)
    img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)

    return img


def add_cols(img, amount):
    """
    Inserts columns to an image.

    Args:
        img (np.array): Image data
        amount (int): nubmer of columns to insert into image

    Returns:
        np.array: Image with inserted columns

    """
    tmp_img = img.copy()

    seams = []
    # Find seams the same way as if we want to delete to use them as positions
    # for newly inserted seams
    for _ in Bar("Finding indexes\t", suffix="%(percent).1f%%").iter(range(amount)):
        seam = find_seam(img=tmp_img)
        tmp_img = delete_seam(img=tmp_img, seam=seam)

        seams.append(seam)

    seams = np.array(seams)
    # For each found seam insert new seam at that position
    for seam_i, seam in Bar("Inserting seams\t", suffix="%(percent).1f%%").iter(
        enumerate(seams, start=1)
    ):
        img = insert_seam(img=img, seam=seam)

        # Remaining seams must be updated to acount for shift in indexing
        seams[seam_i:][seams[seam_i:] >= seam] += 2

    return img


def remove_cols(img, amount):
    """
    Removes columns from an image.

    Args:
        img (np.array): Image data
        amount (int): nubmer of columns to remove from image

    Returns:
        np.array: Image with removed columns

    """
    _, width = img.shape[:2]

    # If amount of columns that user wants to delete is greater than number of columns
    # in the image, just remove all columns
    if amount > width:
        amount = width

    # Find seams with the lowest value of cumulative enrgy to delete
    for _ in Bar("Removing seams\t", suffix="%(percent).1f%%").iter(range(amount)):
        seam = find_seam(img=img)
        img = delete_seam(img=img, seam=seam)

    return img


# -------------------------------------------------------------------------


@jit
def find_seam(img):
    """
    Finds index of pixel for each row in the image (seam) with least cumulative enrgy.

    Args:
        img (np.array): Image data

    Returns:
        np.array: Pixel indexes

    """
    height, width = img.shape[:2]

    # Calculate matrix with energy value for each pixel in the image
    M = calculate_energy(img=img)
    backtrack = np.zeros((height, width), dtype=np.int)

    for row_i in range(height - 1):
        lower_row_i = row_i - 1

        # Calculate for first column
        min_value_i = np.argmin(M[lower_row_i, 0:2])
        M[row_i, 0], backtrack[row_i, 0] = (
            M[row_i, 0] + M[lower_row_i, min_value_i],
            min_value_i,
        )

        # Calculate for rest of columns
        for col_i in range(1, width):
            min_value_i = np.argmin(M[lower_row_i, col_i - 1 : col_i + 2])
            M[row_i, col_i], backtrack[row_i, col_i] = (
                M[row_i, col_i] + M[lower_row_i, min_value_i],
                min_value_i + col_i - 1,
            )

    backtrack = np.flip(backtrack, axis=0)
    seam = [np.argmin(M[-1])]

    # Backtrack to find indices of seam
    for row_i in range(1, height):
        seam.append(backtrack[row_i, seam[-1]])

    return np.flip(seam)


@jit
def delete_seam(img, seam):
    """
    Removes pixels at seam indexes from an image.

    Args:
        img (np.array): Image data
        seam (np.array): Pixel indexes (one for each row)

    Returns:
        np.array: Image with removed pixels

    """
    # Deleting one pixel in each row
    return np.array(
        [np.delete(row, pixel_i, axis=0) for row, pixel_i in zip(img, seam)]
    )


@jit
def insert_seam(img, seam):
    """
    Inserts pixels at seam indexes to an image.

    Args:
        img (np.array): Image data
        seam (np.array): Pixel indexes (one for each row)

    Returns:
        np.array: Image with inserted pixels

    """
    # Inserting one pixel to each row
    return np.array(
        [
            np.insert(
                row,
                pixel_i + 1,
                np.average(row[pixel_i : pixel_i + 2], axis=0).astype(np.uint8),
                axis=0,
            )
            for row, pixel_i in zip(img, seam)
        ]
    )


@jit
def calculate_energy(img):
    """
    Calculates energy for an image as a gradient of grayscale image.

    Args:
        img (np.array): Image data

    Returns:
        np.array: Matrix with energy value of each pixel

    """
    # Convert image to grayscale
    bw_img = np.dot(img[..., :3], [0.2989, 0.5870, 0.1140])

    # Calculate gradient
    grad_x, grad_y = np.gradient(bw_img)

    return abs(grad_x) + abs(grad_y)


# -------------------------------------------------------------------------

if __name__ == "__main__":
    main()
