Nazev projektu  : BlockEditor (ICP)
Autori          : David Bazout (xbazou00) Peter Uhrin (xuhrin02)

Zakladni navod k pouziti :
Po kompilaci prikazem make a spustenim pomoci make run se zobrazi
okno editoru.

Stisknutim tlacitek s obrazky bloku na horni liste muzete pridavat bloky
do pracovni plochy.

Tlacitka save a load slouzi k nacitani drive ulozeneho schematu ze souboru
a nebo ukladani nove vytvoreneho schematu do souboru.

Tlacitko compute propocita veskere bloky. Tlacitko step slouzi ke krokovani
vypoctu. Tlacitko stop vraci vypocet na zacatek.

Dale zde naleznete rozbalovaci nabidku s moznostmi move, remove, connect a show.

Pokud zvolite moznost move, muzete zpusobem drag and drop posouvat bloky po
pracovni plose.

Moznost remove umoznuje odstranovani bloku nebo odstranovani
spoju mezi nimi.

Pri zvoleni moznosti connect je mozne spojovat bloky stisknutim
tlacitka mysi nad vystupnim portem bloku a uvolnenim tlacitka mysi nad vstupnim portem
jineho bloku.

Moznost show umoznuje zobrazovat aktualni hodnoty bloku nebo spoju mezi nimi.
Staci kliknout na dany element a jeho hodnota se zobrazi na spodni liste.

Tlacitko clean odstranuje veskere elementy z pracovni plochy.
