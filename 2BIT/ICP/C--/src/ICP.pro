TEMPLATE = app
TARGET = blockeditor
INCLUDEPATH += .
QMAKE_CXXFLAGS += -std=c++17 -pedantic -Wall -Wextra
QT += widgets gui core
HEADERS += menu.h internal.h blocks.h
SOURCES += main.cpp menu.cpp internal.cpp blocks.cpp
