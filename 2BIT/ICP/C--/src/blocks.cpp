#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <set>
#include "blocks.h"

/* Connection. */

/** @addtogroup model Model
 *  @{
 */

/** @addtogroup connection Trida Connection
 *  @{
 */

/**
 * @brief Konstruktor tridy Connection
 * @param Block* ukazatel na prvni blok
 * @param Block* ukazatel na druhy blok
 */
Connection::Connection(Block *prev, Block *next, bool position)
{
    this->value = prev->get_value();
    this->prev = prev;
    this->next = next;
    this->position = position;
}

/**
 * @brief Ziska hodnotu spojeni
 * @return double hodnota spojeni
 */
double Connection::get_value()
{
    return this->value;
}

/**
 * @brief Nastavuje hodnoty spojeni
 * @param double nova hodnota
 */
void Connection::set_value(double new_value)
{
    this->value = new_value;
}

/**
 * @brief Ziska vrstvu spojeni
 * @return int hodnota vrstvy spojeni
 */
int Connection::get_layer()
{
    return this->layer;
}

/**
 * @brief Nastavuje hodnotu vrstvy
 * @param int nova hodnota vrstvy
 */
void Connection::set_layer(int layer)
{
    this->layer = layer;
}

/**
 * @brief Nastavuje typ bloku spojeni
 * @param std::string novy typ spojeni
 */
void Connection::set_type(std::string type)
{
    this->type = type;
}

/**
 * @brief Ziska ukazatel druhy blok spojeni
 * @return Block* ukazatel na polozku
 */
Block *Connection::get_next()
{
    return this->next;
}

/**
 * @brief Ziska cislo input portu druheho bloku spojeni
 * @return bool cislo input portu druheho bloku spojeni
 */
bool Connection::get_position()
{
    return this->position;
}

/**
 * @brief Smazani spojeni
 */
void Connection::delete_connection()
{
    this->prev->set_output(0, NULL);
    this->next->set_input((position ? 0 : 1), NULL);
}

/** @}*/

/* Block. */

/** @addtogroup block Trida Block
 *  @{
 */

/**
 * @brief Konstruktor tridy Block
 * @param int id bloku
 */
Block::Block(int id, std::string type)
{
    this->id = id;
    this->type = type;
    this->value = std::nan("1");
}

/**
 * @brief Metoda pro vypis hodnot pro testovani
 */
void Block::test_block()
{
    std::cout << "ID:\t" << this->id << std::endl;
    std::cout << "LAYER:\t" << this->layer << std::endl;
    std::cout << "VALUE:\t" << this->value << std::endl;
    std::cout << "-----------------------" << std::endl;

}

/**
 * @brief Nastavuje input port bloku
 * @param unsigned cislo input portu bloku
 * @param Connection* ukazatel na spojeni
 */
void Block::set_input(unsigned idx, Connection *wire)
{
    if (this->inputs.size() > idx) {
        this->inputs[idx] = wire;
    }
}

/**
 * @brief Nastavuje output port bloku
 * @param unsigned cislo output portu bloku
 * @param Connection* ukazatel na spojeni
 */
void Block::set_output(unsigned idx, Connection *wire)
{
    if (this->outputs.size() > idx) {
        this->outputs[idx] = wire;
        if (wire != NULL) {
            this->outputs[idx]->set_type(this->type);
            this->outputs[idx]->set_layer(this->layer);
            this->outputs[idx]->set_value(this->value);
        }
    }
}

/**
 * @brief Ziska hodnotu bloku
 * @return double hodnota bloku
 */
double Block::get_value()
{
    return this->value;
}

/**
 * @brief Ziska hodnotu vrstvy bloku
 * @return int hodnota vrstvy bloku
 */
int Block::get_layer()
{
    return this->layer;
}

/**
 * @brief Ziska typ portu bloku
 * @return std::string typ portu bloku
 */
std::string Block::get_type()
{
    return this->type;
}

/**
 * @brief Ziska id bloku
 * @return int id bloku
 */
int Block::get_id()
{
    return this->id;
}

/**
 * @brief Updatuje vrstvu každého bloku v simulaci
 * @param std::set<int>& reference na mnozinu jiz videnych id bloku
 */
void Block::update_layers(std::set<int> &seen)
{
    if (seen.count(this->get_id())) {
        throw std::string("This Connection would cause loop in scheme");
    } else {
        int max_layer = 0;
        for (auto &input: this->inputs) {
            int input_layer = (input == NULL) ? 0 : input->get_layer();
            max_layer = (max_layer < input_layer) ? input_layer : max_layer;
        }

        this->layer = max_layer + 1;

        if (this->outputs.size() == 1 && this->outputs[0] != NULL) {
            seen.insert(this->get_id());
            this->outputs[0]->set_layer(this->layer);
            this->outputs[0]->get_next()->update_layers(seen);
        }
    }
}

/**
 * @brief Smaze spojeni na output portu bloku a spusti update_layers
 */
void Block::delete_output()
{
    if (this->outputs.size() == 1 && this->outputs[0] != NULL) {
        Connection *tmp = this->outputs[0];
        tmp->delete_connection();
        std::set<int> seen = std::set<int>();
        tmp->get_next()->update_layers(seen);
        delete tmp;
    }
}

/**
 * @brief Smaze blok, vsechny spojeni na neho navazane a spusti update_layers
 */
void Block::delete_block()
{
    for (unsigned i = 0; i < this->inputs.size(); i++) {
        if (this->inputs[i] != NULL) {
            Connection *tmp = this->inputs[i];
            tmp->delete_connection();
            delete tmp;
        }
    }

    this->delete_output();
}

/** @}*/

/* Compute_block. */

/** @addtogroup compute_block Trida Compute_block
 *  @{
 */

/**
 * @brief Konstruktor tridy Compute_block ktera dedi ze tridy Block
 * @param int id bloku
 * @param int typ operace bloku
 */
Compute_block::Compute_block(int id, int o, std::string type): Block(id, type)
{
    // Inicializace hodnoty.
    this->layer = 1;
    // Ukladani typu operace
    this->operation = o;
    // Nastavovani vstupu a vystupu.
    this->inputs.push_back(NULL);
    this->inputs.push_back(NULL);
    this->outputs.push_back(NULL);
}

/**
 * @brief Destruktor tridy Compute_block
 */
Compute_block::~Compute_block()
{
    return;
}

/**
 * @brief Zjisti, jestli se jedna o blok typu Input_block
 * @return bool false
 */
bool Compute_block::is_input()
{
    return false;
}

/**
 * @brief Je tu pouze kvuli polymorfismu
 * @return double nova hodnota bloku
 */
void Compute_block::set_value(double new_value)
{
    (void) new_value;
    return;
}

/**
 * @brief Vypocita novou hodnotu bloku
 */
void Compute_block::compute_value()
{
    double result = std::nan("1");
    if (this->inputs[0] != NULL && this->inputs[1] != NULL) {
        // Ziskavani hodnot na vstupech.
        double val1 = this->inputs[0]->get_value();
        double val2 = this->inputs[1]->get_value();

        // Vypocet vystupni hodnoty.
        switch (this->operation) {
            case 0:
                result = val1 + val2;
                break;
            case 1:
                result = val1 - val2;
                break;
            case 2:
                result = val1 * val2;
                break;
            case 3:
                if (val2 == 0) {
                    this->value = std::nan("1");
                    throw std::string("Division by zero");
                }
                result = val1 / val2;
                break;
            case 4:
                result = std::pow(val1, val2);
                break;
        }
    } else {
        throw std::string("Please connect inputs to all blocks");
    }

    // Nastavovani interni promenne.
    this->value = result;
    // Nastavovani hodnoty na propoj.
    if (this->outputs[0] != NULL) {
        this->outputs[0]->set_value(result);
    }
}

/** @}*/

/* Input_block */

/** @addtogroup input_block Trida Input_block
 *  @{
 */

/**
 * @brief Konstruktory tridy Input_block, ktera dedi ze tridy Block
 * @param int id bloku
 */
Input_block::Input_block(int id, std::string type): Block(id, type)
{
    // Nastaveni propoju.
    this->outputs.push_back(NULL);
    this->value = std::nan("1");
    this->layer = 0;
}

/**
 * @brief Destruktor tridy Input_block
 */
Input_block::~Input_block()
{
    return;
}

/**
 * @brief Nastavuje hodnotu bloku
 * @param double nova hodnota bloku
 */
void Input_block::set_value(double new_value)
{
    this->value = new_value;
    compute_value();
}

/**
 * @brief Zjisti, jestli se jedna o blok typu Input_block
 * @return bool true
 */
bool Input_block::is_input()
{
    return true;
}

/**
 * @brief Vypocita novou hodnotu bloku
 */
void Input_block::compute_value()
{
    if (this->outputs[0] != NULL) {
        this->outputs[0]->set_value(this->value);
    }
}

/** @}*/

/* Output_block */

/** @addtogroup output_block Trida Output_block
 *  @{
 */

/**
 * @brief Konstruktor tridy Output_block, ktera dedi ze tridy Block
 * @param int id bloku
 */
Output_block::Output_block(int id, std::string type): Block(id, type)
{
    // Nastaveni propoju.
    this->inputs.push_back(NULL);
    this->layer = 1;
}

/**
 * @brief Destruktor tridy Output_block
 */
Output_block::~Output_block()
{
    return;
}

/**
 * @brief Zjisti, jestli se jedna o blok typu Input_block
 * @return bool false
 */
bool Output_block::is_input()
{
    return false;
}

/**
 * @brief Je tu pouze kvuli polymorfismu
 * @return double nova hodnota bloku
 */
void Output_block::set_value(double new_value)
{
    (void) new_value;
    return;
}

/**
 * @brief Vypocita novou hodnotu bloku
 */
void Output_block::compute_value()
{
    if (this->inputs[0] != NULL) {
        this->value = this->inputs[0]->get_value();
    } else {
        this->value = std::nan("1");
    }
}

/** @}*/

/** @}*/
