#ifndef BLOCKS_H
#define BLOCKS_H

#include <iostream>
#include <vector>
#include <string>
#include <set>

class Block;

class Connection
{
    // Trida reprezentujici propoj mezi elementy.
    private:
        // Hodnota propojeni
        double value;
        // Vrstva
        int layer;
        // Typ
        std::string type;
        // Ukazatel na predchozi blok
        Block *prev;
        // Ukazatel na nasledujici blok
        Block *next;
        // Pozice input portu nasledujiciho bloku
        bool position;

    public:
        // Konstruktor
        Connection(Block*, Block*, bool);
        // Vraci aktualni hodnotu propoje.
        double get_value();
        // Nastavuje hodnotu propoje.
        void set_value(double);
        // Zjisti vrstvu propoje
        int get_layer();
        // Nastavi vrstvu propoje
        void set_layer(int);
        // Nastavy typ propoje
        void set_type(std::string);
        // Vrati ukazatel na dalsi blok
        Block *get_next();
        // Vrati pozidi input portu dalsiho bloku
        bool get_position();
        // Vykona potrebne věci pro pripravu smazani bloku
        void delete_connection();
};

class Block
{
    // Trida reprezentujici blok.

    protected:
        // Input a output porty bloku
        std::vector <Connection *> inputs;
        std::vector <Connection *> outputs;
        // Interni hodnota bloku.
        double value;
        // Vrstva (layer)
        int layer;
        // ID bloku
        int id;
        // Typ se kterým blok pracuje
        std::string type;

    public:
        // Konstruktor
        Block(int id, std::string);
        // Destruktor
        virtual ~Block() {}
        // Testovaci vypisy
        void test_block();
        // Spocita aktualni hodnotu bloku
        virtual void compute_value() = 0;
        // Kontrola, jestli se jedná o Input blok
        virtual bool is_input() = 0;
        // Nastavuje hodnotu bloku
        virtual void set_value(double) = 0;
        // Ziskavani hodnoty elementu.
        double get_value();
        // Funkce pro nastavovani propoju.
        void set_input(unsigned, Connection *);
        void set_output(unsigned, Connection *);
        // Zjisti vrstvu bloku
        int get_layer();
        // Zjisti s jakym typem blok pracuje
        std::string get_type();
        // Zjisti id bloku
        int get_id();
        // Updatuje vrstvy vsech nasledujicich bloku
        void update_layers(std::set<int> &);
        // Smaze propojení na output portu
        void delete_output();
        // Smaze vsechny propojeni na portech a věci důlezite pro pripraveni smazani bloku
        void delete_block();
};

class Compute_block : public Block
{
    private:
        // Vypocetni operace bloku.
        int operation;

    public:
        // Konstruktor
        Compute_block(int, int, std::string);
        // Destruktor
        ~Compute_block();
        // Funkce k prepsani
        bool is_input() override;
        void set_value(double) override;
        void compute_value() override;
};

class Input_block : public Block
{
    public:
        // Konstruktor
        Input_block(int, std::string);
        // Destruktor
        ~Input_block();
        // Funkce k prepsani
        bool is_input() override;
        void set_value(double) override;
        void compute_value() override;
};

class Output_block : public Block
{
    public:
        // Konstruktor
        Output_block(int, std::string);
        // Destruktor
        ~Output_block();
        // Funkce k prepsani
        bool is_input() override;
        void set_value(double) override;
        void compute_value() override;
};

#endif // BLOCKS_H
