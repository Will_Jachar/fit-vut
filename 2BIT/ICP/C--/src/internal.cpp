#include <iostream>
#include <sstream>
#include <cmath>
#include "internal.h"
#include "blocks.h"

/** @addtogroup controller Controller
 *  @{
 */

/** @addtogroup simulator Trida Simulator
 *  @{
 */

/**
 * @brief Vytvari novy blok v backendu
 * @param const int& typ bloku
 * @param const int& id bloku
 * @param const std::string typ hodnoty zpracovavany blokem
 */
void Simulator::slot_new_element(const int &operation, const int &id, std::string type)
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;
    Block *new_block = NULL;
    // Novy Compute_block.
    if(operation >= 0 && operation <= 4)
    {
        new_block = new Compute_block(id, operation, type);
    }
    // Novy Input_block.
    if(operation == 5)
    {
        new_block = new Input_block(id, type);
    }
    // Novy uotput_block.
    if(operation == 6)
    {
        new_block = new Output_block(id, type);
    }
    this->blocks[id] = new_block;
}

/**
 * @brief Vytvari nove spojeni v backendu
 * @param const int& id prvniho bloku
 * @param const int& id druheho bloku
 * @param const const bool& index inport portu druheho bloku
 */
void Simulator::slot_new_connection(const int &block1, const int &block2, const bool &position)
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;

    if (this->blocks[block1]->get_type() != this->blocks[block2]->get_type()) {
        emit sig_exception("Blocks are of different types");
    }

    // Vytvareni noveho spoje.
    Connection *new_connection = new Connection(this->blocks[block1], this->blocks[block2], position);
    // Propojovani spoju.
    this->blocks[block1]->set_output(0,new_connection);
    if (position) {
        this->blocks[block2]->set_input(0,new_connection);
    } else {
        this->blocks[block2]->set_input(1,new_connection);
    }

    try {
        std::set<int> seen = std::set<int>();
        this->blocks[block2]->update_layers(seen);
    } catch (std::string &msg) {
        emit sig_exception(msg);
    }
}

/**
 * @brief Smaze blok a jeho spojeni v backendu
 * @param const int& id bloku
 */
void Simulator::slot_del_element(const int &block_id)
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;
    this->blocks[block_id]->delete_block();
    delete this->blocks[block_id];
    this->blocks.erase(block_id);
}

/**
 * @brief Vytvari novy blok v backendu
 * @param const int& id bloku
 */
void Simulator::slot_del_connection(const int &block_id)
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;
    this->blocks[block_id]->delete_output();
}

/**
 * @brief Spocita hodnoty bloku
 */
void Simulator::slot_compute()
{
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;

    try {
        compute_blocks();
    } catch (std::string &msg) {
        emit sig_exception(msg);
    }
}

/**
 * @brief Dalsi krok pri krokovani schematu
 * @param int& promenna ve ktere se vraci cislo aktualne vypocitane vrstvy
 */
void Simulator::slot_step(int &out_step)
{
    if (!this->computed_blocks.empty() && this->computed_blocks.back()->get_layer() < this->step) {
        this->step = 0;
        out_step = -1;
        return;
    }

    if (this->step == 0) {
        compute_blocks();
        this->computed_layers.clear();
    }

    this->computed_layers.insert(this->step);
    out_step = this->step;
    this->step++;
}

/**
 * @brief Ukonci krokovani schematu
 */
void Simulator::slot_stop()
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();
    // Nastaveni debuggovaciho kroku na zacatek kvuli zmene ve schematu
    this->step = 0;
}

/**
 * @brief Zjisti hodnotu bloku
 * @param const int& id bloku
 * @param std::string promena pro vraceni hodnoty
 */
void Simulator::slot_get_value(const int &block_id, std::string &out_msg)
{
    std::ostringstream msg;
    if (this->blocks[block_id]->is_input()) {
        msg << this->blocks[block_id]->get_value();
    } else if (!this->computed_blocks.empty() && this->computed_layers.count(this->blocks[block_id]->get_layer())) {
        double value = this->blocks[block_id]->get_value();
        if (std::isnan(value)) {
            msg << "Value not computed (plug in both inputs)";
        } else {
            msg << value;
        }
    } else if (!this->computed_blocks.empty()) {
        msg << "Value not yet computed";
    } else {
        msg << "Scheme has changed (aplly 'compute')";
    }
    out_msg = msg.str();
}

/**
 * @brief Nastavi hodnotu input bloku
 * @param const int& id bloku
 * @param const double& nova hodnota bloku
 */
void Simulator::slot_new_value(const int &block_id, const double &new_value)
{
    if (this->blocks[block_id]->is_input()) {
        this->blocks[block_id]->set_value(new_value);
    }
}

/**
 * @brief Uklizeni pred ukoncenim programu
 */
void Simulator::slot_quit()
{
    for (auto &item: this->blocks) {
        item.second->delete_block();
        delete item.second;
    }
}

/**
 * @brief Vypisuje debugovaci informace
 */
void Simulator::slot_print_value()
{
    std::cout << "computed_blocks: ";
    for (auto &item: this->computed_blocks) {
        std::cout << item->get_layer() << ", ";
    }
    std::cout << std::endl << "computed_layers: ";
    for (auto &item: this->computed_layers) {
        std::cout << item << ", ";
    }
    std::cout << std::endl;
    for (auto &item: this->blocks) {
        item.second->test_block();
    }
}

/**
 * @brief Vypocita hodnoty vsech bloku
 */
void Simulator::compute_blocks()
{
    // Vycisteni seznamu spocitanych bloku kvuli zmene ve schematu (tudiz zneplatneni vypocitanych vysledku)
    this->computed_blocks.clear();
    this->computed_layers.clear();

    for (auto &item: this->blocks) {
        this->computed_blocks.push_back(item.second);
    }

    std::sort(this->computed_blocks.begin(), this->computed_blocks.end(), compare_layers);

    for (auto &item: this->computed_blocks) {
        try {
            item->compute_value();
        } catch (std::string &msg) {
            emit sig_exception(msg);
        }
        this->computed_layers.insert(item->get_layer());
    }
}

/**
 * @brief Pomocna funkce pro porovnani dvou vrstev bloku
 * @param Block* ukazatel na jeden blok
 * @param Block* ukazatel na druhy blok
 */
bool compare_layers(Block *block1, Block *block2)
{
    return block1->get_layer() < block2->get_layer();
}

/** @}*/

/** @}*/
