#ifndef INTERNAL_H
#define INTERNAL_H

#include <QObject>
#include "blocks.h"

class Simulator : public QObject
{
    Q_OBJECT

    signals:
        // Signalizace vyjimky
        void sig_exception(std::string);

    private:
        // Promenna pro ukladani nove pridanych bloku.
        std::map <int, Block*> blocks;
        // Promenna pouzivana pri vypoctu hodnot bloku
        std::vector <Block*> computed_blocks;
        // Promenna uchovavajici vrstvy, ktere jsou uz spocitane
        std::set <int> computed_layers;
        // Krok pri krokovani
        int step = 0;

        // Funkce provede vypocty hodnot bloku
        void compute_blocks();

    public slots:
        // Slot pro pridavani novych bloku.
        void slot_new_element(const int&, const int&, std::string);
        // Slot pro pridavani spoju mezi bloky.
        void slot_new_connection(const int&, const int&, const bool&);
        // Slot pro odstranovani bloku.
        void slot_del_element(const int&);
        // Slot pro odstranovani spoju.
        void slot_del_connection(const int&);
        // Slot pro zahajeni vypoctu
        void slot_compute();
        // Slot pro krokovani programu
        void slot_step(int&);
        // Slot pro ukonceni krokovani
        void slot_stop();
        // Slot pro prijem zadosti o hodnotu bloku.
        void slot_get_value(const int&, std::string&);
        // Slot pro nastaveni hodnoty input bloku
        void slot_new_value(const int&, const double&);
        // Slot pro signalizaci ukonceni programu
        void slot_quit();
        // Debug slot.
        void slot_print_value();
};

// Pomocna funkce pro razeni podle vrstvy
bool compare_layers(Block*, Block*);


#endif // INTERNAL_H
