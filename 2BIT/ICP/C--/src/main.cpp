#include <QMainWindow>
#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include "menu.h"
#include "internal.h"


int main(int argc, char **argv)
{
    // Vytvareni hlavniho objektu cele aplikace.
    QApplication app(argc, argv);

    // Objekt pro provadeni veskerych vypoctu.
    Simulator editor;

    // Vytvareni okna editoru.
    BlockEditorWindow window;
    window.resize(900, 700);
    window.move(150,150);
    window.setWindowTitle("BlockEditor");
    window.show();

    // Propojeni grafiky a backendu pomoci signalu.
    QObject::connect(&window, SIGNAL(msg_new_element(int,int,std::string)), &editor, SLOT(slot_new_element(int,int,std::string)),Qt::DirectConnection);
    QObject::connect(&window, SIGNAL(msg_new_connection(int,int,bool)), &editor, SLOT(slot_new_connection(int,int,bool)));
    QObject::connect(&window, SIGNAL(msg_del_element(int)), &editor, SLOT(slot_del_element(int)));
    QObject::connect(&window, SIGNAL(msg_del_connection(int)), &editor, SLOT(slot_del_connection(int)));
    QObject::connect(&window, SIGNAL(msg_get_value(int,std::string&)), &editor, SLOT(slot_get_value(int,std::string&)),Qt::DirectConnection);
    QObject::connect(&window, SIGNAL(msg_compute()), &editor, SLOT(slot_compute()));
    QObject::connect(&window, SIGNAL(msg_step(int&)), &editor, SLOT(slot_step(int&)),Qt::DirectConnection);
    QObject::connect(&window, SIGNAL(msg_stop()), &editor, SLOT(slot_stop()));
    QObject::connect(&editor, SIGNAL(sig_exception(std::string)), &window, SLOT(slot_exception(std::string)));
    QObject::connect(&window, SIGNAL(msg_new_value(int,double)), &editor, SLOT(slot_new_value(int,double)),Qt::DirectConnection);
    QObject::connect(&window, SIGNAL(msg_quit()), &editor, SLOT(slot_quit()),Qt::DirectConnection);

    // Debug vypis.
    QObject::connect(&window, SIGNAL(msg_print()), &editor, SLOT(slot_print_value()));

    // Spusteni aplikace.
    return app.exec();

}
