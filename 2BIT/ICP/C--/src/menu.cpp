// Standartni knihovny jazyka C/C++.
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <istream>
#include <stdlib.h>

// Knihovny Qt.
#include <QToolBar>
#include <QPainter>
#include <QIcon>
#include <QAction>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QPoint>
#include <QQueue>
#include <QList>
#include <QInputDialog>
#include <QLabel>
#include <QFileDialog>
#include <QLine>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QComboBox>
#include <QStatusBar>

// Import vlastnich modulu.
#include "menu.h"

/** @addtogroup view View
 *  @{
 */

/* Operace s pracovni plochou. */

/** @addtogroup pracovni_plocha Operace s pracovni plochou
 *  @{
 */

/**
 * @brief Funkce pro odstraneni vsech polozek z pracovni plochy.
 */
void BlockEditorWindow::clean_all()
{
    // Mazani bloku z backendu.
    for(auto block : elements)
    {
        emit msg_del_element(element_ids.value(block));
        block->hide();
    }
    // Mazani bloku z grafiky.
    elements.clear();
    element_ids.clear();
    element_types.clear();
    connections.clear();
    // Prekresleni vysledku.
    redraw_connections();
}

/**
 * @brief Slot pro odstraneni vsech polozek z pracovni plochy.
 */
void BlockEditorWindow::clean_workspace()
{
    clean_all();
}

/**
 * @brief Slot realizujici pridani noveho bloku do pracovni plochy.
 */
void BlockEditorWindow::add_element()
{
    static int id = 0;
    QObject *s = sender();
    // Rozpoznavani odesilatele zpravy.
    int i;
    QPixmap *element = 0;
    for(i = 0; i < icons.count() ; i++)
    {
        if(s == icon_actions[i])
        {
            element = icons[i];
            break;
        }
    }
    // Umistovani noveho elementu.
    place_element(*element,0,0,i,id);
    id++;
}

/**
 * @brief Pridava novy blok do pracovni plochy.
 * @param QPixmap obrazek bloku
 * @param int souradnice x
 * @param int souradnice y
 * @param int typ bloku
 * @param int identifikator bloku
 */
void BlockEditorWindow::place_element(QPixmap element,int x, int y,int type,int id)
{
    //Resizing.
    element = element.scaled(1000,50,Qt::KeepAspectRatio);
    // Placing image into label widget.
    qreal x1 = x;
    qreal y1 = y;
    QGraphicsPixmapItem *pixmap = scene->addPixmap(element);
    pixmap->setPos(x1,y1);
    // Ukladani informaci o elementech.
    elements.append(pixmap);
    element_ids.insert(pixmap,id);
    element_types.insert(pixmap,type);
    // Odesilani signalu.
    emit msg_new_element(type,id,"double");
    // Odesilani hodnoty vstupniho bloku.
    if(type == 5)
    {
        dialog_send_value(id);
    }
}

/** @}*/

/* Reakce na ovladani mysi.*/

/** @addtogroup ovladani_mysi Ovladani mysi
 *  @{
 */

/**
 * @brief Ziskava ukazatel na polozku na dane pozici.
 * @param QPointF pozice na pracovni plose
 * @return QGraphicsItem* ukazatel na polozku
 */
QGraphicsItem * BlockEditorWindow::get_item(QPointF point)
{
    // Tolerance pri kliknuti.
    qreal prec = 5;
    QList<QGraphicsItem*> list = scene->items(QRectF(point.x()-prec/2,point.y()-prec/2,prec,prec));
    // Polozka nejblize kliknuti.
    if(list.size() > 0) 
    {
        return list.first();
    }
    return NULL;
}

/**
 * @brief Handler pro zpracovani udalosti z graficke sceny.
 * @param QObject* puvodce udalosti
 * @param QEvent* udalost
 * @return bool uspesnost zpracovani udalosti
 */
bool BlockEditorWindow::eventFilter(QObject *target, QEvent *event)
{
    QPointF end_point;
    static QGraphicsItem *item = NULL;
    // Pretypovani promenne event.
    const QGraphicsSceneMouseEvent* const mouse_event = static_cast<const QGraphicsSceneMouseEvent*>(event);
    
    // Reakce na stisk tlacitka mysi.
    if (event->type() == QEvent::GraphicsSceneMousePress && target == scene)
    {
        item = get_item(mouse_event->scenePos());
    }

    // Reakce na posun kurzoru mysi.
    if (event->type() == QEvent::GraphicsSceneMouseMove && target == scene && settings->currentIndex() == 0)
    {
        end_point = mouse_event->scenePos();
        // Zpracovani ovladani mysi.
        mouse_control(item,end_point);                
    }

    // Reakce na uvolneni tlacitka mysi.
    if (event->type() == QEvent::GraphicsSceneMouseRelease && target == scene)
    {
        end_point = mouse_event->scenePos();
        // Zpracovani ovladani mysi.
        mouse_control(item,end_point);   
    }
    return QMainWindow::eventFilter(target, event);
}

/**
 * @brief Handler pro zpracovani presouvani bloku.
 * @param QGraphicsItem* polozka na miste stisknuti tlacitka mysi
 * @param QPointF misto uvolneni tlacitka mysi
 */
void BlockEditorWindow::mouse_move(QGraphicsItem *item_press, QPointF release)
{
    // Omezujici podminky pro presun.
    if(item_press == NULL || item_press->type() != 7)    {   return; }
    // Presouvani elementu.
    item_press -> setPos(release.x(),release.y());
    // Prekreslovani car.
    redraw_connections();
}

/**
 * @brief Handler pro zpracovani odstranovani bloku nebo spojeni.
 * @param QGraphicsItem* polozka na miste stisknuti tlacitka mysi
 */
void BlockEditorWindow::mouse_remove(QGraphicsItem *item_press)
{
    if(item_press == NULL) {    return; }
    QGraphicsItemGroup *group = item_press->group();
    // Mazani propoje.
    if(group != NULL)
    {
        // Ziskavani id pocatecniho hradla.
        QGraphicsItem *start = group_src.value(group);
        int id_bloku = element_ids.value(start);
        // Mazani zaznamu o spoji.
        connections.remove(start);
        pin_position.remove(start);
        // Odesilani informaci backgroundu.
        emit msg_del_connection(id_bloku);
        group->hide();
        redraw_connections();
    }
    // Mazani elementu. - musim odstranit i jeho spoje v grafice.
    else
    {
        // Blok byl cilovy.
        QList<QGraphicsItem *> to_delete = connections.keys(item_press);
        for(int i = 0 ; i<to_delete.count();i++)
        {
            connections.remove(to_delete[i]);
        }
        // Promazat connections a pin_position.
        connections.remove(item_press);
        pin_position.remove(item_press);
        elements.removeOne(dynamic_cast<QGraphicsPixmapItem*>(item_press));
        // Odesilani informaci backendu.
        emit msg_del_element(element_ids.value(item_press));
        // Prekreslovani sceny.
        redraw_connections();
        scene->removeItem(item_press);
    }
}

/**
 * @brief Handler pro zpracovani spojovani bloku.
 * @param QGraphicsItem* polozka na miste stisknuti tlacitka mysi
 * @param QGraphicsItem* polozka na miste uvolneni tlacitka mysi
 * @param QPointF misto uvolneni tlacitka mysi
 */
void BlockEditorWindow::mouse_connect(QGraphicsItem *item_press, QGraphicsItem *item_release, QPointF release)
{
    // Omezujici podminky pro vytvoreni spojeni.
    if(item_press == NULL || item_release == NULL || item_press == item_release )
    {    
        return; 
    }
    if(element_types.value(dynamic_cast<QGraphicsPixmapItem *>(item_release)) == 5)
    {
        return;
    }
    if(element_types.value(dynamic_cast<QGraphicsPixmapItem *>(item_press)) == 6)
    {
        return;
    }
    // Vypocet pozice vstupu.
    bool first_pin = true;
    // Output block ma jenom jeden vstup.
    if(element_types.value(dynamic_cast<QGraphicsPixmapItem *>(item_release)) != 6)
    {
        pin_pos(item_release,&release, &first_pin);
    }
    // Graficke znazorneni propoje a ukladani udaju o nem.
    save_connection(item_press,item_release,first_pin);
}

/**
 * @brief Handler pro zpracovani zobrazeni hodnoty bloku.
 * @param QGraphicsItem* polozka na miste stisknuti tlacitka mysi
 */
void BlockEditorWindow::mouse_show(QGraphicsItem *item_press)
{
    // Konfigurace zvyrazneni.  
    QPen pen;
    pen.setWidth(2);
    pen.setColor(Qt::red);
    // Nebyla stisknuta zadna polozka.
    if(item_press == NULL)  {   return; }
    QGraphicsItem *start;
    QGraphicsItemGroup *group = item_press->group();
    // Hodnota spoje.
    if(group != NULL)
    {
        start = group_src.value(group);
        QRectF rectangle = group->boundingRect();
        highlighting = scene->addRect(rectangle,pen);
        show_value(element_ids.value(start));
    }
    // Hodnota bloku.
    else if(elements.indexOf(dynamic_cast<QGraphicsPixmapItem*>(item_press)) != -1)
    {
        start = item_press;
        QPointF pt = item_press->scenePos();
        highlighting = scene->addRect(0,0,120,70,pen);
        highlighting->setPos(pt.x()-10,pt.y()-10);
        show_value(element_ids.value(start));
    }
}

/**
 * @brief Funkce zajistujici korektni reakce na ovladani mysi.
 * @param QGraphicsItem* polozka na ktere byla mys stisknuta
 * @param QPointF bod uvolneni tlacitka mysi
 */
void BlockEditorWindow::mouse_control(QGraphicsItem *item_press,QPointF release)
{
    // Detekce polozek v oblasti kliknuti.
    QGraphicsItem *item_release = get_item(release);
    
    // Presouvani elementu.
    if(settings->currentIndex() == 0)   {   mouse_move(item_press,release);   }
    
    // Odstranovani elementu.
    if(settings->currentIndex() == 1)   {   mouse_remove(item_press);         }
    
    // Propojovani elementu.
    if(settings->currentIndex() == 2)   {   mouse_connect(item_press,item_release,release);  }

    // Odstranovani highlighting.
    if(highlighting != NULL)    { highlighting->hide();   }

    // Zobrazovani hodnot bloku na liste a zvyrazneni.
    if(settings->currentIndex() == 3)   {   mouse_show(item_press);         }
        
}
/** @}*/

/* Funkce pro prace se spoji elementu.*/

/** @addtogroup spoje_bloku Prace se spoji bloku
 *  @{
 */

/**
 * @brief Vykresluje spoj mezi zadanymi souradnicemi.
 * @param int x-ova souradnice pocatku
 * @param int y-ova souradnice pocatku
 * @param int x-ova souradnice koncoveho bodu
 * @param int y-ova souradnice koncoveho bodu
 * @return QGraphicsItemGroup* ukazatel na skupinu polozek spoje
 */
QGraphicsItemGroup *BlockEditorWindow::draw_connection(int x1,int y1,int x2, int y2)
{
    // Skupina polozek realizujici propoj.
    QList<QGraphicsItem *> connection;
    // Nastaveni sily cary.
    QPen pen;
    pen.setWidth(2);
    // Kresleni pravouhle cary pro kladne souradnice.
    int mid = std::abs((x2+x1)/2);
    connection.append(scene->addLine(x1,y1,mid,y1,pen));
    connection.append(scene->addLine(mid,y1,mid,y2,pen));
    connection.append(scene->addLine(mid,y2,x2,y2,pen));
    // Kresleni kruhu kolem propoje.
    qreal size = 5;
    pen.setWidth(size);
    connection.append(scene->addEllipse(QRectF(x1-size/2,y1-size/2,size,size),pen));
    connection.append(scene->addEllipse(QRectF(x2-size/2,y2-size/2,size,size),pen));
    // Vraci skupinu polozek, ktere tvori spoj.
    return scene->createItemGroup(connection);
}

/**
 * @brief Prekresluje spoje v pripade posunu bloku.
 */
void BlockEditorWindow::redraw_connections()
{
    // Prekresli veskere spoje po presunuti hradla.
    for(int i = 0; i< connection_items.count(); i++)
    {
        connection_items[i]->hide();
    }
    connection_items.clear();
    // Nakresleni novych spojeni.
    for(int i = 0; i<elements.count();i++)
    {
        QGraphicsItem *start = elements[i];
        QGraphicsItem *end = connections.value(start);
        bool first_pin = pin_position.value(start);
        if(start != NULL && end != NULL)
        {
            // Vypis vysledku.
            QPointF point = pin_pos(start);
            QPointF end_point = pin_pos(end,first_pin);
            //Aktualizace udaju o spoji.
            QGraphicsItem* start = get_item(point);
            // Graficke znazorneni propoje.
            QGraphicsItemGroup *group = draw_connection(point.x(),point.y(),end_point.x(),end_point.y());
            group_src.insert(group,start);
            connection_items.append(group);
        }
    }
}

/**
 * @brief Uklada propoj do systemu.
 * @param QGraphicsItem* ukazatel na pocatecni blok
 * @param QGraphicsItem* ukazatel na koncovy blok
 * @param bool prvni/druhy vstup
 */
void BlockEditorWindow::save_connection(QGraphicsItem *item_press, QGraphicsItem *item_release, bool first_pin)
{
    // Ziskavani bodu pro vykresleni.
    QPointF point = pin_pos(item_press);
    QPointF end_point = pin_pos(item_release,first_pin);
    // Vykreslovani spoje.
    QGraphicsItemGroup *group = draw_connection(point.x(),point.y(),end_point.x(),end_point.y());
    // Ukladani udaju o spoji.
    group_src.insert(group,item_press);
    connection_items.append(group);
    connections.insert(item_press,item_release);
    pin_position.insert(item_press,first_pin);
    // Ukladani propoje do backendu.
    emit msg_new_connection(element_ids.value(item_press), element_ids.value(item_release),pin_position.value(item_press));

}

/** @}*/

/* Funkce pro vypocet pozice pinu elementu.*/

/** @addtogroup pozice_pinu Vypocet pozice pinu
 *  @{
 */

/**
 * @brief Ziskava souradnice vstupniho pinu bloku.
 * @param QGraphicsItem* ukazatel na blok
 * @param bool prvni/druhy vstup bloku
 * @return QPointF souradnice vstupniho pinu
 */
QPointF BlockEditorWindow::pin_pos(QGraphicsItem *item,bool first_pin)
{
    qreal ypos;
    qreal xpos = item->x() + 3;
    if(first_pin)
    {
        ypos = item->y() + 14;
    }
    else
    {
        ypos = item->y() + 37;
    }
    QPointF point = QPointF(xpos,ypos);
    return point;
}

/**
 * @brief Ziskava souradnice vstupniho pinu bloku.
 * @param int x-ova souradnice bloku
 * @param int y-ova souradnice bloku
 * @param int prvni/druhy vstup bloku
 * @return QPointF souradnice vstupniho pinu
 */
QPointF BlockEditorWindow::pin_pos(int x, int y,int first_pin)
{
    qreal ypos;
    qreal xpos = x + 3;
    if(first_pin)
    {
        ypos = y + 14;
    }
    else
    {
        ypos = y + 37;
    }
    QPointF point = QPointF(xpos,ypos);
    return point;
}

/**
 * @brief Ziskava souradnice vstupniho pinu bloku.
 * @param QGraphicsItem* ukazatel na blok
 * @param QPointF* misto uvolneni tlacitka mysi
 * @param bool* prvni/druhy vstupni pin
 * @return QPointF souradnice vstupniho pinu
 */
QPointF BlockEditorWindow::pin_pos(QGraphicsItem *item,QPointF *click, bool *first_pin)
{
    // Vraci souradnice spravneho vstupniho pinu hradla predaneho parametrem.
    qreal ypos;
    qreal xpos = item->x() + 3;
    if(click->y() - item->y() < 25)
    {
        ypos = item->y() + 14;
        *first_pin = true;
    }
    else
    {
        ypos = item->y() + 37;
        *first_pin = false;
    }
    QPointF point = QPointF(xpos,ypos);
    return point;
}

/**
 * @brief Ziskava souradnice vystupniho pinu bloku.
 * @param QGraphicsItem* ukazatel na blok
 * @return QPointF souradnice vystupniho pinu
 */
QPointF BlockEditorWindow::pin_pos(QGraphicsItem *item)
{
    // Vraci souradnice vystupniho pinu hradla predaneho parametrem.
    qreal xpos = item->x() + 95;
    qreal ypos = item->y() + 25;
    QPointF point = QPointF(xpos,ypos);
    return point;
}

/**
 * @brief Ziskava souradnice vystupniho pinu bloku.
 * @param int x-ova souradnice bloku
 * @param int y-ova souradnice bloku
 * @return QPointF souradnice vystupniho pinu
 */
QPointF BlockEditorWindow::pin_pos(int x, int y)
{
    // Vraci souradnice vystupniho pinu hradla predaneho parametrem.
    qreal xpos = x + 95;
    qreal ypos = y + 25;
    QPointF point = QPointF(xpos,ypos);
    return point;
}
/** @}*/

/* Ukladani do souboru. */

/** @addtogroup files Prace se souborem
 *  @{
 */

/**
 * @brief Uklada pracovni plochu do souboru.
 */
void BlockEditorWindow::save_scheme()
{
    // Ziskavani cesty k souboru.
    QString filename = QFileDialog::getSaveFileName(this);
    // Otevirani souboru pro ulozeni dat.
    std::ofstream file;
    file.open(filename.toStdString().c_str());
    // Unikatni string pro identifikaci ulozeneho schematu.
    file << "blockeditorscheme" << std::endl;
    // Ukladani elementu.
    for(int i = 0; i < elements.count() ; i++)
    {
        QGraphicsPixmapItem *item = elements[i];
        file << "element,"<< element_ids.value(item) <<","<< element_types.value(item) << "," << item->x() << "," << item->y() << std::endl;
    }
    // Ukladani spoju mezi elementy.
    for(auto wire:connections.toStdMap())
    {
        QGraphicsItem *start = wire.first;
        QGraphicsItem *end = wire.second;
        bool first_pin = pin_position.value(start);
        file << "wire," << first_pin << "," << element_ids.value(start) << "," << element_ids.value(end) << std::endl;
    }
    // Zavirani souboru.
    file.close();
}

/**
 * @brief Nacita pracovni plochu ze souboru.
 */
void BlockEditorWindow::load_scheme()
{
    // Ziskavani cesty k souboru.
    QString filename = QFileDialog::getOpenFileName(this);
    // Vycisteni pracovni plochy pred nacitanim.
    clean_all();
    // Otevirani souboru pro ulozeni dat.
    std::ifstream file;
    file.open(filename.toStdString().c_str());
    std::string line,data;
    // Kontrola unikatniho stringu na zacatku souboru.
    std::getline(file,line);
    if(line != "blockeditorscheme")
    {
        msg_box.setText("File type not supported!");
        msg_box.setStandardButtons(QMessageBox::Ok);
        msg_box.exec();
        return;
    }
    // Nacitani schematu ze souboru.
    while(std::getline(file,line))
    {
        // Vector pro ukladani udaju o elementu.
        std::vector <std::string> config;
        // Nacitani jedne polozky sceny.
        std::stringstream line_stream(line);
        while(std::getline(line_stream,data,','))
        {
            config.push_back(data);
        }
        // Pridavani elementu na scenu.
        if(config[0] == "element")
        {
            QPixmap *pixmap = icons[std::stoi(config[2])];
            place_element(*pixmap,std::stoi(config[3]),std::stoi(config[4]),std::stoi(config[2]),std::stoi(config[1]));
        }
        // Pridavani spoje na scenu.
        else
        {
            // Ziskavani dat ze souboru.
            bool first_pin = (bool)std::stoi(config[1]);
            QGraphicsItem *item_press = element_ids.key(std::stoi(config[2]));
            QGraphicsItem *item_release = element_ids.key(std::stoi(config[3]));
            // Ukladani dat do systemu.
            save_connection(item_press,item_release,first_pin);
        }
    }
    // Zavirani souboru.
    file.close();
}
/** @}*/

/* Komunikace s backendem. */

/** @addtogroup backend Komunikace s backendem
 *  @{
 */

/**
 * @brief Slot pro vytvoreni dialogoveho okna s vypisem vyjimky.
 * @param std::string text popisujici vyjimku
 */
void BlockEditorWindow::slot_exception(std::string exception)
{
    // Vytvoreni dialogoveho okna s chybou.
    msg_box.setText(exception.c_str());
    msg_box.setStandardButtons(QMessageBox::Ok);
    msg_box.exec();
}

/**
 * @brief Posila zadost o novy krok a vypisuje informace do statusbaru.
 */
void BlockEditorWindow::next_step()
{
    // Ziskavani pozice aktualniho layeru.
    int layer;
    emit msg_step(layer);
    std::string msg;
    // Vypis zpravy o layeru.
    if(layer != -1)
    {
        msg = "Actually computed layer : ";
        msg += std::to_string(layer);
    }
    else
    {
        msg = "Debuging has reached an end.";
    }
    statusBar()->showMessage(QString(msg.c_str()));
}

/**
 * @brief Ziskava z backendu informaci o hodnote bloku
 * @param int identifikator bloku
 * @return aktualni hodnota bloku
 */
std::string BlockEditorWindow::get_value(int block_id)
{
    std::string value;
    // Odesilani zadosti do backendu.
    emit msg_get_value(block_id, value);
    // Vraceni ziskanych dat.
    return value;
}

/**
 * @brief Zobrazuje hodnotu bloku na statusbaru.
 * @param int identifikator bloku
 */
void BlockEditorWindow::show_value(int block_id)
{
    // Vypis hodnoty do listy.
    std::string msg = "Value of element : ";
    msg += get_value(block_id);
    statusBar()->showMessage(QString(msg.c_str()));
}

/**
 * @brief Odesila novou hodnotu bloku do backendu.
 * @param int identifikator bloku
 */
void BlockEditorWindow::dialog_send_value(int block_id)
{
    // Dialogove okno pro ziskavani hodnoty.
    double value = QInputDialog::getDouble(this,tr("Value of input block :"),tr("Value of input block :"));
    // Odesilani hodnoty do backendu.
    emit msg_new_value(block_id,value);
}

/** @}*/

/* Inicializace okna editoru.*/


void BlockEditorWindow::exit_application()
{
    emit msg_quit();
    QApplication::quit();
}

/**
 * @brief Inicializace okna s pracovni plochou.
 * @param QWidget* rodicovske okno
 */
BlockEditorWindow::BlockEditorWindow(QWidget *parent): QMainWindow(parent) 
{
    // Nacitani obrazku ikon panelu nastroju.
    const char* icon_names[] = {"img/add.png","img/sub.png","img/mul.png","img/div.png","img/pow.png","img/input.png","img/output.png"};
    for(int i = 0; i<7; i++ )
    {
        QPixmap *icon = new QPixmap(icon_names[i]);
        icons.append(icon);
    }
    
    // Vytvareni panelu nastroju.
    toolbar = addToolBar("toolbar");
    toolbar->setMovable(false);

    // Pridavani polozek do panelu nastroju.
    for(int i = 0; i<7 ; i++ )
    {
        QAction *new_action = toolbar->addAction(QIcon(*icons[i]),"");
        connect(new_action,SIGNAL(triggered()),this,SLOT(add_element()));
        icon_actions.append(new_action);
    }

    // Tlacitko pro ukladani schematu.
    toolbar->addSeparator();
    QAction *save = toolbar->addAction("Save");
    connect(save,SIGNAL(triggered()),this,SLOT(save_scheme()));
    QAction *load = toolbar->addAction("Load");
    connect(load,SIGNAL(triggered()),this,SLOT(load_scheme()));

    // Tlacitko pro spusteni vypoctu.
    toolbar->addSeparator();
    QAction *compute = toolbar->addAction("Compute");
    connect(compute,SIGNAL(triggered()),this,SIGNAL(msg_compute()));
    QAction *step = toolbar->addAction("Step");
    connect(step,SIGNAL(triggered()),this,SLOT(next_step()));
    QAction *stop = toolbar->addAction("Stop");
    connect(stop,SIGNAL(triggered()),this,SIGNAL(msg_stop()));

    // Nastaveni provadene operace.
    toolbar->addSeparator();
    settings = new QComboBox();
    settings->addItem("Move");
    settings->addItem("Remove");
    settings->addItem("Connect");
    settings->addItem("Show");
    toolbar->addWidget(settings);

    // Tlacitko na vycisteni pracovni plochy.
    toolbar->addSeparator();
    QAction *clean = toolbar->addAction("Clean");
    connect(clean,SIGNAL(triggered()),this,SLOT(clean_workspace()));

    // Polozka pro ukonceni aplikace.
    toolbar->addSeparator();
    QAction *quit = toolbar->addAction("Quit Application");
    connect(quit,SIGNAL(triggered()),this,SLOT(exit_application()));
    
    // Vytvareni graficke sceny.
    view = new QGraphicsView();
    scene = new QGraphicsScene();
    scene->installEventFilter(this);
    view->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    view->setScene(scene);
    setCentralWidget(view);

    // Inicializace message boxu.
    statusBar()->showMessage("");

}

/** @}*/
