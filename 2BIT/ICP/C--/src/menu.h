#ifndef MENU_H
#define MENU_H

// Knihovny Qt.
#include <QMainWindow>
#include <QApplication>
#include <QGridLayout>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QPainter>
#include <QMouseEvent>
#include <QPoint>
#include <QLine>
#include <QQueue>
#include <QList>
#include <QMap>
#include <QLabel>
#include <QStatusBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QComboBox>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>

// Trida reprezentujici okno editoru.
class BlockEditorWindow : public QMainWindow 
{
    // Makro nutne uvest v kazdem Qt objektu.
    Q_OBJECT

    signals:
        // Pomocny signal pro ladici vypis.
        void msg_print();
        // Novy blok.
        void msg_new_element(const int&,const int&,std::string);
        // Novy spoj.
        void msg_new_connection(const int&,const int&,const bool&);
        // Odstraneni bloku.
        void msg_del_element(const int&);
        // Odstraneni spoje.
        void msg_del_connection(const int&);
        // Spusteni vypoctu.
        void msg_compute();
        // Zadost o hodnotu bloku.
        void msg_get_value(const int&,std::string&);
        // Zadost o krok vypoctu.
        void msg_step(int&);
        // Zahozeni vypocitanych hodnot.
        void msg_stop();
        // Nova hodnota vstupniho bloku.
        void msg_new_value(int,double);
        // Signal o ukonceni programu.
        void msg_quit();

    public slots:
        // Slot pro ukonceni aplikace.
        void exit_application();
        // Slot pro odchytavani vyjimek.
        void slot_exception(std::string);
        // Krokovani vypoctu.
        void next_step();
        // Vycisteni pracovni plochy.
        void clean_workspace();
        // Slot pro pridavani elementu do graficke sceny.
        void add_element();
        // Ukladani schemat do souboru.
        void save_scheme();
        // Nacitani schemat ze souboru.
        void load_scheme();
    
    public:
        // Konstruktor editoru.
        BlockEditorWindow(QWidget *parent = 0);
    
    protected:
        
        // Handler pro sledovani mysi.
        bool eventFilter(QObject *target, QEvent *event);

        /* Ovladani mysi.*/

        // Posun bloku.
        void mouse_move(QGraphicsItem *, QPointF);
        // Odstranovani elementu.
        void mouse_remove(QGraphicsItem *);
        // Spojovani bloku.
        void mouse_connect(QGraphicsItem *,QGraphicsItem *,QPointF);
        // Zobrazovani hodnot.
        void mouse_show(QGraphicsItem *);
        // Polozka na zadane pozici.
        QGraphicsItem *get_item(QPointF point);
        // Ovladani mysi.
        void mouse_control(QGraphicsItem *item_press,QPointF release);
        // Zvyrazneni elementu.
        QGraphicsItem *highlighting = NULL;

        /* Funkce pro prace se spoji elementu. */

        // Prekresleni vsech spoju.
        void redraw_connections();
        // Ukladani spoje do systemu.
        void save_connection(QGraphicsItem *, QGraphicsItem *, bool);
        // Vykresleni noveho spoje.
        QGraphicsItemGroup * draw_connection(int x1,int y1, int x2, int y2);
        
        /* Funkce pro vypocet pozice pinu elementu. */

        QPointF pin_pos(QGraphicsItem *item);
        QPointF pin_pos(QGraphicsItem *item,QPointF *point,bool *first_pin);
        QPointF pin_pos(QGraphicsItem *item,bool first_pin);
        QPointF pin_pos(int x, int y);
        QPointF pin_pos(int x, int y,int first_pin);

        /* Ostatni funkce. */

        // Dialogove okno pro hodnotu vstupniho bloku.
        void dialog_send_value(int block_id);
        // Ziskavani hodnoty bloku.
        std::string get_value(int block_id);
        // Zobrazovani hodnoty do statusbaru.
        void show_value(int block_id);
        // Pridavani elementu do graficke sceny.
        void place_element(QPixmap,int,int,int,int);
        // Odstranovani vsech polozek pracovni plochy.
        void clean_all();

        /* Atributy editoru. */

        // Obrazky panelu nastroju.
        QList <QPixmap *> icons;
        QList <QAction *> icon_actions;
        // Dialogove okno.
        QMessageBox msg_box;
        // Panel nastroju.
        QToolBar *toolbar;
        QComboBox *settings;
        QStatusBar *status_bar;
        QLabel *status_text;
        // Nastroj pro zobrazovani sceny.
        QGraphicsScene *scene;
        QGraphicsView *view;
        // Informace o realizovanych propojich.
        QMap <QGraphicsItem *, QGraphicsItem *> connections;
        QMap <QGraphicsItem *, bool> pin_position;
        // Graficka reprezentace propoju.
        QMap <QGraphicsItemGroup *,QGraphicsItem*> group_src;
        QList <QGraphicsItemGroup *> connection_items;
        // Informace o elementech.
        QList <QGraphicsPixmapItem *> elements;
        QMap <QGraphicsItem *, int> element_ids;
        QMap <QGraphicsPixmapItem *, int> element_types;
};

#endif // MENU_H
