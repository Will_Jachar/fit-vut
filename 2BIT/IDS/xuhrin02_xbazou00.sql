-- IDS - Projekt 2018

-- Peter Uhrin (xuhrin02)
-- David Bazout (xbazou00)

-- Drop starych tabulek

DROP TABLE neandrtalec CASCADE CONSTRAINTS;
DROP TABLE lovecka_vyprava CASCADE CONSTRAINTS;
DROP TABLE mamuti_jama CASCADE CONSTRAINTS;
DROP TABLE mamut CASCADE CONSTRAINTS;
DROP TABLE hlidka CASCADE CONSTRAINTS;
DROP TABLE lovec CASCADE CONSTRAINTS;
DROP TABLE zprava CASCADE CONSTRAINTS;
DROP TABLE lovec_se_ucastni CASCADE CONSTRAINTS;
DROP TABLE vycet_mamutu CASCADE CONSTRAINTS;
DROP TABLE vyprava_lovi CASCADE CONSTRAINTS;
DROP TABLE byl_uloven CASCADE CONSTRAINTS;

-- Drop starych sekvenci

DROP SEQUENCE neandrtalec_seq;
DROP SEQUENCE lovecka_vyprava_seq;
DROP SEQUENCE mamuti_jama_seq;
DROP SEQUENCE mamut_seq;
DROP SEQUENCE zprava_seq;

-- Vytvareni novych sekvenci

CREATE SEQUENCE neandrtalec_seq START WITH 1 INCREMENT BY 1 NOCYCLE;
CREATE SEQUENCE lovecka_vyprava_seq START WITH 1 INCREMENT BY 1 NOCYCLE;
CREATE SEQUENCE mamuti_jama_seq START WITH 1 INCREMENT BY 1 NOCYCLE;
CREATE SEQUENCE mamut_seq START WITH 1 INCREMENT BY 1 NOCYCLE;
CREATE SEQUENCE zprava_seq START WITH 1 INCREMENT BY 1 NOCYCLE;

-- Vytvareni novych tabulek

CREATE TABLE neandrtalec (
    id_neandrtalec NUMBER NOT NULL PRIMARY KEY,
    jmeno VARCHAR2(32) NOT NULL,
    prijmeni VARCHAR2(32) NOT NULL,
    datum_narozeni DATE NOT NULL,
    zdravi VARCHAR2(7) CHECK (zdravi IN ('zdravy', 'nemocny', 'mrtvy')) NOT NULL,
    dovednosti VARCHAR2(128)
);

CREATE TABLE lovecka_vyprava (
    id_lovecka_vyprava NUMBER NOT NULL PRIMARY KEY,
    zahajeni DATE NOT NULL,
    stav VARCHAR2(8) CHECK (stav IN ('bezici', 'skoncila')) NOT NULL,
    uspesnost VARCHAR2(9) CHECK (uspesnost IN ('uspesna', 'neuspesna'))
);

CREATE TABLE mamuti_jama (
    id_mamuti_jama NUMBER NOT NULL PRIMARY KEY,
    id_lovecka_vyprava NUMBER REFERENCES lovecka_vyprava(id_lovecka_vyprava),
    lokace VARCHAR2(23) CHECK (REGEXP_LIKE (lokace, '-?\d{1,3}.\d{1,6} -?\d{1,3}.\d{1,6}'))  -- souradnice ve tvaru (-)DDD.DD (-)DDD.DD
);

CREATE TABLE mamut (
    id_mamut NUMBER NOT NULL PRIMARY KEY,
    zvalstni_znaky VARCHAR2(128),
    zvyky VARCHAR2(128),
    charakterove_vlastnosti VARCHAR2(128)
);

CREATE TABLE hlidka (
    id_hlidka NUMBER NOT NULL PRIMARY KEY REFERENCES neandrtalec(id_neandrtalec) ON DELETE CASCADE,
    stanoviste NUMBER NOT NULL
);

CREATE TABLE lovec (
    id_lovec NUMBER NOT NULL PRIMARY KEY REFERENCES neandrtalec(id_neandrtalec) ON DELETE CASCADE,
    uslapan_mamutem NUMBER REFERENCES mamut(id_mamut)
);

CREATE TABLE zprava (
    id_zprava NUMBER NOT NULL PRIMARY KEY,
    id_hlidka NUMBER NOT NULL REFERENCES hlidka(id_hlidka),
    odeslano DATE NOT NULL
);

CREATE TABLE lovec_se_ucastni (
    id_lovec NUMBER NOT NULL REFERENCES lovec(id_lovec),
    id_lovecka_vyprava NUMBER NOT NULL REFERENCES lovecka_vyprava(id_lovecka_vyprava),
    PRIMARY KEY (id_lovec, id_lovecka_vyprava)
);

CREATE TABLE vycet_mamutu (
    id_zprava NUMBER NOT NULL REFERENCES zprava(id_zprava),
    id_mamut NUMBER NOT NULL REFERENCES mamut(id_mamut),
    PRIMARY KEY (id_zprava, id_mamut)
);

CREATE TABLE vyprava_lovi (
    id_lovecka_vyprava NUMBER NOT NULL REFERENCES lovecka_vyprava(id_lovecka_vyprava),
    id_mamut NUMBER NOT NULL REFERENCES mamut(id_mamut),
    PRIMARY KEY (id_lovecka_vyprava, id_mamut)
);

CREATE TABLE byl_uloven (
    id_mamut NUMBER NOT NULL REFERENCES mamut(id_mamut),
    id_lovec NUMBER NOT NULL REFERENCES lovec(id_lovec),
    id_lovecka_vyprava NUMBER NOT NULL REFERENCES lovecka_vyprava(id_lovecka_vyprava),
    id_mamuti_jama NUMBER NOT NULL REFERENCES mamuti_jama(id_mamuti_jama),
    PRIMARY KEY (id_mamut, id_lovec, id_lovecka_vyprava, id_mamuti_jama)
);

-------------------------------------------------------------------------------

-- Trigger pro autoinkrementaci PK pro tabulku 'neandrtalec'
CREATE OR REPLACE TRIGGER neandrtalec_trigger BEFORE INSERT ON neandrtalec
    FOR EACH ROW
    BEGIN
        SELECT neandrtalec_seq.NEXTVAL
        INTO : NEW.id_neandrtalec
        FROM dual;
    END;
/

-------------------------------------------------------------------------------

-- Procedura zjisti, jaka byla procentualni uspesnost lovecke vypravy
-- (kolik mamutu bylo loveno / kolik bylo uloveno)
CREATE OR REPLACE PROCEDURE uspesnost_vypravy(id_lovecka_vyprava_arg lovecka_vyprava.id_lovecka_vyprava%TYPE) AS
    CURSOR vyprava_mamut_cursor IS
        SELECT *
        FROM vyprava_lovi NATURAL LEFT JOIN byl_uloven
        WHERE id_lovecka_vyprava = id_lovecka_vyprava_arg;
    vyprava_mamut_row vyprava_mamut_cursor%ROWTYPE;
    mamuti_celkem NUMBER;
    mamuti_uloveni NUMBER;
    BEGIN
        mamuti_celkem := 0;
        mamuti_uloveni := 0;
        OPEN vyprava_mamut_cursor;
        LOOP
            FETCH vyprava_mamut_cursor INTO vyprava_mamut_row;
            EXIT WHEN vyprava_mamut_cursor%NOTFOUND;
            mamuti_celkem := mamuti_celkem + 1;
            IF vyprava_mamut_row.id_lovec IS NOT NULL THEN
                mamuti_uloveni := mamuti_uloveni + 1;
            END IF;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE('Vyprava c.'||id_lovecka_vyprava_arg||' byla uspesna na '||ROUND(mamuti_uloveni / mamuti_celkem * 100)||'%.');
        CLOSE vyprava_mamut_cursor;
    EXCEPTION
        WHEN ZERO_DIVIDE THEN
            DBMS_OUTPUT.PUT_LINE('Lovecka vyprava c.'||id_lovecka_vyprava_arg||' neexistuje, nebo nelovila zadneho mamuta.');
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('CHYBA2');
    END;
/

-------------------------------------------------------------------------------

-- Vkladani testovacich dat

INSERT INTO neandrtalec (id_neandrtalec, jmeno, prijmeni, datum_narozeni, zdravi, dovednosti) VALUES (NULL, 'Peter', 'Uhrin', '19.11.1996', 'zdravy', 'Vsechno krome SQL');
INSERT INTO neandrtalec (id_neandrtalec, jmeno, prijmeni, datum_narozeni, zdravi, dovednosti) VALUES (NULL, 'David', 'Bazout', '15.05.1997', 'nemocny', 'Ani SQL, ani nic jineho');
INSERT INTO neandrtalec (id_neandrtalec, jmeno, prijmeni, datum_narozeni, zdravi, dovednosti) VALUES (NULL, 'Vsechno', 'Umi', '01.01.1996', 'zdravy', 'Umi vsechno, zel neni s nami v tymu');
INSERT INTO neandrtalec (id_neandrtalec, jmeno, prijmeni, datum_narozeni, zdravi, dovednosti) VALUES (NULL, 'Karel', 'Fluidni', '06.09.2001', 'nemocny', 'Genderove vyvazuje testovaci data');
INSERT INTO neandrtalec (id_neandrtalec, jmeno, prijmeni, datum_narozeni, zdravi, dovednosti) VALUES (NULL, 'Pate', 'Kolo', '23.04.1999', 'mrtvy', 'Umi byt do poctu');

INSERT INTO lovecka_vyprava (id_lovecka_vyprava, zahajeni, stav, uspesnost) VALUES (lovecka_vyprava_seq.NEXTVAL, '01.04.2018', 'skoncila', 'uspesna');
INSERT INTO lovecka_vyprava (id_lovecka_vyprava, zahajeni, stav) VALUES (lovecka_vyprava_seq.NEXTVAL, CURRENT_DATE, 'bezici');

INSERT INTO mamuti_jama (id_mamuti_jama, id_lovecka_vyprava, lokace) VALUES (mamuti_jama_seq.NEXTVAL, '1', '49.226570 16.596667');
INSERT INTO mamuti_jama (id_mamuti_jama, id_lovecka_vyprava, lokace) VALUES (mamuti_jama_seq.NEXTVAL, '2', '49.286091 16.631853');
INSERT INTO mamuti_jama (id_mamuti_jama, lokace) VALUES (mamuti_jama_seq.NEXTVAL, '50.100150 14.448931');

INSERT INTO mamut (id_mamut, zvalstni_znaky) VALUES (mamut_seq.NEXTVAL, 'Ma velike zahnute zuby nahoru');
INSERT INTO mamut (id_mamut, charakterove_vlastnosti) VALUES (mamut_seq.NEXTVAL, 'Nema rad kdyz si lidi pili vanocni stromek v lese');
INSERT INTO mamut (id_mamut, zvyky) VALUES (mamut_seq.NEXTVAL, 'Rad utika za lidmi, kteri pili vanocni stromek v lese');
INSERT INTO mamut (id_mamut) VALUES (mamut_seq.NEXTVAL);
INSERT INTO mamut (id_mamut) VALUES (mamut_seq.NEXTVAL);

INSERT INTO hlidka (id_hlidka, stanoviste) VALUES ('2', '1');
INSERT INTO hlidka (id_hlidka, stanoviste) VALUES ('3', '2');

INSERT INTO lovec (id_lovec) VALUES ('1');
INSERT INTO lovec (id_lovec) VALUES ('4');
INSERT INTO lovec (id_lovec, uslapan_mamutem) VALUES ('5', '3');

INSERT INTO zprava (id_zprava, id_hlidka, odeslano) VALUES (zprava_seq.NEXTVAL, '2', '31.03.2018');
INSERT INTO zprava (id_zprava, id_hlidka, odeslano) VALUES (zprava_seq.NEXTVAL, '3', '01.04.2018');

INSERT INTO lovec_se_ucastni (id_lovec, id_lovecka_vyprava) VALUES ('1', '1');
INSERT INTO lovec_se_ucastni (id_lovec, id_lovecka_vyprava) VALUES ('5', '1');
INSERT INTO lovec_se_ucastni (id_lovec, id_lovecka_vyprava) VALUES ('1', '2');
INSERT INTO lovec_se_ucastni (id_lovec, id_lovecka_vyprava) VALUES ('4', '2');

INSERT INTO vycet_mamutu (id_zprava, id_mamut) VALUES ('1', '2');
INSERT INTO vycet_mamutu (id_zprava, id_mamut) VALUES ('1', '3');
INSERT INTO vycet_mamutu (id_zprava, id_mamut) VALUES ('1', '4');
INSERT INTO vycet_mamutu (id_zprava, id_mamut) VALUES ('2', '1');

INSERT INTO vyprava_lovi (id_lovecka_vyprava, id_mamut) VALUES ('1', '2');
INSERT INTO vyprava_lovi (id_lovecka_vyprava, id_mamut) VALUES ('1', '3');
INSERT INTO vyprava_lovi (id_lovecka_vyprava, id_mamut) VALUES ('1', '4');
INSERT INTO vyprava_lovi (id_lovecka_vyprava, id_mamut) VALUES ('2', '1');

INSERT INTO byl_uloven (id_mamut, id_lovec, id_lovecka_vyprava, id_mamuti_jama) VALUES ('4', '1', '1', '1');

-------------------------------------------------------------------------------

-- 2 dotazy se spojenim dvou tabulek
    -- Vypise vsechny lovce (z tabulky lovec) a jejich jmena a prijmeni (z tabulky neandrtalec)
SELECT id_lovec, jmeno, prijmeni
FROM lovec JOIN neandrtalec ON lovec.id_lovec = neandrtalec.id_neandrtalec;

    -- Najde vsechny mamuty, kteri nekdy uslapali nejakeho lovce
SELECT id_mamut, zvalstni_znaky, zvyky, charakterove_vlastnosti
FROM mamut JOIN lovec ON mamut.id_mamut = lovec.uslapan_mamutem;

-- 1 dotaz pro spojeni tri tabulek
    -- Informace o mamutech, kteri jsou prave loveni nejakou loveckou vypravou
SELECT id_mamut, zvalstni_znaky, zvyky, charakterove_vlastnosti
FROM lovecka_vyprava NATURAL JOIN vyprava_lovi NATURAL JOIN mamut
WHERE stav = 'bezici';

-- 2 dotazy s GROUP BY a agregacni funkci
    -- Seznam vsech loveckych vyprav a pocet lovcu kteri se jich ucastnili
SELECT id_lovecka_vyprava, zahajeni, stav, uspesnost, COUNT(*) AS pocet_lovcu
FROM lovecka_vyprava NATURAL JOIN lovec_se_ucastni NATURAL JOIN lovec
GROUP BY id_lovecka_vyprava, zahajeni, stav, uspesnost;

    -- Seznam lovcu, kteri nekdy zabili mamuta s poctem jimi ulovenych mamutu
SELECT id_lovec, COUNT(*) AS zabiti_mamuti
FROM lovec NATURAL JOIN byl_uloven NATURAL JOIN mamut
GROUP BY id_lovec;

-- 1 dotaz s EXISTS
    -- Seznam vsech neandrtalcu kteri nekdy zabili mamuta
SELECT *
FROM neandrtalec
WHERE EXISTS (SELECT id_lovec FROM byl_uloven WHERE id_lovec = neandrtalec.id_neandrtalec);

-- 1 dotaz s IN a vnorenym selectem
    -- Seznam všech neandrtalcu co nikdy neulovili mamuta
SELECT *
FROM neandrtalec
WHERE id_neandrtalec NOT IN (SELECT id_lovec FROM byl_uloven);

-------------------------------------------------------------------------------

-- Volani procedur
BEGIN
    uspesnost_vypravy(1);
    uspesnost_vypravy(2);
    uspesnost_vypravy(3);
END;
/

-------------------------------------------------------------------------------

-- Explain plan pred indexem
EXPLAIN PLAN FOR
SELECT id_neandrtalec, jmeno, prijmeni, COUNT(*) AS zabiti_mamuti
FROM neandrtalec JOIN byl_uloven ON neandrtalec.id_neandrtalec = byl_uloven.id_lovec
                 JOIN mamut ON byl_uloven.id_mamut = mamut.id_mamut
GROUP BY id_neandrtalec, jmeno, prijmeni;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);

CREATE INDEX id_neandrtalec_index ON neandrtalec(id_neandrtalec, jmeno, prijmeni);

-- Explain plan po indexu
EXPLAIN PLAN FOR
SELECT id_neandrtalec, jmeno, prijmeni, COUNT(*) AS zabiti_mamuti
FROM neandrtalec JOIN byl_uloven ON neandrtalec.id_neandrtalec = byl_uloven.id_lovec
                 JOIN mamut ON byl_uloven.id_mamut = mamut.id_mamut
GROUP BY id_neandrtalec, jmeno, prijmeni;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);

DROP INDEX id_neandrtalec_index;

-------------------------------------------------------------------------------

-- Prava pro xbazou00
    -- Prava na tabulky
GRANT ALL ON neandrtalec TO xbazou00;
GRANT ALL ON lovecka_vyprava TO xbazou00;
GRANT ALL ON mamuti_jama TO xbazou00;
GRANT ALL ON mamut TO xbazou00;
GRANT ALL ON hlidka TO xbazou00;
GRANT ALL ON lovec TO xbazou00;
GRANT ALL ON zprava TO xbazou00;
GRANT ALL ON lovec_se_ucastni TO xbazou00;
GRANT ALL ON vycet_mamutu TO xbazou00;
GRANT ALL ON vyprava_lovi TO xbazou00;
GRANT ALL ON byl_uloven TO xbazou00;

    -- Prava na spousteni procedur
GRANT EXECUTE ON uspesnost_vypravy TO xbazou00;

-------------------------------------------------------------------------------

-- Materializovany pohled
DROP MATERIALIZED VIEW nej_lovci_view;

CREATE MATERIALIZED VIEW LOG ON byl_uloven WITH PRIMARY KEY, ROWID INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW nej_lovci_view NOLOGGING CACHE BUILD IMMEDIATE REFRESH FAST ON COMMIT ENABLE QUERY REWRITE AS
    SELECT id_lovec, COUNT(*) AS zabiti_mamuti
    FROM byl_uloven
    GROUP BY id_lovec;

GRANT ALL ON nej_lovci_view TO xbazou00;

SELECT * FROM nej_lovci_view;
INSERT INTO byl_uloven (id_mamut, id_lovec, id_lovecka_vyprava, id_mamuti_jama) VALUES ('1', '4', '2', '2');
COMMIT;
SELECT * FROM nej_lovci_view;
