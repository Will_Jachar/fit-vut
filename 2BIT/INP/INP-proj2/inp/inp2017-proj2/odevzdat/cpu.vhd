-- cpu.vhd: Simple 8-bit CPU (BrainLove interpreter)
-- Copyright (C) 2017 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Peter Uhrin <xuhrin02 AT fit.vutbr.cz>
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
    port (
        CLK   : in std_logic;  -- hodinovy signal
        RESET : in std_logic;  -- asynchronni reset procesoru
        EN    : in std_logic;  -- povoleni cinnosti procesoru

        -- synchronni pamet ROM
        CODE_ADDR : out std_logic_vector(11 downto 0); -- adresa do pameti
        CODE_DATA : in std_logic_vector(7 downto 0);   -- CODE_DATA <- rom[CODE_ADDR] pokud CODE_EN='1'
        CODE_EN   : out std_logic;                     -- povoleni cinnosti

        -- synchronni pamet RAM
        DATA_ADDR  : out std_logic_vector(9 downto 0); -- adresa do pameti
        DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
        DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
        DATA_RDWR  : out std_logic;                    -- cteni z pameti (DATA_RDWR='0') / zapis do pameti (DATA_RDWR='1')
        DATA_EN    : out std_logic;                    -- povoleni cinnosti

        -- vstupni port
        IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA obsahuje stisknuty znak klavesnice pokud IN_VLD='1' a IN_REQ='1'
        IN_VLD    : in std_logic;                      -- data platna pokud IN_VLD='1'
        IN_REQ    : out std_logic;                     -- pozadavek na vstup dat z klavesnice

        -- vystupni port
        OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
        OUT_BUSY : in std_logic;                       -- pokud OUT_BUSY='1', LCD je zaneprazdnen, nelze zapisovat,  OUT_WE musi byt '0'
        OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
    );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

    -- Definice vnitrnich signalu.
    type t_state is (
        S_INIT,
        S_DECODE,
        S_R_ARROW,
        S_L_ARROW,
        S_PLUS, S_PLUS_R,
        S_MINUS, S_MINUS_R,
        S_DOT, S_DOT_W,
        S_COMMA, S_COMMA_R,
        S_L_BRACKET, S_L_BRACKET_R, S_L_BRACKET_COND, S_L_BRACKET_LOOP,
        S_R_BRACKET, S_R_BRACKET_R, S_R_BRACKET_COND, S_R_BRACKET_LOOP_1, S_R_BRACKET_LOOP_2,
        S_TILDA, S_TILDA_COND, S_TILDA_LOOP,
        S_IDLE);

    signal present_state, next_state : t_state;

    signal PC       : std_logic_vector(11 downto 0);
    signal PC_sign  : std_logic_vector(1 downto 0);

    signal CNT      : std_logic_vector(11 downto 0);
    signal CNT_sign : std_logic_vector(1 downto 0);
    signal CNT_one  : std_logic;

    signal PTR      : std_logic_vector(9 downto 0);
    signal PTR_sign : std_logic_vector(1 downto 0);

begin

    ------------------------------------------------------------

    PC_reg: process(CLK, RESET, PC_sign)
	begin
		if (RESET = '1') then
            PC <= (others => '0');
        elsif (CLK'event and CLK = '1') then
            if (PC_sign = "01") then
                PC <= PC + 1;
            elsif (PC_sign = "10") then
                PC <= PC - 1;
            end if;
		end if;
	end process PC_reg;

    ------------------------------------------------------------

    CNT_reg: process(CLK, RESET, CNT_sign, CNT_one)
	begin
        if (CLK'event and CLK = '1') then
            if (CNT_one = '1') then
                CNT <= "000000000001";
            elsif (CNT_sign = "01") then
                CNT <= CNT + 1;
            elsif (CNT_sign = "10") then
                CNT <= CNT - 1;
            end if;
		end if;
    end process CNT_reg;
    
    ------------------------------------------------------------

    PTR_reg: process(CLK, RESET, PTR_sign)
	begin
		if (RESET = '1') then
            PTR <= (others => '0');
        elsif (CLK'event and CLK = '1') then
            if (PTR_sign = "01") then
                PTR <= PTR + 1;
            elsif (PTR_sign = "10") then
                PTR <= PTR - 1;
            end if;
		end if;
    end process PTR_reg;

    ------------------------------------------------------------

    -- Prirazeni vystupu registru na prislusne vystupni porty
    CODE_ADDR <= PC;
    DATA_ADDR <= PTR;

    ------------------------------------------------------------

    -- Synchronizace prepinani stavu s CLK
    fsm_sync : process(RESET, CLK, EN)
    begin

    if (RESET = '1') then
        present_state <= S_INIT;
    elsif (CLK'event and CLK = '1' and EN = '1') then
        present_state <= next_state;
    end if;

    end process fsm_sync;

    ------------------------------------------------------------

    -- Stavovy automat
    fsm : process(CLK, RESET, present_state)
    begin

        CODE_EN <= '0';
        DATA_EN <= '0';
        OUT_WE <= '0';

        DATA_RDWR <= '0';

        PC_sign <= "00";
        CNT_sign <= "00";
        CNT_one <= '0';
        PTR_sign <= "00";

        case (present_state) is
            when S_INIT =>
                CODE_EN <= '1';
                next_state <= S_DECODE;
            ------------------------------
            when S_DECODE =>
                case (CODE_DATA) is
                    when X"3E" => next_state <= S_R_ARROW;      -- '>'
                    when X"3C" => next_state <= S_L_ARROW;      -- '<'
                    when X"2B" => next_state <= S_PLUS;         -- '+'
                    when X"2D" => next_state <= S_MINUS;        -- '-'
                    when X"2E" => next_state <= S_DOT;          -- '-'
                    when X"2C" => next_state <= S_COMMA;        -- ','
                    when X"5B" => next_state <= S_L_BRACKET;    -- '['
                    when X"5D" => next_state <= S_R_BRACKET;    -- ']'
                    when X"7E" => next_state <= S_TILDA;        -- '~'
                    when X"00" => next_state <= S_IDLE;         -- 'null'
                    when others =>
                        PC_sign <= "01";
                        next_state <= S_INIT;
                end case;
            ------------------------------
            when S_R_ARROW =>
                PC_sign <= "01";
                PTR_sign <= "01";
                next_state <= S_INIT;
            ------------------------------
            when S_L_ARROW =>
                PC_sign <= "01";
                PTR_sign <= "10";
                next_state <= S_INIT;
            ------------------------------
            when S_PLUS =>
                PC_sign <= "01";
                DATA_EN <= '1';                 -- Povoleni cteni/zapisu z/do RAM
                DATA_RDWR <= '0';               -- Rezim cteni dat do RAM
                next_state <= S_PLUS_R;
            when S_PLUS_R =>
                DATA_EN <= '1';
                DATA_RDWR <= '1';               -- Rezim zapisu dat do RAM
                DATA_WDATA <= DATA_RDATA + 1;   -- Precteni nactenych dat
                next_state <= S_INIT;
            ------------------------------
            when S_MINUS =>
                PC_sign <= "01";
                DATA_EN <= '1';                 -- Povoleni cteni/zapisu z/do RAM
                DATA_RDWR <= '0';               -- Rezim cteni dat do RAM
                next_state <= S_MINUS_R;
            when S_MINUS_R =>
                DATA_EN <= '1';
                DATA_RDWR <= '1';               -- Rezim zapisu dat do RAM
                DATA_WDATA <= DATA_RDATA - 1;   -- Precteni nactenych dat
                next_state <= S_INIT;
            ------------------------------
            when S_DOT =>
                PC_sign <= "01";
                DATA_EN <= '1';                 -- Povoleni cteni/zapisu z/do RAM
                DATA_RDWR <= '0';               -- Rezim cteni dat z RAM
                next_state <= S_DOT_W;
            when S_DOT_W =>
                if (OUT_BUSY = '0') then
                    OUT_WE <= '1';              -- Povoleni zapisovani na displej
                    OUT_DATA <= DATA_RDATA;
                    next_state <= S_INIT;
                else
                    next_state <= S_DOT_W;
                end if;
            ------------------------------
            when S_COMMA =>
                IN_REQ <= '1';                  -- Cekani na vypis
                next_state <= S_COMMA_R;
            when S_COMMA_R =>
                if (IN_VLD = '1') then
                    PC_sign <= "01";
                    DATA_EN <= '1';             -- Povoleni cteni/zapisu z/do RAM
                    DATA_RDWR <= '1';           -- Rezim zapisu dat do RAM
                    DATA_WDATA <= IN_DATA;      -- Zapsani prectenych dat
                    IN_REQ <= '0';
                    next_state <= S_INIT;
                else
                    next_state <= S_COMMA_R;
                end if;
            ------------------------------
            when S_L_BRACKET =>
                PC_sign <= "01";
                DATA_EN <= '1';                 -- Povoleni cteni/zapisu z/do RAM
                DATA_RDWR <= '0';               -- Rezim cteni dat z RAM
                next_state <= S_L_BRACKET_R;
            when S_L_BRACKET_R =>
                if (DATA_RDATA = "000000000") then
                    CNT_one <= '1';
                    next_state <= S_L_BRACKET_COND;
                else
                    next_state <= S_INIT;
                end if;
            when S_L_BRACKET_COND =>
                if (CNT /= "000000000000") then
                    CODE_EN <= '1';
                    next_state <= S_L_BRACKET_LOOP;
                else
                    next_state <= S_R_BRACKET;
                end if;
            when S_L_BRACKET_LOOP =>
                if (CODE_DATA = X"5B") then     -- [
                    CNT_sign <= "01";
                elsif (CODE_DATA = X"5D") then  -- ]
                    CNT_sign <= "10";
                end if;
                PC_sign <= "01";
                next_state <= S_L_BRACKET_COND; -- null
                if (CODE_DATA = X"00") then
                    next_state <= S_IDLE;
                end if;
            ------------------------------
            when S_R_BRACKET =>
                DATA_EN <= '1';                 -- Povoleni cteni/zapisu z/do RAM
                DATA_RDWR <= '0';               -- Rezim cteni dat z RAM
                next_state <= S_R_BRACKET_R;
            when S_R_BRACKET_R =>
                if (DATA_RDATA = "000000000") then
                    PC_sign <= "01";
                    next_state <= S_INIT;
                else
                    PC_sign <= "10";
                    CNT_one <= '1';             -- Nasteveni CNT na jednicku
                    next_state <= S_R_BRACKET_COND;
                end if;
            when S_R_BRACKET_COND =>
                if (CNT /= "000000000000") then
                    CODE_EN <= '1';
                    next_state <= S_R_BRACKET_LOOP_1;
                else
                    next_state <= S_INIT;
                end if;
            when S_R_BRACKET_LOOP_1 =>
                if (CODE_DATA = X"5D") then     -- ]
                    CNT_sign <= "01";
                elsif (CODE_DATA = X"5B") then  -- ]
                    CNT_sign <= "10";
                end if;
                next_state <= S_R_BRACKET_LOOP_2;
                if (PC = "111111111111") then   -- Dosli jsme na zacatek
                    next_state <= S_IDLE;
                end if;
            when S_R_BRACKET_LOOP_2 =>
                if (CNT = "000000000000") then
                    PC_sign <= "01";
                else
                    PC_sign <= "10";
                end if;
                next_state <= S_R_BRACKET_COND;
            ------------------------------
            when S_TILDA =>
                PC_sign <= "01";
                CNT_one <= '1';
                next_state <= S_TILDA_COND;
            when S_TILDA_COND =>
                if (CNT /= "000000000000") then
                    CODE_EN <= '1';
                    next_state <= S_TILDA_LOOP;
                else
                    next_state <= S_INIT;
                end if;
            when S_TILDA_LOOP =>
                if (CODE_DATA = X"5B") then     -- [
                    CNT_sign <= "01";
                elsif (CODE_DATA = X"5D") then  -- ]
                    CNT_sign <= "10";
                end if;
                PC_sign <= "01";
                next_state <= S_TILDA_COND;
                if (CODE_DATA = X"00") then     -- null
                    next_state <= S_IDLE;
                end if;
            ------------------------------
            when S_IDLE =>
                if (RESET = '1') then
                    next_state <= S_INIT;
                else
                    next_state <= S_IDLE;
                end if;
            ------------------------------
            when others =>
                next_state <= S_INIT;
        end case;

    end process fsm;

    ------------------------------------------------------------

end behavioral;
