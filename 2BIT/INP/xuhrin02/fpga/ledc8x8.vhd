library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is

    -- Popis rozhrani obvodu.
    port (
        RESET	: in std_logic;
        SMCLK	: in std_logic;
        ROW		: out std_logic_vector (0 to 7);
        LED		: out std_logic_vector (0 to 7)
    );

end ledc8x8;

architecture main of ledc8x8 is

    -- Definice vnitrnich signalu.
    signal ce: std_logic;
    signal ce_cnt: std_logic_vector(7 downto 0);
    signal rows: std_logic_vector(7 downto 0);
    signal columns: std_logic_vector(7 downto 0);

begin

    -- Popis obvodu. Komunikaci mezi procesy,
    -- realizovana pomoci vnitrnich signalu deklarovanych vyse.

    ----- Snizeni frekvence (jeden impulz za 256 tiku) -----
    process(SMCLK, RESET)
    begin

        if (RESET = '1') then
            ce_cnt <= "00101010";
        elsif (SMCLK'event) and (SMCLK = '1') then
            ce_cnt <= ce_cnt + 1;
        end if;

    end process;

    ce <= '1' when ce_cnt = "00101010" else '0';


    ----- Prepinac radku (posuvny registr) -----
    process(RESET, ce, SMCLK)
    begin

        if (RESET = '1') then
            rows <= "10000000";
        elsif (SMCLK'event) and (SMCLK = '1') and (ce = '1') then
            rows <= rows(0) & rows(7 downto 1);
        end if;

    end process;


    ----- Dekoder radku displaye -----
    process(rows)
    begin

        case rows is
            when "10000000" => columns <= "00001111";
            when "01000000" => columns <= "01101111";
            when "00100000" => columns <= "01101111";
            when "00010000" => columns <= "00000110";
            when "00001000" => columns <= "01110110";
            when "00000100" => columns <= "01110110";
            when "00000010" => columns <= "11110110";
            when "00000001" => columns <= "11110000";
            when others     => columns <= "00000000";
        end case;

    end process;


    ----- Posleme do displaye -----
    ROW <= rows;
    LED <= columns;

end main;
