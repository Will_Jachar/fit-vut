/*Sablona pro projekty do predmetu IPA, projekty s vizualizaci
*Autor: Tomas Goldmann, igoldmann@fit.vutbr.cz, cast kodu prevzata z cviceni IPA od autora Davida Hermana
*
*LOGIN STUDENTA: xuhrin02
*/

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <windows.h>
#include <stddef.h>
#include <malloc.h>
#include <xmmintrin.h>
#include <GL/freeglut.h>
#include "ipa_tool.h"

#define PROJECT_NAME "IPA-GLUT projekt 2017"

#define G 9.81
#define NEIG_COUNT 8
#define PARTICLES_COUNT 512
#define MASS 0.2
#define M_PI 3.141592653589
#define H 0.0625
#define H2 0.00390625
#define DENSITY_MAGIC_NUM 1.076615309e11
#define PRESSURE_MAGIC_NUM 240315917.2

__declspec(align(4)) typedef struct {
	float x;
	float y;
	float z;

	float acc_x;
	float acc_y;
	float acc_z;

	float *vel_x;
	float *vel_y;
	float *vel_z;

	float *pressure_x;
	float *pressure_y;
	float *pressure_z;

	float *density;
	float *pressure;
	float *viscosity;
} t_particle;

__declspec(align(32)) typedef struct {
	float normal_x[NEIG_COUNT];
	float normal_y[NEIG_COUNT];
	float normal_z[NEIG_COUNT];

	float distance[NEIG_COUNT];

	float *vel_x[NEIG_COUNT];
	float *vel_y[NEIG_COUNT];
	float *vel_z[NEIG_COUNT];

	float *pressure_x[NEIG_COUNT];
	float *pressure_y[NEIG_COUNT];
	float *pressure_z[NEIG_COUNT];

	float *density[NEIG_COUNT];
	float *pressure[NEIG_COUNT];
	float *viscosity[NEIG_COUNT];

	float exists[NEIG_COUNT];
} t_neighbours;

t_particle particles[PARTICLES_COUNT];


//extern "C" void optim_function(t_particle *particles);
//extern "C" void optim_function();
//extern "C" void density(t_particle *particle, t_neighbours *array);
// OpenGL.
double L;								  // SIZE OF DIMENSION
double radius = 4.0;                      // radius of molecule
double minExtent[3], maxExtent[3];        // extent of system volume
int xWindowSize = 640, yWindowSize = 640; // window size in screen pixels
GLdouble aspectRatio;                     // window aspect ratio
GLdouble fovy, nearClip, farClip;         // variables for 3D projection
GLdouble eye[3], center[3], up[3];        // more projection variables
GLuint sphereID, configID;                // display list ID numbers
int phi, theta;                           // to rotate system using arrow keys
int angle = 5;                            // rotation angle in degrees

float init_x = -5 * radius;
float init_y = 5 * radius;
float init_z = -5 * radius;

										  // Declarations.
void initView(double *minExtent, double *maxExtent);		// Init GL.
void display();												// Display.
void reshape(GLsizei width, GLsizei height);				// Reshape.
void takeStep();											// Take a step.
void createRenderList();										// Make particles.
void makeSphere(GLuint sphereID, float radius);				// Make sphere.

bool* keyStates = new bool[256]; // Create an array of boolean values of length 256 (0-255)  

void keyPressed(unsigned char key, int x, int y)
{
	keyStates[key] = true; // Set the state of the current key to pressed  
}

void keyUp(unsigned char key, int x, int y)
{
	keyStates[key] = false; // Set the state of the current key to pressed  
}
															// Main.
int main(int argc, char *argv[])
{
	L = 320;					// Size of the cube.

	int index = 0;
	for (int x = 0; x < 8; x++) {
		for (int y = 0; y < 8; y++) {
			for (int z = 0; z < 8; z++, index++) {
				particles[index].x = init_x + x * radius + x * 4;
				particles[index].y = init_y + y * radius + y * 4;
				particles[index].z = init_z + z * radius + z * 4;

				particles[index].acc_x = 0;
				particles[index].acc_y = -G;
				particles[index].acc_z = 0;
			}
		}
	}

	//optim_function();
	phi = 0; theta = 0;

	glutInit(&argc, argv);


	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(xWindowSize, yWindowSize);
	glutCreateWindow(PROJECT_NAME);

	// Set cube boundaries.
	for (int i = 0; i < 3; i++) {
		minExtent[i] = -L / 2;
		maxExtent[i] = L / 2;
	}

	initView(minExtent, maxExtent);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(takeStep);
	glutKeyboardFunc(keyPressed); 
	glutKeyboardUpFunc(keyUp);


	sphereID = glGenLists(1);
	makeSphere(sphereID,(float) radius);
	configID = glGenLists(1);


	glutMainLoop();
}

void initView(double *minExtent, double *maxExtent) {
	// use a single light source to illuminate the scene
	GLfloat lightDiffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat lightPosition[] = { 0.5, 0.5, 1.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);              // use single light number 0
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	// compute the distance scale of the system
	double difExtent[3];
	for (int i = 0; i < 3; i++)
		difExtent[i] = maxExtent[i] - minExtent[i];
	double dist = 0;
	for (int i = 0; i < 3; i++)
		dist += difExtent[i] * difExtent[i];
	dist = sqrt(dist);

	// locate the center of the system, camera position, and orientation
	for (int i = 0; i < 3; i++)
		center[i] = minExtent[i] + difExtent[i] / 2;

	eye[0] = center[0];
	eye[1] = center[1];
	eye[2] = center[2] + dist;        // along z axis is towards viewer
	up[0] = 0;
	up[1] = 1;                        // y axis is up
	up[2] = 0;

	// set up clipping planes, field of view angle in degrees in y direction
	nearClip = (dist - difExtent[2] / 2.0) / 2.0;
	farClip = 2.0 * (dist + difExtent[2] / 2.0);

	fovy = difExtent[1] / (dist - difExtent[2] / 2.0) / 2.0;
	fovy = 2.0 * atan(fovy) / M_PI * 180.0;
	fovy *= 1.2;
}

// Display.
void display() {
	// Clear buffers.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Set camera.
	gluLookAt(eye[0], eye[1], eye[2],			// Where the camera is.
		center[0], center[1], center[2],	// What the camera see (point).
		up[0], up[1], up[2]);				// Orientation of the camera.

											// Draw particles.
	glCallList(configID);

	// Swap buffers.
	glutSwapBuffers();
}

// Handler for window's re-size event.
void reshape(GLsizei width, GLsizei height)
{
	// Prevent divide by 0.
	if (height == 0) height = 1;

	// Compute new aspect ratio.
	aspectRatio = width / double(height);

	// Set the viewport to cover entire application window.
	// View port is in pixels in Window's coordinates (origin at top left corner).
	// Projection plane is transformed via Viewport transformation to viewport on screen.
	glViewport(0, 0, width, height);

	// Select the aspect ratio of the clipping area to mach the viewport.
	glMatrixMode(GL_PROJECTION);	// Select the projection matrix.
	glLoadIdentity();				// Reset the projection matrix.

									// fovy is the angle between the mottom and top of the projectors.
									// aspectRatio is the ratio of width and height of the front clipping plane.
									// zNear and zFar specify the front and back clipping planes.
									// Coordinates of the clipping volume are relative to the camera's eye position.
	gluPerspective(fovy, aspectRatio, nearClip, farClip);

	// Reset the Model View matrix.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//DOPLNIT
// Take step.
void takeStep() {

	if (keyStates['w']) {
		
	}


	//IPA-TODO muzete libovolne menit, prepoklada se, ze se zde bude vykonavat jeden simulacni krok
	//optim_function();
	createRenderList();
	glutPostRedisplay();

}


void createRenderList()
{
	// Create new list.
	glNewList(configID, GL_COMPILE);
	glColor3f(0.0, 1.0, 0.0);

	for (int i = 0; i < PARTICLES_COUNT; i++) {
		glPushMatrix();
		glTranslated(particles[i].x, particles[i].y, particles[i].z); // Set position of sphere/particle.
		glCallList(sphereID);
		glPopMatrix();
	}

	// Space.
	glColor3ub(255, 255, 255);		// White color.
	glutWireCube(L);				// Cubical space.


	glEndList();
}

// Make sphere.
void makeSphere(GLuint sphereID, float radius)
{
	int nTheta = 9;                       // Number of polar angle slices.
	int nPhi = 18;                        // Number of azimuthal angle slices.
	glNewList(sphereID, GL_COMPILE);
	glutSolidSphere(radius, nPhi, nTheta);
	glEndList();
}