;#Template for IPA project
;#Author: Tomas Goldmann, igoldmann@fit.vutbr.cz
;#
;#STUDENT LOGIN: xuhrin02
;#
[bits 32]

NEIG_COUNT equ 32

section .data
	counter 		   dd 0
	MASS 			   dd 0.2
	PI 				   dd 3.141592653589
	H 				   dd 0.0625
	H2 				   dd 0.00390625
	DENSITY_MAGIC_NUM  dd 1.076615309e11
	PRESSURE_MAGIC_NUM dd 240315917.2


global _optim_function
export _optim_function
global _density
export _density


section .text
	%define neighbours.x 0
	%define neighbours.y NEIG_COUNT*4
	%define neighbours.z NEIG_COUNT*2*4

	%define neighbours.vel_x NEIG_COUNT*3*4
	%define neighbours.vel_y NEIG_COUNT*4*4
	%define neighbours.vel_z NEIG_COUNT*5*4

	%define neighbours.pressure_x NEIG_COUNT*6*4
	%define neighbours.pressure_y NEIG_COUNT*7*4
	%define neighbours.pressure_z NEIG_COUNT*8*4

	%define neighbours.density NEIG_COUNT*9*4
	%define neighbours.pressure NEIG_COUNT*10*4
	%define neighbours.viscosity NEIG_COUNT*11*4

;##########################################################

	%define particle.x 0
	%define particle.y 4
	%define particle.z 8

	%define particle.vel_x 12
	%define particle.vel_y 16
	%define particle.vel_z 20

	%define particle.pressure_x 24
	%define particle.pressure_y 28
	%define particle.pressure_z 32

	%define particle.density 36
	%define particle.pressure 40
	%define particle.viscosity 44

_find_neighbours:
	push ebp
	mov ebp, esp

	mov eax, [ebp + 8]	; ulozime si particle pro kterou pocitame
	mov esi, [ebo + 12] ; ukazatel na pole particlu
	mov edi, [ebp + 16]	; ukazatel na pole sousedu

	mov edx, esp
	and esp, 0xFFFFFFF0	; zarovnani zasobniku



	popad
	mov esp, ebp
	pop ebp

_density:
	push ebp
	mov ebp, esp

	mov esi, [ebp + 8]	; ulozime si particle pro kterou pocitame
	mov edi, [ebp + 12]	; ukazatel na sousedy
	mov ecx, 8			; pocet sousedu

	mov edx, esp
	and esp, 0xFFFFFFF0	; zarovnani zasobniku

	; Zkopirujeme pozici jedne castice kterou porovnavame
	movss xmm0, [esi + particle.x]
	shufps xmm0, xmm0, 0
	movss xmm1, [esi + particle.y]
	shufps xmm1, xmm1, 0
	movss xmm2, [esi + particle.z]
	shufps xmm2, xmm2, 0

	xorps xmm7, xmm0

	.FOR_ALL_NEIG:
		movups xmm3, [edi + neighbours.x + ecx * 4 - 4]
		movups xmm4, [edi + neighbours.y + ecx * 4 - 4]
		movups xmm5, [edi + neighbours.z + ecx * 4 - 4]

		; Velikost vektoru
		subps xmm3, xmm0
		mulps xmm3, xmm3
		subps xmm4, xmm1
		mulps xmm4, xmm4
		subps xmm5, xmm2
		mulps xmm5, xmm5
		addps xmm3, xmm4
		addps xmm3, xmm5

		; Nasobeni konstantou u umocneni na treti
		movss xmm4, [H2]
		shufps xmm4, xmm4, 0
		subps xmm4, xmm3
		movups xmm5, xmm4
		mulps xmm4, xmm5
		mulps xmm4, xmm5
		
		; Nasobeni konstantou
		movss xmm3, [DENSITY_MAGIC_NUM]
		shufps xmm3, xmm3, 0
		mulps xmm4, xmm3

		; Nasobeni konstantou
		movss xmm3, [MASS]
		shufps xmm3, xmm3, 0
		mulps xmm4, xmm3

		; Secteni ctyr mezivysledku dohromady
		haddps xmm4, xmm4
		haddps xmm4, xmm4

		; pricteni finalniho mezivysledku k celkovemu souctu
		addss xmm7, xmm4

		sub ecx, 4
		cmp ecx, 0
		jnz .FOR_ALL_NEIG

	movss xmm0, xmm7

	mov esp, edx
	popad
	mov esp, ebp
	pop ebp

ret

_optim_function:
	push ebp
	mov ebp, esp

	mov esi, [ebp + 8]
	mov ecx, [ebp + 12]

	cmp esi, 0
	je .END

	mov [counter], ecx
	mov edx, 0

	.FOR_ALL_1:
		; mov eax, [esi + edx]
		; apply forces on eax
		; predict position on eax
		; add edx, 4
	loop .FOR_ALL_1

	mov ecx, [counter]
	mov edx, 0

	.FOR_ALL_2:
		; mov eax, [esi + edx]
		; find neigfboring neighbours on eax
		; add edx, 4
	loop .FOR_ALL_2

	.END:

	mov esp, ebp
	pop ebp

ret