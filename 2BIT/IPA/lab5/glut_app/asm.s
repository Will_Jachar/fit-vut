;#Template for IPA project, 3D game
;#Author: Tomas Goldmann, igoldmann@fit.vutbr.cz, most source code from Anton Claes (https://www.youtube.com/watch?v=a3MACw5hB2Q)
;#
;#STUDENT LOGIN:
;#
[bits 32]

SPHERES_COUNT equ 512

section .data


global _optim_function
export _optim_function


section .text
	%define spheres.x 0
	%define spheres.y SPHERES_COUNT*4
	%define spheres.z SPHERES_COUNT*2*4
	%define spheres.r SPHERES_COUNT*3*4
	%define spheres.m SPHERES_COUNT*4*4
	%define spheres.vector_x SPHERES_COUNT*5*4
	%define spheres.vector_y SPHERES_COUNT*6*4
	%define spheres.vector_z SPHERES_COUNT*7*4
	%define spheres.color SPHERES_COUNT*8*4
	
_optim_function:
	push ebp
	mov ebp, esp
	;ukazatel
	mov esi, [ebp+8]
	;pocet kouli
	mov ecx, [ebp+12]
	;dt
	mov ebx, [ebp+16]
	pushad ;ulozi registry na zasobnik
	mov edi, esp ;ukladam si esbp do edi aby sme nestratili esp
	and esp,0xFFFFFFF0 ;zarovnanie zasobniku

	%define norm_x esp-16
	%define norm_y esp-32
	%define norm_z esp-48

	%define diff_x esp-16
	%define diff_y esp-32
	
	%define diff_z esp-48
	%define a1_dot esp-64
	%define a2_dot esp-80
	%define dt esp-96
	mov [dt],ebx
	;inner cycle edx/4
	

cycle_outer:
	
	;udelat nacitani x,y,z,r

	;rozkopirovat elementy po celem registru
	movss xmm0, [esi + spheres.x + ecx * 4 - 4]
	movss xmm1, [esi + spheres.y + ecx * 4 - 4]
	movss xmm2, [esi + spheres.z + ecx * 4 - 4]
	movss xmm6, [esi + spheres.r + ecx * 4 - 4]

	shufps xmm0, xmm0, 0
	shufps xmm1, xmm1, 0
	shufps xmm2, xmm2, 0
	shufps xmm6, xmm6, 0
	

	lea edx,[esi+ecx*4-4]
cycle_inner:
	sub edx,16

	
	;nacist x,y,z pro kouli s indexem j

	movups xmm3, [edx + spheres.x]
	movups xmm4, [edx + spheres.y]
	movups xmm5, [edx + spheres.z]

	;udelat rozdil
	movups xmm7, xmm0 ;presun x suradnice I gule
	subps xmm7, xmm3 ;odcitanie x suradnice J gule od i gule
	movups [diff_x], xmm7
	mulps xmm7, xmm7 ;mocnenie (x1 - x2)^2
	;movups xmm3, xmm7 ; (x1 - x2)^2 do xmm3
	addps xmm3, xmm7

	movups xmm7, xmm1 ;presun y suradnice I gule
	subps xmm7, xmm4 ;odcitanie x suradnice J gule od i gule
	movups [diff_y], xmm7
	mulps xmm7, xmm7 ;mocnenie (y1 - y2)^2
	;movups xmm4, xmm7 ;(y1 - y2)^2 do xmm4
	addps xmm3, xmm7

	movups xmm7, xmm2 ;presun z suradnice I gule
	subps xmm7, xmm5 ;odcitanie z suradnice J gule od i gule
	movups [diff_z], xmm7 
	mulps xmm7, xmm7 ;mocnenie (z1 - z2)^2
	;movups xmm5, xmm7 ;(z1 - z2)^2 do xmm4
	addps xmm3, xmm7

	sqrtps xmm3, xmm3
	movups xmm7, [edx + spheres.r]



	;subps xmm1, xmm3
	;subps xmm2, xmm3






	;udelat rozdil


	

	;vytvoreni masky
	sqrtps xmm3,xmm3
	movups xmm7,[edx+spheres.r]

	xorps xmm4,xmm4
	cmpneqps xmm4,xmm3

	;# xmm4=r1+r2
	addps xmm7,xmm6
	;# xmm7=mask
	cmpnleps xmm7, xmm3
	andps xmm7,xmm4


	;Vypocet normy

	;priklad pro x
	movups xmm4,[diff_x]
	divps xmm4,xmm3
	movups [norm_x], xmm4

	;priklad pro y
	movups xmm4,[diff_y]
	divps xmm4,xmm3
	movups [norm_y], xmm4

	;priklad pro z
	movups xmm4,[diff_z]
	divps xmm4,xmm3
	movups [norm_z], xmm4


	;double a1_dot = spheres.vectorX[i] * n_x_norm + spheres.vectorY[i] * n_y_norm + spheres.vectorZ[i] * n_z_norm;
	movss xmm4, [esi + spheres.vector_x + ecx * 4 - 4]
	shufps xmm4, xmm4, 0
	mulps xmm4, [norm_x]
	movups xmm5, xmm4

	movss xmm4, [esi + spheres.vector_y + ecx * 4 - 4]
	shufps xmm4, xmm4, 0
	mulps xmm4, [norm_y]
	addps xmm5, xmm4

	movss  xmm4, [esi + spheres.vector_z + ecx * 4 - 4]
	shufps xmm4, xmm4, 0
	mulps xmm4, [norm_z]
	addps xmm5, xmm4
	
	movups [a1_dot], xmm5

	;;;;;double a1_dot = spheres.vectorX[i] * n_x_norm + spheres.vectorY[i] * n_y_norm + spheres.vectorZ[i] * n_z_norm;


	;double a2_dot = spheres.vectorX[j] * n_x_norm + spheres.vectorY[j] * n_y_norm + spheres.vectorZ[j] * n_z_norm;
	movups xmm4, [edx + spheres.vector_x]
	mulps xmm4, [norm_x]
	movups xmm5, xmm4

	movups xmm4, [edx + spheres.vector_y]
	mulps xmm4, [norm_y]
	addps xmm5, xmm4

	movups  xmm4, [edx + spheres.vector_z]
	mulps xmm4, [norm_z]
	addps xmm5, xmm4
	movups [a2_dot], xmm5
	
	;;;;;double a2_dot = spheres.vectorX[j] * n_x_norm + spheres.vectorY[j] * n_y_norm + spheres.vectorZ[j] * n_z_norm;



	;double P =( 2.0*(a1_dot - a2_dot))/(spheres[i].m + spheres[j].m);

	movups xmm3, a1_dot
	movups xmm4, a2_dot
	subps xmm3, xmm4 ; vysledok a1_dot - a2_dot
	movups xmm4, 2.0;
	mulps xmm3, xmm4 ; 2.0 * (a1_dot - a2_dot)

	;movss  xmm4, [esi + spheres.m + ecx * 4 - 4]
	;shufps xmm4, xmm4, 0
	;movups xmm5, [edx + spheres.m]
	;addps xmm4, xmm5
	;divps xmm3, xmm4


	subps xmm5, [a1_dot]
	addps xmm5, xmm5
	movss xmm3, [esi + spheres.m + ecx * 4 - 4]
	shufps xmm3, xmm3, 0
	movups xmm4, [edx + spheres.m]
	addps xmm3, xmm4
	divps xmm5, xmm3




	;spheres[i].vectorX = spheres[i].vectorX - P*spheres[j].m* n_x_norm;
	;spheres[i].vectorY = spheres[i].vectorY - P*spheres[j].m* n_y_norm;
	;spheres[i].vectorZ = spheres[i].vectorZ - P*spheres[j].m* n_z_norm;
	;spheres[j].vectorX = spheres[j].vectorX + P*spheres[i].m* n_x_norm;
	;spheres[j].vectorY = spheres[j].vectorY + P*spheres[i].m* n_y_norm;
	;spheres[j].vectorZ = spheres[j].vectorZ + P*spheres[i].m* n_z_norm;



	;for spheres[i].vectorX
	;update_vector(m,norm,vector)
	;doplnit makro pro aktualizaci vektoru s indexem i
	%macro update_vector_i 4
		movups xmm3, [edx+%1]
		mulps xmm4, xmm5
		mulps xmm3, [%2]
		
		movss xmm4, [esi + %3 + ecx * 4 - 4]
		andps xmm3, xmm7
		haddps xmm3, xmm3
		haddps xmm3, xmm3
		
		adds xmm4, xmm3 
		movss [esi + %3 + ecx * 4 - 4] , xmm4

	%endmacro

	update_vector_i spheres.m,norm_x,spheres.vector_x,xmm0
	update_vector_i spheres.m,norm_y,spheres.vector_y,xmm1
	update_vector_i spheres.m,norm_z,spheres.vector_z,xmm2

	;for spheres[j].vectorX
	;update_vector(m,norm,vector)
	;doplnit makro pro aktualizaci vektoru s indexem j
	%macro update_vector_j 3

		movss xmm3, [esi + %1 + ecx * 4 - 4]
		shufps xmm3, xmm3 ,0
		mulps xmm3, xmm5
		mulps xmm3, [%2]
		movups xmm4, [edx + %3]
		andps xmm3, xmm7
		subps xmm4, xmm3
		movups [edx + %3], xmm4
			
	%endmacro

	update_vector_j spheres.m,norm_x,spheres.vector_x
	update_vector_j spheres.m,norm_y,spheres.vector_y
	update_vector_j spheres.m,norm_z,spheres.vector_z

	movmskps eax,xmm7

	mov ebx,2
	;colision comparsion
	test eax,1
	je next1
	mov [esi+spheres.color+ecx*4-4],ebx
	mov [edx+spheres.color],ebx
next1:
	test eax,2
	je next2
	mov [esi+spheres.color+ecx*4-4],ebx
	mov [edx+spheres.color+4],ebx
next2:
	test eax,4
	je next3
	mov [esi+spheres.color+ecx*4-4],ebx
	mov [edx+spheres.color+8],ebx
next3:
	test eax,8
	je next4
	mov [esi+spheres.color+ecx*4-4],ebx
	mov [edx+spheres.color+12],ebx
next4:


	cmp edx,esi
	ja cycle_inner
test:

	;roznasobeni dt
	movss xmm3,[dt]
	movss xmm4,[esi+spheres.vector_x+ecx*4-4]
	mulss xmm4, xmm3
	addps xmm0, xmm4

	movss xmm4,[esi+spheres.vector_y+ecx*4-4]
	mulss xmm4, xmm3
	addps xmm1, xmm4

	movss xmm4,[esi+spheres.vector_z+ecx*4-4]
	mulss xmm4, xmm3
	addps xmm2, xmm4


	;movss [esi+spheres.x+ecx*4-4],xmm0
	;movss [esi+spheres.y+ecx*4-4],xmm1
	;movss [esi+spheres.z+ecx*4-4],xmm2
	
	dec ecx
	cmp ecx,4
	jne cycle_outer

	mov esp, edi
	popad
	mov esp, ebp
	pop ebp

ret 
