#include<intrin.h>
#include <intrin.h>
#include <inttypes.h>
#include <stdio.h>
#include <uchar.h>


#include "ipa_algorithm.h"


void ipa_algorithm(unsigned char *input_data, unsigned char *output_data)
{
	rotate(input_data, output_data);
}

void rotate(unsigned char *input_data, unsigned char *output_data)
{

	//1) moznost je programovani pomoci intrinsic funkci
	__m128i value = _mm_set_epi32(1, 10, 100, 1000);
	__m128i result;

	result = _mm_hadd_epi32(value, value);
	result = _mm_hadd_epi32(result, result);

	//2) moznosti je inline asembler

	__asm
	{
		mov eax, 0; 
		mov ecx, 0; 
		shl eax, cl;
	}
}
