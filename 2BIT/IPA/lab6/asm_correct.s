;#IPA - lab6 AVX

;#Author: Tomas Goldmann
;#STUDENT LOGIN: igoldmann
[bits 64]

SPHERES_COUNT equ 4096

section .data

G dd 8.90
constant_0_5 dd 0.50
constant_n160 dd -160.0
constant_n160vec dd -160.0,-160.0,-160.0,-160.0,-160.0,-160.0,-160.0,-160.0

global BouncingBall_ASM
export BouncingBall_ASM

section .text
	%define spheres.x 0
	%define spheres.y SPHERES_COUNT*4
	%define spheres.z SPHERES_COUNT*2*4
	%define spheres.r SPHERES_COUNT*3*4
	%define spheres.v SPHERES_COUNT*4*4
	%define spheres.q SPHERES_COUNT*5*4

BouncingBall_ASM:
	push rbp
	mov rbp, rsp
	;and spl,0x0f
	sub rsp, 16

	;doplnit nacteni parametru
	;pointer - rsi

	mov rsi, rcx ; *_spheres	
	mov rcx, rdx ; count 	
	movss [rbp - 8], xmm2 ;dt	




	
	;counter - rcx

	;dt -rbp-8 a rbx
	;mov rbx, r8

	

	;vypocet dt*G
	vbroadcastss ymm7, [rbp-8]
	vbroadcastss ymm6, [rel G]
	vmulps ymm5,ymm6,ymm7	; G*dt;

	;ulozeni konstant
	vbroadcastss ymm3,[rel constant_0_5]

nav:
	lea rdx,[rel rsi+4*rcx-32]
	
	;doplnit kod vypoctu

	vmovaps ymm0, [rdx + spheres.v] ;spheres->v 
	vaddps ymm1, ymm0, ymm5 ; spheres->v + (float) G * dt = speed_new
	vmovaps ymm2, [rdx + spheres.y] ;spheres->y
	
	vmulps  ymm0, ymm0, ymm7
	vsubps  ymm2, ymm2, ymm0	;spheres->y - v_old * dt

	vmulps ymm6, ymm3, ymm5		; 0.5*G*dt
	vmulps ymm6, ymm6, ymm7	    ; 0.5*G*dt*dt

	vaddps ymm2, ymm2, ymm6		;spheres->y - v_old * dt * 0.5 * G * dt *dt = y_new
	vmovaps [rel rdx + spheres.y], ymm2 ;spheres->y[i] = y_new
	vmovaps [rel rdx + spheres.v], ymm1 ;spheres->v[i] = speed_new

	;podmienka if (y_new < -160.0)
	vmovups ymm4, [rel constant_n160vec]

	vcmpltps ymm0, ymm2, ymm4     
	vcmpgeps ymm6, ymm2, ymm4
	
	vandps   ymm2, ymm2, ymm6
	vmaskmovps ymm0, ymm0, [rel constant_n160vec]
	vorps ymm2, ymm2, ymm0
	vmovaps [rel rdx + spheres.y], ymm2

	vxorps ymm4, ymm4
	vmaskmovps ymm4, ymm0, [rel rdx+spheres.q]
	vmulps ymm4, ymm4, ymm1

	vandps ymm1, ymm1, ymm6
	vaddps ymm1, ymm4, ymm1
	vmovaps [rel rdx + spheres.v], ymm1

	sub rcx,8
	cmp rcx,0
	jg nav

	mov rsp, rbp
	pop rbp
ret