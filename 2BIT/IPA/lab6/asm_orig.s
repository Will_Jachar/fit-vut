;#IPA - lab6 AVX

;#Author: Tomas Goldmann
;#STUDENT LOGIN: igoldmann
[bits 64]

SPHERES_COUNT equ 4096

section .data

G dd 8.90
constant_0_5 dd 0.50
constant_n160 dd -160.0
constant_n160vec dd -160.0,-160.0,-160.0,-160.0,-160.0,-160.0,-160.0,-160.0

global BouncingBall_ASM
export BouncingBall_ASM

section .text
	%define spheres.x 0
	%define spheres.y SPHERES_COUNT*4
	%define spheres.z SPHERES_COUNT*2*4
	%define spheres.r SPHERES_COUNT*3*4
	%define spheres.v SPHERES_COUNT*4*4
	%define spheres.q SPHERES_COUNT*5*4

BouncingBall_ASM:
	push rbp
	mov rbp, rsp
	;and spl,0x0f
	sub rsp, 16

	;doplnit nacteni parametru
	;pointer - rsi
	mov rsi, rcx
	
	;counter - rcx
	mov rcx, rdx

	;dt -rbp-8 a rbx
	;mov rbx, r8
	movss [rbp-8], xmm2

	;vypocet dt*G
	vbroadcastss ymm7, [rbp-8]
	vbroadcastss ymm6, [rel G]
	vmulps ymm5,ymm6,ymm7

	;ulozeni konstant
	vbroadcastss ymm3,[rel constant_0_5]

nav:
	lea rdx,[rel rsi+4*rcx-32]
	
	;doplnit kod vypoctu
	vmovaps ymm0, [rel rdx+spheres.v]
	vaddps ymm1, ymm0, ymm5				; speed_new = ymm1

	vmovaps ymm2, [rel rdx+spheres.y]
	vmulps ymm0, ymm7
	vsubps ymm2, ymm0
	vmulps ymm0, ymm5, ymm7
	vmulps ymm0, ymm3
	vaddps ymm0, ymm2					; y_new = ymm0

	vmovaps [rel rdx+spheres.y], ymm0
	vmovaps [rel rdx+spheres.v], ymm1

	vmovups ymm4, [rel constant_n160vec]
	vcmpltps ymm3, ymm0, ymm4
	vcmpgeps ymm6, ymm0, ymm4

	vandps ymm0, ymm0, ymm6

	vmaskmovps ymm3, ymm3, [rel constant_n160vec]
	vorps ymm0, ymm0, ymm3
	vmovaps [rel rdx+spheres.y], ymm0

	vxorps ymm4, ymm4
	vmaskmovps ymm4, ymm0, [rel rdx+spheres.q]

	vmulps ymm4, ymm4, ymm1

	vandps ymm1, ymm1, ymm6
	vaddps ymm1, ymm4, ymm1
	vmovaps [rel rdx+spheres.v], ymm1

	sub rcx,8
	cmp rcx,0
	jg nav

	mov rsp, rbp
	pop rbp
ret