/**
 * ipk-base.cpp
 *
 * IPK Project 1
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include "ipk-base.h"


/**
 * Function prints an error and ends program
 * 
 * @param   error    an error message
 * @param   err_code code with which will program end 
 */
void exit_error(char *error, int err_code) {
    fprintf(stderr, "ERROR: %s\n", error);
    exit(err_code);
}

/**
 * Function for sending file
 * 
 * @param   socket   socket used to send file
 * @param   filepath path to a file which is to be send
 * @param   server   argument used to print additional informations
 * @return           0 is returned if everithing was ok, -1 problem with opening a file, -2 problem with sending
 */
int send_file(int socket, char *filepath, char server) {
    FILE *fr = NULL;            // file
    long filesize = -1;         // filesize in bytes
    fr = fopen(filepath, "rb"); // open file

    // variable for loading ack
    char ack;

    // if there was problem with opening file
    if (fr == NULL) {
        // send -1 as filesize
        if (send(socket, (char *)&filesize, sizeof(long), 0) == -1)
            return -2;
        // wait for ack
        if (recv(socket, &ack, sizeof(char), 0) == -1)
            return -2;
        return -1;
    }

    // get filesize in bytes
    fseek(fr, 0L, SEEK_END);
    filesize = ftell(fr);
    rewind(fr);

    printf("Sending %s\nFilesize: %lu\n", filepath, filesize);

    // send filesize
    if (send(socket, (char *)&filesize, sizeof(long), 0) == -1)
        return -2;
    // wait for ack
    if (recv(socket, &ack, sizeof(char), 0) == -1)
        return -2;

    // buffer for part of the file
    char buffer[BUFFER_SIZE];
    // variables used to print progress
    int progress = 0, progress_pre = 0;
    // bytes left to send
    long bytes_left = filesize;
    // loop until whole file is send
    while (bytes_left > 0) {
        // number of bytes in buffer
        long buffer_size = bytes_left > BUFFER_SIZE ? BUFFER_SIZE : bytes_left;

        // load part of the file into the buffer
        fread(buffer, sizeof(char), BUFFER_SIZE, fr);

        // send part of the file
        if (send(socket, buffer, buffer_size, 0) == -1) {
            fclose(fr);
            return -2;
        }

        // print progress of sending in client
        if (server == 0) {
            progress = (filesize - bytes_left) / (filesize / 100);
            if (progress != progress_pre) {
                printf("\r%d%%", progress);
                fflush(stdout);
            }
            progress_pre = progress;
        }

        // wait for ack
        if (recv(socket, &ack, sizeof(char), 0) == -1) {
            fclose(fr);
            return -2;
        }

        // new value of bytes_left
        bytes_left -= buffer_size;
    }

    // 100% done
    if (server == 0)
        printf("\r100%%\n");

    // close file
    fclose(fr);

    return 0;
}

/**
 * Function for recieving file
 * 
 * @param   socket   socket used to recieve file
 * @param   filepath path to a file which is to be recieved
 * @param   server   argument used to print additional informations
 * @return           0 is returned if everithing was ok, -1 problem with opening a file, -2 problem with sending
 */
int recv_file(int socket, char *filepath, char server) {
    long filesize = -1; // filesize in bytes

    // recieves filesize
    if (recv(socket, (char *)&filesize, sizeof(long), 0) == -1)
        return -2;
    // sends ack
    if (send(socket, "", sizeof(char), 0) == -1)
        return -2;

    // file was not opend on the other side
    if (filesize == -1)
        return -1;

    printf("Recieving %s\nFilesize: %lu\n", filepath, filesize);

    FILE *fw = NULL;            // file
    fw = fopen(filepath, "wb"); // open file

    // if there was problem with opening file
    if (fw == NULL)
        return -1;

    // buffer for part of the file
    char buffer[BUFFER_SIZE];
    // variables used to print progress
    int progress = 0, progress_pre = 0;
    // bytes left to send
    long bytes_left = filesize;

    // loop until whole file is recieved
    while (bytes_left > 0) {
        // number of bytes in buffer
        long bytes_received = bytes_left > BUFFER_SIZE ? BUFFER_SIZE : bytes_left;

        // recieves part of the file
        if (recv(socket, buffer, bytes_received, 0) == -1) {
            fclose(fw);
            return -2;
        }

        // send ack
        if (send(socket, "", sizeof(char), 0) == -1) {
            fclose(fw);
            return -2;
        }

        // print progress of sending in client
        if (server == 0) {
            progress = (filesize - bytes_left) / (filesize / 100);
            if (progress != progress_pre) {
                printf("\r%d%%", progress);
                fflush(stdout);
            }
            progress_pre = progress;
        }

        // writes part of the file to file
        fwrite(buffer, sizeof(char), bytes_received, fw);

        // new value of bytes_left
        bytes_left -= bytes_received;
    }

    // 100% done
    if (server == 0)
        printf("\r100%%\n");

    // close file
    fclose(fw);

    return 0;
}
