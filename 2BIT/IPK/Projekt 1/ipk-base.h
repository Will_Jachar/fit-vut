/**
 * ipk-base.h
 *
 * IPK Project 1
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef IPK_BASE
#define IPK_BASE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#define MAX_FILENAME 255 + 1    // max length of filename
#define BUFFER_SIZE 1024        // length of one part of the file


// structure for program arguments
typedef struct args {
    char *server_hostname;
    char *filepath;
    char *port_number;
    char  rw;
    int   valid;
    char *err_msg;
} arguments_t;

// function for printing error and exiting
void exit_error(char *error, int err_code);
// function for sending file
int send_file(int socket, char *filepath, char server);
// function for recieving file
int recv_file(int socket, char *filepath, char server);


// function for loading, parsing and validating arguments (client and server has their own implementations)
arguments_t get_arguments(int argc, char *argv[]);

#endif