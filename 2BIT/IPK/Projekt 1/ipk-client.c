/**
 * ipk-client.cpp
 *
 * IPK Project 1
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

// #include "ipk-client.h"
#include "ipk-base.h"


/**
 * Main function of server
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       1 if error has occured, else keep running until interruped
 */
int main (int argc, char *argv[]) {
    int client_socket;                  // client socket
    struct addrinfo *addr_info = NULL;  // structure for information about address

    // load and check arguments
    arguments_t args = get_arguments(argc, argv);
    if (args.valid == 0)
        exit_error(args.err_msg, EXIT_FAILURE);

    // gets info about address
    if (getaddrinfo(args.server_hostname, args.port_number, NULL, &addr_info) != 0) {
        fprintf(stderr, "ERROR: no such host as %s\n", args.server_hostname);
        exit(EXIT_FAILURE);
    }

    // initiate client socket
    if ((client_socket = socket(addr_info->ai_family, SOCK_STREAM, IPPROTO_TCP)) <= 0)
        exit_error("socket", EXIT_FAILURE);

    // connects to server
    if (connect(client_socket, addr_info->ai_addr, addr_info->ai_addrlen) == -1)
        exit_error("connect", EXIT_FAILURE);

    char ack;   // variable for recieving ack
    char filename[MAX_FILENAME];    // buffer for outcoming filename
    // make copy of filename to buffer
    strncpy(filename, basename(args.filepath), MAX_FILENAME - 1);
    // adds r/w flag at the end
    filename[MAX_FILENAME - 1] = args.rw;

    // sends filename to server
    if (send(client_socket, filename, MAX_FILENAME, 0) == -1) {
        perror("ERROR in sendto");
        freeaddrinfo(addr_info);
        close(client_socket);
        return EXIT_FAILURE;
    }
    // waits for ack from server
    if (recv(client_socket, &ack, sizeof(char), 0) == -1) {
        perror("ERROR while sending file");
        freeaddrinfo(addr_info);
        close(client_socket);
        return EXIT_FAILURE;
    }

    int exit_code = 0;
    // if client wants to read some file from server
    if (args.rw == 'r') {
        // recieving file from server
        exit_code = recv_file(client_socket, args.filepath, 0);
        if (exit_code == -1)
            perror("ERROR while receiving file");
        else if (exit_code == -2)
            fprintf(stderr, "ERROR while receiving file %s\n", args.filepath);
    }
    // else client wants to send (write) some file to server
    else {
        // sending file to server
        exit_code = send_file(client_socket, args.filepath, 0);
        if (exit_code == -1)
            perror("ERROR while receiving file");
        else if (exit_code == -2)
            fprintf(stderr, "ERROR while sending file %s\n", args.filepath);
    }

    // frees structure with address info and closes socket
    freeaddrinfo(addr_info);
    close(client_socket);
    return EXIT_SUCCESS;
}


/**
 * Function for loading, parsing and checking arguments
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       argument_t structure with parsed arguments or with error message
 */
arguments_t get_arguments(int argc, char *argv[]) {
    // initializing structure
    arguments_t args = {
        .server_hostname = NULL,
        .filepath = NULL,
        .port_number = NULL,
        .rw = '\0',
        .valid = 0,
        .err_msg = ""
    };

    int option = 0;
    extern int optind, opterr;

    // supressing errors from getopt
    opterr = 0;
    while ((option = getopt(argc, argv, "h:p:r:w:")) != -1) {
        switch (option) {
            // argument with server hostname
            case 'h':
                args.server_hostname = optarg;
                break;
            // argument with port number
            case 'p':
                args.port_number = optarg;
                break;
            // argument with read flag
            case 'r':
                // check if there is only one r/w flag
                if (args.rw != '\0') {
                    args.err_msg = "Use only one of arguments '-r', '-w'";
                    return args;
                }
                args.rw = 'r';
                args.filepath = optarg;
                break;
            // argument with write flag
            case 'w':
                // check if there is only one r/w flag
                if (args.rw != '\0') {
                    args.err_msg = "Use only one of arguments '-r', '-w'";
                    return args;
                }
                args.rw = 'w';
                args.filepath = optarg;
                break;
            // unwanted argument
            default:
                args.err_msg = "Invalid or missing argument";
                return args;
        }
    }

    // if  server hostname, filepath or port number was not given
    if (args.server_hostname == NULL || args.filepath == NULL || args.port_number == NULL) {
        args.err_msg = "Invalid or missing argument";
        return args;
    }

    // if there were more or less arguments than we wanted
    if (argc - optind > 0) {
        args.err_msg = "Invalid number of arguments";
        return args;
    }

    // all was ok, we can set valid to 1
    args.valid = 1;
    return args;
}
