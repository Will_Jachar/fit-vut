/**
 * ipk-server.cpp
 *
 * IPK Project 1
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "ipk-base.h"


/**
 * Function for handling signal from OS
 * 
 * @param n code of the signal 
 */
void SigCatcher(int n) {
    (void)n;
	int pid = wait(NULL);
	printf("Child %d ended.\n", pid);
}

/**
 * Main function of server
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       1 if error has occured, else keep running until interruped
 */
int main (int argc, char *argv[]) {
    int rc, welcome_socket, port_number;    // read/write flag, welcome socket, port number
    struct sockaddr_in6 sa, sa_client;      // structures for information about addresses
    char str[INET6_ADDRSTRLEN];

    // load and check arguments
    arguments_t args = get_arguments(argc, argv);
    if (args.valid == 0)
        exit_error(args.err_msg, EXIT_FAILURE);

    port_number = atoi(args.port_number);

    // initiate welcome socket
    socklen_t sa_client_len = sizeof(sa_client);
    if ((welcome_socket = socket(PF_INET6, SOCK_STREAM, 0)) < 0) {
        perror("ERROR: socket");
        exit(EXIT_FAILURE);
    }

    // sets interface for server
    memset(&sa, 0, sizeof(sa));
    sa.sin6_family = AF_INET6;
    sa.sin6_addr = in6addr_any;
    sa.sin6_port = htons(port_number);

    // binds socket with interface we made before
    if ((rc = bind(welcome_socket, (struct sockaddr*)&sa, sizeof(sa))) < 0) {
        perror("ERROR: bind");
        exit(EXIT_FAILURE);
    }

    // starts to listen on given socket
    if ((listen(welcome_socket, 1)) < 0) {
        perror("ERROR: listen");
        exit(EXIT_FAILURE);
    }

    // picks up system signals
    signal(SIGCHLD, SigCatcher);

    while (1) {
        // initiate new client socket
        int client_socket = accept(welcome_socket, (struct sockaddr*)&sa_client, &sa_client_len);
        if (client_socket <= 0)
            continue;

        // fork into two processes
        int pid = fork();
		if (pid < 0) {
			perror("fork() failed");
			close(client_socket);
            continue;
		}

        // child process takes care of new newly connected client
        if (pid == 0) {
            // print some informations
            if (inet_ntop(AF_INET6, &sa_client.sin6_addr, str, sizeof(str))) {
                printf("INFO: New connection:\n"
                       "INFO: Client address is %s\n"
                       "INFO: Client port is %d\n", str, ntohs(sa_client.sin6_port));
            }

            // buffer for incoming filename
            char filename[MAX_FILENAME];
            // recieving filename with read/write flag from client
            if (recv(client_socket, filename, MAX_FILENAME, 0) == -1) {
                fprintf(stderr, "ERROR while receiving filename\n");
                close(client_socket);
                continue;
            }
            // sending ack back to client
            if (send(client_socket, "", sizeof(char), 0) == -1) {
                fprintf(stderr, "ERROR while receiving filename\n");
                close(client_socket);
                continue;
            }

            int exit_code = 0;
            // if client wants to read some file from server
            if (filename[MAX_FILENAME - 1] == 'r') {
                // sending file to client
                exit_code = send_file(client_socket, filename, 1);
                if (exit_code == -1) {
                    fprintf(stderr, "ERROR while opening file %s\n", filename);
                    close(client_socket);
                    continue;
                } else if (exit_code == -2) {
                    perror("ERROR while receiving file");
                }
            // else client wants to send (write) some file to server
            } else {
                // recieving file from client
                exit_code = recv_file(client_socket, filename, 1);
                if (exit_code == -1) {
                    fprintf(stderr, "ERROR while opening file %s\n", filename);
                    close(client_socket);
                    continue;
                } else if (exit_code == -2) {
                    perror("ERROR while sending file");
                }
            }
            // work of the server is done for this client
            printf("Connection to %s closed\n", str);
            close(client_socket);
            exit(0);
        } else {
            close(client_socket);
        }
    }
}


/**
 * Function for loading, parsing and checking arguments
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       argument_t structure with parsed arguments or with error message
 */
arguments_t get_arguments(int argc, char *argv[]) {
    // initializing structure
    arguments_t args = {
        .port_number = NULL,
        .valid = 0,
        .err_msg = ""
    };

    int option = 0;
    extern int optind, opterr;

    // supressing errors from getopt
    opterr = 0;
    while ((option = getopt(argc, argv, "p:")) != -1) {
        switch (option) {
            // argument with port number
            case 'p':
                args.port_number = optarg;
                break;
            // unwanted argument
            default:
                args.err_msg = "Invalid or missing argument";
                return args;
        }
    }

    // if port number was not given
    if (args.port_number == NULL) {
        args.err_msg = "Invalid or missing argument";
        return args;
    }

    // if there were more or less arguments than we wanted
    if (argc - optind > 0) {
        args.err_msg = "Invalid number of arguments";
        return args;
    }

    // all was ok, we can set valid to 1
    args.valid = 1;
    return args;
}
