/**
 * ipk-dhcpstarve.h
 *
 * IPK Project 2
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef IPK_DHCPSTARVE
#define IPK_DHCPSTARVE

#include <sys/types.h>
#include <netinet/udp.h>   // Provides declarations for udp header


// Structure for DHCP message
struct dhcp_packet {
    u_int8_t  op;                   //   0: Message type
    u_int8_t  htype;                //   1: Hardware addr type
    u_int8_t  hlen;                 //   2: Hardware addr length
    u_int8_t  hops;                 //   3: Number of relay agent hops from client
    u_int32_t xid;                  //   4: Transaction ID
    u_int16_t secs;                 //   8:
    u_int16_t flags;                //  10: Flag bits
    struct in_addr ciaddr;          //  12:
    struct in_addr yiaddr;          //  16:
    struct in_addr siaddr;          //  18:
    struct in_addr giaddr;          //  20:
    u_int32_t chaddr[4];            //  24: MAC address
    char sname [64];                //  40:
    char file [128];                // 104:
    unsigned char options [313];    // 212: Optional parameters
};

char *get_arguments(int argc, char *argv[]);
void SigCatcher(int n);

#endif