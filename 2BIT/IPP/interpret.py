#!/usr/bin/env python3

"""Interpreter for IPPcode18 language"""

import sys
import getopt
import re
import xml.etree.ElementTree as xml

# Structure for syntactic check contains types of operands
ARG_TYPES = {
    "VAR": ("var",),
    "SYMB": ("var", "string", "bool", "int",),
    "LABEL": ("label",),
    "TYPE": ("type",),
    "INT": ("int",),
    "STR": ("string",),
}

# Structure for syntactic check contains all instructions and types of their operands
INSTRUCTIONS = {
    "MOVE": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"]),
    "CREATEFRAME": (),
    "PUSHFRAME": (),
    "POPFRAME": (),
    "DEFVAR": (ARG_TYPES["VAR"],),
    "CALL": (ARG_TYPES["LABEL"],),
    "RETURN": (),
    "PUSHS": (ARG_TYPES["SYMB"],),
    "POPS": (ARG_TYPES["VAR"],),
    "ADD": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "SUB": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "MUL": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "IDIV": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "LT": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "GT": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "EQ": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "AND": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "OR": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "NOT": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"],),
    "INT2CHAR": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"],),
    "STRI2INT": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "READ": (ARG_TYPES["VAR"], ARG_TYPES["TYPE"],),
    "WRITE": (ARG_TYPES["SYMB"],),
    "CONCAT": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "STRLEN": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"],),
    "GETCHAR": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "SETCHAR": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "TYPE": (ARG_TYPES["VAR"], ARG_TYPES["SYMB"],),
    "LABEL": (ARG_TYPES["LABEL"],),
    "JUMPIFNEQ": (ARG_TYPES["LABEL"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "JUMPIFEQ": (ARG_TYPES["LABEL"], ARG_TYPES["SYMB"], ARG_TYPES["SYMB"],),
    "JUMP": (ARG_TYPES["LABEL"],),
    "DPRINT": (ARG_TYPES["SYMB"]),
    "BREAK": ()
}


class ExceptionWithCode(Exception):
    """Exception with custom error code passed as argument"""
    def __init__(self, msg, code):
        """Init exception"""
        super(ExceptionWithCode, self).__init__(msg)
        self.code = code

### CUSTOM EXCEPTIONS BASED ON ExceptionWithCode ########################################

class ArgumentError(ExceptionWithCode):
    """Argument exception"""
    pass


class InstructionError(ExceptionWithCode):
    """Instruction exception"""
    pass


class StackError(ExceptionWithCode):
    """Stack exception"""
    pass


class FrameStackError(ExceptionWithCode):
    """FrameStack exception"""
    pass


class InterpreterError(ExceptionWithCode):
    """Interpreter exception"""
    pass


class ArgumentTypeError(ExceptionWithCode):
    """Argument type exception"""
    pass

#########################################################################################


class Value():
    """Value class"""
    def __init__(self, val_type, val):
        """Init value"""
        self.type = val_type
        self.val = val

    def __repr__(self):
        """Visual representation of value"""
        return "Val({}={})".format(self.type, self.val)


class Argument():
    """Argument class"""
    def __init__(self, argument):
        """Init argument"""
        # Loads argument type
        self.type = argument.attrib["type"].lower()
        # Calls coresponding function for checking specific type of argument
        val = getattr(self, "_check_{}".format(self.type))(argument.text)
        # If function for checking argument returns None, than argument is not valid
        if val is not None:
            self.val = val
        else:
            raise ArgumentError("Invalid argument value", 32)

    def __repr__(self):
        """Visual representation of argument"""
        return "Arg(type={}, val={})".format(self.type, self.val)

    def _check_var(self, text):
        """Checks validity of argument of type VAR using regex"""
        return (text if text.isprintable() and
                re.match(r"^((GF|LF|TF)@([-$&%*\w]+[\S]*))$", text, re.I) else None)

    def _check_label(self, text):
        """Checks validity of argument of type LABEL using regex"""
        return (text if text.isprintable() and
                re.match(r"^([-$&%*\w]+[\S]*)$", text, re.I) else None)

    def _check_type(self, text):
        """Checks validity of argument of type TYPE using regex"""
        return text if re.match(r"^(string|bool|int)$", text, re.I) else None

    def _check_string(self, text):
        """Checks validity of argument of type STRING using regex"""
        if not text:        # Empty string
            return ""
        elif text.isprintable() and re.match(r"^([^\s#]*)$", text): # Basic regex for string check
            # Find all matches of escape sequetions inside of string
            esc_matches = re.findall(r"\\(\d{3})", text)
            # If there are more backslashes than escape sequetions its not valid string
            if len(esc_matches) != len(re.findall(r"\\", text)):
                return None
            # Replace all escape sequentions with unicode characters
            for esc_match in set(esc_matches):
                text = text.replace("\\" + esc_match, chr(int(esc_match)))
            return text
        else:
            return None

    def _check_int(self, text):
        """Checks validity of argument of type INT"""
        try:
            return int(text)
        except ValueError:
            return None

    def _check_bool(self, text):
        """Checks validity of argument of type BOOL"""
        if text.lower() == "true":
            return True
        elif text.lower() == "false":
            return False
        else:
            return None


class Instruction():
    """Instruction class"""
    def __init__(self, instruction):
        """Init instruction"""
        # Loads opcode of instruction and checks if its valid
        opcode = instruction.attrib["opcode"].upper()
        if opcode not in INSTRUCTIONS.keys():
            raise InstructionError("Invalid opcode '{}'".format(opcode), 32)
        self.opcode = opcode

        # Loads order of instruction and checks if its valid int
        try:
            self.order = int(instruction.attrib["order"])
        except ValueError:
            raise InstructionError("Invalid order code '{}'".format(
                instruction.attrib["order"]), 32)

        # Loads operands of instruction and checks if number of operand and its type is correct
        args = [Argument(arg) for arg in instruction]
        if len(args) != len(INSTRUCTIONS[self.opcode]):
            raise ArgumentError("Invalid number of arguments", 31)
        for arg, arg_type in zip(args, INSTRUCTIONS[self.opcode]):
            if arg.type not in arg_type:
                raise ArgumentError("Invalid argument type", 52)
        self.args = args

    def __repr__(self):
        """Visual representation of instruction"""
        return "{}: {}".format(self.opcode, self.args)


class Stack():
    """Stack class"""
    def __init__(self):
        """Init stack"""
        self.stack = []

    def __repr__(self):
        """Visual representation of stack"""
        return str(self.stack)

    def push(self, val):
        """Push value to stack"""
        self.stack.append(val)

    def pop(self):
        """Pop value form stack"""
        try:
            return self.stack.pop()
        except IndexError:
            raise StackError("Pop from an empty stack", 56)

    def top(self):
        """Returns top of the stack"""
        return self.stack[-1] if self.stack else None


class FrameStack():
    """Frame stack class"""
    def __init__(self):
        """Init frame stack"""
        self.frame_stack = Stack()
        self.frames = {
            "GF": {},
            "LF": None,
            "TF": None
        }

    def __repr__(self):
        """Returns visual represantation of actual state of frames and frame_stack"""
        return "Frame stack = {}\nGF = {}\nLF = {}\nTF = {}\n".format(
            self.frame_stack,
            self.frames["GF"],
            self.frames["LF"],
            self.frames["TF"])

    def create_frame(self):
        """Create new temporary frame"""
        self.frames["TF"] = {}

    def push_frame(self):
        """Pushe temporary frame to frame stack (to local frame)"""
        if self.frames["TF"] is None:
            raise FrameStackError("Temporary frame undefined", 55)
        self.frame_stack.push(self.frames["TF"])
        self.frames["TF"] = None
        self.frames["LF"] = self.frame_stack.top()

    def pop_frame(self):
        """Pop frame from frame stack (local frame) to temporary frame"""
        try:
            self.frames["TF"] = self.frame_stack.pop()
        except StackError:
            raise FrameStackError("Frame undefined", 55)
        self.frames["LF"] = self.frame_stack.top()

    def def_var(self, var):
        """Define variable inside frame"""
        var_frame, var_name = var.split("@")
        if self.frames[var_frame] is None:
            raise FrameStackError("Frame undefined", 55)
        try:
            self.frames[var_frame][var_name]
        except KeyError:
            self.frames[var_frame][var_name] = None
        else:
            raise FrameStackError("Variable already defined", 58)

    def get_var(self, var):
        """Get variables value from frame"""
        var_frame, var_name = var.split("@")
        try:
            val = self.frames[var_frame][var_name]
            if val is None:
                raise FrameStackError("Variable is not initialized", 56)
            return val
        except TypeError:
            raise FrameStackError("Frame undefined", 55)
        except KeyError:
            raise FrameStackError("Variable is not defined", 54)

    def set_var(self, var, val):
        """Set variables value"""
        var_frame, var_name = var.split("@")
        try:
            self.frames[var_frame][var_name]
        except TypeError:
            raise FrameStackError("Frame undefined", 55)
        except KeyError:
            raise FrameStackError("Variable is not defined", 54)
        self.frames[var_frame][var_name] = val


def type_check(same_types=False):
    """Decorator function for type checking of instructions operands"""
    def decorator(func):
        """Inner decorator"""
        def wrapper(self, *args):
            """Wrapper"""
            checked_args = []   # Already checked arguments
            symbs = []          # Symb arguments (can be value or variable)
            # For every pair of argument name - argument value
            for arg_name, arg in zip(func.__code__.co_varnames[1:], args):
                # Required type written in function annotations
                required_types = func.__annotations__[arg_name]
                if required_types is not None:      # Argument is not checked
                    arg = self.get_value(arg)       # Get value from symb
                    symbs.append(arg)
                    if required_types == "any":     # If "any" -> ("string", "int", "bool")
                        required_types = ("string", "int", "bool")
                    else:
                        required_types = (required_types,)
                    if arg.type not in required_types:      # Checking argument type
                        raise ArgumentTypeError("Argument of type '{}' should be '{}'".format(
                            arg.type, required_types), 53)
                checked_args.append(arg)
            # If all symb operands needs to have same type
            if same_types and symbs and not all([x.type == symbs[0].type for x in symbs]):
                raise ArgumentTypeError("Argument should be of the same type '{}'".format(
                    required_types), 53)
            # Call original functions with checked and processed arguments
            return func(self, *checked_args)
        return wrapper
    return decorator


class Interpreter():
    """Interpreter class"""
    def __init__(self):
        """Inin interpreter"""
        self.program_counter = 0
        self.instruction_count = 0
        self.labels = {}
        self.call_stack = Stack()
        self.data_stack = Stack()
        self.frames = FrameStack()

    def __repr__(self):
        """Returns visual represantation of interpretes actual state"""
        return ("Program Counter = {}\n"
                "Total instruction count = {}\n"
                "Labels = {}\n"
                "Call stack = {}\n"
                "Data stack = {}\n"
                "{}".format(
                    self.program_counter,
                    self.instruction_count,
                    self.labels,
                    self.call_stack,
                    self.data_stack,
                    self.frames))

    def run(self, program):
        """Function for interpreting a set of instructions given as argument"""
        if not program:
            return

        # Run over all instructions and load every label
        for instruction in program:
            if instruction.opcode == "LABEL":
                self._label(instruction.args[0].val, instruction.order - 1)

        # Main loop running over instructions a interpreting them
        while self.program_counter < len(program):
            # Instruction for readability
            instruction = program[self.program_counter]
            # Skipping LABEL instructions because labels were loaded up front
            if instruction.opcode != "LABEL":
                # Finds and calls coresponding function for instruction based of its opcode
                getattr(self, "_{}".format(instruction.opcode.lower()))(*instruction.args)
            self.program_counter += 1       # Increment program counter
            self.instruction_count += 1     # Increment total instructions interpreted counter

    def get_value(self, symb):
        """Function for retriveing value from symb which can be value itself or variable"""
        if symb.type == "var":
            return self.frames.get_var(symb.val)
        else:
            return Value(symb.type, symb.val)

    def get_label(self, label):
        """Gets program position (order number of instruction) of given label or throws an error"""
        try:
            return self.labels[label.val]
        except KeyError:
            raise InterpreterError("Label does not exist", 52)

    ### INSTRUCTIONS ########################################################################

    # Every function represents coresponding instruction
    # from IPPcode18 language as described in assignment

    def _createframe(self):
        self.frames.create_frame()

    def _pushframe(self):
        self.frames.push_frame()

    def _popframe(self):
        self.frames.pop_frame()

    def _defvar(self, var):
        self.frames.def_var(var.val)

    def _move(self, var, symb):
        val = self.get_value(symb)
        self.frames.set_var(var.val, val)

    def _call(self, label):
        self.call_stack.push(self.program_counter)
        self.program_counter = self.get_label(label)

    def _return(self):
        self.program_counter = self.call_stack.pop()

    def _pushs(self, symb):
        val = self.get_value(symb)
        self.data_stack.push(val)

    def _pops(self, var):
        val = self.data_stack.pop()
        self.frames.set_var(var.val, val)

    @type_check(same_types=True)
    def _add(self, var: None, symb1: "int", symb2: "int"):
        self.frames.set_var(var.val, Value("int", symb1.val + symb2.val))

    @type_check(same_types=True)
    def _sub(self, var: None, symb1: "int", symb2: "int"):
        self.frames.set_var(var.val, Value("int", symb1.val - symb2.val))

    @type_check(same_types=True)
    def _mul(self, var: None, symb1: "int", symb2: "int"):
        self.frames.set_var(var.val, Value("int", symb1.val * symb2.val))

    @type_check(same_types=True)
    def _idiv(self, var: None, symb1: "int", symb2: "int"):
        try:
            self.frames.set_var(var.val, Value("int", symb1.val // symb2.val))
        except ZeroDivisionError:
            raise InterpreterError("Zero division error", 57)

    @type_check(same_types=True)
    def _lt(self, var: None, symb1: "any", symb2: "any"):
        self.frames.set_var(var.val, Value("bool", symb1.val < symb2.val))

    @type_check(same_types=True)
    def _gt(self, var: None, symb1: "any", symb2: "any"):
        self.frames.set_var(var.val, Value("bool", symb1.val > symb2.val))

    @type_check(same_types=True)
    def _eq(self, var: None, symb1: "any", symb2: "any"):
        self.frames.set_var(var.val, Value("bool", symb1.val == symb2.val))

    @type_check(same_types=True)
    def _and(self, var: None, symb1: "bool", symb2: "bool"):
        self.frames.set_var(var.val, Value("bool", symb1.val and symb2.val))

    @type_check(same_types=True)
    def _or(self, var: None, symb1: "bool", symb2: "bool"):
        self.frames.set_var(var.val, Value("bool", symb1.val or symb2.val))

    @type_check()
    def _not(self, var: None, symb: "bool"):
        self.frames.set_var(var.val, Value("bool", not symb.val))

    @type_check()
    def _int2char(self, var: None, symb: "int"):
        try:
            self.frames.set_var(var.val, Value("string", chr(symb.val)))
        except (TypeError, ValueError, OverflowError):
            raise InterpreterError("Value is not a valid unicode code", 58)

    @type_check()
    def _stri2int(self, var: None, symb1: "string", symb2: "int"):
        # Checking if index is in range
        # (try-except not used because negative indexes are valid in python and not in IPPcode18)
        if 0 <= symb2.val < len(symb1.val):
            self.frames.set_var(var.val, Value("int", ord(symb1.val[symb2.val])))
        else:
            raise InterpreterError("Index out of range", 58)

    def _read(self, var, val_type):
        val = input()
        if val_type.val == "int":       # Converint input into int
            try:
                val = int(val)
            except ValueError:
                val = 0
        elif val_type.val == "bool":    # Converint input into bool
            val = True if val.lower() == "true" else False
        self.frames.set_var(var.val, Value(val_type.val, val))

    def _write(self, symb):
        val = self.get_value(symb)
        # Bool variables are printed different than standard Python bool
        if val.type == "bool":
            val.val = "true" if val.val else "false"
        print(val.val)

    @type_check(same_types=True)
    def _concat(self, var: None, symb1: "string", symb2: "string"):
        self.frames.set_var(var.val, Value("string", symb1.val + symb2.val))

    @type_check()
    def _strlen(self, var: None, symb: "string"):
        self.frames.set_var(var.val, Value("int", len(symb.val)))


    @type_check()
    def _getchar(self, var: None, symb1: "string", symb2: "int"):
        # Checking if index is in range
        # (try-except not used because negative indexes are valid in python and not in IPPcode18)
        if 0 <= symb2.val < len(symb1.val):
            self.frames.set_var(var.val, Value("string", symb1.val[symb2.val]))
        else:
            raise InterpreterError("Index out of range", 58)

    @type_check()
    def _setchar(self, var: None, symb1: "int", symb2: "string"):
        src_val = self.frames.get_var(var.val)      # Source string
        if src_val.type != "string":                # Checking if source variable is string
            raise ArgumentTypeError("Argument of type '{}' should be 'string'".format(
                src_val.type), 53)
        # Checking if index is in range
        # (try-except not used because negative indexes are valid in python and not in IPPcode18)
        if 0 <= symb1.val < len(symb2.val):
            # Disassembling string into mutable list and changing char at given index
            res_val = list(src_val.val)[symb1.val] = symb2.val
            self.frames.set_var(var.val, Value("string", "".join(res_val)))
        else:
            raise InterpreterError("Index out of range", 58)

    def _type(self, var, symb):
        try:
            val = self.get_value(symb).type
        except FrameStackError as err:
            if err.code == 56:          # If variable wasn't initialized return empty string
                val = ""
            else:                       # Other error than 'Not initialized' gets raised
                raise err
        self.frames.set_var(var.val, Value("type", val))

    def _label(self, label, order):
        try:
            self.labels[label]
        except KeyError:
            self.labels[label] = order
        else:
            InterpreterError("Redefining label", 52)

    def _jump(self, label):
        self.program_counter = self.get_label(label)

    @type_check(same_types=True)
    def _jumpifeq(self, label: None, symb1: "any", symb2: "any"):
        if symb1.val == symb2.val:
            self.program_counter = self.get_label(label)

    @type_check(same_types=True)
    def _jumpifneq(self, label: None, symb1: "any", symb2: "any"):
        if symb1.val != symb2.val:
            self.program_counter = self.get_label(label)

    def _dprint(self, symb):
        val = self.get_value(symb)
        # Bool variables are printed different than standard Python bool
        if val.type == "bool":
            val = "true" if val.val else "false"
        print(val, file=sys.stderr)

    def _break(self):
        print(self, file=sys.stderr)

    #########################################################################################


def print_help():
    """Print help"""
    print("Program interpret.py se spousti nasledovne, pricemz neni mozne argumenty kombinovat:\n",
          "\t./interpret.py [--help --source=FILE]\n",
          "\t   --help         ->  vypise napovedu k programu interpret.py\n",
          "\t   --source=FILE.xml  ->  FILE je soubor s jiz parsovanym kodem jazyka IPPcode18\n")


def main(argv):
    """Main function for loading XML and interpreting program instructions."""
    # Parsing command line arguments
    source_file = ""
    try:
        args, all_args = getopt.getopt(argv, "hs:", ["help", "source="])
        if all_args or len(args) != 1:
            raise getopt.GetoptError("Invalid or missing parameter")
        arg_name, arg_val = args[0]
        if arg_name in ("-h", "--help"):
            print_help()
            exit(0)
        elif arg_name in ("-s", "--source"):
            source_file = arg_val
    except getopt.GetoptError as err:
        print(err, file=sys.stderr)
        exit(10)

    try:
        # Loading XML file
        tree = xml.parse(source_file)
        program = tree.getroot()

        # Checking if XML has language IPPcode18
        if program.attrib["language"] != "IPPcode18":
            print("Language is not 'IPPcode18'", file=sys.stderr)
            exit(31)

        # Loading up instructions form XML file and checking instructions order
        instructions = []
        for order, ins in enumerate(program, start=1):
            if int(ins.attrib["order"]) != order:
                raise InstructionError("Missing or duplicite instruction", 52)
            instructions.append(Instruction(ins))

        # Interpret loaded instructions
        Interpreter().run(instructions)
    # Custom exceptions for interpret related errors
    except IOError as err:
        print(err, file=sys.stderr)
        exit(11)
    except ExceptionWithCode as err:
        print(err, file=sys.stderr)
        exit(err.code)
    # KeyError for XML related errors
    except (KeyError, xml.ParseError):
        print("XML not well-formed", file=sys.stderr)
        exit(31)
    # Other internal errors
    except Exception as err:
        print(err, file=sys.stderr)
        exit(99)


if __name__ == '__main__':
    main(sys.argv[1:])
