<?php

$symb   = "((GF|LF|TF)@([-_$&%*\w]+[\S]*)|(string)@([\S]*)|(bool)@(true|false){1}|(int)@([+-]?[\d]+))";
$var    = "((GF|LF|TF)@([-_$&%*\w]+[\S]*))";
$label  = "([-_$&%*\w]+[\S]*)";
$type   = "(string|bool|int)";

$INSTRUCTIONS = array(
    "MOVE"          => "/\s*$var\s*$symb$/i",
    "CREATEFRAME"   => "/(^$)/i",
    "PUSHFRAME"     => "/(^$)/i",
    "POPFRAME"      => "/(^$)/i",
    "DEFVAR"        => "/\s*$var$/i",
    "CALL"          => "/\s*$label$/i",
    "RETURN"        => "/(^$)/i",
    "PUSHS"         => "/\s*$symb$/i",
    "POPS"          => "/\s*$var$/i",
    "ADD"           => "/\s*$var\s*$symb\s*$symb$/i",
    "SUB"           => "/\s*$var\s*$symb\s*$symb$/i",
    "MUL"           => "/\s*$var\s*$symb\s*$symb$/i",
    "IDIV"          => "/\s*$var\s*$symb\s*$symb$/i",
    "LT"            => "/\s*$var\s*$symb\s*$symb$/i",
    "GT"            => "/\s*$var\s*$symb\s*$symb$/i",
    "EQ"            => "/\s*$var\s*$symb\s*$symb$/i",
    "AND"           => "/\s*$var\s*$symb\s*$symb$/i",
    "OR"            => "/\s*$var\s*$symb\s*$symb$/i",
    "NOT"           => "/\s*$var\s*$symb\s*$symb$/i",
    "INT2CHAR"      => "/\s*$var\s*$symb$/i",
    "STRI2INT"      => "/\s*$var\s*$symb\s*$symb$/i",
    "READ"          => "/\s*$var\s*$type$/i",
    "WRITE"         => "/\s*$symb$/i",
    "CONCAT"        => "/\s*$var\s*$symb\s*$symb$/i",
    "STRLEN"        => "/\s*$var\s*$symb$/i",
    "GETCHAR"       => "/\s*$var\s*$symb\s*$symb$/i",
    "SETCHAR"       => "/\s*$var\s*$symb\s*$symb$/i",
    "TYPE"          => "/\s*$var\s*$symb$/i",
    "LABEL"         => "/\s*$label$/i",
    "JUMPIFNEQ"     => "/\s*$label\s*$symb\s*$symb$/i",
    "JUMPIFEQ"      => "/\s*$label\s*$symb\s*$symb$/i",
    "JUMP"          => "/\s*$label$/i",
    "DPRINT"        => "/\s*$symb$/i",
    "BREAK"         => "/(^$)/i"
);

$RE_INSTRUCTIONS_PATTERN = "/(" . join("|", array_keys($INSTRUCTIONS)) . ")(.*)/i";


# MAIN #############################################################################################
# nacteni argumentu
$arguments = getopt("hs:", array("help", "source:"));

# kontrola validity argumentu
if (count($argv) > 2 or count($arguments) + 1 != count($argv)) {
    fwrite(STDERR, "Neplatna kombinace argumentu\n");
    exit(10);
}

# vypsani help
if (array_key_exists("help", $arguments)) {
    print_help();
}

# pouziti stdin nebo souboru na zaklade zadaneho argumentu
$filename = array_key_exists("source", $arguments) ? $arguments["source"] : "php://stdin";

$xml = new DOMDocument();
$xml->formatOutput = true;

# parsovani vstupu
$source_file;
parse_file($filename, $xml);

# vypsani vysledneho xml
$xml->save("php://stdout");
####################################################################################################

# funkce pro vypis napovedy
function print_help() {
    echo "Program parse.php se spousti nasledovne, pricemz neni mozne argumenty kombinovat:\n",
         "\tphp parse.php [--help --source=FILE]\n",
         "\t   --help         ->  vypise napovedu k programu parse.php\n",
         "\t   --source=FILE  ->  FILE je soubor s kodem jazyka IPPcode18, ktery se ma zkontrolovat\n";
    exit(0);
}

# funkce ktera prochazi vstup a parsuje ho
function parse_file($filename) {
    global $source_file;
    global $xml;

    # otevreni vstupniho souboru
    if ($source_file = fopen($filename, "r")) {
        # kontrola, jestli soubor zacina predepsanym zpusobem
        if (trim(strtoupper(fgets($source_file))) != ".IPPCODE18") {
            fwrite(STDERR, "Soubor nezacina s '.IPPcode18'\n");
            exit(21);
        }

        # zacatek vysledneho XML
        $xml_root = $xml->appendChild($xml->createElement("program"));
        $xml_root->appendChild(
            $xml->createAttribute("language"))->appendChild(
                $xml->createTextNode("IPPcode18"));

        $instruction_counter = 0;

        # zpracovavani vstupu po jednotlivych radcich
        while (!feof($source_file)) {
            $line = fgets($source_file);            # nacteni radku
            $line = trim(explode("#", $line)[0]);   # vymazani komentare
            if ($line) {                            # pokud se na radku nachazi i neco jineho nez komentar
                $instruction_counter += 1;
                # zpracovani radku
                parse_instruction($line, $instruction_counter, $xml_root);
            }
        }
        # uzavreni vstupniho souboru
        fclose($source_file);
    } else {
        fwrite(STDERR, "Chyba behem otevirani souboru\n");
        exit(11);
    }
}

# funkce pro parsovani jednoho radku (jedne instrukce) ze vstupu
function parse_instruction($instruction, $instruction_counter, $xml_root) {
    global $INSTRUCTIONS;
    global $RE_INSTRUCTIONS_PATTERN;
    global $source_file;
    global $xml;

    # kontrola, jestli se na radku nachazi validni instrukce
    if (preg_match($RE_INSTRUCTIONS_PATTERN, $instruction, $ins_match)) {
        $ins_match[1] = strtoupper($ins_match[1]);
        $re_subpattern = $INSTRUCTIONS[$ins_match[1]];              # vyhledani regularnio vyrau pro operandy dane instrukce
        # kontrola, jestli jsou operandy dane instrukce validni
        if (preg_match($re_subpattern, $ins_match[2], $op_match)) {
            # generovani XMP pro instrukci
            $xml_instruction = $xml_root->appendChild(
                $xml->createElement("instruction"));
            $xml_instruction->appendChild(
                $xml->createAttribute("order"))->appendChild(
                    $xml->createTextNode($instruction_counter));
            $xml_instruction->appendChild(
                $xml->createAttribute("opcode"))->appendChild(
                    $xml->createTextNode(strtoupper($ins_match[1])));

            # parsovani jednotlivich operandu
            parse_operands($ins_match[2], $xml_instruction);
        } else {
            fwrite(STDERR, "Neplatne operandy\n");
            fclose($source_file);
            exit(21);
        }
    } else {
        fwrite(STDERR, "Neplatna instrukce\n");
        fclose($source_file);
        exit(21);
    }
}

# funkce pro parsovani a zapsani operandu do XML
function parse_operands($operands, $xml_instruction) {
    global $source_file;
    global $xml;

    # pro kazdy operand
    foreach (preg_split("/\s+/", $operands) as $i => $op) {
        if ($op) {
            # pokud se v operatory nachazi '@' muze se jednat o 'var', nebo literal
            if (strpos($op, "@")) {
                $op_split = explode("@", $op);
                $first = strtoupper($op_split[0]);

                # pokud operand zacina 'GF', 'LF' nebo 'TF' jedna se o typ 'var'
                if ($first == "GF" or $first == "LF" or $first == "TF") {
                    $op_type = "var";
                    $op_val = $first . "@" . $op_split[1];
                # jinak se jedna o literal
                } else {
                    # kontrola validity escape sekvence
                    if (!check_esc_sequence($op_split[1])) {
                        fwrite(STDERR, "Neplatna escape sekvence\n");
                        fclose($source_file);
                        exit(21);
                    }
                    $op_type = $first;
                    # prevest true nebo false na lowercase
                    if ($first == "BOOL") {
                        $op_val = strtolower($op_split[1]);
                    } else {
                        $op_val = $op_split[1];
                    }
                }
            # typ type
            } elseif (strtolower($op) == "string" or strtolower($op) == "bool" or strtolower($op) == "int") {
                $op_type = "type";
                $op_val = strtolower($op);
            # jinak se jedna o label
            } else {
                $op_type = "label";
                $op_val = $op;
            }

            # generovani XML pro jednotlive operandy
            $xml_op = $xml_instruction->appendChild(
                $xml->createElement("arg" . ($i), $op_val));
            $xml_op->appendChild(
                $xml->createAttribute("type"))->appendChild(
                    $xml->createTextNode(strtoupper($op_type)));
        }
    }
}

# funkce pro kontrolu validity escape sekvence ve stringu
function check_esc_sequence($string) {
    $re_numpattern = "/\\\\([0-1]\d\d|[2][0-4]\d|25[0-5])/";
    if (substr_count($string, "\\") != preg_match_all($re_numpattern, $string, $esc_match)) {
        return false;
    }
    return true;
}

?>