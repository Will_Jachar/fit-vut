import re
import sys
import xml.etree.ElementTree as xml
from collections import OrderedDict


INSTRUCTIONS = OrderedDict(
    MOVE=r"\s*{var}\s*{symb}(.*)",
    CREATEFRAME=r"(^$)",
    PUSHFRAME=r"(^$)",
    POPFRAME=r"(^$)",
    DEFVAR=r"\s*{var}(.*)",
    CALL=r"\s*{label}(.*)",
    RETURN=r"(^$)",
    PUSHS=r"\s*{symb}(.*)",
    POPS=r"\s*{var}(.*)",
    ADD=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    SUB=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    MUL=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    IDIV=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    LT=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    GT=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    EQ=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    AND=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    OR=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    NOT=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    INT2CHAR=r"\s*{var}\s*{symb}(.*)",
    STRI2INT=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    READ=r"\s*{var}\s*{type}(.*)",
    WRITE=r"\s*{symb}(.*)",
    CONCAT=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    STRLEN=r"\s*{var}\s*{symb}(.*)",
    GETCHAR=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    SETCHAR=r"\s*{var}\s*{symb}\s*{symb}(.*)",
    TYPE=r"\s*{var}\s*{symb}(.*)",
    LABEL=r"\s*{label}(.*)",
    JUMPIFNEQ=r"\s*{label}\s*{symb}\s*{symb}(.*)",
    JUMPIFEQ=r"\s*{label}\s*{symb}\s*{symb}(.*)",
    JUMP=r"\s*{label}(.*)",
    DPRINT=r"\s*{symb}(.*)",
    BREAK=r"(^$)"
)

INSTRUCTION_RE = r"(" + r"|".join(INSTRUCTIONS.keys()) + r")"

OPERANDS_RE = dict(
    symb=(
        r"((GF|LF|TF)@([-_$&%*\w]+[\S]*)|"
        r"(string)@([\S]*)|"
        r"(bool)@(true|false){1}|"
        r"(int)@(-?[\d]+))"),
    var=r"((GF|LF|TF)@([-_$&%*\w]+[\S]*))",
    label=r"([-_$&%*\w]+[\S]*)",
    type=r"(string|bool|int)",
)


def parse(source):
    counter = 0
    xml_root = xml.Element("program", language="IPPcode18")
    pattern = INSTRUCTION_RE + r"\s*(.*)"
    for num, line in enumerate(source):
        try:
            line = line[:line.index("#")].strip()
        except ValueError:
            line = line.strip()
        if not line:
            continue
        # Syntactic check ###############################################
        counter += 1
        try:
            match = re.match(pattern, line, re.I).groups()
            subpattern = INSTRUCTIONS[match[0]].format(**OPERANDS_RE)
            op_match = re.match(subpattern, match[1], re.I).groups()
            if op_match[-1]:
                raise AttributeError
        except AttributeError:
            print("ERROR at line {}".format(num + 1), file=sys.stderr)
            exit(21)
        # Generate XML ##################################################
        xml_instruction = xml.SubElement(
            xml_root,
            "instruction",
            order=str(counter),
            opcode=match[0].upper())
        for i, op in enumerate(match[1].split()):
            if "@" in op:
                if op.upper().startswith(("GF", "LF", "TF")):
                    op_type = "var"
                    op_val = op
                else:
                    op_type, op_val = op.split("@")
            elif op in ("int", "string", "bool"):
                op_type = "type"
                op_val = op
            else:
                op_type = "label"
                op_val = op
            xml.SubElement(
                xml_instruction,
                "arg{}".format(i + 1),
                type=op_type).text = op_val
    return xml.ElementTree(xml_root)


def main():
    with open("test.ippcode", "r") as source:
        xml_tree = parse(source)
    if xml_tree:
        xml_tree.write("xuhrin02_tmp.xml")


if __name__ == '__main__':
    main()

#############################################################################
# TODO: zkontrolovat všude case insensitive
# TODO: návratové kódy
