import math
from functools import reduce

def cordic(alpha, n=16):

    alphatab = [math.atan(2**(-i)) for i in range(n)]

    K = reduce(lambda x, y: x * y, [math.cos(a) for a in alphatab])
    x, y, z = K, 0.0, alpha

    for i in range(n):
        if z < 0:
            xn = x + y * 2**(-i)
            yn = x - y * 2**(-i)
            z += alphatab[i]
        else:
            xn = x - y * 2**(-i)
            yn = x + y * 2**(-i)
            z -= alphatab[i]
        x, y = xn, yn

    return x, y


print(cordic(0))