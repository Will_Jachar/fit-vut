#!/usr/bin/env python3

import numpy as np
import scipy.io.wavfile as wav
from scipy.fftpack import fft
from scipy.signal import tf2zpk, freqz, lfilter

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter



def main():

    filename = "xuhrin02.wav"
    try:
        wav_file = wav.read(filename)
    except (FileNotFoundError, ValueError):
        print("Soubor", filename, "nelze otevrit")
        return

    # Filtr
    b = [0.2324, -0.4112, 0.2324]
    a = [1.0, 0.2289, 0.4662]

    # Informace o wav souboru
    rate = wav_file[0]
    frames = len(wav_file[1])
    duration = frames / rate

    # Signal
    signal = wav_file[1]

    # Operace nad signalem
    signal_fft = fft(signal)
    filtered_signal = lfilter(b, a, signal)
    filtered_signal_fft = fft(filtered_signal)


    # Uloha c. 1
    # -----------------------------------------------------------------------
    print("Uloha cislo  1:")

    print("\tVzorkovaci frekvence signalu:\t", rate, "Hz\n"
          "\tDelka signalu ve vzorcich:   \t", frames, "\n"
          "\tDelka signalu ve vterinach:  \t", duration, " s")
    # -----------------------------------------------------------------------


    # Uloha c. 2
    # -----------------------------------------------------------------------
    print("\nUloha cislo  2:")
    print("\t-- graf --")

    # y = 10 * np.log10(2.0 / rate * np.abs(signal_fft[0:rate // 2]))
    y = 2.0 / rate * np.abs(signal_fft[0:rate // 2])


    plt.plot(y, linewidth=0.5)
    plt.title("Úloha č.2 - Spektrum signálu")
    plt.xlabel("f (Hz)")
    plt.axvspan(y.argmax(), y.argmax(), color='red', alpha=0.25)
    plt.grid(alpha=0.2)

    plt.show()
    # -----------------------------------------------------------------------


    # Uloha c. 3
    # -----------------------------------------------------------------------
    print("\nUloha cislo  3:")

    print("\tMaximum modulu spektra je na frekvenci:\t", y.argmax(), "Hz")
    # -----------------------------------------------------------------------


    # Uloha c. 4
    # -----------------------------------------------------------------------
    print("\nUloha cislo  4:")
    print("\t-- graf --")

    n, p, k = tf2zpk(b, a)

    fig = plt.figure(figsize=(5, 5))
    plt.xlim([-1.5, 1.5])
    plt.ylim([-1.5, 1.5])

    ang = np.linspace(0, 2 * np.pi, 1000)
    plt.plot(np.cos(ang), np.sin(ang), linewidth=0.5)

    plt.plot(p.real, p.imag, "x")
    plt.plot(n.real, n.imag, "o", markerfacecolor="None")
    plt.title("Úloha č.4 - Nuly a póly přenosové funkce filtru")
    plt.grid(alpha=0.2)

    plt.show()
    # -----------------------------------------------------------------------


    # Uloha c. 5
    # -----------------------------------------------------------------------
    print("\nUloha cislo  5:")
    print("\t-- graf --")

    _, H = freqz(b, a, rate // 2)

    plt.plot(np.abs(H), linewidth=0.5)
    plt.title("Úloha č.5 - Model kmitočtové charakteristiky")
    plt.xlabel("f (Hz)")
    plt.ylabel(r"$|H(j\omega)|$")
    plt.grid(alpha=0.2)

    plt.show()
    # -----------------------------------------------------------------------


    # Uloha c. 6
    # -----------------------------------------------------------------------
    print("\nUloha cislo  6:")
    print("\t-- graf --")

    y = 2.0 / rate * np.abs(filtered_signal_fft[0:rate // 2])

    plt.plot(y, linewidth=0.5)
    plt.title("Úloha č.6 - Spektrum signálu")
    plt.xlabel("f (Hz)")
    plt.axvspan(y.argmax(), y.argmax(), color='red', alpha=0.25)
    plt.grid(alpha=0.2)

    plt.show()
    # -----------------------------------------------------------------------


    # Uloha c. 7
    # -----------------------------------------------------------------------
    print("\nUloha cislo  7:")
    print("\tMaximum modulu spektra je na frekvenci:\t", y.argmax(), "Hz")
    # -----------------------------------------------------------------------


    # Uloha c. 8
    # -----------------------------------------------------------------------
    print("\nUloha cislo  8:")
    print("\t-- graf --")

    x = range(frames)
    y = lfilter([1, 1, -1, -1] * 80, 1.0, signal)

    plt.plot(x, y, linewidth=0.5)
    plt.title("Úloha č.8 - Nalezení obdélníkových impulsů")
    plt.axvspan(y.argmax() - 160, y.argmax() + 160, color='red', alpha=0.25)
    plt.grid(alpha=0.2)

    plt.show()

    print("\tObdelnikove impulzy se vyskytuji kolem vzorku",
          y.argmax(), "(+- 160 vzorku)\n\tcoz odpovida cca",
          y.argmax() / rate, "s (+-", 160 / rate, "s)")
    # -----------------------------------------------------------------------


    # Uloha c. 9
    # -----------------------------------------------------------------------
    print("\nUloha cislo  9:")
    print("\t-- graf --")

    y = signal - np.mean(signal)
    norm = np.sum(y ** 2)
    coefs = (np.correlate(y, y, mode='full') / norm)[frames - 50: frames + 51]

    k = range(-50, 51)
    plt.plot(k, coefs, linewidth=0.5)
    plt.title("Úloha č.9 - Autokorelační koeficienty")
    plt.axvspan(10, 10, color='red', alpha=0.25)
    plt.grid(alpha=0.2)

    plt.show()
    # -----------------------------------------------------------------------


    # Uloha c. 10
    # -----------------------------------------------------------------------
    print("\nUloha cislo 10:")

    print("\tAutokorelacni koeficient R[10]:", coefs[50 + 10])
    # -----------------------------------------------------------------------


    # Uloha c. 11
    # -----------------------------------------------------------------------
    print("\nUloha cislo 11:")
    print("\t-- graf --")

    signal_min = int(min(signal))
    signal_max = int(max(signal))
    signal_range = signal_max - signal_min

    norm_signal = [
        int(round((signal[i] - signal_min) / signal_range * 50))
        for i in range(frames)]

    x, y = np.meshgrid(range(50), range(50))
    z = np.zeros((50, 50))

    # Funkce pro vypocitani pravdepodobnosti
    # (Samostatne pro zvyseni citelnosti a optimalizaci)
    def probability(norm_signal, x_pos, y_pos, offset):
        count = 0
        for i in range(len(norm_signal) - offset):
            if (norm_signal[i] == x_pos) and (norm_signal[i + offset] == y_pos):
                count += 1
        return count / frames

    for x_pos in range(0, 50):
        for y_pos in range(0, 50):
            z[x_pos][y_pos] = probability(norm_signal, x_pos, y_pos, 10)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surface = ax.plot_surface(x, y, z, cmap=cm.coolwarm)
    fig.colorbar(surface, shrink=0.5, aspect=5)
    plt.title("Úloha č.11 - Časový odhad sdružené funkce\n"
              "hustoty rozdělení pravděpodobnosti")

    plt.show()
    # -----------------------------------------------------------------------

    # Uloha c. 12
    # -----------------------------------------------------------------------
    print("\nUloha cislo 12:")

    print("\tPo secteni vsech pravdepodobnosti bychom měli dostat hodnotu 1 "
          "a dostali jsme:", np.sum(z))
    # -----------------------------------------------------------------------


    # Uloha c. 13
    # -----------------------------------------------------------------------
    print("\nUloha cislo 13:")

    for x_pos in range(0, 50):
        for y_pos in range(0, 50):
            z[x_pos][y_pos] = z[x_pos][y_pos] * x_pos * y_pos

    print("\tAutokorelacni koeficient R[10]:", np.sum(z))
    # -----------------------------------------------------------------------


if __name__ == "__main__":
    main()
