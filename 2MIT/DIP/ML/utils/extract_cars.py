# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import glob
import os
import xml.etree.ElementTree as ET

import cv2

from pathlib import Path


# Size of one car image
DEFAULT_SIZE = 64


def get_pklot_bounding_boxes(filename):
    """
    Convert PKLot annotations to python list
    """

    # Parse XML
    tree = ET.parse(filename)
    root = tree.getroot()

    bounding_boxes = []

    for space in root:
        # Get only bounding boxes with cars
        if space.attrib.get("occupied") != "1":
            continue

        x_coords = []
        y_coords = []

        # Get all points in contour and convert them to rectangle

        for point in space.find("contour"):
            x_coords.append(int(point.attrib["x"]))
            y_coords.append(int(point.attrib["y"]))

        x_coords.sort()
        y_coords.sort()

        x_min, x_max = x_coords[0], x_coords[-1]
        y_min, y_max = y_coords[0], y_coords[-1]

        side = max((x_max - x_min), (y_max - y_min))

        bounding_boxes.append((x_min, x_min + side, y_min, y_min + side))

    return bounding_boxes


def save_annotations(filename, annotations):
    """
    Save annotations with desired format
    """

    with open(filename, "w") as fw:
        for annotation in annotations:
            fw.write(
                f"{annotation['class']}"
                f" {annotation['x']:.6f} {annotation['y']:.6f}"
                f" {annotation['width']:.6f} {annotation['height']:.6f}\n"
            )


def process_pklot_dir(src_dir_path, dst_dir_path):
    """
    Process all the PKLot annotations
    """

    image_paths = Path(src_dir_path).rglob("*.jpg")

    for _, image_path in enumerate(image_paths, start=1):
        print(image_path)

        name = image_path.stem
        image_path = str(image_path)

        image = cv2.imread(image_path)

        # Parse annotations
        try:
            bounding_boxes = get_pklot_bounding_boxes(
                filename=f"{image_path.rsplit('.', 1)[0]}.xml",
            )
        except FileNotFoundError:
            continue

        # Save parsed annotations and extracted car image
        for car_number, bounding_box in enumerate(bounding_boxes, start=1):
            x_min, x_max, y_min, y_max = bounding_box

            cv2.imwrite(
                os.path.join(dst_dir_path, f"{name}#{car_number:03}.jpg"),
                cv2.resize(
                    image[y_min:y_max, x_min:x_max], (DEFAULT_SIZE, DEFAULT_SIZE)
                ),
            )


def main():
    src_path = "/mnt/d/Data/PKLot/PKLots"
    dst_path = "/mnt/d/Data/PKLot/Cars"

    src_dirs = glob.glob(os.path.join(src_path, "*/"))

    for dataset_dir in src_dirs:
        dir_name = dataset_dir.split("/")[-2]

        if dir_name == "PUCPR":
            continue

        process_pklot_dir(
            src_dir_path=dataset_dir,
            dst_dir_path=os.path.join(dst_path, dir_name),
        )


if __name__ == "__main__":
    main()
