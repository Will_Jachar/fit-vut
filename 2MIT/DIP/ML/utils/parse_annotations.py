# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import glob
import random
import xml.etree.ElementTree as ET

import cv2


def get_pklot_annotations(filename, image_width, image_height):
    """
    Convert PKLot annotations to python list
    """

    # Parse XML
    tree = ET.parse(filename)
    root = tree.getroot()

    annotations = []

    for space in root:
        # Get only bounding boxes with cars
        if space.attrib["occupied"] != "1":
            continue

        x_coords = []
        y_coords = []

        # Get all points in contour and convert them to rectangle

        for point in space.find("contour"):
            x_coords.append(int(point.attrib["x"]))
            y_coords.append(int(point.attrib["y"]))

        x_coords.sort()
        y_coords.sort()

        x_min, x_max = x_coords[0], x_coords[-1]
        y_min, y_max = y_coords[0], y_coords[-1]

        width, height = (x_max - x_min), (y_max - y_min)
        x_center, y_center = (x_min + (width / 2)), (y_min + (height / 2))

        # Normalize annotations
        annotations.append(
            {
                "class": 0,
                "x": x_center / image_width,
                "y": y_center / image_height,
                "width": width / image_width,
                "height": height / image_height,
            }
        )

    return annotations


def get_carpk_annotations(filename, image_width, image_height):
    """
    Convert CARPK annotations to python list
    """

    annotations = []

    with open(filename, "r") as fr:
        for line in fr:
            # Parse annotations
            x_min, y_min, x_max, y_max = map(int, line.split(" ")[:-1])

            width, height = (x_max - x_min), (y_max - y_min)
            x_center, y_center = (x_min + (width / 2)), (y_min + (height / 2))

            # Normalize annotations
            annotations.append(
                {
                    "class": 0,
                    "x": x_center / image_width,
                    "y": y_center / image_height,
                    "width": width / image_width,
                    "height": height / image_height,
                }
            )

    return annotations


def save_annotations(filename, annotations):
    """
    Save annotations with desired format
    """

    with open(filename, "w") as fw:
        for annotation in annotations:
            fw.write(
                f"{annotation['class']}"
                f" {annotation['x']:.6f} {annotation['y']:.6f}"
                f" {annotation['width']:.6f} {annotation['height']:.6f}\n"
            )


def main():
    image_names = glob.glob("*.jpg") + glob.glob("*.png")
    random.shuffle(image_names)

    for number, image_name in enumerate(image_names, start=1):
        filename = image_name[:-4]

        image = cv2.imread(image_name)

        image_height, image_width, *_ = image.shape

        # Change to get_pklot_annotations if needed
        annotations = get_carpk_annotations(
            filename=f"{filename}.txt",
            image_width=image_width,
            image_height=image_height,
        )

        save_annotations(
            filename=f"done/{number:03}.txt",
            annotations=annotations,
        )

        cv2.imwrite(f"done/{number:03}.jpg", image)


if __name__ == "__main__":
    main()
