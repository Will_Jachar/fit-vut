# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import glob
import os
import random

from collections import defaultdict
from math import ceil
from pathlib import Path

import imageio
import numpy as np
import tensorflow
import tensorflow_addons

from imgaug import augmenters as iaa
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import (
    Conv2D,
    Dense,
    Dropout,
    Flatten,
    Lambda,
    MaxPooling2D,
)
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam


# Size of car embedding vector
EMBEDDING_SIZE = 16


class TripletsGenerator:
    """
    Generator class for loading car images and augmenting them
    """

    def __init__(self, dataset_path):
        # Augmentation pipeline
        self.aug_pipeline = iaa.Sequential(
            [
                # Scale and translation
                iaa.Affine(
                    scale=(0.8, 1.2),
                    translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
                    mode="edge",
                ),
                # Change brightness
                iaa.MultiplyAndAddToBrightness(mul=(0.5, 1.5), add=(-32, 32)),
                # Change contrast
                iaa.GammaContrast((0.5, 2.0)),
                # Change color temperature
                iaa.ChangeColorTemperature((3000, 9000)),
            ]
        )

        self.dataset_filenames = self._load_filenames(dataset_path)

    def batch_generator(self, batch_size=32):
        while True:
            for dataset_dir in self.dataset_filenames:
                random.shuffle(dataset_dir)

            # Get filenames of parking lot images
            for filenames in zip(*self.dataset_filenames):
                filenames = [filename for sublist in filenames for filename in sublist]

                # Calculate augmentation count
                aug_count = max(ceil(batch_size / len(filenames)), 5)

                indices = np.arange(len(filenames) * aug_count)
                np.random.shuffle(indices)

                images = np.array(
                    [imageio.imread(filename) for filename in filenames] * aug_count
                )[indices][:batch_size]
                labels = np.array(list(range(len(filenames))) * aug_count)[indices][
                    :batch_size
                ]

                # imgaug version=0.4.0 has a known bug that will cause an crash here
                # manual fix is required until new version is released
                # https://github.com/aleju/imgaug/issues/646
                yield self.aug_pipeline(images=images), labels

    @staticmethod
    def _load_filenames(path):
        dataset_dirs = glob.glob(os.path.join(path, "*/"))

        dataset_filenames = []

        for dataset_dir in dataset_dirs:
            batch_filenames = defaultdict(list)

            for filename in Path(dataset_dir).glob("*.jpg"):
                batch_filenames[filename.stem.rsplit("_", 1)[0]].append(filename)

            dataset_filenames.append(list(batch_filenames.values()))

        return dataset_filenames


def create_embedding_model():
    """
    Create model for calculating embedding vecotrs for cars
    """

    model = Sequential()

    model.add(Conv2D(32, (3, 3), activation="relu", input_shape=(64, 64, 3)))
    model.add(Conv2D(32, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(64, (3, 3), activation="relu"))
    model.add(Conv2D(64, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(EMBEDDING_SIZE, activation=None))
    model.add(Lambda(lambda x: tensorflow.math.l2_normalize(x, axis=1)))

    return model


if __name__ == "__main__":
    model = create_embedding_model()
    model.compile(
        loss=tensorflow_addons.losses.TripletSemiHardLoss(),
        optimizer=Adam(0.001),
    )

    model.summary()

    # Change paths if necessary
    data_path = "data"
    checkpoint_path = "checkpoint/model_{epoch:02d}_{loss:.2f}"

    model_checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_path,
        save_weights_only=False,
        save_best_only=False,
    )

    generator = TripletsGenerator(data_path)

    model.fit(
        generator.batch_generator(256),
        steps_per_epoch=3500,
        epochs=5,
        callbacks=[model_checkpoint_callback],
    )
