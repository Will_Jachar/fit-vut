# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import os

from celery import Celery


# Initialize Celery app
app = Celery(
    "worker",
    backend=os.getenv("REDIS_URL"),
    broker=os.getenv("REDIS_URL"),
    include=["app.tasks"],
)
app.autodiscover_tasks()
