# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import cv2
import numpy as np

from app.modules.embedder import load_embedding_model
from app.modules.image import prepare_car_for_embedding


class DetectionPreprocessor:
    """
    Preprocessor class detecting cars in the image and calculating embadding vectors
    """

    DETECTION_THRESHOLD = 0.3  # Confidence for detections
    NMS_THRESHOLD = 0.3  # Threshold for NMS algorithm

    def __init__(self, detector_cfg_path, detector_weights_path, embedder_path):
        # Load and initialize detector and embedder
        self.detector = cv2.dnn_DetectionModel(detector_cfg_path, detector_weights_path)
        self.detector.setInputParams(size=(416, 416), scale=(1 / 255), swapRB=True)

        self.embedder = load_embedding_model(embedder_path)

    def preprocess_frame(self, frame):
        """
        Method performing detection and embedding calculation on the image
        """

        # Detect bounding boxes
        _, _, boxes = self.detector.detect(
            frame, self.DETECTION_THRESHOLD, self.NMS_THRESHOLD
        )

        if boxes.size == 0:
            return []

        frame_height, frame_width, *_ = frame.shape

        car_frames = []
        normalized_boxes = []

        # Normalize coordinates and prepare image for embedder
        for box in boxes:
            x_1, y_1, width, height = box
            x_2, y_2 = x_1 + width, y_1 + height

            car_frames.append(
                prepare_car_for_embedding(frame=frame, box=(x_1, y_1, x_2, y_2))
            )
            normalized_boxes.append(
                (
                    (float(x_1 / frame_width), float(y_1 / frame_height)),
                    (float(x_2 / frame_width), float(y_2 / frame_height)),
                )
            )

        # Calculate embeddings
        embeddings = self.embedder.predict(np.array(car_frames))

        return list(zip(normalized_boxes, embeddings))
