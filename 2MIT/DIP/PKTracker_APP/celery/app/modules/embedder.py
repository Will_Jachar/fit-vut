# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import tensorflow
import tensorflow_addons

from tensorflow.keras.layers import (
    Conv2D,
    Dense,
    Dropout,
    Flatten,
    Lambda,
    MaxPooling2D,
)
from tensorflow.keras.models import Sequential, load_model


# Size of car embedding vector
EMBEDDING_SIZE = 16


def create_embedding_model():
    """
    Create model for calculating embedding vecotrs for cars
    """

    model = Sequential()

    model.add(Conv2D(32, (3, 3), activation="relu", input_shape=(64, 64, 3)))
    model.add(Conv2D(32, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(64, (3, 3), activation="relu"))
    model.add(Conv2D(64, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(EMBEDDING_SIZE, activation=None))
    model.add(Lambda(lambda x: tensorflow.math.l2_normalize(x, axis=1)))

    return model


def load_embedding_model(filepath):
    """
    Load pretrained model for calculating embedding vecotrs for cars
    """

    return load_model(
        filepath, custom_objects={"tf": tensorflow, "tfa": tensorflow_addons}
    )
