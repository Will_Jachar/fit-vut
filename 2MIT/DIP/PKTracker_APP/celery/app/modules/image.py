# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import base64
import re

import cv2
import numpy as np


# Size of one car image
CAR_FRAME_SIZE = 64


def read_base64(data):
    """
    Load Base64 string as an OpenCV image
    """

    frame_data = re.sub("^data:image/.+;base64,", "", data)

    np_image = np.frombuffer(base64.b64decode(frame_data), np.uint8)
    image = cv2.imdecode(np_image, cv2.IMREAD_COLOR)

    return image


def pad_to_square(frame, target_size=CAR_FRAME_SIZE):
    """
    Resize image to CAR_FRAME_SIZE and add black bars to square the image
    """

    height, width, *_ = frame.shape

    ratio = target_size / max(width, height)
    new_width, new_height = int(width * ratio), int(height * ratio)

    frame = cv2.resize(frame, (new_width, new_height))

    delta_width = target_size - new_width
    delta_height = target_size - new_height
    left, right = delta_width // 2, delta_width - (delta_width // 2)
    top, bottom = delta_height // 2, delta_height - (delta_height // 2)

    return cv2.copyMakeBorder(
        frame, top, bottom, left, right, cv2.BORDER_CONSTANT, value=(0, 0, 0)
    )


def prepare_car_for_embedding(frame, box, target_size=CAR_FRAME_SIZE):
    """
    Resize image, add black bars to square the image and swaps color channels
    """

    left, top, right, bottom = box

    car_frame = pad_to_square(
        frame=frame[top:bottom, left:right], target_size=target_size
    )

    return car_frame[:, :, [2, 1, 0]]
