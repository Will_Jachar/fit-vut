# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import os

import cv2
import requests

from celery import Task

from app import app
from app.modules.image import read_base64
from app.modules.detection_preprocessor import DetectionPreprocessor


class MLPreprocessorTask(Task):
    """
    Celery ML preprocessor task
    """

    def __init__(self):
        super().__init__()

        self.preprocessor = None

    def __call__(self, *args, **kwargs):
        if not self.preprocessor:
            self.preprocessor = DetectionPreprocessor(
                detector_cfg_path=os.getenv("DETECTOR_CFG_PATH"),
                detector_weights_path=os.getenv("DETECTOR_WEIGHTS_PATH"),
                embedder_path=os.getenv("EMBEDDER_PATH"),
            )

        return self.run(*args, **kwargs)


@app.task(base=MLPreprocessorTask, bind=True)
def preprocess_frame(self, filename, frame, callback_url):
    """
    Task for detecting cars on image and calculating embedding vectors for them
    """

    frame = read_base64(frame)

    cv2.imwrite(os.path.join(os.getenv("FRAMES_DIR"), filename), frame)

    detections = self.preprocessor.preprocess_frame(frame=frame)

    # Send results back to Flask app
    requests.post(
        callback_url,
        json=[
            {"box": box, "embedding": embedding.tolist()}
            for box, embedding in detections
        ],
    )
