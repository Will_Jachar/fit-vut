# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import os

from celery import Celery
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

# Initialize Flask app
app = Flask(__name__, static_folder="static", template_folder="templates")
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("DATABASE_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

api = Api(app)

# Initialize celery
celery_app = Celery(
    __name__, backend=os.getenv("REDIS_URL"), broker=os.getenv("REDIS_URL")
)

# Initialize DB connection
db = SQLAlchemy(app, session_options={"autoflush": False})

# Import routes (to prevent circular importú
from app import routes
