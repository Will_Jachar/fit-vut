# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app.models.car import Car
from app.models.detection import Detection
from app.models.parking_lot import ParkingLot
from app.models.parking_spot import ParkingSpot
from app.models.record import Record
