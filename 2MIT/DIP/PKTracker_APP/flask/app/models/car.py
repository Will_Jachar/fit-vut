# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app import db
from app.models.dictable import DictableTable


class Car(db.Model, DictableTable):
    __tablename__ = "car"

    id = db.Column(db.Integer, primary_key=True)
    start_timestamp = db.Column(db.DateTime)
    end_timestamp = db.Column(db.DateTime)

    detections = db.relationship("Detection", back_populates="car", uselist=True)
