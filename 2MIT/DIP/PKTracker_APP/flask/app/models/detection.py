# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app import db
from app.models.dictable import DictableTable


class Detection(db.Model, DictableTable):
    __tablename__ = "detection"

    id = db.Column(db.Integer, primary_key=True)
    embedding = db.Column(db.ARRAY(db.Float), nullable=False)

    record_id = db.Column(
        db.Integer, db.ForeignKey("record.id", ondelete="CASCADE"), nullable=False
    )
    parking_spot_id = db.Column(
        db.Integer, db.ForeignKey("parking_spot.id", ondelete="CASCADE"), nullable=False
    )
    car_id = db.Column(
        db.Integer, db.ForeignKey("car.id", ondelete="CASCADE"), nullable=False
    )

    parking_spot = db.relationship("ParkingSpot", uselist=False)
    record = db.relationship("Record", back_populates="detections", uselist=False)
    car = db.relationship("Car", back_populates="detections", uselist=False)
