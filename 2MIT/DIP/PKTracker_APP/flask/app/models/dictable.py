# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from sqlalchemy import inspect


class DictableTable:
    """
    Class used for SQLAlchemy table classes to provide them (as)dict representation
    and (from)dict creation.
    """

    @classmethod
    def from_dict(cls, **source_dict):
        """
        Creates new SQLAlchemy table object based on data inside source_dict.
        """

        obj = cls()
        # Update only columns, not relationship attributes
        for column in inspect(cls).columns:
            value = source_dict.get(column.key)
            # Not to mess with default values inserted by Table itself
            if value is not None:
                setattr(obj, column.key, value)

        return obj

    def update_from_dict(self, **source_dict):
        """
        Updates attributes of SQLAlchemy table object from with data prof source_dict.
        """

        # Update only columns, not relationship attributes
        for column in inspect(type(self)).columns:
            if not column.primary_key and not column.foreign_keys:
                setattr(self, column.key, source_dict.get(column.key))

        return self