# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from collections.abc import Iterable

from sqlalchemy.sql.expression import UnaryExpression
from sqlalchemy.sql import operators
from sqlalchemy.types import Boolean, UserDefinedType


class Geometry(UserDefinedType):
    """
    SQLAlchemy representation of basic geometric types in PostgreSQL
    """

    name = "GEOMETRY"

    def get_col_spec(self, **kw):
        return self.name

    def bind_processor(self, dialect):
        def process(value):
            if value is None:
                return None

            return ",".join(
                [str(tuple(x)) if isinstance(x, Iterable) else str(x) for x in value]
            )

        return process

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is None:
                return None

            return [
                tuple(map(float, x.split(","))) for x in value.strip("()").split("),(")
            ]

        return process

    class comparator_factory(UserDefinedType.Comparator):
        """Define custom operations for geometry types."""

        def __eq__(self, other):
            return self.op("~=")(other)

        def center(self):
            return UnaryExpression(
                self.expr,
                operator=operators.custom_op("@@"),
                type_=Point,
            )

        def contains(self, other, **kwargs):
            return self.op("@>", return_type=Boolean)(other)


class Point(Geometry):
    """
    SQLAlchemy representation of 'point' geometric type in PostgreSQL
    """

    name = "POINT"

    def result_processor(self, dialect, coltype):
        def process(value):
            return super(Point, self).result_processor(dialect, coltype)(value)[0]

        return process


class Box(Geometry):
    """
    SQLAlchemy representation of 'box' geometric type in PostgreSQL
    """

    name = "BOX"

    def result_processor(self, dialect, coltype):
        def process(value):
            if value is None:
                return None

            box = super(Box, self).result_processor(dialect, coltype)(value)
            return (box[1], box[0])

        return process


class Polygon(Geometry):
    """
    SQLAlchemy representation of 'polygon' geometric type in PostgreSQL
    """

    name = "POLYGON"
