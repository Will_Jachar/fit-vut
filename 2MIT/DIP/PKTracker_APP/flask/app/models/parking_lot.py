# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from sqlalchemy import select, func, and_
from sqlalchemy.orm import column_property

from app import db
from app.models.dictable import DictableTable
from app.models.geometry import Polygon
from app.models.parking_spot import ParkingSpot
from app.models.record import Record


class ParkingLot(db.Model, DictableTable):
    __tablename__ = "parking_lot"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    roi = db.Column(Polygon(), nullable=False, default=((0, 0), (0, 1), (1, 1), (1, 0)))
    total_parking_spots = db.Column(db.Integer, nullable=False)
    total_detected_parking_spots = column_property(
        select([func.count(ParkingSpot.id)]).where(
            and_(
                ParkingSpot.parking_lot_id == id,
                ParkingSpot.valid.is_(True),
                ParkingSpot.removed.is_(False),
            )
        )
    )
    total_parking_spots_in_roi = column_property(
        select([func.count(ParkingSpot.id)]).where(
            and_(
                ParkingSpot.parking_lot_id == id,
                ParkingSpot.valid.is_(True),
                ParkingSpot.removed.is_(False),
                roi.contains(ParkingSpot.box_center),
            )
        )
    )
    last_updated = column_property(
        select([func.max(Record.created)]).where(Record.parking_lot_id == id)
    )

    parking_spots = db.relationship(
        "ParkingSpot", back_populates="parking_lot", uselist=True
    )
    records = db.relationship("Record", back_populates="parking_lot", uselist=True)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "total_parking_spots": self.total_parking_spots,
            "total_detected_parking_spots": self.total_detected_parking_spots,
            "total_parking_spots_in_roi": self.total_parking_spots_in_roi,
            "last_updated": self.last_updated,
        }
