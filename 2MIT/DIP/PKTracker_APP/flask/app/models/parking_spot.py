# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from sqlalchemy.orm import column_property

from app import db
from app.models.dictable import DictableTable
from app.models.geometry import Box


class ParkingSpot(db.Model, DictableTable):
    __tablename__ = "parking_spot"

    id = db.Column(db.Integer, primary_key=True)
    valid = db.Column(db.Boolean, nullable=False, default=False)
    removed = db.Column(db.Boolean, nullable=False, default=False)
    box = db.Column(Box(), nullable=False)
    box_center = column_property(box.center())

    parking_lot_id = db.Column(
        db.Integer, db.ForeignKey("parking_lot.id", ondelete="CASCADE"), nullable=False
    )

    parking_lot = db.relationship(
        "ParkingLot", back_populates="parking_spots", uselist=False
    )

    @property
    def box_area(self):
        return (self.box[1][0] - self.box[0][0]) * (self.box[1][1] - self.box[0][1])

    def to_dict(self):
        return {"id": self.id, "center": self.box_center}
