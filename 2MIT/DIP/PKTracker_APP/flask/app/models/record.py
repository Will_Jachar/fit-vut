# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app import db
from app.models.dictable import DictableTable


class Record(db.Model, DictableTable):
    __tablename__ = "record"

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String, nullable=False)
    created = db.Column(db.DateTime, nullable=False)

    parking_lot_id = db.Column(
        db.Integer, db.ForeignKey("parking_lot.id", ondelete="CASCADE"), nullable=False
    )

    parking_lot = db.relationship("ParkingLot", back_populates="records", uselist=False)
    detections = db.relationship("Detection", back_populates="record", uselist=True)
