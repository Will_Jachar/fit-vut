# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from scipy.spatial.distance import cdist


def calculate_intersect_area(first, second):
    """
    Calculate intersection area of two parking spots
    """

    left, top, right, bottom = (
        max(first.box[0][0], second.box[0][0]),
        max(first.box[0][1], second.box[0][1]),
        min(first.box[1][0], second.box[1][0]),
        min(first.box[1][1], second.box[1][1]),
    )

    if left > right or top > bottom:
        return 0

    return (right - left) * (bottom - top)


def is_same_spot(first, second, threshold):
    """
    Check if two parking spots are the same
    """

    intersect_area = calculate_intersect_area(first=first, second=second)
    union_area = first.box_area + second.box_area - intersect_area

    return intersect_area / union_area > threshold


def find_nearest_spots(first, second):
    """
    Find nearest parking spot for each of the given parking spots
    """

    if not second:
        return [(spot, None) for spot in first]

    first_positions = [spot.box[0] for spot in first]
    second_positions = [spot.box[0] for spot in second]

    nearest = cdist(first_positions, second_positions).argmin(axis=1)

    return [
        (spot, second[nearest_index]) for spot, nearest_index in zip(first, nearest)
    ]
