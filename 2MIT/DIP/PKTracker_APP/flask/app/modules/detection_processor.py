# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import numpy as np

from app.models import Car, Detection, ParkingSpot
from app.modules.bounding_box import find_nearest_spots, is_same_spot


class DetectionProcessor:
    """
    Detection processor class
    """

    NMS_THRESHOLD = 0.3  # Threshold for pairing parking spaces
    SIMILARITY_THRESHOLD = 0.7  # Threshold for embedding vector comparasion

    @classmethod
    def process_detections(cls, detections, timestamp, prev_cars, parking_lot):
        """
        Process detections from celery preprocessor
        """

        cars = cls._process_parking_spots(
            detections=detections, parking_lot=parking_lot
        )

        processed_cars = cls._process_cars(
            timestamp=timestamp, cars=cars, prev_cars=prev_cars
        )

        return processed_cars

    @classmethod
    def _process_parking_spots(cls, detections, parking_lot):
        """
        Convert raw detections to parking spot objects
        """

        if not detections:
            return []

        new_spots = []

        # Create parking spot objects from detections
        for detection in detections:
            new_spots.append(ParkingSpot(box=detection["box"]))

        # Pair parking spots
        nearest_spots = find_nearest_spots(
            first=new_spots, second=parking_lot.parking_spots
        )

        # Compare if are paired parking spots the same and use the old one if they are
        detected_spots = []
        for new_spot, old_spot in nearest_spots:
            if old_spot and is_same_spot(
                first=new_spot, second=old_spot, threshold=cls.NMS_THRESHOLD
            ):
                detected_spots.append(old_spot)
            else:
                detected_spots.append(new_spot)
                parking_lot.parking_spots.append(new_spot)

        return [
            Detection(embedding=detection["embedding"], parking_spot=parking_spot)
            for parking_spot, detection in zip(detected_spots, detections)
            if not parking_spot.removed
        ]

    @classmethod
    def _process_cars(cls, timestamp, cars, prev_cars):
        """
        Process detected cars
        """

        # If there are no new detections all all cars left
        if not cars:
            for car in prev_cars:
                car.car.end_timestamp = timestamp

            return []

        #  If there are no previous detections all new detection are new cars
        if not prev_cars:
            for car in cars:
                car.car = Car(start_timestamp=timestamp)

            return cars

        processed_cars = []
        removed_cars = set(prev_cars)

        # Compare cars on the same parking spots
        for car, prev_car in cls._pair_cars_on_same_spot(
            cars=cars, prev_cars=prev_cars
        ):
            # If there was a car on the same spot on previous frame
            if prev_car:
                try:
                    removed_cars.remove(prev_car)
                except KeyError:
                    continue

                # Calculate euclidian distance of embedding vectors
                similarity = np.linalg.norm(
                    np.subtract(car.embedding, prev_car.embedding)
                )

                # Compare two cars
                if similarity < cls.SIMILARITY_THRESHOLD:
                    car.car = prev_car.car
                    car.parking_spot.valid = True
                else:
                    prev_car.car.end_timestamp = timestamp
                    car.car = Car(start_timestamp=timestamp)
            else:
                car.car = Car(start_timestamp=timestamp)

            processed_cars.append(car)

        for car in removed_cars:
            car.car.end_timestamp = timestamp

        return processed_cars

    @classmethod
    def _pair_cars_on_same_spot(cls, cars, prev_cars):
        """
        Pair new cars with cars on the same parking spot from the previous frame
        """

        prev_cars_by_spot_id = {car.parking_spot.id: car for car in prev_cars}

        return [(car, prev_cars_by_spot_id.get(car.parking_spot.id)) for car in cars]
