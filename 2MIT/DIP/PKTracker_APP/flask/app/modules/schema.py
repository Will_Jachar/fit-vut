# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from marshmallow import Schema, fields, post_load, validate

from app.models import ParkingLot, ParkingSpot, Record
from app.modules.timestamp import convert_aware_to_utc, convert_utc_to_aware


class ConvertableDateTime(fields.AwareDateTime):
    """
    Datetime type for serialization
    """

    def _serialize(self, value, attr, obj, **kwargs):
        """
        Add UTC tzinfo
        """

        value = convert_utc_to_aware(value)

        return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        """
        Convert to UTC and remove tzinfo
        """

        value = super()._deserialize(value, attr, data, **kwargs)

        return convert_aware_to_utc(value)


class ParkingSpotFilterSchema(Schema):
    region_of_interest_only = fields.Boolean(default=False)

    @post_load
    def make_db_filters(self, data, **kwargs):
        """
        Create DB filters from data attributes
        """

        roi_filters = []

        if data.get("region_of_interest_only"):
            roi_filters = [ParkingLot.roi.contains(ParkingSpot.box_center)]

        return {**data, "region_of_interest_only": roi_filters}


class CumulativeStatsFilterSchema(ParkingSpotFilterSchema):
    start_timestamp = ConvertableDateTime(default=None, format="iso")
    end_timestamp = ConvertableDateTime(default=None, format="iso")

    @post_load
    def make_db_filters(self, data, **kwargs):
        """
        Create DB filters from data attributes
        """

        data = super().make_db_filters(data, **kwargs)

        time_filters = []

        start_timestamp = data.get("start_timestamp")
        end_timestamp = data.get("end_timestamp")

        if start_timestamp:
            time_filters.append(Record.created >= start_timestamp)
        if end_timestamp:
            time_filters.append(Record.created <= end_timestamp)

        return {**data, "time_filters": time_filters}


class FullStatsFilterSchema(CumulativeStatsFilterSchema):
    scale = fields.Str(
        required=True,
        validate=validate.OneOf(["year", "month", "day", "hour", "minute"]),
    )


class DetectionSchema(Schema):
    box = fields.Tuple(
        (
            fields.Tuple((fields.Float(), fields.Float())),
            fields.Tuple((fields.Float(), fields.Float())),
        ),
        required=True,
    )

    embedding = fields.List(fields.Float(), required=True)


class ParkingLotSchema(Schema):
    id = fields.Integer()
    name = fields.String(required=True)
    total_parking_spots = fields.Integer(required=True, validate=validate.Range(min=1))
    total_detected_parking_spots = fields.Integer()
    total_parking_spots_in_roi = fields.Integer()
    last_updated = ConvertableDateTime(format="iso")


class RegionOfInterestSchema(Schema):
    region_of_interest = fields.List(
        fields.Tuple((fields.Float, fields.Float)), required=True
    )


class ParkingLotFrameSchema(Schema):
    frame = fields.String(required=True)
    created = ConvertableDateTime(required=True, format="iso")


class ParkingSpotSchema(Schema):
    id = fields.Integer(required=True)
    center = fields.Tuple((fields.Float(), fields.Float()), required=True)


class StatsValuesSchema(Schema):
    total_cars = fields.Integer(required=True)
    average_occupancy = fields.Float(required=True)
    average_stay_time = fields.Float(required=True)


class BasicStatsSchema(Schema):
    cumulative_stats = fields.Nested(StatsValuesSchema, required=True)
    step_stats = fields.Dict(
        keys=ConvertableDateTime(format="iso"), values=fields.Nested(StatsValuesSchema)
    )


class SpotStatsSchema(BasicStatsSchema):
    id = fields.Integer(required=True)


class StatsSchema(Schema):
    time_steps = fields.List(ConvertableDateTime(format="iso"))
    basic_stats = fields.Nested(BasicStatsSchema, required=True)
    spot_stats = fields.Nested(SpotStatsSchema, many=True)
