# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import pytz

from dateutil.relativedelta import relativedelta


def convert_aware_to_utc(timestamp):
    """
    Convert to UTC and remove tzinfo
    """

    if not timestamp:
        return None

    return timestamp.astimezone(pytz.utc).replace(tzinfo=None)


def convert_utc_to_aware(timestamp):
    """
    Add UTC tzinfo
    """

    if not timestamp:
        return None

    return timestamp.replace(tzinfo=pytz.utc)


def get_time_steps(first_timestamp, last_timestamp, scale):
    """
    Create list of timestamp steps between first_timestamp and last_timestamp
    based on scale
    """

    time_deltas = {
        "year": relativedelta(years=1),
        "month": relativedelta(months=1),
        "day": relativedelta(days=1),
        "hour": relativedelta(hours=1),
        "minute": relativedelta(minutes=1),
    }

    delta = time_deltas[scale]

    time_steps = [first_timestamp]

    while time_steps[-1] < last_timestamp:
        time_steps.append(time_steps[-1] + delta)

    return time_steps


def truncate_timestamp(timestamp, scale):
    """
    Truncate timestamp beyond scale
    """

    truncate_params = {
        "year": {
            "month": 1,
            "day": 1,
            "hour": 0,
            "minute": 0,
            "second": 0,
            "microsecond": 0,
        },
        "month": {"day": 1, "hour": 0, "minute": 0, "second": 0, "microsecond": 0},
        "day": {"hour": 0, "minute": 0, "second": 0, "microsecond": 0},
        "hour": {"minute": 0, "second": 0, "microsecond": 0},
        "minute": {"second": 0, "microsecond": 0},
    }

    return timestamp.replace(**truncate_params[scale])
