# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from collections import defaultdict

from sqlalchemy import distinct, func

from app import models


def get_cumulative_stats(parking_lot, parking_spots, time_filters):
    """
    Retrieve cumulative statistics for whole parking lot from the database
    """

    return (
        models.ParkingLot.query.join(models.Record)
        .join(models.Detection)
        .join(models.ParkingSpot)
        .join(models.Car)
        .with_entities(
            func.count(distinct(models.Record.id)).label("records_count"),
            func.count(distinct(models.Detection.id)).label("cumulative_cars_count"),
            func.count(distinct(models.Detection.car_id)).label("distinct_cars_count"),
            func.avg(
                func.coalesce(models.Car.end_timestamp, parking_lot.last_updated)
                - models.Car.start_timestamp
            ).label("average_stay_time"),
        )
        .filter(
            models.ParkingSpot.id.in_(parking_spots),
            *time_filters,
        )
        .first()
    )


def get_step_stats(parking_lot, parking_spots, time_filters, scale):
    """
    Retrieve step statistics for whole parking lot from the database
    """

    return (
        models.ParkingLot.query.join(models.Record)
        .join(models.Detection)
        .join(models.ParkingSpot)
        .join(models.Car)
        .with_entities(
            func.date_trunc(scale, models.Record.created).label("timestamp"),
            func.count(distinct(models.Record.id)).label("records_count"),
            func.count(distinct(models.Detection.id)).label("cumulative_cars_count"),
            func.count(distinct(models.Detection.car_id)).label("distinct_cars_count"),
            func.avg(
                func.coalesce(models.Car.end_timestamp, parking_lot.last_updated)
                - models.Car.start_timestamp
            ).label("average_stay_time"),
        )
        .filter(
            models.ParkingSpot.id.in_(parking_spots),
            *time_filters,
        )
        .group_by(func.date_trunc(scale, models.Record.created))
        .order_by(func.date_trunc(scale, models.Record.created))
        .all()
    )


def get_cumulative_spot_stats(parking_lot, parking_spots, time_filters):
    """
    Retrieve cumulative statistics for each parking spot from the database
    """

    return (
        models.ParkingLot.query.join(models.Record)
        .join(models.Detection)
        .join(models.ParkingSpot)
        .join(models.Car)
        .with_entities(
            models.ParkingSpot.id,
            func.count(distinct(models.Detection.id)).label("cumulative_cars_count"),
            func.count(distinct(models.Detection.car_id)).label("distinct_cars_count"),
            func.avg(
                func.coalesce(models.Car.end_timestamp, parking_lot.last_updated)
                - models.Car.start_timestamp
            ).label("average_stay_time"),
        )
        .filter(
            models.ParkingSpot.id.in_(parking_spots),
            *time_filters,
        )
        .group_by(models.ParkingSpot.id)
        .all()
    )


def get_step_spot_stats(parking_lot, parking_spots, time_filters, scale):
    """
    Retrieve step statistics for each parking spot from the database
    """

    return (
        models.ParkingLot.query.join(models.Record)
        .join(models.Detection)
        .join(models.ParkingSpot)
        .join(models.Car)
        .with_entities(
            models.ParkingSpot.id,
            func.date_trunc(scale, models.Record.created).label("timestamp"),
            func.count(distinct(models.Detection.id)).label("cumulative_cars_count"),
            func.count(distinct(models.Detection.car_id)).label("distinct_cars_count"),
            func.avg(
                func.coalesce(models.Car.end_timestamp, parking_lot.last_updated)
                - models.Car.start_timestamp
            ).label("average_stay_time"),
        )
        .filter(
            models.ParkingSpot.id.in_(parking_spots),
            *time_filters,
        )
        .group_by(models.ParkingSpot.id, func.date_trunc(scale, models.Record.created))
        .order_by(func.date_trunc(scale, models.Record.created))
        .all()
    )


def process_cumulative_stats(total_spots, cumulative_stats):
    """
    Finish calculating cumulative statistics for whole parking lot
    and create result structure
    """

    if cumulative_stats.records_count:
        stats = {
            "total_cars": cumulative_stats.distinct_cars_count,
            "average_occupancy": (
                cumulative_stats.cumulative_cars_count
                / (total_spots * cumulative_stats.records_count)
            ),
            "average_stay_time": cumulative_stats.average_stay_time.total_seconds()
            / 3600,
        }
    else:
        stats = {"total_cars": 0, "average_occupancy": 0, "average_stay_time": 0}

    return stats, cumulative_stats.records_count


def process_step_stats(total_spots, step_stats):
    """
    Finish calculating step statistics for whole parking lot and create result structure
    """

    time_steps = []
    step_stats_by_timestamp = {}
    step_records_count = {}

    for row in step_stats:
        time_steps.append(row.timestamp)
        step_stats_by_timestamp[row.timestamp] = {
            "total_cars": row.distinct_cars_count,
            "average_occupancy": (
                row.cumulative_cars_count / (total_spots * row.records_count)
            ),
            "average_stay_time": row.average_stay_time.total_seconds() / 3600,
        }
        step_records_count[row.timestamp] = row.records_count

    return time_steps, step_stats_by_timestamp, step_records_count


def process_cumulative_spot_stats(cumulative_spot_stats, cumulative_records_count):
    """
    Finish calculating cumulative statistics for each parking spot
    and create result structure
    """

    cumulative_stats_by_spot_id = defaultdict(
        lambda: {"total_cars": 0, "average_occupancy": 0, "average_stay_time": 0}
    )

    for row in cumulative_spot_stats:
        cumulative_stats_by_spot_id[row.id] = {
            "total_cars": row.distinct_cars_count,
            "average_occupancy": row.cumulative_cars_count / cumulative_records_count,
            "average_stay_time": row.average_stay_time.total_seconds() / 3600,
        }

    return cumulative_stats_by_spot_id


def process_step_spot_stats(step_spot_stats, step_records_count):
    """
    Finish calculating step statistics for each parking spot and create result structure
    """

    step_stats_by_spot_id = defaultdict(dict)

    for row in step_spot_stats:
        step_stats_by_spot_id[row.id][row.timestamp] = {
            "total_cars": row.distinct_cars_count,
            "average_occupancy": (
                row.cumulative_cars_count / step_records_count[row.timestamp]
            ),
            "average_stay_time": row.average_stay_time.total_seconds() / 3600,
        }

    return step_stats_by_spot_id
