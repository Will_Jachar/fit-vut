# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app.resources.detections import Detections
from app.resources.parking_lots import ParkingLots
from app.resources.regions_of_interest import RegionsOfInterest
from app.resources.parking_lot_frames import ParkingLotFrames, ParkingLotLatestFrame
from app.resources.parking_spots import ParkingSpots
from app.resources.cumulative_statistics import CumulativeStatistics
from app.resources.full_statistics import FullStatistics
