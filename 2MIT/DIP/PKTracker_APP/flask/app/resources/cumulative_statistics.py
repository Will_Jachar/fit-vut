# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import models
from app.modules.schema import CumulativeStatsFilterSchema, StatsSchema
from app.modules.use_cases.statistics import (
    get_cumulative_stats,
    get_cumulative_spot_stats,
    process_cumulative_stats,
    process_cumulative_spot_stats,
)


class CumulativeStatistics(Resource):
    """
    Cumulative statistics resource for REST API
    """

    def get(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        # Parse query filters
        try:
            filters = CumulativeStatsFilterSchema().load(request.args)
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Retrieve all valid parking spots i roi
        parking_spots_in_roi = (
            models.ParkingSpot.query.join(models.ParkingLot)
            .with_entities(models.ParkingSpot.id)
            .filter(
                models.ParkingLot.id == parking_lot_id,
                models.ParkingSpot.removed.is_(False),
                models.ParkingSpot.valid.is_(True),
                *filters["region_of_interest_only"],
            )
        ).all()

        parking_spots_ids = [parking_spot.id for parking_spot in parking_spots_in_roi]

        # Get cumulative statistics from DB
        cumulative_record_stats = get_cumulative_stats(
            parking_lot=parking_lot,
            parking_spots=parking_spots_ids,
            time_filters=filters["time_filters"],
        )

        cumulative_spot_stats = get_cumulative_spot_stats(
            parking_lot=parking_lot,
            parking_spots=parking_spots_ids,
            time_filters=filters["time_filters"],
        )

        # Use bigger value so occupancy is never over 100%
        total_spots = max(
            parking_lot.total_parking_spots, parking_lot.total_parking_spots_in_roi
        )

        # Process stats from DB
        cumulative_stats, cumulative_records_count = process_cumulative_stats(
            total_spots=total_spots, cumulative_stats=cumulative_record_stats
        )

        cumulative_stats_by_spot_id = process_cumulative_spot_stats(
            cumulative_spot_stats=cumulative_spot_stats,
            cumulative_records_count=cumulative_records_count,
        )

        return (
            StatsSchema().dump(
                {
                    "basic_stats": {"cumulative_stats": cumulative_stats},
                    "spot_stats": [
                        {
                            "id": parking_spot.id,
                            "cumulative_stats": (
                                cumulative_stats_by_spot_id[parking_spot.id]
                            ),
                        }
                        for parking_spot in parking_spots_in_roi
                    ],
                }
            ),
            200,
        )
