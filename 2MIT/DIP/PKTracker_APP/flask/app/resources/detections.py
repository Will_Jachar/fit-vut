# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import db, models
from app.modules.detection_processor import DetectionProcessor
from app.modules.schema import DetectionSchema


class Detections(Resource):
    """
    Detections resource for REST API
    """

    def post(self, record_id):
        record = models.Record.query.get(record_id)

        if not record:
            abort(404, message="Record doesn't exist")

        # Validate request data
        try:
            raw_detections = DetectionSchema(many=True).load(request.get_json())
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Get previous record
        prev_record = (
            models.Record.query.filter(models.Record.created < record.created)
            .order_by(models.Record.created.desc())
            .first()
        )
        prev_cars = prev_record.detections if prev_record else []

        # Process new detections
        detections = DetectionProcessor.process_detections(
            detections=raw_detections,
            timestamp=record.created,
            prev_cars=prev_cars,
            parking_lot=record.parking_lot,
        )

        # Save detections to DB
        record.detections.extend(detections)

        db.session.commit()

        return {}, 204
