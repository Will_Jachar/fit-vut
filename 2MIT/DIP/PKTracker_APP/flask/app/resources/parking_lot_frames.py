# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import os

from urllib.parse import urljoin

from flask import request, send_from_directory, url_for
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import db, models, celery_app
from app.modules.schema import ParkingLotFrameSchema


class ParkingLotLatestFrame(Resource):
    """
    Parking lot latest frame resource for REST API
    """

    def get(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        # Get latest record in DB
        latest_record = (
            models.Record.query.filter(models.Record.parking_lot_id == parking_lot_id)
            .order_by(models.Record.created.desc())
            .first()
        )

        if not latest_record:
            return {}, 204

        # Return frame
        return send_from_directory(os.getenv("FRAMES_DIR"), latest_record.filename)


class ParkingLotFrames(Resource):
    """
    Parking lot frames resource for REST API
    """

    def post(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        # Validate request data
        try:
            frames = sorted(
                ParkingLotFrameSchema(many=True).load(request.get_json()),
                key=lambda x: x["created"],
            )
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Get previous record and check if the new record is not out of order
        prev_record = models.Record.query.order_by(models.Record.created.desc()).first()

        if prev_record and prev_record.created >= frames[0]["created"]:
            abort(400, message="Sequence out of order")

        tasks = []

        # Create celery task for each frame
        for frame_info in frames:
            filename = (
                f"{parking_lot.id:04}_"
                f"{frame_info['created'].strftime('%Y-%m-%d_%H_%M_%S')}.jpg"
            )

            # Save record to DB
            record = models.Record(
                filename=filename,
                created=frame_info["created"],
                parking_lot=parking_lot,
            )

            db.session.add(record)
            db.session.flush()

            # Create celery task
            tasks.append(
                celery_app.send_task(
                    "app.tasks.preprocess_frame",
                    kwargs={
                        "filename": filename,
                        "frame": frame_info["frame"],
                        "callback_url": urljoin(
                            os.getenv("FLASK_CALLBACK_URL"),
                            url_for("detections", record_id=record.id),
                        ),
                    },
                ).id
            )

        db.session.commit()

        return {"tasks": tasks}, 201
