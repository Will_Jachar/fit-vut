# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import db
from app import models
from app.modules.schema import ParkingLotSchema


class ParkingLots(Resource):
    """
    Parking lots resource for REST API
    """

    def get(self, parking_lot_id=None):
        # Return one parking lot or all of them
        if parking_lot_id:
            parking_lot = models.ParkingLot.query.get(parking_lot_id)

            if not parking_lot:
                abort(404, message="Parking lot doesn't exist")

            return ParkingLotSchema().dump(parking_lot.to_dict()), 200

        parking_lots = models.ParkingLot.query.order_by(models.ParkingLot.id).all()

        return (
            ParkingLotSchema(many=True).dump(
                [parking_lot.to_dict() for parking_lot in parking_lots]
            ),
            200,
        )

    def post(self):
        # Validate request data
        try:
            parking_lot_data = ParkingLotSchema().load(request.get_json())
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Create new parking lot
        parking_lot = models.ParkingLot.from_dict(**parking_lot_data)

        db.session.add(parking_lot)
        db.session.commit()

        return ParkingLotSchema().dump(parking_lot.to_dict()), 201

    def patch(self, parking_lot_id=None):
        if not parking_lot_id:
            abort(400, message="Parking lot ID missing")

        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        # Validate request data
        try:
            parking_lot_data = ParkingLotSchema().load(request.get_json())
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Update parking lot
        parking_lot.update_from_dict(**parking_lot_data)

        db.session.commit()

        return ParkingLotSchema().dump(parking_lot.to_dict()), 200
