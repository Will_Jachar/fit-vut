# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import db
from app import models
from app.modules.schema import ParkingSpotSchema, ParkingSpotFilterSchema


class ParkingSpots(Resource):
    """
    Parking spots resource for REST API
    """

    def get(self, parking_lot_id, parking_spot_id=None):
        # Validate query filters
        try:
            filters = ParkingSpotFilterSchema().load(request.args)
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Create query to get all valid parking spots that satisfy filters
        parking_spots_query = models.ParkingSpot.query.join(models.ParkingLot).filter(
            models.ParkingLot.id == parking_lot_id,
            models.ParkingSpot.removed.is_(False),
            models.ParkingSpot.valid.is_(True),
            *filters["region_of_interest_only"],
        )

        # Get only one parking spot or all of them
        if parking_spot_id:
            parking_spot = parking_spots_query.filter(
                models.ParkingSpot.id == parking_spot_id
            ).first()

            if not parking_spot:
                abort(404, message="Parking spot doesn't exist")

            return ParkingSpotSchema().dump(parking_spot.to_dict()), 200

        parking_spots = parking_spots_query.all()

        return (
            ParkingSpotSchema(many=True).dump(
                [parking_spot.to_dict() for parking_spot in parking_spots]
            ),
            200,
        )

    def delete(self, parking_lot_id, parking_spot_id=None):
        if not parking_spot_id:
            abort(400, message="Parking spot ID missing")

        parking_spot = models.ParkingSpot.query.get(parking_spot_id)

        # Cannot remove nonexisting parking spot
        if (
            not parking_spot
            or parking_spot.parking_lot_id != parking_lot_id
            or parking_spot.removed
        ):
            abort(404, message="Parking spot doesn't exist")

        # Remove parking spot
        parking_spot.removed = True

        db.session.commit()

        return {}, 204
