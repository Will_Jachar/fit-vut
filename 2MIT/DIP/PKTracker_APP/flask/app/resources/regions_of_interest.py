# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import request
from flask_restful import Resource, abort
from marshmallow import ValidationError

from app import db
from app import models
from app.modules.schema import RegionOfInterestSchema


class RegionsOfInterest(Resource):
    """
    Regions of interest resource for REST API
    """

    def get(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        return (
            RegionOfInterestSchema().dump({"region_of_interest": parking_lot.roi}),
            200,
        )

    def patch(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404, message="Parking lot doesn't exist")

        # Validate request data
        try:
            roi_data = RegionOfInterestSchema().load(request.get_json())
        except ValidationError as error:
            abort(400, message="Invalid data format", errors=error.messages)

        # Check if roi is a valid polygon with some area
        if len(roi_data["region_of_interest"]) < 3:
            abort(400, message="Region of interest must have at least three points")

        # Update roi
        parking_lot.roi = roi_data["region_of_interest"]

        db.session.commit()

        return RegionOfInterestSchema().dump({"region_of_interest": parking_lot.roi})
