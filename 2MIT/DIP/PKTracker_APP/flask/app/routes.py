# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app import app, api, views, resources


# Add URL rules for views
app.register_error_handler(404, views.not_found)

app.add_url_rule("/", view_func=views.ParkingLots.as_view("list-view"))
app.add_url_rule(
    "/parking-lot/<int:parking_lot_id>/detail",
    view_func=views.ParkingLot.as_view("detail-view"),
)
app.add_url_rule(
    "/parking-lot/<int:parking_lot_id>/statistics",
    view_func=views.ParkingStatistics.as_view("statistics-view"),
)

# Add URL rules for REST resources
api.add_resource(
    resources.Detections, "/detections/<int:record_id>", endpoint="detections"
)
api.add_resource(
    resources.ParkingLots,
    "/parking-lots",
    "/parking-lots/<int:parking_lot_id>",
    endpoint="parking-lots",
)
api.add_resource(
    resources.RegionsOfInterest,
    "/parking-lots/<int:parking_lot_id>/regions-of-interest",
    endpoint="regions-of-interest",
)
api.add_resource(
    resources.ParkingSpots,
    "/parking-lots/<int:parking_lot_id>/parking-spots",
    "/parking-lots/<int:parking_lot_id>/parking-spots/<int:parking_spot_id>",
    endpoint="parking-spots",
)
api.add_resource(
    resources.ParkingLotFrames,
    "/parking-lots/<int:parking_lot_id>/frames",
    endpoint="parking-lot-frames",
)
api.add_resource(
    resources.ParkingLotLatestFrame,
    "/parking-lots/<int:parking_lot_id>/frames/latest",
    endpoint="parking-lot-latest-frame",
)
api.add_resource(
    resources.FullStatistics,
    "/parking-lots/<int:parking_lot_id>/full-statistics",
    endpoint="full-statistics",
)
api.add_resource(
    resources.CumulativeStatistics,
    "/parking-lots/<int:parking_lot_id>/cumulative-statistics",
    endpoint="cumulative-statistics",
)
