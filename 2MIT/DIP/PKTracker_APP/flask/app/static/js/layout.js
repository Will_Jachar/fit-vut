// Master thesis at FIT BUT - Unique car counting
// Copyright (c) 2021 Peter Uhrín
// Licensed under MIT


// Display error message on top of the page
function displayAlert(type, message) {
  const alertArea = document.getElementById("alert-area");
  const alertTemplate = document.getElementById("alert-template");
  const alertNode = alertTemplate.content.cloneNode(true);

  alertNode.firstElementChild.classList.add(`alert-${type}`);
  alertNode.firstElementChild.firstChild.textContent = message;

  alertArea.appendChild(alertNode);
}


// Toggle buttons state
function toggleButton(button) {
  button.disabled = !button.disabled;
}


// Toggle buttons state and activate loader
function setLoadingButton(button, enabled, label) {
  if (enabled) {
    button.disabled = false;
    button.lastChild.textContent = label;
    button.firstElementChild.style.display = "none";
  } else {
    button.disabled = true;
    button.lastChild.textContent = "Processing...";
    button.firstElementChild.style.display = "";
  }
};


// Parse response errors and display error messages to appropriate form inputs
function handleFormErrors(form, data) {
  Object.keys(data.errors).forEach(field => {
    document.getElementById(`${field}-invalid-feedback`).textContent = data.errors[field][0];
    form[field].classList.add("is-invalid");
  });
}


// Display favicon if image is unavailable
function handleNoImageError(image) {
  const noImageTemplate = document.getElementById("no-image-template");
  const noImageNode = noImageTemplate.content.cloneNode(true);

  image.replaceWith(noImageNode);
}


// Fetch json from URL and call appropriate callbacks
function fetchJSON(url, successCallback, errorCallback) {
  fetch(url)
    .then(async response => {
      const data = await response.json();

      if (response.ok) {
        return successCallback(data);
      } else {
        if (errorCallback) {
          return errorCallback(data);
        }
      }
    })
    .catch(error => console.error(error));
}
