// Master thesis at FIT BUT - Unique car counting
// Copyright (c) 2021 Peter Uhrín
// Licensed under MIT


import { SelectableSpot, ParkingSpotsSketch } from "/static/js/parking_spots.js";


// p5 sketch for displaying and selecting parking spots for deletion

class ParkingSpotsDeleteSketch extends ParkingSpotsSketch {
  // p5 sketch for deleting parking spots based on ParkingSpotsSketch
  constructor(parent, imageURL) {
    super(parent, imageURL);

    this.selectedSpots = {};
  }

  // override mouse pressed handler to append selected spots to selected list
  handleMousePressed() {
    super.handleMousePressed();

    if (this.selectedSpot) {
      // Select or deselect spot
      if (this.selectedSpots[this.selectedSpot.id]) {
        this.selectedSpot.baseColor = this.selectedSpot.orgBaseColor || this.getHeatmapColor(0);
        delete this.selectedSpots[this.selectedSpot.id];
      } else {
        this.selectedSpot.baseColor = this.color("rgb(255, 0, 0)");
        this.selectedSpots[this.selectedSpot.id] = this.selectedSpot;
      }
    }

    this.redraw();
  }

  // Handler for reset button will revert base colors of the spots and deselect all spots
  reset() {
    Object.values(this.selectedSpots).forEach(spot => {
      spot.baseColor = spot.orgBaseColor || this.getHeatmapColor(0);
      delete this.selectedSpots[spot.id]
    });

    this.selectedSpot = null;

    this.redraw();
  }
}


// p5 sketch for displaying and selecting parking spots for deletion

class ParkingSpotsRoiSketch extends ParkingSpotsSketch {
  // p5 sketch for managing region of interest
  constructor(parent, imageURL) {
    super(parent, imageURL);

    this.roi = [];

    this.containedSpots = {};

    this.pointColor = this.color("rgb(78, 115, 223)");
    this.polygonColor = this.color("rgba(74, 229, 227, 0.25)")
  }

  // Override draw method to render ROI points
  draw() {
    super.draw();

    this.fill(this.polygonColor);

    this.beginShape();
    this.roi.forEach(point => {
      this.vertex(point.x, point.y);
    });
    this.endShape();

    this.roi.forEach(point => {
      point.display();
    });

    if (this.roi.length) {
      this.roi[this.roi.length - 1].displaySelected();
    }
  }

  // Override mouse pressed handler to disable selecting parking spots
  // instead enable selecting ROI points
  handleMousePressed() {
    if (
      this.roi.length
      && this.roi[this.roi.length - 1].isSelected(this.mouseX, this.mouseY)
    ) {
      this.roi.pop();
    } else {
      const newPoint = new SelectableSpot(
        this,
        null,
        this.mouseX / this.parent.offsetWidth,
        this.mouseY / this.parent.offsetHeight,
        this.parent.offsetWidth,
        this.parent.offsetHeight,
        this.pointColor,
      );

      this.roi.push(newPoint);
    }

    // Find all spots that are inside of ROI and change their color
    this.setContainedSpots();

    this.redraw();
  }

  // Override method for window resizing to incorporate ROI points
  windowResized() {
    super.windowResized();

    this.roi.forEach(spot => {
      spot.transform(
        this.parent.offsetWidth,
        this.parent.offsetHeight,
      );
    });

    this.redraw();
  }

  // Override heatmap to only return two colors
  getHeatmapColor(value) {
    if (value) {
      return this.color("rgb(114, 238, 68)");
    }

    return this.color("rgb(233, 62, 58)");
  }

  // Ray casting algorithm for checking if point is inside of polygon or not
  setContainedSpots() {
    if (this.roi.length < 3) {
      Object.values(this.containedSpots).forEach(spot => {
        spot.baseColor = this.getHeatmapColor(0);
        delete this.containedSpots[spot.id]
      });

      return;
    }

    Object.values(this.spots).forEach(spot => {
      let inside = false;

      for (let i = 0, j = this.roi.length - 1; i < this.roi.length; j = i++) {
        let xi = this.roi[i].x, yi = this.roi[i].y;
        let xj = this.roi[j].x, yj = this.roi[j].y;

        let intersect = (
          ((yi > spot.y) != (yj > spot.y))
          && (spot.x < (xj - xi) * (spot.y - yi) / (yj - yi) + xi)
        );

        if (intersect) {
          inside = !inside;
        }
      }

      if (inside) {
        spot.baseColor = this.getHeatmapColor(1);
        this.containedSpots[spot.id] = spot;
      } else {
        spot.baseColor = this.getHeatmapColor(0);
        delete this.containedSpots[spot.id]
      }
    });
  }

  // Handler for rest button resets ROI points
  reset() {
    this.roi = [[0, 0], [0, 1], [1, 1], [1, 0]].map(point => new SelectableSpot(
      this,
      null,
      point[0],
      point[1],
      this.parent.offsetWidth,
      this.parent.offsetHeight,
      this.pointColor,
    ));

    this.setContainedSpots();

    this.redraw();
  }
}


// Load parking spots from the REST API and create corresponding spots for every sketch
function loadParkingSpots(deleteSketch, roiSketch, callback) {
  deleteSketch.spots = {};
  deleteSketch.selectedSpots = {};
  deleteSketch.selectedSpot = null;

  roiSketch.spot = {};
  roiSketch.containedSpots = {};

  fetchJSON(context.urls.parkingSpotsURL, data => {
    data.forEach(spot => {
      deleteSketch.spots[spot.id] = new SelectableSpot(
        deleteSketch,
        spot.id,
        spot.center[0],
        spot.center[1],
        deleteSketch.parent.offsetWidth,
        deleteSketch.parent.offsetHeight,
      );
      roiSketch.spots[spot.id] = new SelectableSpot(
        roiSketch,
        spot.id,
        spot.center[0],
        spot.center[1],
        roiSketch.parent.offsetWidth,
        roiSketch.parent.offsetHeight,
      );
    });

    if (callback) {
      callback();
    }
  });
}


// Load statistics for parking spots from REST API
function loadStats(deleteSketch, callback) {
  fetchJSON(context.urls.statisticsURL, data => {
    data.spot_stats.forEach(spot => {
      const baseColor = deleteSketch.getHeatmapColor(
        spot.cumulative_stats.average_occupancy
      );

      deleteSketch.spots[spot.id].cumulativeStats = spot.cumulative_stats;
      deleteSketch.spots[spot.id].baseColor = baseColor;
      deleteSketch.spots[spot.id].orgBaseColor = baseColor;
    });

    if (callback) {
      callback();
    }
  });
}


// Load ROI from REST API
function loadRegionOfInterest(roiSketch, callback) {
  fetchJSON(context.urls.regionOfInterestURL, data => {
    roiSketch.roi = data.region_of_interest.map(point => new SelectableSpot(
      roiSketch,
      null,
      point[0],
      point[1],
      roiSketch.parent.offsetWidth,
      roiSketch.parent.offsetHeight,
      roiSketch.pointColor,
    ));

    if (callback) {
      callback();
    }
  });
}


// Code for HTML page


document.addEventListener("DOMContentLoaded", () => {
  // Run p5 sketches

  context.deleteSketch = new ParkingSpotsDeleteSketch(
    document.getElementById("parking-spots-frame"),
    context.urls.parkingLotImgURL,
  );
  context.roiSketch = new ParkingSpotsRoiSketch(
    document.getElementById("region-of-interest-frame"),
    context.urls.parkingLotImgURL,
  );

  // Load data

  loadParkingSpots(context.deleteSketch, context.roiSketch, () => {
    context.deleteSketch.redraw();

    context.roiSketch.setContainedSpots();
    context.roiSketch.redraw();
  });

  loadStats(context.deleteSketch, () => {
    context.deleteSketch.redraw();
  });

  loadRegionOfInterest(context.roiSketch, () => {
    context.roiSketch.setContainedSpots();
    context.roiSketch.redraw();
  });

  // Event listeners

  // Save parking lot information
  document.getElementById("parking-lot-detail-form-submit").addEventListener("click", event => {
    event.preventDefault();

    toggleButton(event.target);

    const form_data = new FormData(event.target.form);
    let form_data_json = {};

    form_data.forEach((value, field) => {
      form_data_json[field] = value;
    });

    fetch(context.urls.parkingLotURL, {
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(form_data_json)
    })
      .then(async response => {
        const data = await response.json();

        if (response.ok) {
        } else {
          displayAlert("danger", data.message);
          handleFormErrors(event.target.form, data);
        }

        toggleButton(event.target, true, "Save");
      })
      .catch(error => console.error(error));
  });

  // Reset delete sketch
  document.getElementById("parking-spots-reset").addEventListener("click", event => {
    event.preventDefault();

    context.deleteSketch.reset();
  });

  // Delete all selected parking spots
  document.getElementById("parking-spots-save").addEventListener("click", event => {
    event.preventDefault();

    toggleButton(event.target);

    Promise.all(Object.values(context.deleteSketch.selectedSpots).map(spot => fetch(
      `${context.urls.parkingSpotsURL}/${spot.id}`, { method: "DELETE" }
    )
      .then(response => {
        if (response.ok) {
          delete context.deleteSketch.spots[spot.id];
        }
      })
      .catch(error => console.error(error))
    ))
      .then(() => {
        context.deleteSketch.reset();

        toggleButton(event.target);
      });
  });

  // Reset ROI sketch
  document.getElementById("region-of-interest-reset").addEventListener("click", event => {
    event.preventDefault();

    context.roiSketch.reset();
  });

  // Save ROI points
  document.getElementById("region-of-interest-save").addEventListener("click", event => {
    event.preventDefault();

    toggleButton(event.target);

    fetch(context.urls.regionOfInterestURL, {
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        region_of_interest: context.roiSketch.roi.map(point => [point.orgX, point.orgY])
      })
    })
      .then(async response => {
        const data = await response.json();

        if (response.ok) {
        } else {
          displayAlert("danger", data.message);
        }

        toggleButton(event.target, true, "Save");
      })
      .catch(error => console.error(error));
  });
});