// Master thesis at FIT BUT - Unique car counting
// Copyright (c) 2021 Peter Uhrín
// Licensed under MIT



// Code for HTML page


// Create a card for a parking lot from a HTML template
function createParkingLotCard(parkingLotsArea, parkingLotTemplate, parkingLot) {
  const parkingLotNode = parkingLotTemplate.content.cloneNode(true);

  let node = parkingLotNode.getElementById("parking-lot-frame");
  node.src = node.src.replace("parking_lot_id", parkingLot.id);

  node = parkingLotNode.getElementById("parking-lot-detail");
  node.href = node.href.replace("parking_lot_id", parkingLot.id);

  node = parkingLotNode.getElementById("parking-lot-statistics");
  node.href = node.href.replace("parking_lot_id", parkingLot.id);

  parkingLotNode.getElementById("parking-lot-id").textContent = parkingLot.id;
  parkingLotNode.getElementById("parking-lot-name").textContent = parkingLot.name;
  parkingLotNode.getElementById("parking-lot-last-updated").textContent = (
    parkingLot.last_updated ? new Date(parkingLot.last_updated).toLocaleString() : "-"
  );

  parkingLotsArea.appendChild(parkingLotNode);
}

document.addEventListener("DOMContentLoaded", () => {
  // Fetch parking lots from the REST API and create cards for them
  fetchJSON("/parking-lots", data => {
    const parkingLotsArea = document.getElementById("parking-lots-area");
    const parkingLotTemplate = document.getElementById("parking-lot-template");

    data.forEach(parkingLot => createParkingLotCard(parkingLotsArea, parkingLotTemplate, parkingLot));
  })

  // Process data from form and send data to REST API to create new parking lot
  document.getElementById("new-parking-lot-form-submit").addEventListener("click", event => {
    event.preventDefault();

    toggleButton(event.target);

    const form_data = new FormData(event.target.form);
    let form_data_json = {};

    form_data.forEach((value, field) => {
      form_data_json[field] = value;
    });

    fetch("/parking-lots", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(form_data_json)
    })
      .then(async response => {
        const data = await response.json();

        if (response.ok) {
          event.target.form.reset();

          createParkingLotCard(
            document.getElementById("parking-lots-area"),
            document.getElementById("parking-lot-template"),
            data,
          );
        } else {
          displayAlert("danger", data.message);
          handleFormErrors(event.target.form, data);
        }

        toggleButton(event.target);
      })
      .catch(error => console.error(error));
  });
});
