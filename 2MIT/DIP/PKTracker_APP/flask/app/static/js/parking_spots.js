// Master thesis at FIT BUT - Unique car counting
// Copyright (c) 2021 Peter Uhrín
// Licensed under MIT


// p5 SelectableSpot class

class SelectableSpot {
  // Assign created spot to specified p5 sketch
  constructor(sketch, id, x, y, frameWidth, frameHeight, baseColor) {
    this.sketch = sketch;

    this.id = id;
    this.orgX = x;
    this.orgY = y;
    this.baseColor = baseColor || sketch.getHeatmapColor();

    this.transform(frameWidth, frameHeight);
  }

  // Display spot as an circle with assigned color
  display() {
    this.sketch.stroke("rgb(58, 59, 69)");
    this.sketch.strokeWeight(1);

    this.sketch.fill(this.baseColor);
    this.sketch.circle(this.x, this.y, this.diameter);
  }

  // Display spot as an circle with specified color and green border
  displaySelected() {
    this.sketch.stroke("rgb(0, 255, 0)");
    this.sketch.strokeWeight(5);

    this.sketch.fill(this.baseColor);
    this.sketch.circle(this.x, this.y, this.diameter);
  }

  // Display spot as an circle with custom color
  displayCustom(color) {
    this.sketch.stroke("rgb(58, 59, 69)");
    this.sketch.strokeWeight(1);

    this.sketch.fill(color);
    this.sketch.circle(this.x, this.y, this.diameter);
  }

  // Test if spot is selected (method called upon mouse click)
  isSelected(mouseX, mouseY) {
    if (
      mouseX >= this.left && mouseX <= this.right
      && mouseY >= this.top && mouseY <= this.bottom
    ) {
      return this;
    }

    return null;
  }

  // Calculate pixel coordinates of spot from normalized coordinates
  transform(frameWidth, frameHeight) {
    this.x = this.orgX * frameWidth;
    this.y = this.orgY * frameHeight;
    this.diameter = frameHeight / 25;

    let radius = this.diameter / 2;
    this.left = this.x - radius;
    this.right = this.x + radius;
    this.top = this.y - radius;
    this.bottom = this.y + radius;
  }
}


// p5 sketch for displaying and selecting parking spots


class ParkingSpotsSketch extends p5 {
  // Create p5 sketch
  constructor(parent, imageURL) {
    super(() => { }, parent);

    this.parent = parent;
    this.imageURL = imageURL;
    this.parkingLotImage = null;

    this.spots = {};
    this.selectedSpot = null;
  }

  // Setup p5 sketch
  setup() {
    // Create HTML canvas
    const canvas = this.createCanvas(
      this.parent.offsetWidth,
      this.parent.offsetHeight,
    );

    // Assign mouse pressed handler
    canvas.mousePressed(() => this.handleMousePressed());

    // Load parking pot image
    this.loadImage(this.imageURL, img => {
      this.parkingLotImage = img;
      this.redraw();
    }, () => {
      this.parkingLotImage = undefined;
      this.redraw();
    });

    // Don't rerender in loop
    this.noLoop();
  }

  // Draw function for rerendering canvas
  draw() {
    this.background(this.parkingLotImage || 255);

    Object.values(this.spots).forEach(spot => {
      spot.display();
    });

    if (this.selectedSpot) {
      this.selectedSpot.displaySelected(this);
    }
  };

  // Recalculate positions of parking spots and rerender canvas
  windowResized() {
    this.resizeCanvas(
      this.parent.offsetWidth,
      this.parent.offsetHeight,
    );

    Object.values(this.spots).forEach(spot => {
      spot.transform(
        this.parent.offsetWidth,
        this.parent.offsetHeight,
      );
    });

    this.redraw();
  }

  // Mouse pressed handler checks id some parking spot was selected
  handleMousePressed() {
    this.selectedSpot = Object.values(this.spots).reduce(
      (selected, spot) => spot.isSelected(this.mouseX, this.mouseY) || selected,
      null,
    );

    this.redraw();
  }

  // Return color mapped to heatmap
  getHeatmapColor(value) {
    if (value === undefined) {
      return this.color("rgb(255, 243, 59)");
    }

    return this.lerpColor(
      this.color("rgb(255, 243, 59)"),
      this.color("rgb(233, 62, 58)"),
      value,
    );
  }
}


export { SelectableSpot, ParkingSpotsSketch };

