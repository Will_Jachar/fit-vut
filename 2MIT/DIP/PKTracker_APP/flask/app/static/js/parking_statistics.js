// Master thesis at FIT BUT - Unique car counting
// Copyright (c) 2021 Peter Uhrín
// Licensed under MIT


import { createChart } from "/static/js/chart_creator.js";
import { SelectableSpot, ParkingSpotsSketch } from "/static/js/parking_spots.js";


// Create shared global context
context.timeSteps = [];
context.elements = {
  parkingLot: {
    averageOccupancyChartCanvas: document.getElementById("average-occupancy-chart"),
    averageOccupancyChart: undefined,
    totalCarsCard: document.getElementById("total-cars-card"),
    averageStayTimeCard: document.getElementById("average-stay-time-card")
  },
  parkingSpot: {
    averageOccupancyChartCanvas: document.getElementById("spot-average-occupancy-chart"),
    averageOccupancyChart: undefined,
    totalCarsCard: document.getElementById("spot-total-cars-card"),
    averageStayTimeCard: document.getElementById("spot-average-stay-time-card")
  }
};


// p5 sketch for displaying and selecting parking spots

class ParkingSpotsStatsSketch extends ParkingSpotsSketch {
  // Override mouse pressed handler to update parking spot charts upon selecting 
  handleMousePressed() {
    super.handleMousePressed();

    if (this.selectedSpot) {
      updateCharts(
        context.elements.parkingSpot,
        this.selectedSpot.cumulativeStats,
        this.selectedSpot.stepStats,
      );
    } else {
      updateCharts(
        context.elements.parkingSpot,
        { total_cars: 0, average_occupancy: 0, average_stay_time: 0 },
        [],
      );
    }
  }
}


// Chart.js graphs


// Update charts based on provided data
function updateCharts(elements, cumulativeStats, stepStats) {
  elements.averageOccupancyChart.data.datasets[0].data = [];

  context.timeSteps.forEach(timestamp => {
    elements.averageOccupancyChart.data.datasets[0].data.push(
      (
        stepStats[timestamp]
        || { total_cars: 0, average_occupancy: 0, average_stay_time: 0 }
      ).average_occupancy * 100
    );
  });

  elements.totalCarsCard.textContent = cumulativeStats.total_cars;
  elements.averageStayTimeCard.textContent = cumulativeStats.average_stay_time.toFixed(2);
  elements.averageOccupancyChart.update();
}


// Create average occupancy chart for parking lot
context.elements.parkingLot.averageOccupancyChart = createChart(
  context.elements.parkingLot.averageOccupancyChartCanvas,
  "Occupancy",
  (tooltipItem, chart) => (
    `${chart.datasets[tooltipItem.datasetIndex].label || ""}: ${tooltipItem.yLabel.toFixed(2)} %`
  ),
  { suggestedMin: 0, suggestedMax: 100 }
);


// Create average occupancy chart for parking spot
context.elements.parkingSpot.averageOccupancyChart = createChart(
  context.elements.parkingSpot.averageOccupancyChartCanvas,
  "Occupancy",
  (tooltipItem, chart) => (
    `${chart.datasets[tooltipItem.datasetIndex].label || ""}: ${tooltipItem.yLabel.toFixed(2)} %`
  ),
  { suggestedMin: 0, suggestedMax: 100 }
);


// Load parking spots from REST API and create corresponding spots for p5 sketch
function loadParkingSpots(sketch, callback) {
  fetchJSON(context.urls.parkingSpotsURL + "?" + new URLSearchParams({ region_of_interest_only: true }), data => {
    sketch.spots = {};

    data.forEach(spot => {
      sketch.spots[spot.id] = new SelectableSpot(
        sketch,
        spot.id,
        spot.center[0],
        spot.center[1],
        sketch.parent.offsetWidth,
        sketch.parent.offsetHeight,
      )
    });

    if (callback) {
      callback();
    }
  });
}


// Load and parse statistics from the REST API
function loadStats(sketch, filters, callback) {
  filters.region_of_interest_only = true;

  fetchJSON(context.urls.statisticsURL + "?" + new URLSearchParams(filters), data => {
    context.timeSteps = data.time_steps;

    sketch.selectedSpot = null;

    data.spot_stats.forEach(spot => {
      sketch.spots[spot.id].cumulativeStats = spot.cumulative_stats;
      sketch.spots[spot.id].stepStats = spot.step_stats;
      sketch.spots[spot.id].baseColor = sketch.getHeatmapColor(
        spot.cumulative_stats.average_occupancy
      );
    });

    const labels = data.time_steps.map(timestamp => new Date(timestamp).toLocaleString());

    context.elements.parkingLot.averageOccupancyChart.data.labels = labels;
    context.elements.parkingSpot.averageOccupancyChart.data.labels = labels;

    // Update charts for parking lot
    updateCharts(
      context.elements.parkingLot,
      data.basic_stats.cumulative_stats,
      data.basic_stats.step_stats,
    );
    updateCharts(
      context.elements.parkingSpot,
      { total_cars: 0, average_occupancy: 0, average_stay_time: 0 },
      [],
    );

    if (callback) {
      callback();
    }
  });
}


// Code for HTML page


document.addEventListener("DOMContentLoaded", () => {
  // Init date range picker for filters
  const dateRangePicker = new DateRangePicker(
    document.getElementById("time-range-inputs"),
    {
      buttonClass: "btn",
      clearBtn: true,
      allowOneSidedRange: true,
      format: "yyyy-mm-dd"
    }
  );

  // Create sketch for selecting parking spots
  context.sketch = new ParkingSpotsStatsSketch(
    document.getElementById("parking-lot-frame"),
    context.urls.parkingLotImgURL,
  );

  // Load parking spots from REST API
  loadParkingSpots(context.sketch, () => {
    context.sketch.redraw();
    loadStats(context.sketch, { scale: "hour" }, () => context.sketch.redraw());
  });

  // Apply filters and reload filtered data from REST API
  document.getElementById("filters-form-submit").addEventListener("click", event => {
    event.preventDefault();

    setLoadingButton(event.target, false);

    const filters = new FormData(event.target.form);
    let filters_json = {};

    filters.forEach((value, field) => {
      filters_json[field] = value;
    });

    const dateRange = dateRangePicker.getDates();

    if (dateRange[0]) {
      filters_json.start_timestamp = dateRange[0].toISOString();
    }
    if (dateRange[1]) {
      filters_json.end_timestamp = dateRange[1].toISOString();
    }

    loadParkingSpots(context.sketch, () => {
      context.sketch.redraw();
      loadStats(context.sketch, filters_json, () => {
        context.sketch.redraw();
        setLoadingButton(event.target, true, "Apply");
      });
    });
  });

});
