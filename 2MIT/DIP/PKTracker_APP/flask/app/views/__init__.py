# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app.views.errors import not_found
from app.views.parking_lots import ParkingLots
from app.views.parking_lot import ParkingLot
from app.views.parking_statistics import ParkingStatistics
