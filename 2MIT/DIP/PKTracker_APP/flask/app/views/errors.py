# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import render_template


def not_found(_):
    """
    Not Found error handler
    """

    return render_template("404.html", active=None), 404
