# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import abort, render_template
from flask.views import View

from app import models


class ParkingLot(View):
    """
    Parking lot detail view for web application
    """

    def dispatch_request(self, parking_lot_id):
        parking_lot = models.ParkingLot.query.get(parking_lot_id)

        if not parking_lot:
            abort(404)

        return render_template("parking_lot.html", parking_lot=parking_lot)
