# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from flask import render_template
from flask.views import View


class ParkingLots(View):
    """
    Parking lots list view for web application
    """

    def dispatch_request(self):
        return render_template("parking_lots.html")
