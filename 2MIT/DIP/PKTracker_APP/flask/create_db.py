# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


from app import db


if __name__ == "__main__":
    db.drop_all()
    db.create_all()
