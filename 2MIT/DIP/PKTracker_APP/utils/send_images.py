# Master thesis at FIT BUT - Unique car counting
# Copyright (c) 2021 Peter Uhrín
# Licensed under MIT


import base64
import datetime

import click
import requests

from pathlib import Path


@click.command()
@click.option("--src", required=True, help="Source image folder.")
@click.option("--parking_lot_id", required=True, type=int, help="ID of parking lot")
def main(src, parking_lot_id):
    """
    Load images of parking lot and send them to PKTracker app
    """

    filenames = []

    for extention in ["*.jpg", "*.jpeg", "*.png"]:
        filenames.extend(Path(src).rglob(extention))

    for image_path in sorted(filenames, key=lambda x: x.stem):
        # Parse timestamp from filename
        timestamp = datetime.datetime.strptime(
            image_path.stem, "%Y-%m-%d_%H_%M_%S"
        ).strftime(
            "%Y-%m-%dT%H:%M:%S+0000"
        )  # Use UTC time for demonstration

        image = None
        with open(image_path, "rb") as image_file:
            image = base64.b64encode(image_file.read()).decode("utf-8")

        # Use localhost for demonstration
        response = requests.post(
            f"http://127.0.0.1/parking-lots/{parking_lot_id}/frames",
            json=[{"created": timestamp, "frame": image}],
        )

        print(timestamp, " -> ", response.json())

        if not response.ok:
            return


if __name__ == "__main__":
    main()
