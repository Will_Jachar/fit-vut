/**
 * main.cpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include <iostream>
#include <string>
#include <boost/program_options.hpp>

#include "mnist_loader.hpp"
#include "network.hpp"
#include "types.hpp"
#include "utils.hpp"

#define MNIST_CLASSES 10
#define MNIST_FEATURES 784

namespace options = boost::program_options;

int main(int argc, char* argv[])
{
    // Define acceptable arguments
    options::options_description arguments("Required arguments");
    arguments.add_options()
        ("train-features", options::value<std::string>()->required(), "Path to file with training features")
        ("train-labels", options::value<std::string>()->required(), "Path to file with training labels")
        ("test-features", options::value<std::string>()->required(), "Path to file with test features")
        ("test-labels", options::value<std::string>()->required(), "Path to file with test labels")
        ("epochs", options::value<unsigned>()->default_value(10), "Number of training epochs")
        ("batch-size", options::value<unsigned>()->default_value(256), "Batch size")
        ("lr", options::value<double>()->default_value(0.01), "Training learning rate");

    options::variables_map paths_map;

    // Parse arguments
    try {
        options::store(options::parse_command_line(argc, argv, arguments), paths_map);
        options::notify(paths_map);
    }
    catch (std::exception& error) {
        std::cerr << "Error: " << error.what() << std::endl;
        std::cerr << arguments << std::endl;

        return 1;
    }

    dataset train_data;
    dataset test_data;

    // Load training and testing datasets
    try {
        load_dataset(
            paths_map["train-features"].as<std::string>(),
            paths_map["train-labels"].as<std::string>(),
            MNIST_CLASSES,
            train_data);
        load_dataset(
            paths_map["test-features"].as<std::string>(),
            paths_map["test-labels"].as<std::string>(),
            MNIST_CLASSES,
            test_data);
    }
    catch (std::string& error)
    {
        std::cerr << "ERROR: " << error << std::endl;
        return 1;
    }

    try {
        // Define network
        Network test_net = Network({ MNIST_FEATURES, 16, MNIST_CLASSES });

        // Train network
        test_net.fit(
            train_data,
            test_data,
            paths_map["epochs"].as<unsigned>(),
            paths_map["batch-size"].as<unsigned>(),
            paths_map["lr"].as<double>());
    }
    catch (std::string& error)
    {
        std::cerr << "ERROR: " << error << std::endl;
        return 1;
    }

    return 0;
}