/**
 * mnist_loader.cpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "mnist_loader.hpp"

void load_features(std::string filepath, std::vector<features>& data)
{
    std::ifstream file;
    file.open(filepath, std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        throw "Unable to open " + filepath;
    }

    // Skip magic number
    file.seekg(sizeof(int32_t));

    // Load number of images and image dimensions
    int32_t images_count, rows, columns;

    file.read(reinterpret_cast<char*>(&images_count), sizeof(images_count));
    file.read(reinterpret_cast<char*>(&rows), sizeof(rows));
    file.read(reinterpret_cast<char*>(&columns), sizeof(columns));

    images_count = endian::big_to_native(images_count);
    rows = endian::big_to_native(rows);
    columns = endian::big_to_native(columns);

    data.clear();
    data.reserve(images_count);

    // Load one image at a time
    std::vector<uint8_t> buffer(rows * columns);

    for (int i = 0; i < images_count; i++)
    {
        file.read(reinterpret_cast<char*>(&buffer[0]), buffer.size());

        data.push_back(features(rows * columns, 1));
        std::copy(buffer.begin(), buffer.end(), data.back().data().begin());
        data.back() /= 255;
    }
}

void load_labels(
    std::string filepath,
    unsigned classes_count,
    std::vector<label>& data)
{
    std::ifstream file;
    file.open(filepath, std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        throw "Unable to open " + filepath;
    }

    // Skip magic number
    file.seekg(sizeof(int32_t));

    // Load number of labels
    int32_t labels_count;

    file.read(reinterpret_cast<char*>(&labels_count), sizeof(labels_count));

    labels_count = endian::big_to_native(labels_count);

    data.clear();
    data.reserve(labels_count);

    // Load one label at a time
    uint8_t buffer;

    for (int i = 0; i < labels_count; i++)
    {
        file.read(reinterpret_cast<char*>(&buffer), sizeof(uint8_t));

        if (buffer >= classes_count) {
            throw "Label " + std::to_string(buffer) + " out of range of classes count";
        }

        // Convert label to categorical format
        data.push_back(label(classes_count, 1, 0));
        ublas::matrix_column<label> label_column(data.back(), 0);

        label_column[buffer] = 1;
    }
}

void load_dataset(
    std::string features_filepath,
    std::string labels_filepath,
    unsigned classes_count,
    dataset& data)
{
    std::vector<features> features_data;
    std::vector<label> labels_data;

    load_features(features_filepath, features_data);
    load_labels(labels_filepath, classes_count, labels_data);

    if (features_data.size() != labels_data.size())
    {
        throw "Different number of features and labels";
    }

    data.clear();
    data.reserve(features_data.size());

    for (unsigned i = 0; i < features_data.size(); i++)
    {
        data.push_back({ features_data[i], labels_data[i] });
    }
}