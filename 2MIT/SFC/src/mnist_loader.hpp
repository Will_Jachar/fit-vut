/**
 * mnist_loader.hpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef MNIST_LOADER
#define MNIST_LOADER

#include <fstream>
#include <string>
#include <vector>
#include <boost/endian/conversion.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include "types.hpp"

namespace ublas = boost::numeric::ublas;
namespace endian = boost::endian;

/**
 * @brief Loads features from MNIST data file
 *
 * @param filepath Filepath to MNIST features file
 * @param data Container there the loaded features will be stored
 */
void load_features(std::string filepath, std::vector<features>& data);

/**
 * @brief Loads labels from MNIST data file and converts them to categorical format
 *
 * @param filepath Filepath to MNIST labels file
 * @param classes_count Number of classes
 * @param data Container there the loaded labels will be stored
 */
void load_labels(
    std::string filepath,
    unsigned classes_count,
    std::vector<label>& data);

/**
 * @brief Loads features and labels from MNIST data files
 *
 * @param features_filepath Filepath to MNIST features file
 * @param labels_filepath Filepath to MNIST labels file
 * @param classes_count Number of classes
 * @param data Container there the loaded dataset will be stored
 */
void load_dataset(
    std::string features_filepath,
    std::string labels_filepath,
    unsigned classes_count,
    dataset& data);

#endif