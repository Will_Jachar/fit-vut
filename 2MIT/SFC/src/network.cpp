/**
 * network.cpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "network.hpp"

 // Public ------------------------------------------------------------------------------

Network::Network(std::vector<unsigned> layer_sizes)
{
    if (layer_sizes.size() < 2)
    {
        throw "Newtwork needs to have at least two layers";
    }

    this->layer_sizes = layer_sizes;

    for (unsigned layer = 1; layer < layer_sizes.size(); layer++)
    {
        ublas::matrix<double> weights(layer_sizes[layer], layer_sizes[layer - 1]);
        ublas::matrix<double> biases(layer_sizes[layer], 1);

        // Init biases and weights with random values from normal distribution with mean = 0 and variance = 1
        std::generate(weights.data().begin(), weights.data().end(), normal_random_generator);
        std::generate(biases.data().begin(), biases.data().end(), normal_random_generator);

        this->weights.push_back(weights);
        this->biases.push_back(biases);
    }
}

result Network::predict(features& sample)
{
    if (sample.size1() != this->layer_sizes.front()) // Matrix has dimensions (sample_size, 1)
    {
        throw "Sample dimensions doesn't match with networks input";
    }

    this->activation_history.clear();
    this->activation_history.push_back(sample);

    // Calculate activation of each layer at a time
    for (unsigned layer = 0; layer < this->layer_sizes.size() - 1; layer++)
    {
        this->activation_history.push_back(
            this->compute_layer(layer, this->activation_history.back()));
    }

    return this->activation_history.back();
}

void Network::fit(
    dataset& training_data,
    dataset& validation_data,
    unsigned epochs,
    unsigned batch_size,
    double learning_rate)
{
    if (training_data.empty() || validation_data.empty())
    {
        throw "Training or validation dataset is empty";
    }
    else if (
        training_data[0].first.size1() != this->layer_sizes.front()
        || training_data[0].second.size1() != this->layer_sizes.back()
        || validation_data[0].first.size1() != this->layer_sizes.front()
        || validation_data[0].second.size1() != this->layer_sizes.back())
    {
        throw "Data dimensions doesn't match with networks input or output";
    }

    std::pair<double, double> stats = this->evaluate(validation_data);

    std::cout << "Initial accuracy: " << stats.first << "%" << std::endl;
    std::cout << "Training:" << std::endl;


    unsigned batches_count = training_data.size() / batch_size;

    for (unsigned i = 0; i < epochs; i++)
    {
        // Shuffle training data at random for each epoch
        std::random_shuffle(training_data.begin(), training_data.end());

        // Apply backpropagation algorithm for each mini-batch
        for (unsigned batch = 0; batch < batches_count; batch++)
        {
            this->learn_from_mini_batch(
                training_data, batch * batch_size, batch_size, learning_rate);
        }

        // Evaluate accuracy on validation data after each epoch
        stats = this->evaluate(validation_data);

        std::cout << "  Epoch " << i + 1 << " => Accuracy: " << stats.first << "% ";
        std::cout << "Loss: " << stats.second << std::endl;
    }

    std::cout << "End accuracy: " << stats.first << "%" << std::endl;
}

std::pair<double, double> Network::evaluate(
    dataset& data)
{
    if (data[0].first.size1() != this->layer_sizes.front() || data[0].second.size1() != this->layer_sizes.back())
    {
        throw "Data dimensions doesn't match with networks input or output";
    }

    unsigned correct_predictions = 0;
    result loss(this->layer_sizes.back(), 1, 0);

    for (auto& sample : data)
    {
        // Forward pass
        result output = this->predict(sample.first);

        // Check if the networks prediction was correct
        unsigned output_max_index = std::max_element(output.data().begin(), output.data().end()) - output.data().begin();
        unsigned label_max_index = std::max_element(sample.second.data().begin(), sample.second.data().end()) - sample.second.data().begin();

        if (output_max_index == label_max_index)
        {
            correct_predictions++;
        }

        // Calculate loss
        result output_loss = sample.second - output;
        loss += ublas::element_prod(output_loss, output_loss);
    }

    // Calculate total accuracy and average loss
    double loss_sum = std::accumulate(loss.data().begin(), loss.data().end(), 0);

    return { static_cast<double>(correct_predictions) / data.size() * 100, loss_sum / data.size() };
}

// Private -----------------------------------------------------------------------------

result Network::compute_layer(unsigned layer_index, features& inputs)
{
    // Dot product of weights and inputs
    result output = ublas::prod(this->weights[layer_index], inputs);

    // Adding bias
    ublas::matrix_column<result> output_column(output, 0);
    ublas::matrix_column<ublas::matrix<double>> bias_column(this->biases[layer_index], 0);
    output_column += bias_column;

    // Applying activation function
    std::for_each(output.data().begin(), output.data().end(), sigmoid);

    return output;
}

void Network::learn_from_mini_batch(
    dataset& data,
    unsigned start_index,
    unsigned batch_size,
    double learning_rate)
{
    std::vector<std::vector<ublas::matrix<double>>> weights_deltas(this->weights.size());
    std::vector<std::vector<ublas::matrix<double>>> biases_deltas(this->biases.size());

    // Run backpropagation for each sample form the training mini-batch
    for (unsigned i = start_index; i < start_index + batch_size; i++)
    {
        // Forward pass
        this->predict(data[i].first);

        // Calculating error and deltas for weights and biases of the output layer
        ublas::matrix<double> error = data[i].second - this->activation_history.back();
        this->calculate_layer_delta(this->activation_history.size() - 1, error, weights_deltas, biases_deltas);

        // Backpropagate error and calculate deltas for weights and biases of every layer
        for (unsigned layer = this->activation_history.size() - 2; layer > 0; layer--)
        {
            error = ublas::prod(ublas::trans(this->weights[layer]), biases_deltas[layer].back());
            this->calculate_layer_delta(layer, error, weights_deltas, biases_deltas);
        }
    }

    // Apply deltas from the mini-batch to the weights and biases of each layer
    this->apply_deltas(learning_rate, weights_deltas, biases_deltas);
}

void Network::calculate_layer_delta(
    unsigned layer_index,
    ublas::matrix<double>& error,
    std::vector<std::vector<ublas::matrix<double>>>& weights_deltas,
    std::vector<std::vector<ublas::matrix<double>>>& biases_deltas)
{
    // Calculate gradient
    ublas::matrix<double> gradient(this->activation_history[layer_index]);
    std::for_each(gradient.data().begin(), gradient.data().end(), sigmoid_derivative);
    gradient = ublas::element_prod(gradient, error);

    // Calculate weights deltas
    ublas::matrix<double> weights_delta = ublas::prod(
        gradient, ublas::trans(this->activation_history[layer_index - 1]));

    // Store the result deltas
    weights_deltas[layer_index - 1].push_back(weights_delta);
    biases_deltas[layer_index - 1].push_back(gradient);
}

void Network::apply_deltas(
    double learning_rate,
    std::vector<std::vector<ublas::matrix<double>>>& weights_deltas,
    std::vector<std::vector<ublas::matrix<double>>>& biases_deltas)
{
    // Apply deltas to the weights and biases of each layer
    for (unsigned layer = 0; layer < weights_deltas.size(); layer++)
    {
        // Sum deltas from each samplce of the minibatch
        ublas::matrix<double> weights_delta_sum = std::accumulate(
            weights_deltas[layer].begin(),
            weights_deltas[layer].end(),
            ublas::matrix<double>(this->weights[layer].size1(), this->weights[layer].size2(), 0));
        ublas::matrix<double> biases_delta_sum = std::accumulate(
            biases_deltas[layer].begin(),
            biases_deltas[layer].end(),
            ublas::matrix<double>(this->biases[layer].size1(), this->biases[layer].size2(), 0));

        // Apply deltas
        this->weights[layer] += (weights_delta_sum * learning_rate);
        this->biases[layer] += (biases_delta_sum * learning_rate);
    }
}
