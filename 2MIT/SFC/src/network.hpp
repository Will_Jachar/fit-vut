/**
 * network.hpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef NETWORK
#define NETWORK

#include <algorithm>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include "types.hpp"
#include "utils.hpp"

namespace ublas = boost::numeric::ublas;

class Network
{
private:
    std::vector<unsigned> layer_sizes;
    std::vector<ublas::matrix<double>> weights;
    std::vector<ublas::matrix<double>> biases;
    std::vector<result> activation_history;

    /**
     * @brief Computes activations for specified layer in the network
     *
     * @param layer_index Index of specified layer
     * @param inputs Input features for the layer
     * @return Calculated activations of specified layer
     */
    result compute_layer(unsigned layer_index, features& inputs);

    /**
     * @brief Bacpropagation for one batch of training data
     *
     * @param data Training data
     * @param start_index Start index of the batch
     * @param batch_size Batch size
     * @param learning_rate Learning rate
     */
    void learn_from_mini_batch(
        dataset& data,
        unsigned start_index,
        unsigned batch_size,
        double learning_rate);

    /**
     * @brief Backpropagation for one layer
     *
     * @param layer_index Index pro layer
     * @param error Calculated loss for the layer
     * @param weights_deltas Container with caltulated deltas of weights
     * @param biases_delatas Container with caltulated deltas of biases
     */
    void calculate_layer_delta(
        unsigned layer_index,
        ublas::matrix<double>& error,
        std::vector<std::vector<ublas::matrix<double>>>& weights_deltas,
        std::vector<std::vector<ublas::matrix<double>>>& biases_deltas);

    /**
     * @brief Summs all deltas for each layer and applies them to weights and biases
     *
     * @param learning_rate Learning rate
     * @param weights_deltas Container with caltulated deltas of weights
     * @param biases_delatas Container with caltulated deltas of biases
     */
    void apply_deltas(
        double learning_rate,
        std::vector<std::vector<ublas::matrix<double>>>& weights_deltas,
        std::vector<std::vector<ublas::matrix<double>>>& biases_deltas);

public:
    /**
     * @brief Constructor for the Network class
     *
     * @param layer_sizes Number of neurons of each layer in the network (including input and output layers)
     */
    Network(std::vector<unsigned> layer_sizes);

    /**
     * @brief Computes feedforward pass through thw whole network
     *
     * @param inputs Input values for the network
     * @return Calculated activations of the network
     */
    result predict(features& input);

    /**
     * @brief Trains the network on training data
     *
     * @param training_data Features-labels pairs for training
     * @param validation_data Features-labels pairs for testing
     * @param epochs Number of training epochs
     * @param batch_size Size of training mini-batch
     * @param learning_rate Learning rate
     */
    void fit(
        dataset& training_data,
        dataset& validation_data,
        unsigned epochs,
        unsigned batch_size,
        double learning_rate);

    /**
     * @brief Evaluates network on test dataset
     *
     * @param data Test dataset
     * @return Pair of result accuracy and loss
     */
    std::pair<double, double> evaluate(dataset& data);
};

#endif
