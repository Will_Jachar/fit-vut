/**
 * types.hpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef TYPES
#define TYPES

#include <vector>
#include <boost/numeric/ublas/matrix.hpp>

namespace ublas = boost::numeric::ublas;

typedef ublas::matrix<double> features;
typedef ublas::matrix<double> label;
typedef ublas::matrix<double> result;

typedef std::vector<std::pair<features, label>> dataset;

#endif
