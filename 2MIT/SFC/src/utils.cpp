/**
 * utils.cpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "utils.hpp"

void sigmoid(double& value)
{
    value = 1.0 / (1.0 + exp(-value));
}

void sigmoid_derivative(double& value)
{
    value = value * (1.0 - value);
}

double normal_random_generator()
{
    static std::default_random_engine generator;
    static std::normal_distribution<double> distribution(0, 1);

    return distribution(generator);
}
