/**
 * utils.hpp
 *
 * SFC Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef UTILS
#define UTILS

#include <cmath>
#include <random>

#include "types.hpp"

namespace ublas = boost::numeric::ublas;

/**
 * @brief Sigmoid function
 *
 * @param value Input value
 */
void sigmoid(double& value);

/**
 * @brief Derivation of sigmoid function
 *
 * @param value Input value
 */
void sigmoid_derivative(double& value);

/**
 * @brief Random generator generating numbers from normal distribution with mean = 0 and variance = 1
 *
 * @return Random number
 */
double normal_random_generator();

#endif