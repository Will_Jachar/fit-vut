/**
 * @file library.mjs
 * @author Peter Uhrín (xuhrin02)
 *
 * General implementation of Memento design pattern
 */


/**
 * Implementation of Originator class from Memento design pattern (abstract class)
 */
export class Originator {
    /**
     * Constructor of an abstract class throws an error if instantiated directly
     */
    constructor() {
        if (new.target === Originator) {
            throw Error("Originator is an abstract class and can't be instantiated directly!");
        }
    }

    /**
     * Saves current state of the Originator object (abstract method)
     */
    saveState() {
        throw new Error("Save state not implemented!");
    }

    /**
     * Restores state of the originator object based on state provided in Memento object (abstract method)
     *
     * @param memento State of the Originator encapsulated in Memento object
     */
    restoreState(memento) {
        throw new Error("Restore state not implemented!");
    }

    /**
     * Saves last change of the Originator state (abstract method)
     */
    saveStep() {
        throw new Error("Save step not implemented!");
    }

    /**
     * Undoes last change of the Originators state based on change provided in Memento object (abstract method)
     *
     * @param memento Change of the Originators state encapsulated in Memento object
     */
    undoStep(memento) {
        throw new Error("Undo step not implemented!");
    }
}

/**
 * Implementation of Memento class from Memento design pattern
 */
export class Memento {
    /**
     * Constructor saves provided state object
     *
     * @param state Representation of inner state of Originator
     */
    constructor(state) {
        this.state = state;
    }

    /**
     * Getter for state attribute
     *
     * @returns Value of state attribute
     */
    getState() {
        return this.state;
    }
}


/**
 * Implementation of Caretaker class from Memento design pattern
 */
export class Caretaker {
    /**
     * Constructor creates lists for Memento objects
     */
    constructor() {
        this.stateHistory = [];
        this.stepHistory = [];
    }

    /**
     * Saves provided Memento state object
     *
     * @param memento Memento object representing state of Originator
     */
    addState(memento) {
        this.stateHistory.push(memento);
    }

    /**
     * Get latest Memento state object
     *
     * @returns Latest Memento object representing state of Originator
     */
    getState() {
        // Throw error if there is no object to be returned
        if (this.stateHistory.length == 0) {
            throw new Error("No previous states saved!")
        }

        return this.stateHistory.pop();
    }

    /**
     * Saves provided Memento step object
     *
     * @param memento Memento object representing change of Originators state
     */
    addStep(memento) {
        this.stepHistory.push(memento);
    }

    /**
     * Get latest Memento step object
     *
     * @returns Latest Memento object representing change of Originators state
     */
    getStep() {
        // Throw error if there is no object to be returned
        if (this.stepHistory.length == 0) {
            throw new Error("No previous steps saved!")
        }

        return this.stepHistory.pop();
    }
}
