/**
 * @file model.mjs
 * @author Peter Uhrín (xuhrin02)
 *
 * Example of Memento design pattern
 */


import readline from "readline";
import { Originator, Memento, Caretaker } from "./library.mjs";


class TicTacToe extends Originator {
    /**
     * Constructor for TicTacToe game
     *
     * @param size Size of one side of the playing field
     */
    constructor(size) {
        super();

        // Max playing field size is 9 for demonstration purposes
        if (size > 9) {
            size = 9;
        }

        this.size = size;

        // Prepare header and divider for playing field of specified size
        this.header = "    " + new Array(this.size).fill().map(
            (_, index) => `${String.fromCharCode("A".charCodeAt(0) + index)}:`
        ).join("  ");
        this.divider = "   " + new Array(this.size).fill("---").join("+");

        // Initialize empty playing field of specified size
        this.board = new Array(this.size).fill().map(
            () => new Array(this.size).fill(null)
        );

        // Initialize reminding properties
        this.activePlayer = "X";
        this.lastStep = { "activePlayer": null, "move": null };
    }

    /**
     * Getter for activePlayer attribute
     * @returns Value of activePlayer attribute
     */
    getActivePlayer() {
        return this.activePlayer;
    }

    /**
     * Performs move on behalf of the active player
     *
     * @param moveStr String representation of players move
     */
    playMove(moveStr) {
        // Parse move string into 'file' and 'rank' specifying position on playing field
        const move = this.parseMove(moveStr);

        // Throw error if specified position is already taken
        if (this.board[move.rank][move.file]) {
            throw new Error(`Field '${moveStr}' already taken!`);
        }

        // Perform move and save it as a last move
        this.board[move.rank][move.file] = this.activePlayer;
        this.lastStep = { "activePlayer": this.activePlayer, "move": move };

        // Update active player
        this.activePlayer = this.activePlayer == "X" ? "O" : "X";
    }

    /**
     * Parses and validates string representation of players move
     *
     * @param moveStr String representation of players move
     * @returns Correctly parsed and validated move
     */
    parseMove(moveStr) {
        // Match if move string is in format <file><rank>
        const match = moveStr.match(/(?<file>[A-Z])(?<rank>[1-9])/i);

        // Throw error if move string isn't in correct format or if it isn't a valid move
        if (
            !match
            || (match.groups.file.toUpperCase().charCodeAt(0) - "A".charCodeAt(0)) > (this.size - 1)
            || match.groups.rank > this.size
        ) {
            throw new Error(`Invalid move '${moveStr}'!`);
        }

        // Return correctly parsed and validated move
        return {
            "file": match.groups.file.toUpperCase().charCodeAt(0) - "A".charCodeAt(0),
            "rank": match.groups.rank - 1
        };
    }

    /**
     * Creates string representation of playing field
     *
     * @returns String representation of playing field
     */
    renderBoard() {
        // Generate string representation of plying field
        const board = new Array(this.size).fill().map(
            (_, index) => `${index + 1}:  ${this.board[index].map(
                (value) => !value ? " " : value
            ).join(" | ")}`
        ).join(`\n${this.divider}\n`);

        // Adding pre-generated header to string representation of plying field
        return `${this.header}\n${board}\n\n`;
    }

    /**
     * Saves current state of the game (implementation of an abstract method from Originator class)
     *
     * @returns Current state of the game encapsulated in Memento object
     */
    saveState() {
        // Creating memento of current state (deep copy)
        return new Memento({
            "activePlayer": this.activePlayer,
            "board": JSON.parse(JSON.stringify(this.board))
        });
    }

    /**
     * Restores state of the game from provided memento (implementation of an abstract method from Originator class)
     *
     * @param memento State of the game encapsulated in Memento object
     */
    restoreState(memento) {
        const state = memento.getState();

        // Restoring current state (deep copy) from memento
        this.activePlayer = state.activePlayer;
        this.board = JSON.parse(JSON.stringify(state.board));
    }

    /**
     * Saves last step of the game (implementation of an abstract method from Originator class)
     *
     * @returns Last step of the game encapsulated in Memento object
     */
    saveStep() {
        // Creating memento of last move (deep copy)
        return new Memento({
            "activePlayer": this.lastStep.activePlayer,
            "move": this.lastStep.move
        });
    }

    /**
     * Undoes last step from provided memento (implementation of an abstract method from Originator class)
     *
     * @param memento Last step of the game encapsulated in Memento object
     */
    undoStep(memento) {
        const state = memento.getState();

        // Undoing last move from memento
        this.activePlayer = state.activePlayer;
        this.board[state.move.rank][state.move.file] = null;
    }
}


class App extends Caretaker {
    constructor() {
        // Creating lists for mementos
        super();

        // Initializing interface
        this.readline = readline.createInterface({
            "input": process.stdin,
            "output": process.stdout
        });
        this.notice = "";

        // Initializing game
        this.game = new TicTacToe(5);
    }

    /**
     * Main app function rendering playing field and prompting user for actions in a loop
     */
    run() {
        // Render playing field and prompt player for next action
        this.preparePrompt();
        this.readline.prompt();

        // Event listener for user input
        this.readline.on("line", (line) => {
            line = line.trim().split(/\s+/);

            // Main switch recognizing different user actions
            switch (line[0]) {
                // Action 'exit' ends the program
                case "exit":
                    this.readline.close();
                    process.exit(0);
                    break;
                // Action 'save' saves current state of the game into memento
                case "save":
                    this.addState(this.game.saveState());
                    this.notice = "Current state saved";
                    break;
                // Action 'load' restores state of the game to latest save
                case "load":
                    try {
                        this.game.restoreState(this.getState());
                        this.notice = "Previously saved state loaded";
                    } catch (error) {
                        this.notice = `ERROR: ${error.message}`;
                    }
                    break;
                // Action 'undo' undoes last move
                case "undo":
                    try {
                        this.game.undoStep(this.getStep());
                        this.notice = "Last move undone";
                    } catch (error) {
                        this.notice = `ERROR: ${error.message}`;
                    }
                    break;
                // Action 'play <file><rank>' expects file and rank as additional arguments
                // and performs the specified move
                case "play":
                    try {
                        const activePlayer = this.game.getActivePlayer();
                        const moveStr = line[line.length - 1];

                        this.game.playMove(moveStr);
                        this.addStep(this.game.saveStep());

                        this.notice = `Player [${activePlayer}] played '${moveStr}'`;
                    } catch (error) {
                        this.notice = `ERROR: ${error.message}`;
                    }
                    break;
                // No valid command was recognized
                default:
                    this.notice = `ERROR: Invalid command '${line}'!`;
                    break;
            }

            // Re-render playing field nad prompt player for another action
            this.preparePrompt();
            this.readline.prompt();
        });
    }

    /**
     * Prepares terminal and re-render the playing field
     */
    preparePrompt() {
        // Prepare terminal for re-rendering of the playing board
        process.stdout.cursorTo(0, 0);
        console.clear();

        // Writing potential errors or other messages
        console.log(this.notice + "\n");
        this.notice = "";

        // Setting playing board as a prompt player for next action
        this.readline.setPrompt(
            this.game.renderBoard() + `Player [${this.game.getActivePlayer()}]:\n> `
        );
    }
}


// Creating and running the app object
const app = new App();
app.run();
