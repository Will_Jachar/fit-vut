# Mammoth

School project for IIS

### Setup Virtual Enviroment
- Go to repository folder
- Create new virtual enviroment using:
	- ```python3 -m venv mammoth-env```
- To activate virtual enviroment:
	- ```source mammoth-env/bin/activate```
- To deactivate virtual enviroment:
	- ```deactivate```
	
### Install dependencies
- To install dependencies activate your virtual enviroment and then use command:
	- ```pip install -r requirements.txt```

