import sqlite3


def main():
    con = sqlite3.connect("instance/mammoth.db")
    cur = con.cursor()
    cur.executescript(
        """
        BEGIN TRANSACTION;
        DROP TABLE IF EXISTS `pit_belongs`;
        CREATE TABLE IF NOT EXISTS `pit_belongs` (
            `pit_id`	INTEGER NOT NULL,
            `expedition_id`	INTEGER NOT NULL,
            FOREIGN KEY(`pit_id`) REFERENCES `pit`(`id`),
            PRIMARY KEY(`pit_id`,`expedition_id`),
            FOREIGN KEY(`expedition_id`) REFERENCES `expedition`(`id`)
        );
        INSERT INTO `pit_belongs` (pit_id,expedition_id) VALUES (1,1),
        (2,1),
        (1,2),
        (2,2),
        (3,2),
        (1,3),
        (2,4),
        (3,4),
        (4,5),
        (4,6),
        (5,6),
        (4,7),
        (5,7),
        (4,8),
        (5,8),
        (4,9),
        (4,10),
        (5,10),
        (8,11),
        (7,11),
        (4,12),
        (5,12),
        (4,13),
        (5,13),
        (4,14),
        (5,14);
        DROP TABLE IF EXISTS `pit`;
        CREATE TABLE IF NOT EXISTS `pit` (
            `id`	INTEGER NOT NULL,
            `lattitude`	VARCHAR ( 32 ),
            `longitude`	VARCHAR ( 32 ),
            `active_expedition_id`	INTEGER,
            `removed`	BOOLEAN NOT NULL,
            PRIMARY KEY(`id`),
            FOREIGN KEY(`active_expedition_id`) REFERENCES `expedition`(`id`),
            CHECK(removed IN(0,1))
        );
        INSERT INTO `pit` (id,lattitude,longitude,active_expedition_id,removed) VALUES (1,'45.23212','17.0984',3,0),
        (2,'11.287','98.287',4,0),
        (3,'55.232','56.2124',4,0),
        (4,'57.123','32.323',14,0),
        (5,'141.51','51.112',14,0),
        (6,'32.555','11.222',NULL,0),
        (7,'12.55','150.23',NULL,0),
        (8,'13.11','55.189',NULL,0);
        DROP TABLE IF EXISTS `patrol`;
        CREATE TABLE IF NOT EXISTS `patrol` (
            `id`	INTEGER NOT NULL,
            `position`	INTEGER,
            FOREIGN KEY(`id`) REFERENCES `homo`(`id`),
            PRIMARY KEY(`id`)
        );
        INSERT INTO `patrol` (id,position) VALUES (3,NULL),
        (10,NULL),
        (12,NULL),
        (13,NULL),
        (14,NULL),
        (15,NULL),
        (16,NULL);
        DROP TABLE IF EXISTS `message`;
        CREATE TABLE IF NOT EXISTS `message` (
            `id`	INTEGER NOT NULL,
            `sent_timestamp`	DATETIME NOT NULL,
            `body`	VARCHAR,
            `resolved`	BOOLEAN NOT NULL,
            `patrol_id`	INTEGER NOT NULL,
            FOREIGN KEY(`patrol_id`) REFERENCES `patrol`(`id`),
            PRIMARY KEY(`id`),
            CHECK(resolved IN(0,1))
        );
        INSERT INTO `message` (id,sent_timestamp,body,resolved,patrol_id) VALUES (1,'2018-02-03 21:36:24.542951','This is a sample message.',1,3),
        (2,'2018-12-03 21:51:46.564167','This is a sample body message with 1 mammoth seen',1,3),
        (3,'2018-12-03 21:52:40.930615','Sample Body Message.',0,3);
        DROP TABLE IF EXISTS `mammoths_enum`;
        CREATE TABLE IF NOT EXISTS `mammoths_enum` (
            `message_id`	INTEGER NOT NULL,
            `mammoth_id`	INTEGER NOT NULL,
            FOREIGN KEY(`mammoth_id`) REFERENCES `mammoth`(`id`),
            PRIMARY KEY(`message_id`,`mammoth_id`),
            FOREIGN KEY(`message_id`) REFERENCES `message`(`id`)
        );
        INSERT INTO `mammoths_enum` (message_id,mammoth_id) VALUES (1,10),
        (1,9),
        (1,8),
        (2,13),
        (3,16),
        (3,15),
        (3,14);
        DROP TABLE IF EXISTS `mammoth`;
        CREATE TABLE IF NOT EXISTS `mammoth` (
            `id`	INTEGER NOT NULL,
            `name`	VARCHAR ( 128 ) NOT NULL,
            `special_chars`	VARCHAR ( 128 ) NOT NULL,
            `habits`	VARCHAR ( 128 ) NOT NULL,
            `removed`	BOOLEAN NOT NULL,
            PRIMARY KEY(`id`),
            CHECK(removed IN(0,1))
        );
        INSERT INTO `mammoth` (id,name,special_chars,habits,removed) VALUES (1,'Johnson','White Tusk, Brown fur, Big feet, ','Eats, sleeps, mates',0),
        (2,'Rick','Long trunk, Big ears, Short tusks','Eats, Runs, Sleeps, Jumps',0),
        (3,'Morty','Spotty, Furry, White tusk','SLEEPS, MATES',0),
        (4,'Derek','Cunning, Big eyes, Purple fur','Runs, jumps, eats',0),
        (5,'Kevin','TUSK, BIG, FURRY, WHITE','RUNS, MATES, SHITS',0),
        (6,'David','SLOW, FATTY, FURRY','RUNS, MATES, SLEEPS',0),
        (7,'Thomas','BROWN TUSK, FURRY, FAT','SLEEPS, MATES, RUNS',0),
        (8,'Malek','FURRY, WHITE, SPOTTY','MATES, RUNS, SHITS',0),
        (9,'Ronin','SPOTTY, WHITE, FURRY','EATS, SHITS, MATES',0),
        (10,'Karen','STRONG, STRONG, SLOW, AGILE','RUNS, SWIMS, EATS',0),
        (11,'Horns','FURRY, WHITE TUSK, SPOTTY, BIG','RUNS, MATES, EATS',0),
        (12,'Karl','READ, HIGH JUMP, FAST, AGILE','SLEEPS, EATS, SHITS, RUNS, MATES',0),
        (13,'Seen1','HIGH JUMP, STRONG, BIG, HORNS','RUNS, MATES, EATS',0),
        (14,'Seen2','None','None',0),
        (15,'Seen3','None','None',0),
        (16,'Seen4','None','None',0),
        (17,'Mammoth1','SLOW, FATTY, FURRY','RUNS, MATES, SLEEPS',0),
        (18,'Valentin','FURRY, WHITE, SPOTTY','EATS, SHITS, MATES',0);
        DROP TABLE IF EXISTS `killed_mammoth`;
        CREATE TABLE IF NOT EXISTS `killed_mammoth` (
            `mammoth_id`	INTEGER NOT NULL UNIQUE,
            `hunter_id`	INTEGER NOT NULL,
            `expedition_id`	INTEGER NOT NULL,
            `pit_id`	INTEGER NOT NULL,
            FOREIGN KEY(`hunter_id`) REFERENCES `hunter`(`id`),
            FOREIGN KEY(`mammoth_id`) REFERENCES `mammoth`(`id`),
            FOREIGN KEY(`expedition_id`) REFERENCES `expedition`(`id`),
            FOREIGN KEY(`pit_id`) REFERENCES `pit`(`id`),
            PRIMARY KEY(`mammoth_id`,`hunter_id`,`expedition_id`,`pit_id`)
        );
        INSERT INTO `killed_mammoth` (mammoth_id,hunter_id,expedition_id,pit_id) VALUES (1,5,1,1),
        (2,5,1,1),
        (3,6,1,2),
        (4,7,2,1),
        (7,19,6,4),
        (5,18,8,4),
        (9,18,10,4),
        (8,19,10,5),
        (6,19,11,8),
        (10,18,11,7),
        (11,19,11,8),
        (12,18,13,4),
        (13,19,13,5);
        DROP TABLE IF EXISTS `hunter_participates`;
        CREATE TABLE IF NOT EXISTS `hunter_participates` (
            `hunter_id`	INTEGER NOT NULL,
            `expedition_id`	INTEGER NOT NULL,
            PRIMARY KEY(`hunter_id`,`expedition_id`),
            FOREIGN KEY(`hunter_id`) REFERENCES `hunter`(`id`),
            FOREIGN KEY(`expedition_id`) REFERENCES `expedition`(`id`)
        );
        INSERT INTO `hunter_participates` (hunter_id,expedition_id) VALUES (4,1),
        (5,1),
        (6,1),
        (6,2),
        (7,2),
        (9,2),
        (4,3),
        (7,3),
        (9,4),
        (11,4),
        (17,4),
        (18,5),
        (19,5),
        (20,5),
        (18,6),
        (19,6),
        (21,6),
        (18,7),
        (19,7),
        (20,7),
        (18,8),
        (19,8),
        (18,9),
        (19,9),
        (18,10),
        (19,10),
        (18,11),
        (19,11),
        (20,11),
        (18,12),
        (19,12),
        (18,13),
        (19,13),
        (20,13),
        (18,14),
        (19,14),
        (21,14);
        DROP TABLE IF EXISTS `hunter`;
        CREATE TABLE IF NOT EXISTS `hunter` (
            `id`	INTEGER NOT NULL,
            `active_expedition_id`	INTEGER,
            `killed_by_mammoth_id`	INTEGER,
            FOREIGN KEY(`active_expedition_id`) REFERENCES `expedition`(`id`),
            FOREIGN KEY(`id`) REFERENCES `homo`(`id`),
            PRIMARY KEY(`id`),
            FOREIGN KEY(`killed_by_mammoth_id`) REFERENCES `mammoth`(`id`)
        );
        INSERT INTO `hunter` (id,active_expedition_id,killed_by_mammoth_id) VALUES (4,3,NULL),
        (5,NULL,1),
        (6,NULL,4),
        (7,3,NULL),
        (8,NULL,NULL),
        (9,4,NULL),
        (11,4,NULL),
        (17,4,NULL),
        (18,14,NULL),
        (19,14,NULL),
        (20,NULL,NULL),
        (21,14,NULL),
        (22,NULL,NULL);
        DROP TABLE IF EXISTS `homo`;
        CREATE TABLE IF NOT EXISTS `homo` (
            `id`	INTEGER NOT NULL,
            `email`	VARCHAR ( 64 ) NOT NULL UNIQUE,
            `password`	VARCHAR ( 128 ) NOT NULL,
            `first_name`	VARCHAR ( 32 ),
            `last_name`	VARCHAR ( 32 ),
            `date_of_birth`	DATE,
            `health`	VARCHAR ( 7 ) NOT NULL,
            `skills`	VARCHAR ( 128 ),
            `removed`	BOOLEAN NOT NULL,
            PRIMARY KEY(`id`),
            CHECK(removed IN(0,1)),
            CHECK(health IN('HEALTHY','WOUNDED','DEAD'))
        );
        INSERT INTO `homo` (id,email,password,first_name,last_name,date_of_birth,health,skills,removed) VALUES (1,'admin@admin.com','$2b$12$dxEhDAnD8lSYgCFGxeLx8epmYnCB4hwiHhDY61MT68omwN4E8hFvW','Main','Admin',NULL,'HEALTHY',NULL,0),
        (3,'patrol@patrol.com','$2b$12$AjI1eeArln.vVFOokIBEoOmd2qyas7IKbthDN00rztn3ENpcPupsK','Showcase','Patrol','2000-11-16','HEALTHY','',0),
        (4,'hunter@hunter.com','$2b$12$oZ8G86836T2C4TtBdcJRZef/ViXhKi7QIzXRvvz9R.SLR51x5Inpe','Showcase','Hunter','2000-04-17','HEALTHY','',0),
        (5,'kamen@email.com','$2b$12$e8zorEIKruZZMdEzYgkb3.uTqzNP7f.yLQEDD3XgEvMvoVpmUtuQK','Kamen','Rrrrr','2018-12-03','DEAD','',0),
        (6,'kamenkamen@kamen.com','$2b$12$XrQ0RwtKFQNuvuTXYLEHqO9NVpvtN9yoTtnM0kbrgGqUko7ntWBA6','Kamen','Kamen','2003-08-22','DEAD','',0),
        (7,'Joshua@kamen.com','$2b$12$my7RcyQZacL7AICggdYjE.U3HVBVMghva/nT7D8TtxxtG1q9SXcBS','Joshua','Veliky','2004-09-23','HEALTHY','',0),
        (8,'Gatsby@big.com','$2b$12$7DK/c.ARNnundZ/qMyYQIesgDLs3CbS5G/Sad5efTystV9rfXEHHC','Big','Gatsby','2007-05-02','WOUNDED','',0),
        (9,'alexander@kamen.com','$2b$12$lDgoaoG3AFQjC9cyITEzFe8iILqNQuyYwy3LBGZ8lXzskfrY9D5Ba','Alexander S','Velkym','2004-04-12','HEALTHY','',0),
        (10,'Arthas@lich.com','$2b$12$LqtC1ROnPFznDz6ENPZ5luLqHxKnVI3cOnyRdY.3Yr7KoiIeWpmoG','Arthas','Menethil','2005-09-28','DEAD','',0),
        (11,'Geralt@rivia.com','$2b$12$RYn9wJywpAJQczUW8reCLuZcwII6E6uJcUCqOnE4KZi7A7ZG1glU6','Geralt of ','Rivia','2006-03-01','HEALTHY','',0),
        (12,'Galahad@kulatystul.com','$2b$12$s0bw2qbOIcwmHj8vmOdjmuCr94kq2Gwr60/15GG3.YFqkgCTJa9EO','Sir','Galahad','2000-08-16','HEALTHY','',0),
        (13,'Mordok@kamen.com','$2b$12$QPQQUWHIDv1f8EOp1nwwpeSPbCUBMXp7Ahn7EGe6ECyUZNqBuBLvq','Mordok','Kamen','2002-11-29','WOUNDED','',0),
        (14,'Kamen2@patrol.com','$2b$12$YwPQqdnuu4O7QogxzssM.u9zam5W/2C6zyOY.1A7VMQlHmWqWEogW','Kamen','2','2000-06-30','HEALTHY','',0),
        (15,'David@misa.com','$2b$12$rYIw68Qze9PB6AVQsrAy3.k0bXDEdi1iXbZZkOoRhyGe9ZHbkP6SK','David','Bazout','2001-12-19','HEALTHY','',0),
        (16,'matous@kamen.com','$2b$12$dJBBplgKXFLJVyI8j5dcdOonAcF5E6.8WsV0ghZIzEDJrtIlxHtl.','Matous','Cak','2004-09-24','HEALTHY','',0),
        (17,'Big@kamen.com','$2b$12$.eIT4OlyaFv.CL/V/SvKjuDjSYdilPvQLVoH6jbnzK0tRAitWZhCa','Big','Lebowski','2002-11-27','HEALTHY','',0),
        (18,'Ferry@kamen.com','$2b$12$CA5YS6.LDOdhhntLZw43LuH5P4e1xerG3JGu3Bl.QiJRic/iCujMe','Ferry','Terry','2003-12-25','HEALTHY','',0),
        (19,'Vesemir@kaermorhen.com','$2b$12$IOfmHt1QL5bvQAkKWDeyq.elk2gYpkU7MvCjPDfiuv.i7Sf6lzioC','Vesemir','Witcher','2003-11-29','HEALTHY','',0),
        (20,'Dan@kamen.com','$2b$12$SOAhbL7D4753tmoCVlFKleqkFAdtLYdynvTHaFYRCQvXwpGeRYGa6','Dan','Blitzerian','2000-04-01','HEALTHY','',0),
        (21,'user2@kamen.com','$2b$12$YMxFEf5qoRy/EVjqtO68wOC2dZ9fIIA94QD9US2seEJzLP5fOkG4y','User','2','2004-09-29','HEALTHY','',0),
        (22,'user3@kamen.com','$2b$12$QYUPdYQwzFXC0nmkl49jt.BHvHTy.NN3WY3gbq7bqVk4i/GIHy29q','User','3','2004-10-01','HEALTHY','',0);
        DROP TABLE IF EXISTS `expedition_hunts`;
        CREATE TABLE IF NOT EXISTS `expedition_hunts` (
            `expedition_id`	INTEGER NOT NULL,
            `mammoth_id`	INTEGER NOT NULL,
            FOREIGN KEY(`mammoth_id`) REFERENCES `mammoth`(`id`),
            PRIMARY KEY(`expedition_id`,`mammoth_id`),
            FOREIGN KEY(`expedition_id`) REFERENCES `expedition`(`id`)
        );
        INSERT INTO `expedition_hunts` (expedition_id,mammoth_id) VALUES (1,1),
        (1,2),
        (1,3),
        (1,4),
        (2,4),
        (2,7),
        (3,5),
        (3,6),
        (3,7),
        (4,5),
        (4,6),
        (4,7),
        (5,5),
        (5,6),
        (6,8),
        (6,7),
        (6,12),
        (7,5),
        (7,6),
        (8,5),
        (8,6),
        (8,8),
        (9,6),
        (9,8),
        (9,9),
        (10,11),
        (10,10),
        (10,9),
        (10,8),
        (11,6),
        (11,10),
        (11,11),
        (11,12),
        (11,13),
        (12,12),
        (12,13),
        (13,13),
        (13,12),
        (13,14),
        (14,14),
        (14,15);
        DROP TABLE IF EXISTS `expedition`;
        CREATE TABLE IF NOT EXISTS `expedition` (
            `id`	INTEGER NOT NULL,
            `start_timestamp`	DATETIME,
            `end_timestamp`	DATETIME,
            `state`	VARCHAR ( 10 ) NOT NULL,
            `success`	BOOLEAN,
            PRIMARY KEY(`id`),
            CHECK(success IN(0,1)),
            CHECK(state IN('ASSEMBLING','ACTIVE','FINISHED'))
        );
        INSERT INTO `expedition` (id,start_timestamp,end_timestamp,state,success) VALUES (1,'2018-01-03 21:59:12.432715','2018-01-04 22:01:37.535268','FINISHED',1),
        (2,'2018-02-03 22:27:18.901839','2018-02-05 22:27:50.728706','FINISHED',1),
        (3,'2018-12-03 22:34:35.282315',NULL,'ACTIVE',NULL),
        (4,'2018-12-03 22:35:15.587791',NULL,'ACTIVE',NULL),
        (5,'2018-03-15 22:40:59.953667','2018-03-16 22:41:13.088819','FINISHED',0),
        (6,'2018-04-03 22:45:14.817487
        ','2018-04-05 22:45:30.258609','FINISHED',1),
        (7,'2018-03-03 22:46:46.231758','2018-03-08 22:47:03.003040','FINISHED',0),
        (8,'2018-04-16 22:48:19.256253','2018-04-18 22:48:33.715551','FINISHED',1),
        (9,'2018-05-14 23:02:30.427861','2018-05-20 23:02:39.356625','FINISHED',0),
        (10,'2018-06-22 23:04:58.648307','2018-06-24 23:05:16.875096','FINISHED',1),
        (11,'2018-06-03 23:06:22.955345','2018-06-06 23:06:44.346923','FINISHED',1),
        (12,'2018-06-25 23:08:16.651339','2018-06-28 23:08:21.469879','FINISHED',0),
        (13,'2018-12-03 23:08:48.704181','2018-12-03 23:09:04.892521','FINISHED',1),
        (14,'2018-12-03 23:09:39.828518',NULL,'ACTIVE',NULL);
        DROP TABLE IF EXISTS `admin`;
        CREATE TABLE IF NOT EXISTS `admin` (
            `id`	INTEGER NOT NULL,
            FOREIGN KEY(`id`) REFERENCES `homo`(`id`),
            PRIMARY KEY(`id`)
        );
        INSERT INTO `admin` (id) VALUES (1);
        COMMIT;
        """
    )


if __name__ == "__main__":
    main()
