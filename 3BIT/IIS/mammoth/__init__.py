import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager


app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
    ENV="dev",
    SECRET_KEY="dev",
    SQLALCHEMY_DATABASE_URI="sqlite:///"
    + os.path.join(app.instance_path, "mammoth.db"),
    SQLALCHEMY_TRACK_MODIFICATIONS=True,
)

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.login_message_category = "info"

# importing routing
from mammoth import views
