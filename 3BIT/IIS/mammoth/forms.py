import json
import datetime

from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    TextAreaField,
    BooleanField,
    DateField,
    SelectField,
    SubmitField,
    DateTimeField,
    DecimalField,
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    DataRequired,
    Length,
    Email,
    EqualTo,
    Optional,
    ValidationError,
)

from mammoth.models import Homo


class LoginForm(FlaskForm):
    email = EmailField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember = BooleanField("Remember Me")
    submit = SubmitField("Log in")


class AddMessageForm(FlaskForm):
    body = TextAreaField("Body")
    mammoths = StringField("Mammoths")

    def validate_mammoths(self, mammoths):
        if mammoths:
            try:
                json.loads(mammoths.data)
            except json.decoder.JSONDecodeError:
                raise ValidationError("List of mammoths in invalid format.")


class AddHomoForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = StringField("Password", validators=[DataRequired()])
    role = SelectField(
        "Role",
        validators=[DataRequired()],
        choices=[("ADMIN", "Admin"), ("HUNTER", "Hunter"), ("PATROL", "Patrol")],
    )
    first_name = StringField("First name")
    last_name = StringField("Last name")
    date_of_birth = DateField(
        "Date of birth",
        format="%Y-%m-%d",
        validators=[Optional()],
        render_kw={"placeholder": "YYYY-MM-DD"},
    )
    health = SelectField(
        "Health",
        validators=[DataRequired()],
        choices=[("HEALTHY", "Healthy"), ("WOUNDED", "Wounded"), ("DEAD", "Dead")],
        default="HEALTHY",
    )
    skills = TextAreaField("Skills")
    submit = SubmitField("Add homo")

    def validate_email(self, email):
        homo = Homo.query.filter(Homo.email == email.data).first()
        if homo:
            raise ValidationError("This email is taken. Please choose a different one.")

    def validate_date_of_birth(self, date):
        if date.data and date.data > datetime.date.today():
            raise ValidationError("Can't be born in the future.")


class EditHomoForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = StringField("Password")
    role = SelectField(
        "Role",
        validators=[DataRequired()],
        choices=[("ADMIN", "Admin"), ("HUNTER", "Hunter"), ("PATROL", "Patrol")],
    )
    first_name = StringField("First name")
    last_name = StringField("Last name")
    date_of_birth = DateField(
        "Date of birth",
        format="%Y-%m-%d",
        validators=[Optional()],
        render_kw={"placeholder": "YYYY-MM-DD"},
    )
    health = SelectField(
        "Health",
        validators=[DataRequired()],
        choices=[("HEALTHY", "Healthy"), ("WOUNDED", "Wounded"), ("DEAD", "Dead")],
        default="HEALTHY",
    )
    skills = TextAreaField("Skills")
    submit = SubmitField("Edit homo")

    def validate_date_of_birth(self, date):
        if date.data and date.data > datetime.date.today():
            raise ValidationError("Can't be born in the future.")


class AddMammothForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    special_chars = TextAreaField("Special characteristics")
    habits = TextAreaField("Habits")
    submit = SubmitField("Edit mammoth")


class EditMammothForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    special_chars = TextAreaField("Special characteristics")
    habits = TextAreaField("Habits")
    attributes = TextAreaField("Attributes")
    submit = SubmitField("Edit mammoth")


class AddPitForm(FlaskForm):
    lattitude = DecimalField("Lattitude", validators=[DataRequired()])
    longitude = DecimalField("Longitude", validators=[DataRequired()])


class EndExpeditionForm(FlaskForm):
    killed_hunters = StringField("Killed hunters")
    killed_mammoths = StringField("Killed mammoths")

    def validate_killed_hunters(self, hunters):
        if hunters:
            try:
                for hunter_id, mammoth_id in json.loads(hunters.data):
                    if not int(hunter_id) or not int(mammoth_id):
                        raise ValueError()
            except (TypeError, ValueError, json.decoder.JSONDecodeError):
                raise ValidationError("List of killed hunters in invalid format.")

    def validate_killed_mammoths(self, mammoths):
        if mammoths:
            try:
                for mammoth_id, hunter_id, pit_id in json.loads(mammoths.data):
                    if not int(mammoth_id) or not int(hunter_id) or not int(pit_id):
                        raise ValueError()
            except (TypeError, ValueError, json.decoder.JSONDecodeError):
                raise ValidationError("List of killed mammoths in invalid format.")
