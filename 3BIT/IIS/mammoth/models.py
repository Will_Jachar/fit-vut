import datetime

from flask_login import UserMixin

from mammoth import db, login_manager


@login_manager.user_loader
def load_user(homo_id):
    return Homo.query.get(int(homo_id))


# #############################################################################
#                                  DB Tables                                  #
# #############################################################################


class Homo(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)

    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    date_of_birth = db.Column(db.Date)
    health = db.Column(db.Enum("HEALTHY", "WOUNDED", "DEAD"), nullable=False)
    skills = db.Column(db.String(128))

    removed = db.Column(db.Boolean, nullable=False, default=False)

    @property
    def role(self):
        if Patrol.query.get(self.id):
            return "PATROL"
        if Hunter.query.get(self.id):
            return "HUNTER"
        if Admin.query.get(self.id):
            return "ADMIN"


class Admin(db.Model):
    id = db.Column(db.Integer, db.ForeignKey("homo.id"), primary_key=True)

    homo = db.relationship("Homo", uselist=False)


class Hunter(db.Model):
    id = db.Column(db.Integer, db.ForeignKey("homo.id"), primary_key=True)
    active_expedition_id = db.Column(db.Integer, db.ForeignKey("expedition.id"))
    killed_by_mammoth_id = db.Column(db.Integer, db.ForeignKey("mammoth.id"))

    homo = db.relationship("Homo", uselist=False)
    active_expedition = db.relationship("Expedition", uselist=False)
    expeditions = db.relationship(
        "Expedition", back_populates="hunters", secondary="hunter_participates"
    )
    killed_mammoths = db.relationship("Mammoth", secondary="killed_mammoth")
    killed_by_mammoth = db.relationship(
        "Mammoth", back_populates="killed_hunters", uselist=False
    )

    def die_by_mammoth(self, mammoth):
        self.killed_by_mammoth = mammoth
        self.homo.health = "DEAD"


class Patrol(db.Model):
    id = db.Column(db.Integer, db.ForeignKey("homo.id"), primary_key=True)
    position = db.Column(db.Integer)

    homo = db.relationship("Homo", uselist=False)
    messages = db.relationship("Message", back_populates="patrol")


class Mammoth(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    special_chars = db.Column(db.String(128), nullable=False)
    habits = db.Column(db.String(128), nullable=False)

    removed = db.Column(db.Boolean, nullable=False, default=False)

    killed_hunters = db.relationship("Hunter", back_populates="killed_by_mammoth")
    killed_by = db.relationship("Hunter", secondary="killed_mammoth", uselist=False)
    killed_in = db.relationship("Pit", secondary="killed_mammoth", uselist=False)


class Expedition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_timestamp = db.Column(db.DateTime)
    end_timestamp = db.Column(db.DateTime)
    state = db.Column(db.Enum("ASSEMBLING", "ACTIVE", "FINISHED"), nullable=False)
    success = db.Column(db.Boolean)

    hunters = db.relationship(
        "Hunter", back_populates="expeditions", secondary="hunter_participates"
    )
    pits = db.relationship("Pit", secondary="pit_belongs")
    hunts_mammoths = db.relationship("Mammoth", secondary="expedition_hunts")
    killed_mammoths = db.relationship("Mammoth", secondary="killed_mammoth")

    def add_hunter(self, hunter, active=True):
        self.hunters.append(hunter)
        if active:
            hunter.active_expedition = self

    def remove_hunter(self, hunter, active=True):
        self.hunters.remove(hunter)
        if active:
            hunter.active_expedition = None

    def add_pit(self, pit, active=True):
        self.pits.append(pit)
        if active:
            pit.active_expedition = self

    def remove_pit(self, pit, active=True):
        self.pits.remove(pit)
        if active:
            pit.active_expedition = None

    def add_killed_mammoth(self, mammoth, hunter, pit):
        db.session.execute(
            killed_mammoth.insert().values(
                mammoth_id=mammoth.id,
                expedition_id=self.id,
                hunter_id=hunter.id,
                pit_id=pit.id,
            )
        )

    def start_expedition(self):
        self.state = "ACTIVE"
        self.start_timestamp = datetime.datetime.now()

    def end_expedition(self, success):
        self.state = "FINISHED"
        self.success = success
        self.end_timestamp = datetime.datetime.now()
        self._free_hunters()
        self._free_pits()

    def _free_hunters(self):
        for hunter in self.hunters:
            hunter.active_expedition = None

    def _free_pits(self):
        for pit in self.pits:
            pit.active_expedition = None


class Pit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    lattitude = db.Column(db.String(32))
    longitude = db.Column(db.String(32))
    active_expedition_id = db.Column(db.Integer, db.ForeignKey("expedition.id"))

    removed = db.Column(db.Boolean, nullable=False, default=False)

    active_expedition = db.relationship("Expedition", uselist=False)


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sent_timestamp = db.Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow
    )
    body = db.Column(db.String)
    resolved = db.Column(db.Boolean, nullable=False, default=False)
    patrol_id = db.Column(db.Integer, db.ForeignKey("patrol.id"), nullable=False)

    patrol = db.relationship("Patrol", back_populates="messages", uselist=False)
    mammoths = db.relationship("Mammoth", secondary="mammoths_enum")


# #############################################################################
#                             Association Tables                              #
# #############################################################################

hunter_participates = db.Table(
    "hunter_participates",
    db.Column("hunter_id", db.Integer, db.ForeignKey("hunter.id"), primary_key=True),
    db.Column(
        "expedition_id", db.Integer, db.ForeignKey("expedition.id"), primary_key=True
    ),
)


pit_belongs = db.Table(
    "pit_belongs",
    db.Column("pit_id", db.Integer, db.ForeignKey("pit.id"), primary_key=True),
    db.Column(
        "expedition_id", db.Integer, db.ForeignKey("expedition.id"), primary_key=True
    ),
)


mammoths_enum = db.Table(
    "mammoths_enum",
    db.Column("message_id", db.Integer, db.ForeignKey("message.id"), primary_key=True),
    db.Column("mammoth_id", db.Integer, db.ForeignKey("mammoth.id"), primary_key=True),
)


expedition_hunts = db.Table(
    "expedition_hunts",
    db.Column(
        "expedition_id", db.Integer, db.ForeignKey("expedition.id"), primary_key=True
    ),
    db.Column("mammoth_id", db.Integer, db.ForeignKey("mammoth.id"), primary_key=True),
)


killed_mammoth = db.Table(
    "killed_mammoth",
    db.Column(
        "mammoth_id",
        db.Integer,
        db.ForeignKey("mammoth.id"),
        primary_key=True,
        unique=True,
    ),
    db.Column("hunter_id", db.Integer, db.ForeignKey("hunter.id"), primary_key=True),
    db.Column(
        "expedition_id", db.Integer, db.ForeignKey("expedition.id"), primary_key=True
    ),
    db.Column("pit_id", db.Integer, db.ForeignKey("pit.id"), primary_key=True),
)
