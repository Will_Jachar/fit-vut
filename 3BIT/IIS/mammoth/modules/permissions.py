from functools import wraps

from mammoth.models import Admin, Hunter, Patrol


def has_permission(user, role):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            roles = [role] if isinstance(role, str) else role
            if any([user.role == r for r in roles]):
                return func(*args, **kwargs)
            return "You don't have permission to access this page"
        return wrapper
    return decorator
