import json
import datetime
import secrets

from flask import session, render_template, url_for, flash, redirect, request, jsonify
from flask_login import login_user, current_user, logout_user, login_required
from sqlalchemy import desc
from sqlalchemy.sql import func
from itertools import groupby

from mammoth import db, app, bcrypt
from mammoth.models import (
    Homo,
    Admin,
    Hunter,
    Patrol,
    Mammoth,
    Expedition,
    Pit,
    Message,
)
from mammoth.forms import (
    LoginForm,
    AddMessageForm,
    AddHomoForm,
    EditHomoForm,
    AddMammothForm,
    EditMammothForm,
    AddPitForm,
    EndExpeditionForm,
)
from mammoth.modules import permissions


@app.before_request
def session_timeout():
    session.permanent = True
    app.permanent_session_lifetime = datetime.timedelta(minutes=60)
    session.modified = True


@app.route("/", methods=["GET", "POST"])
@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("dashboard"))
    form = LoginForm()
    if form.validate_on_submit():
        homo = Homo.query.filter(
            Homo.removed == False, Homo.email == form.email.data
        ).first()
        if homo and bcrypt.check_password_hash(homo.password, form.password.data):
            login_user(homo, remember=form.remember.data)
            next_page = request.args.get("next")
            return redirect(next_page) if next_page else redirect(url_for("dashboard"))
        else:
            flash(
                "Login unsuccessful. Please check your email and password.", "warning"
            )
    return render_template("login.html", title="Login", form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))


@app.route("/dashboard")
@login_required
def dashboard():
    if current_user.is_authenticated:
        active_expeditions = Expedition.query.filter(Expedition.state == "ACTIVE").all()
        top_ten_hunters = (
            db.session.query(Hunter, func.count("*").label("killed_mammoths_count"))
            .join(Hunter.homo)
            .join(Hunter.killed_mammoths)
            .filter(Homo.removed == False)
            .group_by(Hunter.id)
            .order_by(desc("killed_mammoths_count"))
            .limit(10)
            .all()
        )
        top_ten_expeditions = (
            db.session.query(Expedition, func.count("*").label("killed_mammoths_count"))
            .join(Expedition.killed_mammoths)
            .group_by(Expedition.id)
            .order_by(desc("killed_mammoths_count"))
            .limit(10)
            .all()
        )
        healthy_count = Homo.query.filter(
            Homo.removed == False, Homo.health == "HEALTHY"
        ).count()
        wounded_count = Homo.query.filter(
            Homo.removed == False, Homo.health == "WOUNDED"
        ).count()
        dead_count = Homo.query.filter(
            Homo.removed == False, Homo.health == "DEAD"
        ).count()

        expeditions_per_month = {}
        for month in range(1, 13):
            expeditions_per_month[month] = Expedition.query.filter(
                func.extract("month", Expedition.end_timestamp) == month
            ).count()
            print(expeditions_per_month[month])


        return render_template(
            "dashboard.html",
            title="Dashboard",
            expeditions_per_month=expeditions_per_month,
            healthy_count=healthy_count,
            wounded_count=wounded_count,
            dead_count=dead_count,
            active_expeditions=active_expeditions,
            top_ten_hunters=top_ten_hunters,
            top_ten_expeditions=top_ten_expeditions,
        )

    return redirect(url_for("login"))


@app.route("/profile_edit", methods=["GET", "POST"])
@login_required
def profile_edit():
    form = EditHomoForm(obj=current_user)

    if form.validate_on_submit():
        if (current_user.email == form.email.data or
            not Homo.query.filter(Homo.email == form.email.data).first()
        ):
            if form.password.data:
                hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
                    "utf-8"
                )
                current_user.password = hashed_password

            current_user.email = form.email.data
            current_user.first_name = form.first_name.data
            current_user.last_name = form.last_name.data
            current_user.date_of_birth = form.date_of_birth.data
            current_user.health = form.health.data
            current_user.skills = form.skills.data

            db.session.commit()

            flash(f"Your profile has been edited", "success")
        else:
            form.email.errors = list(form.email.errors) + [
                "This email is taken. Please choose a different one."]
    if form.errors:
        flash(f"Form is containing errors", "danger")

    form.password.data = ""
    return render_template("profile_edit.html", title="Edit Profile", form=form)


@app.route("/message_list")
@login_required
@permissions.has_permission(current_user, ["ADMIN", "PATROL"])
def message_list():
    is_admin = current_user.role == "ADMIN"
    if is_admin:
        message_list = Message.query.all()
    else:
        message_list = Message.query.filter(Message.patrol_id == current_user.id).all()
    return render_template(
        "message_list.html",
        title="Messages",
        message_list=message_list,
        is_admin=is_admin,
    )


@app.route("/message_add", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "PATROL")
def message_add():
    form = AddMessageForm()
    mammoths_alive = Mammoth.query.filter(Mammoth.killed_by == None)

    if form.validate_on_submit():
        message = Message(patrol_id=current_user.id)
        message.body = form.body.data

        for mammoth_id in json.loads(form.mammoths.data):
            mammoth = Mammoth.query.get(mammoth_id)

            if mammoth is not None:
                message.mammoths.append(mammoth)

        db.session.add(message)
        db.session.commit()

        flash("Message has been sent", "success")
        return redirect(url_for("message_list"))
    if form.errors:
        flash("Form is containing errors", "danger")

    return render_template(
        "message_add.html",
        title="New Message",
        form=form,
        mammoths_alive=mammoths_alive,
    )


@app.route("/message_edit/<int:message_id>")
@login_required
@permissions.has_permission(current_user, ["ADMIN", "PATROL"])
def message_edit(message_id):
    message = Message.query.get(message_id)

    if not message:
        flash(f"Message with ID {message_id} does not exist.", "warning")
        return redirect(url_for("message_list"))

    return render_template(
        "message_edit.html",
        title="Message Detail",
        message=message,
        is_admin=(current_user.role == "ADMIN"),
    )


@app.route("/message_edit/<int:message_id>/<string:new_expedition>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def resolve_message(message_id, new_expedition):
    message = Message.query.get(message_id)

    if not message:
        flash(f"Message with ID {message_id} does not exist.", "warning")
        return redirect(url_for("message_list"))
    if message.resolved:
        flash(f"Message with ID {message_id} already resolved.", "warning")
        return redirect(url_for("message_list"))

    if new_expedition.lower() == "true":
        return redirect(url_for("expedition_add", message_id=message_id))

    message.resolved = True
    db.session.commit()

    return redirect(url_for("message_list"))


@app.route("/homo_list")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def homo_list():
    homo_list = Homo.query.filter(Homo.removed == False).all()
    return render_template("homo_list.html", title="Homos", homo_list=homo_list)


@app.route("/homo_add", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def homo_add():
    form = AddHomoForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )
        homo = Homo(
            email=form.email.data,
            password=hashed_password,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            date_of_birth=form.date_of_birth.data,
            health=form.health.data,
            skills=form.skills.data,
        )
        db.session.add(homo)
        db.session.flush()

        role = {
            "ADMIN": Admin(id=homo.id),
            "HUNTER": Hunter(id=homo.id),
            "PATROL": Patrol(id=homo.id),
        }[form.role.data]
        db.session.add(role)

        db.session.commit()

        flash(f"New {form.role.data.title()} has been created", "success")
        return redirect(url_for("homo_list"))
    if form.errors:
        flash("Form is containing errors", "danger")

    form.password.data = secrets.token_hex(4)  # Default value for password
    return render_template("homo_add.html", title="Add Homo", form=form)


@app.route("/homo_edit/<int:homo_id>", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def homo_edit(homo_id):
    homo = Homo.query.get(homo_id)
    form = EditHomoForm(obj=homo)

    if not homo or homo.removed:
        flash(f"Homo with ID {homo_id} does not exist or has been removed.", "warning")
        return redirect(url_for("homo_list"))

    active_expedition = None
    if homo.role == "HUNTER":
        active_expedition = Hunter.query.get(homo.id).active_expedition

    if form.validate_on_submit():
        if (homo.email == form.email.data or
            not Homo.query.filter(Homo.email == form.email.data).first()
        ):
            if form.password.data:
                hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
                    "utf-8"
                )
                homo.password = hashed_password

            homo.email = form.email.data
            homo.first_name = form.first_name.data
            homo.last_name = form.last_name.data
            homo.date_of_birth = form.date_of_birth.data
            homo.health = form.health.data
            homo.skills = form.skills.data

            db.session.commit()

            flash(f"Homo {form.first_name.data.title()} has been edited", "success")
            return redirect(url_for("homo_list"))
        else:
            form.email.errors = list(form.email.errors) + [
                "This email is taken. Please choose a different one."]
    if form.errors:
        flash(f"Form is containing errors", "danger")

    form.password.data = ""
    return render_template(
        "homo_edit.html",
        title="Edit Homo",
        form=form,
        homo=homo,
        active_expedition=active_expedition,
    )


@app.route("/homo_remove/<int:homo_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def homo_remove(homo_id):
    homo = Homo.query.get(homo_id)

    if not homo or homo.removed:
        flash(f"Homo with ID {homo_id} does not exist or has been removed.", "warning")
        return redirect(url_for("homo_list"))

    hunter = Hunter.query.get(homo_id)
    if hunter and hunter.active_expedition:
        flash(
            "Unable to remove homo, because it is part of active expedition", "warning"
        )
        return redirect(url_for("homo_edit", homo_id=homo_id))

    homo.removed = True
    db.session.commit()

    flash(f"Homo with ID {homo_id} has been removed", "success")
    return redirect(url_for("homo_list"))


@app.route("/mammoth_list")
@login_required
def mammoth_list():
    mammoth_list = Mammoth.query.filter(Mammoth.removed == False).all()
    return render_template(
        "mammoth_list.html", title="Mammoths", mammoth_list=mammoth_list
    )


@app.route("/mammoth_add", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, ["ADMIN", "PATROL"])
def mammoth_add():
    form = AddMammothForm()

    if form.validate_on_submit():
        mammoth = Mammoth(
            name=form.name.data,
            special_chars=form.special_chars.data,
            habits=form.habits.data,
        )
        db.session.add(mammoth)
        db.session.flush()
        db.session.commit()

        flash("New mammoth has been created", "success")
        return redirect(url_for("mammoth_list"))
    if form.errors:
        flash("Form is containing errors", "danger")

    return render_template("mammoth_add.html", title="Add Mammoth", form=form)


@app.route("/mammoth_edit/<int:mammoth_id>", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, ["ADMIN", "PATROL"])
def mammoth_edit(mammoth_id):
    mammoth = Mammoth.query.get(mammoth_id)

    if not mammoth or mammoth.removed:
        flash(
            f"Mammoth with ID {mammoth_id} does not exist or has been removed.",
            "warning",
        )
        return redirect(url_for("mammoth_list"))

    form = EditMammothForm(obj=mammoth)

    if form.validate_on_submit():
        mammoth.name = form.name.data
        mammoth.special_chars = form.special_chars.data
        mammoth.habits = form.habits.data
        db.session.commit()

        flash("Mammoth has been edited", "success")
        return redirect(url_for("mammoth_list"))
    if form.errors:
        flash("Form is containing errors", "danger")

    return render_template(
        "mammoth_edit.html", title="Edit Mammoth", form=form, mammoth=mammoth
    )


@app.route("/mammoth_remove/<int:mammoth_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def mammoth_remove(mammoth_id):
    mammoth = Mammoth.query.get(mammoth_id)

    if not mammoth or mammoth.removed:
        flash(f"Mammoth with ID {mammoth_id} does not exist or has been removed.", "warning")
        return redirect(url_for("mammoth_list"))

    if Expedition.query.filter(
        Expedition.state.in_(["ASSEMBLING", "ACTIVE"]),
        Expedition.hunts_mammoths.any(id=mammoth_id),
    ).first():
        flash(
            "Unable to remove mammoth, because it is part of active expedition",
            "warning",
        )
        return redirect(url_for("mammoth_edit", mammoth_id=mammoth_id))

    mammoth.removed = True
    db.session.commit()

    flash(f"Mammoth with ID {mammoth_id} has been removed", "success")
    return redirect(url_for("mammoth_list"))


@app.route("/expedition_list")
@login_required
def expedition_list():
    expedition_list = Expedition.query.all()
    return render_template(
        "expedition_list.html", title="Expeditions", expedition_list=expedition_list
    )


@app.route("/expedition_add")
@app.route("/expedition_add/<int:message_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def expedition_add(message_id=None):
    expedition = Expedition(state="ASSEMBLING")

    if message_id:
        message = Message.query.get(message_id)

        if not message:
            flash(f"Message with ID {message_id} does not exist.", "warning")
            return redirect(url_for("message_list"))
        if message.resolved:
            flash(f"Message with ID {message_id} already resolved.", "warning")
            return redirect(url_for("message_list"))

        message.resolved = True
        for mammoth in message.mammoths:
            if not mammoth.killed_by and not mammoth.removed:
                expedition.hunts_mammoths.append(mammoth)

    db.session.add(expedition)
    db.session.commit()

    flash("New expedition has been created", "success")
    return redirect(url_for("expedition_edit", expedition_id=expedition.id))


@app.route("/expedition_edit/<int:expedition_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def expedition_edit(expedition_id):
    expedition = Expedition.query.get(expedition_id)

    if not expedition:
        flash(f"Expedition with ID {expedition_id} does not exist.", "warning")
        return redirect(url_for("expedition_list"))

    transition = request.args.get("transition")
    if not transition:
        free_hunters = Hunter.query.join(Homo).filter(
            Homo.removed == False,
            Hunter.active_expedition == None,
            Homo.health == "HEALTHY",
        )

        free_pits = Pit.query.filter(Pit.removed == False, Pit.active_expedition == None)
        free_mammoths = Mammoth.query.filter(
            Mammoth.removed == False, Mammoth.killed_by == None, Mammoth.killed_in == None
        )

        return render_template(
            "expedition_edit.html",
            title="Edit Expedition",
            expedition=expedition,
            free_hunters=free_hunters,
            free_pits=free_pits,
            free_mammoths=free_mammoths,
        )

    if transition == "ACTIVATE" and expedition.state == "ASSEMBLING":
        if (
            not expedition.hunters
            or not expedition.pits
            or not expedition.hunts_mammoths
        ):
            flash(
                "Expedition can't be activated without at least one hunter, "
                "pit and mammoth",
                "danger",
            )
            return redirect(url_for("expedition_edit", expedition_id=expedition.id))

        expedition.start_expedition()
        db.session.commit()

        flash("Expedition is now running", "success")
        return redirect(url_for("expedition_edit", expedition_id=expedition.id))
    else:
        flash("Invalid transition", "danger")
        return redirect(url_for("expedition_edit", expedition_id=expedition.id))


@app.route("/expedition_end/<int:expedition_id>", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def expedition_end(expedition_id):
    expedition = Expedition.query.get(expedition_id)

    if not expedition:
        flash(f"Expedition with ID {expedition_id} does not exist.", "warning")
        return redirect(url_for("expedition_list"))
    if expedition.state in ["ASSEMBLING", "FINISHED"]:
        flash("Can't end expedition in state 'ASSEMBLING' or 'FINISHED'.", "warning")
        return redirect(url_for("expedition_edit", expedition_id=expedition_id))

    form = EndExpeditionForm()

    if form.validate_on_submit():
        for hunter_id, mammoth_id in json.loads(form.killed_hunters.data):
            hunter = Hunter.query.get(int(hunter_id))
            mammoth = Mammoth.query.get(int(mammoth_id))

            if (not hunter or
                hunter not in expedition.hunters or
                not mammoth or
                mammoth not in expedition.hunts_mammoths
            ):
                db.session.rollback()
                flash("Inconsistent data in form", "danger")
                return redirect(url_for("expedition_end", expedition_id=expedition.id))
            hunter.die_by_mammoth(mammoth)

        for mammoth_id, hunter_id, pit_id in json.loads(form.killed_mammoths.data):
            mammoth = Mammoth.query.get(int(mammoth_id))
            hunter = Hunter.query.get(int(hunter_id))
            pit = Pit.query.get(int(pit_id))

            if (not mammoth or
                mammoth not in expedition.hunts_mammoths or
                not hunter or
                hunter not in expedition.hunters or
                not pit or
                pit not in expedition.pits
            ):
                db.session.rollback()
                flash("Inconsistent data in form", "danger")
                return redirect(url_for("expedition_end", expedition_id=expedition.id))
            expedition.add_killed_mammoth(mammoth, hunter, pit)

        expedition.end_expedition(bool(expedition.killed_mammoths))
        db.session.commit()

        flash("Expedition has been marked as finished", "success")
        return redirect(url_for("expedition_edit", expedition_id=expedition.id))
    if form.errors:
        flash("Inconsistent data in form", "danger")

    return render_template(
        "expedition_end.html",
        title="End Expedition",
        form=form,
        expedition=expedition,
    )


@app.route("/expedition_remove/<int:expedition_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def expedition_remove(expedition_id):
    expedition = Expedition.query.get(expedition_id)

    if not expedition:
        flash(f"Expedition with ID {expedition_id} does not exist.", "warning")
        return redirect(url_for("expedition_list"))

    if expedition.state != "ASSEMBLING":
        flash("Unable to remove expedition, because it active or finished", "warning")
        return redirect(url_for("expedition_edit", expedition_id=expedition_id))

    expedition.end_expedition(False)
    db.session.delete(expedition)
    db.session.commit()

    flash(f"Expedition with ID {expedition_id} has been removed", "success")
    return redirect(url_for("expedition_list"))


@app.route("/pit_list")
@login_required
def pit_list():
    pit_list = Pit.query.filter(Pit.removed == False).all()
    return render_template("pit_list.html", title="Pits", pit_list=pit_list)


@app.route("/pit_add", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def pit_add():
    form = AddPitForm()

    if form.validate_on_submit():
        pit = Pit(
            lattitude=str(form.lattitude.data), longitude=str(form.longitude.data)
        )
        db.session.add(pit)
        db.session.flush()
        db.session.commit()

        flash("New pit has been created", "success")
        return redirect(url_for("pit_list"))

    if form.errors:
        flash("Form is containing errors", "danger")

    return render_template("pit_add.html", title="Add pit", form=form)


@app.route("/pit_edit/<int:pit_id>", methods=["GET", "POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def pit_edit(pit_id):
    pit = Pit.query.get(pit_id)

    if not pit or pit.removed:
        flash(f"Pit with ID {pit_id} does not exist or has been removed.", "warning")
        return redirect(url_for("pit_list"))

    return render_template("pit_edit.html", title="Edit pit", pit=pit)


@app.route("/pit_remove/<int:pit_id>")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def pit_remove(pit_id):
    pit = Pit.query.get(pit_id)

    if not pit or pit.removed:
        flash(f"Pit with ID {pit_id} does not exist or has been removed.", "warning")
        return redirect(url_for("pit_list"))

    if pit.active_expedition:
        flash(
            "Unable to remove pit, because it is part of active expedition", "warning"
        )
        return redirect(url_for("pit_edit", pit_id=pit_id))

    pit.removed = True
    db.session.commit()

    flash(f"Pit with ID {pit_id} has been removed", "success")
    return redirect(url_for("pit_list"))


#  FUNCTIONS FOR AJAX ##################################################################


@app.route("/pending_messages")
@login_required
@permissions.has_permission(current_user, "ADMIN")
def pending_messages():
    count = Message.query.filter(Message.resolved == False).count()
    return jsonify({"count": count})


@app.route("/assign_hunter", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def assign_hunter():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        hunter = Hunter.query.get(int(request.form["hunter_id"]))

        if not hunter or hunter.homo.removed:
            return "", 400

        expedition.add_hunter(hunter)
        db.session.commit()
    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify(
        {
            "id": str(hunter.id),
            "full_name": f"{hunter.homo.first_name} {hunter.homo.last_name}",
            "health": hunter.homo.health,
        }
    )


@app.route("/unassign_hunter", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def unassign_hunter():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        hunter = Hunter.query.get(int(request.form["hunter_id"]))

        expedition.remove_hunter(hunter)
        db.session.commit()
    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify({"id": str(hunter.id)})


@app.route("/assign_pit", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def assign_pit():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        pit = Pit.query.get(int(request.form["pit_id"]))

        if not pit or pit.removed:
            return "", 400

        expedition.add_pit(pit)
        db.session.commit()
    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify(
        {
            "id": str(pit.id),
            "lattitude": str(pit.lattitude),
            "longitude": str(pit.longitude),
        }
    )


@app.route("/unassign_pit", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def unassign_pit():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        pit = Pit.query.get(int(request.form["pit_id"]))

        expedition.remove_pit(pit)
        db.session.commit()
    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify({"id": str(pit.id)})


@app.route("/assign_mammoth", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def assign_mammoth():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        mammoth = Mammoth.query.get(int(request.form["mammoth_id"]))

        if not mammoth or mammoth.removed:
            return "", 400

        expedition.hunts_mammoths.append(mammoth)
        db.session.commit()

    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify(
        {
            "id": str(mammoth.id),
            "name": str(mammoth.name),
            "special_chars": str(mammoth.special_chars),
            "habits": str(mammoth.habits),
        }
    )


@app.route("/unassign_mammoth", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def unassign_mammoth():
    try:
        expedition = Expedition.query.get(int(request.form["expedition_id"]))
        mammoth = Mammoth.query.get(int(request.form["mammoth_id"]))

        expedition.hunts_mammoths.remove(mammoth)
        db.session.commit()
    except (TypeError, ValueError, AttributeError):
        return "", 400
    return jsonify({"id": str(mammoth.id)})


@app.route("/kill_mammoth", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def kill_mammoth():
    pass


@app.route("/kill_hunter", methods=["POST"])
@login_required
@permissions.has_permission(current_user, "ADMIN")
def kill_hunter():
    pass


@app.route("/is_mammoth_alive", methods=["POST"])
@login_required
@permissions.has_permission(current_user, ["ADMIN", "PATROL"])
def is_mammoth_alive():
    try:
        mammoth = Mammoth.query.get(int(request.form["mammoth_id"]))
    except (TypeError, ValueError, AttributeError):
        return "", 400

    if mammoth.killed_by:
        return "", 400

    return jsonify(
        {
            "id": str(mammoth.id),
            "name": str(mammoth.name),
            "special_chars": str(mammoth.special_chars),
            "habits": str(mammoth.habits),
            "health": "ALIVE",
        }
    )
