import os

from mammoth import app, db


if __name__ == '__main__':
    # Create database and all tables if its not already existing
    if app.config['ENV'] == 'dev':
        if not os.path.exists(app.instance_path):
            os.makedirs(app.instance_path)
        if not os.path.isfile(os.path.join(app.instance_path, 'mammoth.db')):
            import mammoth.models
            db.create_all()

    app.run(debug=True)
