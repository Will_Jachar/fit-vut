/**
 * IMP Projekt - Mereni vzdalenosti ultrazvukovym senzorem
 *
 * @file main.c
 * @author Peter Uhrin (xuhrin02)
 * @brief Program byl vypracovan ze 100% jako original
 * @date 2018-12-24
 */

// Header file with all the essential definitions for a given type of MCU
#include "MK60D10.h"

/* ############################## Macros and constants ############################## */

// Number used to calculate distance
// Speed of sound = 0.0343 cm/us
// Divide by 2 because sound pulse travels twice the distance (back and forth)
// Divide by 1.5625 because of clock speed of the timer
#define MAGIC_NUM 0.01112f

#define PIT_LDVAL_100MS 4799999 // PIT interrupt every 100ms
#define DELAY_LEDD 2000 // Delay for active time of one digit on the display

#define PTA_TRIGGER_MASK 0x1000000 // Mask for trigger pin on PTA
#define PTA_ECHO_MASK 0x2000000 // Mask for echo pin on PTA

// Display segments masks
#define PTA_SEGMENTS_MASK 0x0FC0
#define PTD_SEGMENTS_MASK 0xC000

#define NUM_OF_POSITIONS 4
#define NUM_OF_DIGITS 10

// Activation constants for digit position of the display (active with log. 0)
const unsigned POSITIONS[NUM_OF_POSITIONS] = {
    0x3200, // PTD8
    0x1300, // PTD13
    0x2300, // PTD12
    0x3100, // PTD9
};

// Activation constants for segments of each digit
// (7 segments, (A,B,D,E,F on PTA and C,G on PTD), active in log. 1)
const unsigned DIGITS[NUM_OF_DIGITS + 1][2] = {
    {0x0EC0, 0x4000}, // 0
    {0x0200, 0x4000}, // 1
    {0x0E40, 0x8000}, // 2
    {0x0E00, 0xC000}, // 3
    {0x0280, 0xC000}, // 4
    {0x0C80, 0xC000}, // 5
    {0x0CC0, 0xC000}, // 6
    {0x0A00, 0x4000}, // 7
    {0x0EC0, 0xC000}, // 8
    {0x0E80, 0xC000}, // 9
    {0x0000, 0x8000}  // ERROR
};

// Variables used for measuring and calculation of distance
unsigned start_time = 0;
unsigned end_time = 0;
unsigned echo_duration = 0;
unsigned distance = 0;

/* ############################# Function declarations ############################## */

void MCUInit();
void PortsInit();
void LPTMR0Init();
void PIT0Init();
void PIT0_IRQHandler();
void PORTA_IRQHandler();
void delay(unsigned bound);
void set_digit(unsigned digit);
void print_num(unsigned num);

/* ################################# Init functions ################################# */

// Initialize the MCU - basic clock settings, turning the watchdog off
void MCUInit()
{
    // Setup internal clock
    MCG->C4 |= (MCG_C4_DMX32_MASK | MCG_C4_DRST_DRS(0x1));
    SIM->CLKDIV1 |= SIM_CLKDIV1_OUTDIV1(0x0);
    // Disable watchdog
    WDOG->STCTRLH &= ~WDOG_STCTRLH_WDOGEN_MASK;
}

// Initialize ports
void PortsInit()
{
    SIM->SCGC5 = SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTD_MASK;

    // Inicialize pins used as display segments
    PORTA->PCR[6]  = PORT_PCR_MUX(0x1); // seg E
    PORTA->PCR[7]  = PORT_PCR_MUX(0x1); // seg F
    PORTA->PCR[8]  = PORT_PCR_MUX(0x1); // seg DP
    PORTA->PCR[9]  = PORT_PCR_MUX(0x1); // seg B
    PORTA->PCR[10] = PORT_PCR_MUX(0x1); // seg D
    PORTA->PCR[11] = PORT_PCR_MUX(0x1); // seg A
    PORTD->PCR[14] = PORT_PCR_MUX(0x1); // seg C
    PORTD->PCR[15] = PORT_PCR_MUX(0x1); // seg G

    // Inicialize pins used as display position selector
    PORTD->PCR[8]  = PORT_PCR_MUX(0x1); // pozice 0
    PORTD->PCR[9]  = PORT_PCR_MUX(0x1); // pozice 3
    PORTD->PCR[12] = PORT_PCR_MUX(0x1); // pozice 2
    PORTD->PCR[13] = PORT_PCR_MUX(0x1); // pozice 1

    // Inicialize ultrasonic sensor
    PORTA->PCR[24] = PORT_PCR_MUX(0x1); // Trigger (OUT)
    PORTA->PCR[25] = PORT_PCR_ISF(0x1) | PORT_PCR_IRQC(0xB) | PORT_PCR_MUX(0x1); // Echo (IN)

    // Configuration of pins direction
    PTA->PDDR = GPIO_PDDR_PDD(0x1000FC0);
    PTD->PDDR = GPIO_PDDR_PDD(0x000F300);

    // Enable interrupt on port A
    NVIC_ClearPendingIRQ(PORTA_IRQn);
    NVIC_EnableIRQ(PORTA_IRQn);
}

// Initialize Low Power Timer
void LPTMR0Init()
{
    OSC->CR |= OSC_CR_ERCLKEN_MASK; // Enable OSCERCLK - 50 Mhz
    SIM->SCGC5 |= SIM_SCGC5_LPTIMER_MASK;

    LPTMR0->CSR &= ~LPTMR_CSR_TEN_MASK; // Turn OFF LPTMR to perform setup

    // Select OSCERCLK clock as source and scale speed down to 1.5625 MHz
    LPTMR0->PSR = LPTMR_PSR_PRESCALE(0x4) | LPTMR_PSR_PCS(0x3);

    LPTMR0->CMR = 0xFFFFu; // Set compare value

    LPTMR0->CSR = LPTMR_CSR_TFC_MASK; // Reset on overflow

    LPTMR0->CSR |= LPTMR_CSR_TEN_MASK; // Turn ON LPTMR0 and start counting
}

// Initialize Periodic interrupt timer
void PIT0Init()
{
    SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
    PIT->MCR = 0x0;

    PIT_TCTRL0 &= ~PIT_TCTRL_TEN_MASK; // Turn OFF PIT to perform setup

    PIT_LDVAL0 = PIT_LDVAL_100MS; // Every 100ms

    // Enable interrupt
    PIT_TCTRL0 = PIT_TCTRL_TIE_MASK;
    NVIC_ClearPendingIRQ(PIT0_IRQn);
    NVIC_EnableIRQ(PIT0_IRQn);

    PIT_TCTRL0 |= PIT_TCTRL_TEN_MASK; // Turn ON PIT and start counting
}

/* ############################### Interrupt handlers ############################### */

// Handler for PIT interrupts
void PIT0_IRQHandler()
{
    // Ping trigger pin
    PTA->PDOR |= GPIO_PDOR_PDO(PTA_TRIGGER_MASK);
    delay(100);
    PTA->PDOR &= GPIO_PDOR_PDO(~PTA_TRIGGER_MASK);

    PIT_TFLG0 = PIT_TFLG_TIF_MASK;
}

// Handler for interrupts caused by rising or falling edge on echo pin
void PORTA_IRQHandler()
{
    // If rising edge
    if (PTA->PDIR & PTA_ECHO_MASK)
    {
        // Save start time
        LPTMR0->CNR = 0x1;
        start_time = LPTMR0->CNR;
    }
    // If falling edge
    else
    {
        // Save end time
        LPTMR0->CNR = 0x1;
        end_time = LPTMR0->CNR;

        // Calculate echo duration
        if (start_time >= end_time)
        {
            echo_duration = 0xFFFFu - start_time + end_time; // If overflow occurred
        }
        else
        {
            echo_duration = end_time - start_time;
        }
        
        // Calculate distance (echo_duratin / 2 / num_of_ticks_per_us * speed_of_sound)
        distance = (float)echo_duration * MAGIC_NUM + 0.5f; // + 0.5 for rounding
    }

    PORTA->ISFR = ~0; // Clear ISFR
}

/* ############################### Utility functions ################################ */

// Delay function
void delay(unsigned bound)
{
    for (unsigned i = 0; i < bound; i++)
    {
        __NOP();
    }
}

// Set segments to display digit
void set_digit(unsigned digit)
{
    // Clear segments
    PTA->PDOR &= GPIO_PDOR_PDO(~PTA_SEGMENTS_MASK);
    PTD->PDOR &= GPIO_PDOR_PDO(~PTD_SEGMENTS_MASK);

    if (digit < NUM_OF_DIGITS)
    {
        // Set segments
        PTA->PDOR |= GPIO_PDOR_PDO(DIGITS[digit][0]);
        PTD->PDOR |= GPIO_PDOR_PDO(DIGITS[digit][1]);
    }
    else // Display "-" (ERROR) when digit is invalid
    {
        PTA->PDOR |= GPIO_PDOR_PDO(DIGITS[NUM_OF_DIGITS][0]);
        PTD->PDOR |= GPIO_PDOR_PDO(DIGITS[NUM_OF_DIGITS][1]);
    }
}

// Display last 4 digits of number
void print_num(unsigned num)
{
    // Iterate over digit positions
    for (int i = NUM_OF_POSITIONS - 1; i >= 0; i--)
    {
        set_digit(num % 10);                       // Set digit
        PTD->PDOR |= GPIO_PDOR_PDO(POSITIONS[i]);  // Activate digit on position POSITIONS[i]
        delay(DELAY_LEDD);
        PTD->PDOR &= GPIO_PDOR_PDO(~POSITIONS[i]); // Deactivate digit on position POSITIONS[i]
        num /= 10;
    }
}

/* ###################################### Main ###################################### */

// Main function
int main()
{
    MCUInit();
    PortsInit();
    LPTMR0Init();
    PIT0Init();

    while (1)
    {
        print_num(distance);
    };

    return 0;
}
