/**
 * @file coarse_graining.cpp
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Implementations for functions declared in coarse_graining.h
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include <fstream>
#include <iostream>
#include <vector>

#include "coarse_graining.h"
#include "utils.h"

namespace lga
{

std::vector<struct velocity_vector> coarse_graining(unsigned char *const *const nodes,
                                                    bool *const *const obstacles,
                                                    unsigned const size_x,
                                                    unsigned const size_y,
                                                    unsigned const grain_size,
                                                    float density)
{
    unsigned grain_x = size_x / grain_size;
    unsigned grain_y = size_y / grain_size;

    std::vector<struct velocity_vector> velocity_vector_field;

    for (unsigned x = 0; x < grain_x; x++)
    {
        unsigned x_bd_l = x * grain_size;
        unsigned x_bd_u = (x + 1) * grain_size;

        for (unsigned y = 0; y < grain_y; y++)
        {
            unsigned y_bd_l = y * grain_size;
            unsigned y_bd_u = (y + 1) * grain_size;

            std::vector<unsigned> particles_count = count_particles(nodes, x_bd_l, x_bd_u, y_bd_l, y_bd_u);

            float av_vel_x = (1.0f / (grain_size * grain_size) *
                              (particles_count[0] * COS_0PI_OVER_3 +
                               particles_count[1] * COS_1PI_OVER_3 +
                               particles_count[2] * COS_2PI_OVER_3 +
                               particles_count[3] * COS_3PI_OVER_3 +
                               particles_count[4] * COS_4PI_OVER_3 +
                               particles_count[5] * COS_5PI_OVER_3));
            float av_vel_y = (1.0f / (grain_size * grain_size) *
                              (particles_count[0] * SIN_0PI_OVER_3 +
                               particles_count[1] * SIN_1PI_OVER_3 +
                               particles_count[2] * SIN_2PI_OVER_3 +
                               particles_count[3] * SIN_3PI_OVER_3 +
                               particles_count[4] * SIN_4PI_OVER_3 +
                               particles_count[5] * SIN_5PI_OVER_3));

            unsigned x_coord = x * grain_size + (grain_size / 2);
            unsigned y_coord = y * grain_size + (grain_size / 2);
            if (!obstacles[x_coord][y_coord])
            {
                velocity_vector_field.push_back({x_coord,
                                                 y_coord,
                                                 av_vel_x * grain_size / density,
                                                 av_vel_y * grain_size / density});
            }
        }
    }

    return velocity_vector_field;
}

std::vector<unsigned> count_particles(unsigned char *const *const nodes,
                                      unsigned const x_lb,
                                      unsigned const x_ub,
                                      unsigned const y_lb,
                                      unsigned const y_ub)
{
    std::vector<unsigned> particles_counts(FHP_BITS);
    for (unsigned x = x_lb; x < x_ub; x++)
    {
        for (unsigned y = y_lb; y < y_ub; y++)
        {
            for (unsigned i = 0; i < FHP_BITS; i++)
            {
                particles_counts[i] += static_cast<bool>(nodes[x][y] & (1 << i));
            }
        }
    }

    return particles_counts;
}

} //namespace lga
