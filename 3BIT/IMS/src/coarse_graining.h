/**
 * @file coarse_graining.h
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Declarations for functions are responsible for plotting the results.
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */
#pragma once

#include <vector>

namespace lga
{

/**
 * @brief Precomputed values for performance
 */
constexpr float COS_0PI_OVER_3 = 1.0f;
constexpr float COS_1PI_OVER_3 = 0.5f;
constexpr float COS_2PI_OVER_3 = -0.5f;
constexpr float COS_3PI_OVER_3 = -1.0f;
constexpr float COS_4PI_OVER_3 = -0.5f;
constexpr float COS_5PI_OVER_3 = 0.5f;

constexpr float SIN_0PI_OVER_3 = 0.0f;
constexpr float SIN_1PI_OVER_3 = 0.86602540378f;
constexpr float SIN_2PI_OVER_3 = 0.86602540378f;
constexpr float SIN_3PI_OVER_3 = 0.0f;
constexpr float SIN_4PI_OVER_3 = -0.86602540378f;
constexpr float SIN_5PI_OVER_3 = -0.86602540378f;

/**
 * @brief Structure that holds values for average velocity vector
 */
struct velocity_vector
{
    unsigned x;
    unsigned y;
    float vel_x;
    float vel_y;
};

/**
 * @brief
 *
 * @param nodes Grid of nodes
 * @param obstacles Grid of obstacles
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 * @param grain_size Size of one side of sqare used to compute average velocity vector
 * @param density Density used for scaling the output vector
 * @return std::vector<struct velocity_vector> Vector of velocity vectors uset to output to file
 */
std::vector<struct velocity_vector> coarse_graining(unsigned char *const *const nodes,
                                                    bool *const *const obstacles,
                                                    unsigned const size_x,
                                                    unsigned const size_y,
                                                    unsigned const grain_size,
                                                    float density);

/**
 * @brief Counts all particles on every position in area bounded by other parameters
 *
 * @param nodes Grid of nodes
 * @param x_lb X lower boundry
 * @param x_ub X upper boundry
 * @param y_lb Y lower boundry
 * @param y_ub Y upper boundry
 * @return std::vector<unsigned> Vector with counts of particles in every position
 */
std::vector<unsigned> count_particles(unsigned char *const *const nodes,
                                      unsigned const x_lb,
                                      unsigned const x_ub,
                                      unsigned const y_lb,
                                      unsigned const y_ub);

} //namespace lga
