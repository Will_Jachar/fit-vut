/**
 * @file collision_rules.cpp
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Implemantation of function for generating full states table from rulesets
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include <algorithm>
#include <vector>

#include "collision_rules.h"

namespace lga
{

// FHP-6
ruleset_t FHP_6_LINEAR = {
    // Rule 1)
    {(E | W), (NE | SW), (NW | SE)},
};
ruleset_t FHP_6_TRIPLE = {
    // Rule 2)
    {(E | NW | SW), (NE | W | SE)},
};
ruleset_t FHP_6_LAMBDA = {
    // Rule 3)
    {(E | NE | SW), (E | NW | SE)},
    {(E | W | SE), (NE | SW | SE)},
    {(E | W | SW), (NW | SW | SE)},
    {(NW | W | SE), (NE | W | SW)},
    {(E | NW | W), (NE | NW | SW)},
    {(E | NE | W), (NE | NW | SE)},
};
ruleset_t FHP_6_DUAL_LINEAR = {
    // Rule 4)
    {(E | NE | W | SW), (E | NW | W | SE), (NE | NW | SW | SE)},
};

// FHP-7
ruleset_t FHP_7_LINEAR = {
    // Rule 1)
    {(E | W), (NE | SW), (NW | SE)},
};
ruleset_t FHP_7_TRIPLE_AND_LINEAR_REST = {
    // Rules 2), 3)
    {(E | W | REST), (NE | SW | REST), (NW | SE | REST), (E | NW | SW), (W | NE | SE)},
};
ruleset_t FHP_7_FUNDAMENTAL_REST_AND_JAY = {
    // Rules 4), 5)
    {(E | REST), (NE | SE)},
    {(NE | REST), (E | NW)},
    {(NW | REST), (W | NE)},
    {(W | REST), (NW | SW)},
    {(SW | REST), (W | SE)},
    {(SE | REST), (SW | E)},
};
ruleset_t FHP_7_LAMBDA_AND_JAY_REST = {
    // Rules 6), 7)
    {(E | NE | SW), (E | NW | SE), (NE | SE | REST)},
    {(E | W | SE), (NE | SW | SE), (E | SW | REST)},
    {(E | W | SW), (NW | SW | SE), (W | SE | REST)},
    {(NW | W | SE), (NE | W | SW), (NW | SW | REST)},
    {(E | NW | W), (NE | NW | SW), (NE | W | REST)},
    {(E | NE | W), (NE | NW | SE), (E | NW | REST)},
};

ruleset_t FHP_7_DUAL_LINEAR_AND_DUAL_TRIPLE_REST = {
    // Rules 8), 9)
    {(E | NW | SW | REST), (NE | W | SE | REST), (E | NE | W | SW), (E | NW | W | SE), (NE | NW | SW | SE)},
};
ruleset_t FHP_7_DUAL_LINEAR_REST = {
    // Rule 10)
    {(E | NE | W | SW | REST), (E | NW | W | SE | REST), (NE | NW | SW | SE | REST)},
};
ruleset_t FHP_7_DUAL_FUNDAMENTAL_AND_DUAL_JAY_REST = {
    // Rules 11), 12)
    {(NE | NW | W | SW | SE), (E | NW | W | SW | REST)},
    {(E | NE | NW | W | SW), (NE | NW | W | SE | REST)},
    {(E | NE | NW | W | SE), (E | NE | NW | SW | REST)},
    {(E | NE | NW | SW | SE), (E | NE | W | SE | REST)},
    {(E | NE | W | SW | SE), (E | NW | SW | SE | REST)},
    {(E | NW | W | SW | SE), (NE | W | SW | SE | REST)},
};
ruleset_t FHP_7_DUAL_LAMBDA_REST_AND_DUAL_JAY = {
    // Rules 13), 14)
    {(E | NE | SW | REST), (E | NW | SE | REST), (E | NE | W | SE)},
    {(E | W | SE | REST), (NE | SW | SE | REST), (E | NW | SW | SE)},
    {(E | W | SW | REST), (NW | SW | SE | REST), (NE | W | SW | SE)},
    {(NW | W | SE | REST), (NE | W | SW | REST), (E | NW | W | SW)},
    {(E | NW | W | REST), (NE | NW | SW | REST), (NE | NW | W | SE)},
    {(E | NE | W | REST), (NE | NW | SE | REST), (E | NE | NW | SW)},
};

// FHP-6 Ruleset
const std::vector<ruleset_t> FHP_6_RULES = {
    FHP_6_LINEAR,
    FHP_6_TRIPLE,
    FHP_6_LAMBDA,
    FHP_6_DUAL_LINEAR,
};

// FHP-7 Ruleset
const std::vector<ruleset_t> FHP_7_RULES = {
    FHP_7_LINEAR,
    FHP_7_TRIPLE_AND_LINEAR_REST,
    FHP_7_FUNDAMENTAL_REST_AND_JAY,
    FHP_7_LAMBDA_AND_JAY_REST,
    FHP_7_DUAL_LINEAR_AND_DUAL_TRIPLE_REST,
    FHP_7_DUAL_FUNDAMENTAL_AND_DUAL_JAY_REST,
    FHP_7_DUAL_LAMBDA_REST_AND_DUAL_JAY,
};

transition_map_t get_fhp6_transition_map()
{
    transition_map_t transition_map(FHP_STATES_COUNT);
    init_fhp_transition_map(transition_map, FHP_6_RULES);

    return transition_map;
}

transition_map_t get_fhp7_transition_map()
{
    transition_map_t transition_map(FHP_STATES_COUNT);
    init_fhp_transition_map(transition_map, FHP_7_RULES);

    return transition_map;
}

void init_fhp_transition_map(transition_map_t &transition_map,
                             const std::vector<ruleset_t> &model)
{
    for (unsigned i = 0; i < transition_map.size(); i++)
    {
        transition_map[i].push_back(i);
    }

    for (auto const &ruleset : model)
    {
        for (auto const &rule : ruleset)
        {
            std::vector<unsigned char> rule_states(rule);
            for (unsigned i = 0; i < rule.size(); i++)
            {
                transition_map[rule_states[0]] = std::vector<unsigned char>(rule_states.begin() + 1, rule_states.end());
                std::rotate(rule_states.begin(), rule_states.begin() + 1, rule_states.end());
            }
        }
    }
}

} // namespace lga
