/**
 * @file collision_rules.h
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Declarations for collision rulesets for particle collisions
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#pragma once

#include <vector>

#include "utils.h"

namespace lga
{

/**
 * @brief Size of whole transition map for FHP LGA
 */
constexpr unsigned char FHP_STATES_COUNT = 128;

/**
 * @brief Bit mask representing particle at certain position in node
 */
constexpr unsigned char E = 0b00000001;
constexpr unsigned char NE = 0b00000010;
constexpr unsigned char NW = 0b00000100;
constexpr unsigned char W = 0b00001000;
constexpr unsigned char SW = 0b00010000;
constexpr unsigned char SE = 0b00100000;
constexpr unsigned char REST = 0b01000000;

/**
 * @brief Get the fhp6 transition map object
 *
 * @return transition_map_t Transition map with rules for FHP-6
 */
transition_map_t get_fhp6_transition_map();
/**
 * @brief Get the fhp7 transition map object
 *
 * @return transition_map_t Transition map with rules for FHP-7
 */
transition_map_t get_fhp7_transition_map();
/**
 * @brief Initializes transition map with all the rules of given model
 *
 * @param transition_map
 * @param model
 */
void init_fhp_transition_map(transition_map_t &transition_map,
                             const std::vector<ruleset_t> &model);

} // namespace lga
