/**
 * @file collisions.cpp
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Implementations for functions declared in collisions.h
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include "collisions.h"
#include "utils.h"

namespace lga
{

void handle_collisions(transition_map_t &transition_map,
                       unsigned char *const *const nodes,
                       bool *const *const obstacles,
                       unsigned const size_x,
                       unsigned const size_y)
{
    for (unsigned x = 0; x < size_x; x++)
    {
        for (unsigned y = 0; y < size_y; y++)
        {
            // Handle internal particle collisions within one node
            if (transition_map[nodes[x][y]].size() == 1)
            {
                nodes[x][y] = transition_map[nodes[x][y]][0];
            }
            else
            {
                int random = std::rand();

                unsigned long size = transition_map[nodes[x][y]].size();
                int index = random % size;
                nodes[x][y] = transition_map[nodes[x][y]][index];
            }

            // Handle particle collisions with obstacles
            if (obstacles[x][y])
            {
                nodes[x][y] = rol(nodes[x][y], 3); // Bounce-Back
            }
        }
    }

    // Handle particle collisions with outer boundaries
    for (unsigned x = 0; x < size_x; x++)
    {
        nodes[x][0] = rol(nodes[x][0], 3);
        nodes[x][size_y - 1] = rol(nodes[x][size_y - 1], 3);
    }
}

} // namespace lga
