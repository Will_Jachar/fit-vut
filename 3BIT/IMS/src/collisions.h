/**
 * @file collisions.h
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Declarations for functions that resolve particle collisions
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#pragma once

#include "utils.h"

namespace lga
{

/**
 * @brief Handles all collisions in model (collisions inside nodes, with obstacles or with borders)
 *
 * @param transition_map Transition map with all collision rules of model
 * @param nodes Grid of nodes
 * @param obstacles Grid of obstacles
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 */
void handle_collisions(transition_map_t &transition_map,
                       unsigned char *const *const nodes,
                       bool *const *const obstacles,
                       unsigned const size_x,
                       unsigned const size_y);

} // namespace lga
