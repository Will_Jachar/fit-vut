/**
 * @file file_handler.cpp
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Implementations for functions declared in file_handler.h
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include <fstream>
#include <sstream>

#include "coarse_graining.h"
#include "file_handler.h"
#include "utils.h"

namespace lga
{

int load_file(std::string const &filepath,
              unsigned &size_x, unsigned &size_y,
              std::vector<std::pair<unsigned, unsigned>> &obstacle_coords)
{
    std::ifstream simulation_file(filepath);
    if (simulation_file.is_open())
    {
        std::string line;

        while (std::getline(simulation_file, line))
        {
            line = trim(line);
            if (!line.empty() && line[0] != '#')
            {
                std::stringstream(line) >> size_x >> size_y;
                break;
            }
        }

        while (std::getline(simulation_file, line))
        {
            line = trim(line);
            if (!line.empty() && line[0] != '#')
            {
                std::pair<unsigned, unsigned> coords;
                std::stringstream(line) >> coords.first >> coords.second;
                obstacle_coords.push_back(coords);
            }
        }

        simulation_file.close();
    }
    else
    {
        return -1;
    }

    return 0;
}

void output_simulation_to_file(std::vector<struct velocity_vector> velocity_vector_field)
{
    std::ofstream out_file;
    out_file.open("./output/simulation.dat", std::ios::out);

    if (out_file.is_open())
    {
        for (auto &vel_vector : velocity_vector_field)
        {
            out_file << vel_vector.x << " " << vel_vector.y << " "
                     << vel_vector.vel_x << " " << vel_vector.vel_y << std::endl;
        }

        out_file.close();
    }
}

void output_obstacle_to_file(std::vector<std::pair<unsigned, unsigned>> const &obstacles_coords)
{
    std::ofstream obstacle_file;
    obstacle_file.open("./output/obstacle.dat", std::ios::out);

    if (obstacle_file.is_open())
    {
        for (const auto &coords : obstacles_coords)
        {
            obstacle_file << coords.first << " " << coords.second << std::endl;
        }

        obstacle_file.close();
    }
}

} // namespace lga
