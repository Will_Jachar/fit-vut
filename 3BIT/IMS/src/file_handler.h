/**
 * @file file_handler.h
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Declarations for functions for handling files
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#pragma once

#include <string>
#include <utility>
#include <vector>

#include "coarse_graining.h"

namespace lga
{

/**
 * @brief
 *
 * @param filepath Filepath to simulation file
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 * @param obstacle_coords Vector with coordinates of obstacles
 * @return int Status code
 */
int load_file(std::string const &filepath,
              unsigned &size_x, unsigned &size_y,
              std::vector<std::pair<unsigned, unsigned>> &obstacle_coords);

/**
 * @brief Writes output data of simulation to file used by gnuplot
 *
 * @param velocity_vector_field Field of velocity vectors
 */
void output_simulation_to_file(std::vector<struct velocity_vector> velocity_vector_field);
/**
 * @brief Writes coordinates of obstacle to file used by gnuplot
 *
 * @param obstacles_coords Coordinates of obstacles
 */
void output_obstacle_to_file(std::vector<std::pair<unsigned, unsigned>> const &obstacles_coords);

} // namespace lga
