/**
 * @file main.cpp
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Main file with main function. This is where it all begins
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <unistd.h>
#include <utility>
#include <vector>

#include "file_handler.h"
#include "simulation.h"
#include "utils.h"

/**
 * @brief Runs the simulation.
 *
 * -f <filename> path to file with defined obstacles coordinates
 * -t <unsigned> Number of cycles for simulation (time)
 * -c <unsigned> Chunk size (grain size) for plotting
 * -d <1,2,3>    Directon of flow, 1 - left to right
                                   2 - topleft to bottomright
                                   3 - bottomleft to topright
 * @param argc Number of arguments
 * @param argv Arguments.
 * @return int Error code.
 */
int main(int argc, char **argv)
{
    int arg;
    bool f_flag = false;
    bool t_flag = false;
    bool c_flag = false;
    bool d_flag = false;
    bool r_flag = false;
    bool m_flag = false;

    std::string str_filename;
    std::string str_cycles;
    std::string str_chunk;
    std::string str_direction;
    std::string str_density;
    std::string str_model;

    unsigned cycles = 200; //Mandary but does not matter
    unsigned chunk = 16;   //Mandary but does not matter
    int direction = 1;
    unsigned size_x = 640; //Load from file
    unsigned size_y = 256; //Load from file
    float density = 1.0f;
    bool fhp_6 = false;

    std::vector<std::pair<unsigned, unsigned>> obstacles;

    while ((arg = getopt(argc, argv, "f:t:c:d:r:m:")) != -1)
    {
        if (arg == 'f')
        {
            f_flag = true;
            str_filename = optarg;
        }
        else if (arg == 't')
        {
            t_flag = true;
            str_cycles = optarg;
        }
        else if (arg == 'c')
        {
            c_flag = true;
            str_chunk = optarg;
        }
        else if (arg == 'd')
        {
            d_flag = true;
            str_direction = optarg;
        }
        else if (arg == 'r')
        {
            r_flag = true;
            str_density = optarg;
        }
        else if (arg == 'm')
        {
            m_flag = true;
            str_model = optarg;
        }
        else if (arg == '?')
        {
            if (optopt == 'f' || optopt == 't' || optopt == 'c' || optopt == 'd' || optopt == 'r' || optopt == 'm')
            {
                std::cerr << std::string("-") + char(optopt) + " needs another argument." << std::endl;
            }
            std::cerr << "Invalid arguments. " << std::endl;
            return -1;
        }
    }

    //f, t, c flags are mandatory
    if (!f_flag || !t_flag || !c_flag || !r_flag || !m_flag || !d_flag)
    {
        std::cerr << "Flags -f, -t, -c, -r, -m, -d are mandatory!" << std::endl;
        return -1;
    }

    if (lga::load_file(str_filename, size_x, size_y, obstacles))
    {
        std::cerr << "File '" << str_filename << "' does not exist." << std::endl;
    }

    try
    {
        cycles = static_cast<unsigned>(std::stoi(str_cycles));
        chunk = static_cast<unsigned>(std::stoi(str_chunk));
        density = std::stof(str_density);
        direction = std::stoi(str_direction);
    }
    catch (std::invalid_argument &e)
    {
        // if no conversion could be performed
        std::cerr << "Invalid argument: " << e.what() << std::endl;
        return -1;
    }
    catch (std::out_of_range &e)
    {
        // if the converted value would fall out of the range of the result type
        // or if the underlying function (std::strtol or std::strtoull) sets errno
        // to ERANGE.
        std::cerr << "Argument is out of range." << e.what() << std::endl;
        return -1;
    }

    if (cycles <= 0)
    {
        std::cerr << "Number of cycles must be greater than 0." << std::endl;
        return -1;
    }

    if (chunk <= 0)
    {
        std::cerr << "Chunk size must be greater than 0." << std::endl;
        return -1;
    }

    if (direction < 0 || direction > 5)
    {
        std::cerr << "Direction argument accepts only values 0-5." << std::endl;
        return -1;
    }

    std::transform(str_model.begin(), str_model.end(), str_model.begin(), ::tolower);

    if (str_model.compare("fhp6") != 0 && str_model.compare("fhp7") != 0)
    {
        std::cerr << "Model argument must be string either: fhp6 or fhp7." << std::endl;
        return -1;
    }
    else if (str_model.compare("fhp6") == 0)
    {
        fhp_6 = true;
    }

    if (density < 0 || density > 1)
    {
        std::cerr << "Density must be between 0 and 1" << std::endl;

        return -1;
    }

    //Initialize random
    std::srand(static_cast<int>(std::time(0)));

    lga::run_simulation(size_x, size_y,
                        cycles, chunk,
                        static_cast<unsigned>(direction),
                        density,
                        fhp_6,
                        obstacles);

    return 0;
}
