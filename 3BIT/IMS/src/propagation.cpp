/**
 * @file propagation.cpp
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Implementations for functions declared in propagation.h
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */
#include "propagation.h"
#include "collision_rules.h"
#include "utils.h"

namespace lga
{
void handle_propagation(unsigned char ***const nodes,
                        unsigned char ***const nodes_next,
                        unsigned const size_x,
                        unsigned const size_y)
{
    for (unsigned x = 0; x < size_x; x++)
    {
        for (unsigned y = 0; y < size_y; y++)
        {
            propagate_particle((*nodes)[x][y], *nodes_next, x, y, size_x, size_y);
        }
    }

    // Update current nodes
    unsigned char **nodes_tmp = *nodes;
    *nodes = *nodes_next;
    *nodes_next = nodes_tmp;

    // Erase nodes_next
    for (unsigned x = 0; x < size_x; x++)
    {
        std::fill_n((*nodes_next)[x], size_y, 0);
    }
}

void propagate_particle(unsigned char current_node,
                        unsigned char *const *const nodes_next,
                        unsigned const x,
                        unsigned const y,
                        unsigned const size_x,
                        unsigned const size_y)
{
    if ((current_node & E) && (x < size_x - 1))
    {
        nodes_next[x + 1][y] |= E;
    }

    if ((current_node & NE) && (x < size_x - 1) && (y < size_y))
    {
        if (y % 2 == 0)
        {
            nodes_next[x + 1][y + 1] |= NE;
        }
        else
        {
            nodes_next[x][y + 1] |= NE;
        }
    }

    if ((current_node & NW) && (x > 0) && (y < size_y))
    {
        if (y % 2 == 0)
        {
            nodes_next[x][y + 1] |= NW;
        }
        else
        {
            nodes_next[x - 1][y + 1] |= NW;
        }
    }

    if ((current_node & W) && (x > 0))
    {
        nodes_next[x - 1][y] |= W;
    }

    if ((current_node & SW) && (x > 0) && (y > 0))
    {
        if (y % 2 == 0)
        {
            nodes_next[x][y - 1] |= SW;
        }
        else
        {
            nodes_next[x - 1][y - 1] |= SW;
        }
    }

    if ((current_node & SE) && (x < size_x - 1) && (y > 0))
    {
        if (y % 2 == 0)
        {
            nodes_next[x + 1][y - 1] |= SE;
        }
        else
        {
            nodes_next[x][y - 1] |= SE;
        }
    }

    if (current_node & REST)
    {
        nodes_next[x][y] |= REST;
    }
}

} // namespace lga
