/**
 * @file propagation.h
 * @author Peter Uhrin    (xuhrin02)
 * @author Libor Moravcik (xmorav35)
 * @brief  Declarations for functions that handle propagations that defines the next lattice.
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */
#pragma once

namespace lga
{

/**
 * @brief Handles propagation of all nodes
 *
 * @param nodes Grid of nodes representing current state
 * @param nodes_next Grid of nodes representing next state
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 */
void handle_propagation(unsigned char ***const nodes,
                        unsigned char ***const nodes_next,
                        unsigned const size_x,
                        unsigned const size_y);
/**
 * @brief Handles propagation of single node
 *
 * @param current_node Curren node
 * @param nodes_next Grid of nodes representing next state
 * @param x X coordinate of current_node
 * @param y Y coordinate of current_node
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 */
void propagate_particle(unsigned char current_node,
                        unsigned char *const *const nodes_next,
                        unsigned const x,
                        unsigned const y,
                        unsigned const size_x,
                        unsigned const size_y);

} // namespace lga
