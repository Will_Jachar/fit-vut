/**
 * @file simulation.cpp
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Implementations for functions declared in simulation.h
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#include <fstream>

#include "coarse_graining.h"
#include "collision_rules.h"
#include "collisions.h"
#include "file_handler.h"
#include "propagation.h"
#include "simulation.h"
#include "utils.h"

namespace lga
{

void run_simulation(unsigned const size_x,
                    unsigned const size_y,
                    unsigned const cycles,
                    unsigned const grain_size,
                    unsigned const direction,
                    float const density,
                    bool const fhp_6,
                    std::vector<std::pair<unsigned, unsigned>> const &obstacles_coords)
{

    unsigned char **nodes = nullptr;
    unsigned char **nodes_next = nullptr;
    bool **obstacles = nullptr;

    // 1) Load transitions map
    transition_map_t transition_map;
    if (fhp_6)
    {
        transition_map = get_fhp6_transition_map();
    }
    else
    {
        transition_map = get_fhp7_transition_map();
    }

    init_mem(&nodes, &nodes_next, &obstacles, size_x, size_y);

    // 2) Create obstacles
    for (const auto &coords : obstacles_coords)
    {
        obstacles[coords.first][coords.second] = true;
    }

    // 3) Initialize nodes
    for (unsigned x = 0; x < size_x; x++)
    {
        for (unsigned y = 0; y < size_y; y++)
        {
            if (!fhp_6)
            {
                if (!(obstacles[x][y]) &&
                    (density > static_cast<float>(std::rand()) / RAND_MAX))
                {
                    nodes[x][y] = REST;
                }
            }
        }
    }

    // 4) Run simulation loop
    for (unsigned simulation_cycles = cycles; simulation_cycles > 0; simulation_cycles--)
    {
        // Generating current
        for (unsigned y = 0; y < size_y; y++)
        {
            if (density > static_cast<float>(std::rand()) / RAND_MAX)
            {
                nodes[0][y] |= (1 << direction);
            }
        }

        // 5) First pass: Handle collisions
        handle_collisions(transition_map, nodes, obstacles, size_x, size_y);

        // 6) Second pass: Propagate
        handle_propagation(&nodes, &nodes_next, size_x, size_y);

#ifdef COUNTER
        std::cout << "Remaining: " << simulation_cycles << "      \r" << std::flush;
#endif
    }

    std::cout << std::endl;

    // 7) Coarse graining
    std::vector<struct velocity_vector> velocity_vector_field = coarse_graining(nodes, obstacles, size_x, size_y, grain_size, density);

    // 8) Output results
    output_obstacle_to_file(obstacles_coords);
    output_simulation_to_file(velocity_vector_field);

    // 9) Free all allocated memmory
    free_mem(&nodes, &nodes_next, &obstacles, size_x);
}

// #####################################################################################

void init_mem(unsigned char ***const nodes, unsigned char ***const nodes_next, bool ***const obstacles,
              const unsigned size_x, const unsigned size_y)
{
    (*nodes) = new unsigned char *[size_x];
    (*nodes_next) = new unsigned char *[size_x];
    (*obstacles) = new bool *[size_x];

    for (unsigned x = 0; x < size_x; x++)
    {
        (*nodes)[x] = new unsigned char[size_y]();
        (*nodes_next)[x] = new unsigned char[size_y]();
        (*obstacles)[x] = new bool[size_y]();
    }
}

// #####################################################################################

void free_mem(unsigned char ***const nodes, unsigned char ***const nodes_next, bool ***const obstacles,
              const unsigned size_x)
{
    for (unsigned x = 0; x < size_x; x++)
    {
        delete[](*nodes)[x];
        delete[](*nodes_next)[x];
        delete[](*obstacles)[x];
    }

    delete[](*nodes);
    delete[](*nodes_next);
    delete[](*obstacles);

    *nodes = nullptr;
    *nodes_next = nullptr;
    *obstacles = nullptr;
}

} // namespace lga
