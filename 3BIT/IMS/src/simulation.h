/**
 * @file simulation.h
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Declarations for functions that handles memmory allocations and deallocations and
 *         runs the simulation.
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */
#pragma once

#include <utility>
#include <vector>

namespace lga
{

/**
 * @brief Runs the main simulations cycle and outputs data
 *
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 * @param cycles Simulation cycles
 * @param grain_size Size of sqare used for averaging
 * @param direction Direction of flow
 * @param density Percentual density
 * @param fhp_6 Used model (false for FHP-7)
 * @param obstacles_coords Coordinates of abstacle
 */
void run_simulation(unsigned const size_x,
                    unsigned const size_y,
                    unsigned const cycles,
                    unsigned const grain_size,
                    unsigned const direction,
                    float const density,
                    bool const fhp_6,
                    std::vector<std::pair<unsigned, unsigned>> const &obstacles_coords);
/**
 * @brief Initializes all dynamic variables used by simulation
 *
 * @param nodes Grid of nodes
 * @param nodes_next Grid of nodes
 * @param obstacles Grid of obstacles
 * @param size_x X size of grid of nodes
 * @param size_y Y size of grid of nodes
 */
void init_mem(unsigned char ***const nodes,
              unsigned char ***const nodes_next,
              bool ***const obstacles,
              unsigned const size_x,
              unsigned const size_y);
/**
 * @brief Frees all dynamic variables used by simulation
 *
 * @param nodes Grid of nodes
 * @param nodes_next Grid of nodes
 * @param obstacles Grid of obstacles
 * @param size_x X size of grid of nodes
 */
void free_mem(unsigned char ***const nodes,
              unsigned char ***const nodes_next,
              bool ***const obstacles,
              unsigned const size_x);

} // namespace lga
