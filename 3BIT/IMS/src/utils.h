/**
 * @file utils.h
 * @author Libor Moravcik (xmorav35)
 * @author Peter Uhrin    (xuhrin02)
 * @brief  Contains neat inline functions to bitwise operations
  *        and other useful functions used across the project.
 * @version 2.3.594 public beta
 * @date 2018-12-09
 */

#pragma once

#include <iostream>
#include <vector>

namespace lga
{

/**
 * @brief Used to store one releset for FHP collisions
 */
typedef std::vector<std::vector<unsigned char>> ruleset_t;
/**
 * @brief Used to store whole transition map for FHP LGA
 */
typedef std::vector<std::vector<unsigned char>> transition_map_t;

/**
 * @brief Number of effective bits used to represent state of one node
 */
constexpr unsigned char FHP_BITS = 7;
/**
 * @brief Number of bits used to represent moving particles in one node
 */
constexpr unsigned char MOVING_BITS = 6;
/**
 * @brief Masked used to mask out unused bits
 */
constexpr unsigned char DATA_MASK = static_cast<unsigned char>(~(0b11111111 << MOVING_BITS));

/**
 * @brief Rotation of moving bits in node
 *
 * @param n Node
 * @param d Rotation steps
 * @return New value after rotation
 */
inline unsigned char rol(unsigned char n, unsigned char d)
{
    return ((n << d) | (n >> (MOVING_BITS - d))) & DATA_MASK;
}

/**
 * @brief Trims whitespaces from both sides of string
 *
 * @param main_string String to be trimmed
 * @return std::string Trimmed string
 */
inline std::string trim(std::string main_string)
{
    const std::string whitespaces = " \f\n\r\t\v";
    std::string tmp_string;

    if (!main_string.empty())
    {
        tmp_string = main_string.substr(0, main_string.find_last_not_of(whitespaces) + 1);
        tmp_string = tmp_string.substr(tmp_string.find_first_not_of(whitespaces));
    }

    return tmp_string;
}

} //namespace lga
