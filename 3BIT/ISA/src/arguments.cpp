/**
 * arguments.cpp
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "arguments.h"

namespace feedreader
{

Arguments::Arguments()
{
    show_time = false;
    show_author = false;
    show_url = false;
}

void Arguments::parse(int argc, char *argv[])
{
    // At least URL is required
    if (argc < 2)
    {
        throw ArgumentsError("URL or path to file with URLs is required");
    }

    // Non flag argument must be URL
    if (argv[1][0] != '-')
    {
        std::regex pattern = url_regex();
        if (std::regex_match(argv[1], pattern))
        {
            // Store URL
            urls.push_back(std::string(argv[1]));
            // Go to next argument
            optind += 1;
        }
    }

    opterr = 0; // supressing errors from getopt

    int option;
    while ((option = getopt(argc, argv, "f:c:C:Tau")) != -1)
    {
        switch (option)
        {
        case 'f':
            if (!urls.empty())
            {
                throw ArgumentsError("Can't combine URL and -f <feedfile>");
            }
            load_urls_from_file(optarg);
            break;
        case 'c':
            cert_filepath = std::string(optarg);
            break;
        case 'C':
            cert_dirpath = std::string(optarg);
            break;
        case 'T':
            show_time = true;
            break;
        case 'a':
            show_author = true;
            break;
        case 'u':
            show_url = true;
            break;
        case '?':
            if (optopt == 'f' || optopt == 'c' || optopt == 'C')
            {
                throw ArgumentsError((std::string("-") + char(optopt) + " needs another argument").c_str());
            }

            throw ArgumentsError("Invalid argument");
        default:
            throw ArgumentsError("Invalid argument");
        }
    }

    if (argc - optind > 0)
    {
        throw ArgumentsError("Invalid argument");
    }
}

std::vector<std::string> Arguments::get_urls()
{
    return urls;
}

std::string Arguments::get_cert_filepath()
{
    return cert_filepath;
}

std::string Arguments::get_cert_dirpath()
{
    return cert_dirpath;
}

bool Arguments::show_time_enabled()
{
    return show_time;
}

bool Arguments::show_author_enabled()
{
    return show_author;
}

bool Arguments::show_url_enabled()
{
    return show_url;
}

void Arguments::load_urls_from_file(const char *filepath)
{
    std::ifstream url_file(filepath);
    if (url_file.is_open())
    {
        std::string line;
        while (std::getline(url_file, line))
        {
            line = trim(line);
            if (!line.empty() && line[0] != '#')
            {
                urls.push_back(line);
            }
        }

        url_file.close();
    }
    else
    {
        throw ArgumentsError((std::string("Unable to open ") + filepath).c_str());
    }
}

} // namespace feedreader
