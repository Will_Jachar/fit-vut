/**
 * arguments.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef ARGUMENTS
#define ARGUMENTS

#include <fstream>
#include <string>
#include <vector>
#include <regex>

#include <unistd.h>

#include "utils.h"
#include "errors.h"

namespace feedreader
{

/**
 * @brief Class that parses and holds program arguments
 */
class Arguments
{
private:
  // Vector wihl loaded URLs
  std::vector<std::string> urls;
  // Path to certification file
  std::string cert_filepath;
  // Path to certification folder
  std::string cert_dirpath;
  // -T argument
  bool show_time;
  // -a argument
  bool show_author;
  // -u argument
  bool show_url;

  /**
   * @brief Loads URLs from file
   *
   * @param filepath path to file
   */
  void load_urls_from_file(const char *filepath);

public:
  /**
   * @brief Construct a new Arguments object
   *
   */
  Arguments();

  /**
   * @brief Parses program arguments
   *
   * @param argc number of arguments
   * @param argv array of arguments
   */
  void parse(int argc, char *argv[]);

  /**
   * @brief Get the urls object
   *
   * @return URLs
   */
  std::vector<std::string> get_urls();

  /**
   * @brief Get the cert filepath object
   *
   * @return Path to certification file
   */
  std::string get_cert_filepath();

  /**
   * @brief Get the cert dirpath object
   *
   * @return Path to certification folder
   */
  std::string get_cert_dirpath();

  /**
   * @brief If -T argument was set
   *
   * @return true / false
   */
  bool show_time_enabled();

  /**
   * @brief If -a argument was set
   *
   * @return true / false
   */
  bool show_author_enabled();

  /**
   * @brief If -u argument was set
   *
   * @return true / false
   */
  bool show_url_enabled();
};

} // namespace feedreader

#endif
