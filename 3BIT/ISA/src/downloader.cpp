/**
 * downloader.cpp
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "downloader.h"

namespace feedreader
{

Downloader::Downloader()
{
    // Init openssl library
    SSL_library_init();
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();

    bio = nullptr;
    ssl = nullptr;
    // Init SSL context
    ctx = SSL_CTX_new(SSLv23_client_method());
}

Downloader::~Downloader()
{
    BIO_free_all(bio);
    SSL_CTX_free(ctx);
}

std::string Downloader::download(std::string raw_url)
{
    parse_url(raw_url);

    if (url.https)
    {
        init_https_connection();
    }
    else
    {
        init_http_connection();
    }

    send_request();

    load_response_headers();

    // Supporst only code 200
    if (response_headers.status != 200)
    {
        throw DownloaderError(("Server responded with code: " + std::to_string(response_headers.status)).c_str());
    }
    // Supports only if response is chunked or Content-Length is not 0
    if (!response_headers.chunked && !response_headers.content_length)
    {
        throw DownloaderError("Response is not chunked and Content-Length is 0");
    }

    std::string content = download_content();

    close_connection();

    return content;
}

void Downloader::close_connection()
{
    // Frees all BIO structures
    BIO_free_all(bio);

    bio = nullptr;
    ssl = nullptr;
}

// #####################################################################################

void Downloader::parse_url(const std::string &raw_url)
{
    std::regex pattern = url_regex();
    std::smatch matches;

    if (std::regex_search(raw_url, matches, pattern))
    {
        url.https = to_lower(matches[1].str()) == "https";
        url.host = matches[2].str();
        url.port = matches[4].str();
        url.path = matches[5].str();

        // Default ports
        if (url.port.empty())
        {
            url.port = url.https ? "443" : "80";
        }
    }
    else
    {
        throw DownloaderError(("Invalid URL " + raw_url).c_str());
    }
}

// #####################################################################################

void Downloader::init_http_connection()
{
    std::string connection_url = url.host + ":" + url.port;
    // Tries to create connection and checks if connection was created
    if ((bio = BIO_new_connect(connection_url.c_str())) == NULL || BIO_do_connect(bio) <= 0)
    {
        throw DownloaderError(("Unable to connect to " + connection_url).c_str());
    }
}

void Downloader::init_https_connection()
{
    std::string connection_url = url.host + ":" + url.port;

    // Tries to create connection object
    bio = BIO_new_ssl_connect(ctx);
    // Inicializes SSL structure
    BIO_get_ssl(bio, &ssl);
    // Set auto retry mode
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    // Sets hostname and tries to connect
    BIO_set_conn_hostname(bio, connection_url.c_str());

    // Check if connection was created
    if (BIO_do_connect(bio) <= 0)
    {
        throw DownloaderError(("Unable to connect to " + connection_url).c_str());
    }
    // Check if handshake was successful
    if (SSL_get_verify_result(ssl) != X509_V_OK)
    {
        throw DownloaderError(("Unable to connect to " + connection_url + ": Invalid certificate").c_str());
    }
}

void Downloader::load_certificates(std::string cert_filepath, std::string cert_dirpath)
{
    // If no path has been given use default
    if (cert_filepath.empty() && cert_dirpath.empty())
    {
        if (!SSL_CTX_set_default_verify_paths(ctx))
        {
            throw DownloaderError("Failed to load default certificates");
        }
    }
    else
    {
        if (!SSL_CTX_load_verify_locations(
                ctx,
                cert_filepath.empty() ? nullptr : cert_filepath.c_str(),
                cert_dirpath.empty() ? nullptr : cert_dirpath.c_str()))
        {
            throw DownloaderError("Invalid or missing certificates");
        }
    }
}

void Downloader::send_request()
{
    // Constructing request headers
    std::string headers = ("GET " + url.path + " HTTP/1.1\r\n" +
                           "Host: " + url.host + ":" + url.port + "\r\n" +
                           "User-Agent: MyLittleFeedreader\r\n" +
                           "Accept: application/xml; charset=UTF-8, text/xml; charset=UTF-8\r\n\r\n");

    if (BIO_write(bio, headers.c_str(), headers.size()) <= 0)
    {
        throw DownloaderError("Error while receiving data");
    }
}

// #####################################################################################

void Downloader::load_response_headers()
{
    std::string raw_headers = download_response_headers();
    parse_response_headers(raw_headers);
}

std::string Downloader::download_response_headers()
{
    std::string raw_headers;

    char buffer;
    // Standart is "\r\n\r\n" but we are not living in ideal world
    while (!ends_with(raw_headers, "\r\n\r\n") && !ends_with(raw_headers, "\n\n"))
    {
        if (BIO_read(bio, &buffer, 1) <= 0)
        {
            throw DownloaderError("Error while receiving data");
        }

        raw_headers += buffer;
    }

    return raw_headers;
}

void Downloader::parse_response_headers(const std::string &raw_headers)
{
    std::smatch matches;

    // HTTP
    if (std::regex_search(
            raw_headers,
            matches,
            std::regex("HTTPS?\\/\\d+.\\d+\\s+(\\d+)\\s+.*", std::regex_constants::icase)))
    {
        response_headers.status = std::stoi(matches[1].str());
    }
    else
    {
        throw DownloaderError("STATUS missing in response HTTP headers");
    }

    // Transfer-Encoding
    response_headers.chunked = std::regex_search(
        raw_headers,
        matches,
        std::regex("Transfer-Encoding:\\s*chunked\\s*", std::regex_constants::icase));

    // Content-Length
    if (std::regex_search(
            raw_headers,
            matches,
            std::regex("Content-Length:\\s*(\\d+)\\s*", std::regex_constants::icase)))
    {
        response_headers.content_length = std::stoi(matches[1].str());
    }
    else
    {
        response_headers.content_length = 0;
    }
}

// #####################################################################################

std::string Downloader::download_content()
{
    std::string content;

    if (response_headers.chunked)
    {
        std::string line;
        int chunk_size;
        // Download chunk size and then chunk of that size
        while ((chunk_size = download_chunk_size()))
        {
            line = download_chunk(chunk_size);
            content += line;
        }
    }
    else
    {
        content = download_chunk(response_headers.content_length);
    }

    return content;
}

std::string Downloader::download_chunk(int chunk_size)
{
    std::string chunk;
    int remaining_bytes = chunk_size;

    char buffer[BUFFER_SIZE];
    // Repeat unit all data has been recieved
    while (remaining_bytes > 0)
    {
        int receved = 0;
        // Tries to receive lessr value of those two
        if ((receved = BIO_read(
                 bio,
                 buffer,
                 (remaining_bytes > BUFFER_SIZE - 1 ? BUFFER_SIZE - 1 : remaining_bytes))) <= 0)
        {
            throw DownloaderError("Error while receiving data");
        }

        // End string
        buffer[receved] = '\0';
        chunk += buffer;
        remaining_bytes -= receved;
    }

    if (response_headers.chunked)
    {
        if (BIO_read(bio, buffer, 1) <= 0)
        {
            throw DownloaderError("Error while receiving data");
        }
        // If '\r' then there must be '\n'
        if (buffer[0] == '\r')
        {
            if (BIO_read(bio, buffer, 1) <= 0)
            {
                throw DownloaderError("Error while receiving data");
            }
        }
    }

    return chunk;
}

int Downloader::download_chunk_size()
{
    std::string line;

    char buffer;
    int ret;
    // Chunk size is on one line
    while (!ends_with(line, "\r\n") && !ends_with(line, "\n"))
    {
        if ((ret = BIO_read(bio, &buffer, 1)) <= 0)
        {
            throw DownloaderError("Error while receiving data");
        }

        line += buffer;
    }

    return std::stoi(line, nullptr, 16);
}

} // namespace feedreader
