/**
 * downloader.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef DOWNLOADER
#define DOWNLOADER

#include <string>
#include <regex>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/bio.h>

#include "utils.h"
#include "errors.h"

#define BUFFER_SIZE 65536 // 64 KB

namespace feedreader
{

/**
 * @brief Structure holding parsed URL
 */
typedef struct
{
     bool https;
     std::string host;
     std::string port;
     std::string path;
} parsed_url;

/**
 * @brief Structure loding parsed HTTP headers
 */
typedef struct
{
     int status;
     int content_length;
     bool chunked;
} headers;

/**
 * @brief Class for handling network communication
 */
class Downloader
{
   private:
     // Currently used URL
     parsed_url url;
     // Currently returned headers
     headers response_headers;
     // BIO
     BIO *bio;
     // CTX
     SSL_CTX *ctx;
     // SSL
     SSL *ssl;

     /**
       * @brief Closes connection and frees all BIO structures
       */
     void close_connection();

     /**
       * @brief Parses URL
       *
       * @param raw_url unparsed URL string
       */
     void parse_url(const std::string &raw_url);

     /**
       * @brief Handles unsecured connection
       *
       * @return Unparsed content
       */
     std::string download_http();

     /**
       * @brief Handles secured connection
       *
       * @return Unparsed content
       */
     std::string download_https();

     /**
       * @brief Creates unsecured connection
       */
     void init_http_connection();

     /**
       * @brief Creates secured connection
       */
     void init_https_connection();

     /**
       * @brief Sends request to server
       */
     void send_request();

     /**
       * @brief Handles response headers
       */
     void load_response_headers();

     /**
       * @brief Downloades response headers
       *
       * @return Unparsed response headers
       */
     std::string download_response_headers();

     /**
       * @brief Parses response headers
       *
       * @param raw_headers Unpased response headers
       */
     void parse_response_headers(const std::string &raw_headers);

     /**
       * @brief Handles downloading content
       *
       * @return Content
       */
     std::string download_content();

     /**
       * @brief Donwloades chunk of fixed size
       *
       * @param chunk_size Size to download
       * @return Downloaded chunk
       */
     std::string download_chunk(int chunk_size);

     /**
       * @brief Downloades chunk size
       *
       * @return Chunk size
       */
     int download_chunk_size();

   public:
     /**
       * @brief Construct a new Downloader object
       */
     Downloader();

     /**
       * @brief Destroy the Downloader object
       */
     ~Downloader();

     /**
       * @brief Public interface for downloading content from given URL
       *
       * @param url URL
       * @return Content
       */
     std::string download(std::string url);

     /**
       * @brief Loads certificates
       *
       * @param cert_filepath Path to certification file
       * @param cert_dirpath Path to certification folder
       */
     void load_certificates(std::string cert_filepath, std::string cert_dirpath);
};

} // namespace feedreader

#endif
