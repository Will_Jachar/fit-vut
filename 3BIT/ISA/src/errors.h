/**
 * errors.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#pragma once

#include <stdexcept>

namespace feedreader
{

/**
 * @brief Base custom exception
 */
class FeedreaderError : public std::runtime_error
{
public:
  FeedreaderError(const char *message) : std::runtime_error(message) {}
};

/**
 * @brief Exception thrown by Downloader class
 */
class DownloaderError : public FeedreaderError
{
public:
  DownloaderError(const char *message) : FeedreaderError(message) {}
};

/**
 * @brief Exception thrown by Arguments class
 */
class ArgumentsError : public FeedreaderError
{
public:
  ArgumentsError(const char *message) : FeedreaderError(message) {}
};

/**
 * @brief Exception thrown by XMLParser class
 */
class XMLParserError : public FeedreaderError
{
public:
  XMLParserError(const char *message) : FeedreaderError(message) {}
};

} // namespace feedreader
