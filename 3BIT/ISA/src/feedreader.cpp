/**
 * feedreader.cpp
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "feedreader.h"

/**
 * Main function of feedreader
 *
 * @param   argc number of arguments
 * @param   argv array of arguments
 * @return  1 if error has occured, else keep running until interruped
 */
int main(int argc, char *argv[])
{
    try
    {
        feedreader::Arguments arguments = feedreader::Arguments();
        feedreader::Downloader downloader = feedreader::Downloader();
        try
        {
            // Setting up arguments and downloader
            arguments.parse(argc, argv);
            downloader.load_certificates(
                arguments.get_cert_filepath(),
                arguments.get_cert_dirpath());
        }
        catch (const feedreader::FeedreaderError &error)
        {
            std::cerr << "Feedreader error: " << error.what() << "\n\n";
            print_help();
            return EXIT_FAILURE;
        }

        // Setting up parser
        feedreader::XMLParser parser = feedreader::XMLParser(
            arguments.show_time_enabled(),
            arguments.show_author_enabled(),
            arguments.show_url_enabled());

        std::vector<std::string> urls = arguments.get_urls();
        // For every URL
        for (auto const &url : urls)
        {
            try
            {
                std::string raw_content = downloader.download(url);

                feedreader::parsed_content content = parser.parse(raw_content);
                print_parsed_content(content);
            }
            catch (const feedreader::FeedreaderError &error)
            {
                std::cerr << "Feedreader error: " << error.what() << std::endl;

                // If there was only one URL exit, else try another URL
                if (urls.size() == 1)
                {
                    return EXIT_FAILURE;
                }
            }

            // Print newline only if its not the last URL
            if (&url != &urls.back())
            {
                std::cout << std::endl;
            }
        }
    }
    catch (const std::exception &error)
    {
        std::cerr << "Internal error: " << error.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// #####################################################################################

void print_help()
{
    std::cout << "Usage:\n"
              << "  feedreader <URL | -f <feedfile>> [-c <certfile>] [-C <certaddr>] [-T] [-a] [-u]\n";
}

void print_parsed_content(feedreader::parsed_content &content)
{
    // Title
    std::string printable_content = "*** " + content.title + " ***\n";

    for (auto const &entry : content.entries)
    {
        // Additional informations
        printable_content += !entry.title.empty() ? entry.title + "\n" : "";
        printable_content += !entry.updated.empty() ? "Updated: " + entry.updated + "\n" : "";
        printable_content += !entry.author.empty() ? "Author: " + entry.author + "\n" : "";
        printable_content += !entry.url.empty() ? "URL: " + entry.url + "\n" : "";
        // If some additional imformation was printed and its not the last entry newline is printed
        printable_content += ((&entry != &content.entries.back()) &&
                              (!entry.updated.empty() || !entry.author.empty() || !entry.url.empty()))
                                 ? "\n"
                                 : "";
    }

    std::cout << printable_content;
}
