/**
 * feedreader.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef FEEDREADER
#define FEEDREADER

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#include "utils.h"
#include "errors.h"
#include "arguments.h"
#include "downloader.h"
#include "xmlparser.h"

/**
 * Prints help
 */
void print_help();

/**
 * Prints structured content
 *
 * @param   content structured content
 */
void print_parsed_content(feedreader::parsed_content &content);

#endif
