/**
 * utils.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#pragma once

#include <algorithm>
#include <regex>
#include <string>

namespace feedreader
{

/**
 * @brief Converts string to lower
 *
 * @param main_string String to be converted
 * @return Converted string
 */
inline std::string to_lower(std::string main_string)
{
    std::transform(main_string.begin(), main_string.end(), main_string.begin(), ::tolower);
    return main_string;
}

/**
 * @brief Trims whitespaces from string
 *
 * @param main_string String to be trimmed
 * @return Trimmed string
 */
inline std::string trim(std::string main_string)
{
    const std::string whitespaces = " \f\n\r\t\v";
    std::string tmp_string;

    if (!main_string.empty())
    {
        tmp_string = main_string.substr(0, main_string.find_last_not_of(whitespaces) + 1);
        tmp_string = tmp_string.substr(tmp_string.find_first_not_of(whitespaces));
    }

    return tmp_string;
}

/**
 * @brief Checks if string ends with substring
 *
 * @param main_string String to be chacked
 * @param substring Chaces if this shubstring is on the end
 * @return true / false
 */
inline bool ends_with(const std::string &main_string, const std::string &substring)
{
    if (main_string.size() >= substring.size() &&
        main_string.compare(main_string.size() - substring.size(), substring.size(), substring) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief Returns compiled regex patter for matching valid URL
 *
 * @return Compiled regex patter
 */
inline std::regex url_regex()
{
    return std::regex(
        "^"
        "(http|https)" // Protocol -> Group 1
        ":\\/\\/"
        "([\\w\\d.$+!*'(),_-]*?)"          // Host -> Group 2
        "(:(\\d+))?"                       // Port -> Group 4
        "(\\/[\\w\\d\\/.?&~$+!*'(),=_-]*)" // Path -> Group 5
        "$",
        std::regex_constants::icase);
}

} // namespace feedreader
