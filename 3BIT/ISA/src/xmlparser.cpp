/**
 * xmlparser.cpp
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#include "xmlparser.h"

namespace feedreader
{

XMLParser::XMLParser(bool show_time, bool show_author, bool show_url)
{
    this->show_time = show_time;
    this->show_author = show_author;
    this->show_url = show_url;
    this->xml_content = nullptr;
    this->xml_root_node = nullptr;

    // Delete plank nodes
    xmlKeepBlanksDefault(0);
}

XMLParser::~XMLParser()
{
    xmlFreeDoc(xml_content);
    xmlCleanupParser();
}

parsed_content XMLParser::parse(std::string &raw_content)
{
    parsed_content content;

    // Parse XML pro in memory object
    if (!(xml_content = xmlReadMemory(
              raw_content.c_str(), raw_content.length(), nullptr, "UTF-8", 1)))
    {
        throw XMLParserError("Unable to parse content");
    }

    // Check if parsing was seccessful
    if (!(xml_root_node = xmlDocGetRootElement(xml_content)))
    {
        throw XMLParserError("Empty content");
    }

    std::string node_name = (char *)xml_root_node->name;
    // RSS 1.0
    if (node_name == "RDF")
    {
        if (xml_root_node->children && xml_root_node->children->children)
        {
            content.title = get_title(xml_root_node->children->children);
            content.entries = get_rss_entries(xml_root_node->children);
        }
        else
        {
            throw XMLParserError("XML with invalid RSS format");
        }
    }
    // RSS 2.0
    else if (node_name == "rss")
    {
        if (xml_root_node->children && xml_root_node->children->children)
        {
            content.title = get_title(xml_root_node->children->children);
            content.entries = get_rss_entries(xml_root_node->children->children);
        }
        else
        {
            throw XMLParserError("XML with invalid RSS format");
        }
    }
    // ATOM
    else if (node_name == "feed")
    {
        content.title = get_title(xml_root_node->children);
        content.entries = get_atom_entries(xml_root_node->children);
    }
    else
    {
        throw XMLParserError(("Unsupported format: " + node_name).c_str());
    }

    reset_parser();

    return content;
}

std::string XMLParser::get_title(xmlNodePtr xml_context_node)
{
    // Iterate unit title is found
    for (; xml_context_node != nullptr; xml_context_node = xml_context_node->next)
    {
        std::string node_name = (char *)xml_context_node->name;

        if (node_name == "title")
        {
            return get_content(xml_context_node);
        }
    }

    return std::string();
}

std::vector<parsed_entry> XMLParser::get_rss_entries(xmlNodePtr xml_entries_node)
{
    std::vector<parsed_entry> entries;

    // Iterate over all entries
    for (; xml_entries_node != nullptr; xml_entries_node = xml_entries_node->next)
    {
        std::string node_name = (char *)xml_entries_node->name;
        if (node_name == "item")
        {
            entries.push_back(parse_rss_entry(xml_entries_node));
        }
    }

    return entries;
}

std::vector<parsed_entry> XMLParser::get_atom_entries(xmlNodePtr xml_entries_node)
{
    std::vector<parsed_entry> entries;

    // Iterate over all entries
    for (; xml_entries_node != nullptr; xml_entries_node = xml_entries_node->next)
    {
        std::string node_name = (char *)xml_entries_node->name;
        if (node_name == "entry")
        {
            entries.push_back(parse_atom_entry(xml_entries_node));
        }
    }

    return entries;
}

parsed_entry XMLParser::parse_rss_entry(xmlNodePtr xml_first_entry_node)
{
    parsed_entry entry;

    xmlNodePtr xml_entry_node = xml_first_entry_node->children;
    // Iterate over all data of one entry
    for (; xml_entry_node != nullptr; xml_entry_node = xml_entry_node->next)
    {
        std::string node_name = (char *)xml_entry_node->name;

        // Get requested data
        if (node_name == "title")
        {
            entry.title = get_content(xml_entry_node);
        }
        else if ((node_name == "pubDate" || node_name == "date") && show_time)
        {
            entry.updated = get_content(xml_entry_node);
        }
        else if ((node_name == "author" || node_name == "creator") && show_author)
        {
            entry.author = get_content(xml_entry_node);
        }
        else if (node_name == "link" && show_url)
        {
            entry.url = get_content(xml_entry_node);
        }
    }

    return entry;
}

parsed_entry XMLParser::parse_atom_entry(xmlNodePtr xml_first_entry_node)
{
    parsed_entry entry;

    xmlNodePtr xml_entry_node = xml_first_entry_node->children;
    // Iterate over all data of one entry
    for (; xml_entry_node != nullptr; xml_entry_node = xml_entry_node->next)
    {
        std::string node_name = (char *)xml_entry_node->name;

        // Get requested data
        if (node_name == "title")
        {
            entry.title = get_content(xml_entry_node);
        }
        else if (node_name == "updated" && show_time)
        {
            entry.updated = get_content(xml_entry_node);
        }
        else if (node_name == "author" && show_author)
        {
            xmlNodePtr xml_author_node = xml_entry_node->children;
            for (; xml_author_node != nullptr; xml_author_node = xml_author_node->next)
            {
                std::string author_node_name = (char *)xml_author_node->name;

                if (author_node_name == "name" || author_node_name == "email")
                {
                    // If there are multiple authors, they are separated by ', '
                    if (!entry.author.empty())
                    {
                        entry.author += ", ";
                    }
                    entry.author += get_content(xml_author_node);
                }
            }
        }
        else if (node_name == "link" && show_url)
        {
            xmlChar *href;
            // URL is is href property in ATOM
            if ((href = xmlGetProp(xml_entry_node, (const xmlChar *)"href")))
            {
                entry.url = std::string((char *)href);
                xmlFree(href);
            }
        }
    }

    return entry;
}

std::string XMLParser::get_content(xmlNodePtr xml_content_node)
{
    std::string content;

    // Check if everything is not nullptr to prevent SEGFAULT
    if (xml_content_node && xml_content_node->children && xml_content_node->children->content)
    {
        content = (char *)xml_content_node->children->content;
    }

    return content;
}

void XMLParser::reset_parser()
{
    xmlFreeDoc(xml_content);

    xml_content = nullptr;
    xml_root_node = nullptr;
}

} // namespace feedreader
