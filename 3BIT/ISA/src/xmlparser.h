/**
 * xmlparser.h
 *
 * ISA Project
 *
 * @author Peter Uhrín (xuhrin02)
 *
 */

#ifndef XMLPARSER
#define XMLPARSER

#include <string>
#include <vector>

#include <libxml/parser.h>

#include "errors.h"

namespace feedreader
{

/**
 * @brief Holds structured data from one parsed entry
 */
typedef struct
{
    std::string title;
    std::string updated;
    std::string author;
    std::string url;
} parsed_entry;

/**
 * @brief Holds structured data from parsed downloaded content
 */
typedef struct
{
    std::string title;
    std::vector<parsed_entry> entries;
} parsed_content;

/**
 * @brief Class used to parse downloaded XML content
 */
class XMLParser
{
  private:
    // Search for update | pubDate element
    bool show_time;
    // Search for author | creator element
    bool show_author;
    // Search for link | link href element
    bool show_url;
    // Parsed XML content
    xmlDocPtr xml_content;
    // Pointer to rood element
    xmlNodePtr xml_root_node;

    /**
     * @brief Frees all libxml2 structures
     */
    void reset_parser();

    /**
     * @brief Get the title
     *
     * @param xml_context_node Linked list where should title element be
     * @return Title
     */
    std::string get_title(xmlNodePtr xml_context_node);

    /**
     * @brief Get parsed rdf | rss entries
     *
     * @param xml_entries_node Linked list where should entries element be
     * @return Vector of structured entries
     */
    std::vector<parsed_entry> get_rss_entries(xmlNodePtr xml_entries_node);

    /**
     * @brief Get parsed atom entries
     *
     * @param xml_entries_node Linked list where should entries element be
     * @return Vector of structured entries
     */
    std::vector<parsed_entry> get_atom_entries(xmlNodePtr xml_entries_node);

    /**
     * @brief Parse one rdf | rss entry
     *
     * @param xml_first_entry_node Linked list with data of one entry
     * @return Structured entry
     */
    parsed_entry parse_rss_entry(xmlNodePtr xml_first_entry_node);

    /**
     * @brief Parse one atom entry
     *
     * @param xml_first_entry_node Linked list with data of one entry
     * @return Structured entry
     */
    parsed_entry parse_atom_entry(xmlNodePtr xml_first_entry_node);

    /**
     * @brief Securly get text content from parent
     *
     * @param xml_content_node Node to extract data from
     * @return Content
     */
    std::string get_content(xmlNodePtr xml_content_node);

  public:
    /**
     * @brief Construct a new XMLParser object
     *
     * @param show_time Search for update | pubDate element
     * @param show_author Search for author | creator element
     * @param show_url Search for link | link href element
     */
    XMLParser(bool show_time, bool show_author, bool show_url);

    /**
     * @brief Destroy the XMLParser object
     */
    ~XMLParser();

    /**
     * @brief Parses raw content into structured data
     *
     * @param raw_content Unstructured content
     * @return Structured content
     */
    parsed_content parse(std::string &raw_content);
};

} // namespace feedreader

#endif
