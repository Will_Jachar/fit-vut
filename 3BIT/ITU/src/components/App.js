import React, { Component } from "react";

import Path from "./Path";
import Content from "./Content";
import "./static/css/app.css"
import ReactTooltip from 'react-tooltip'

import dummyData from "../dummyData.json"

export default class App extends Component {
  constructor(props) {
    super(props);
    this.content = React.createRef();
  }

  state = {
    leftPathElements: ["peter"],
    rightPathElements: ["peter"],

    ignoreWhitespaces: false,
  };

  getLayer = (pathToContent) => {
    let currentLocation = dummyData;
    for (let layer of pathToContent) {
      currentLocation = currentLocation.content[layer];
    }

    if (!currentLocation) {
      currentLocation = dummyData.content;
    }

    return currentLocation;
  }

  handleAddToLeftPath = (element) => {
    this.setState((prevState) => ({
      leftPathElements: prevState.leftPathElements.concat(element)
    }));
  };

  handleRemoveFromLeftPath = (elementIndex) => {
    this.setState((prevState) => ({
      leftPathElements: prevState.leftPathElements.slice(0, elementIndex + 1)
    }));
  };

  handleAddToRightPath = (element) => {
    this.setState((prevState) => ({
      rightPathElements: prevState.rightPathElements.concat(element)
    }));
  };

  handleRemoveFromRightPath = (elementIndex) => {
    this.setState((prevState) => ({
      rightPathElements: prevState.rightPathElements.slice(0, elementIndex + 1)
    }));
  };

  handleSwitchSides = () => {
    this.setState((prevState) => ({
      leftPathElements: prevState.rightPathElements,
      rightPathElements: prevState.leftPathElements
    }));
  };

  handleToggleWhitespaces = () => {
    console.log("Whitespaces just disappeared!")
  };

  allMerge = () => {

  };

  render() {
    let diffViewEnabled = (this.getLayer(this.state.leftPathElements).file &&
      this.getLayer(this.state.rightPathElements).file);
    return (
      <main className="App container-fluid">
        <div className="row path-row">
          <div className="col-6">
            <Path
              pathElements={this.state.leftPathElements}
              handleRemoveFromPath={this.handleRemoveFromLeftPath}
            />
          </div>
          <div className="col-6">
            <Path
              pathElements={this.state.rightPathElements}
              handleRemoveFromPath={this.handleRemoveFromRightPath}
            />
          </div>
        </div>
        <Content
          ref={this.content}
          pathToLeftContent={this.state.leftPathElements}
          pathToRightContent={this.state.rightPathElements}
          ignoreWhitespaces={this.state.ignoreWhitespaces}
          handleAddToLeftPath={this.handleAddToLeftPath}
          handleAddToRightPath={this.handleAddToRightPath}
        />

        <div className="row control-row">
          <div className="col-12 control-col">
            <ReactTooltip />
            {
              diffViewEnabled &&
              <div>
                 <ReactTooltip />
                <button
                  type="button"
                  className="btn btn-primary lock-scroll"
                  data-toggle="button"
                  aria-pressed="false"
                  autoComplete="off"
                  data-tip="Lock scroll"
                  onClick={() => { document.querySelector(".CodeMirror-merge-scrolllock").click(); }}
                >
                </button>
                <button
                  className="btn btn-primary merge-left-button"
                  onClick={() => { this.content.current.diffView.current.mergeLeft(); }}
                  data-tip="Merge left"
                >
                </button>
              </div>
            }
            <button
              className="btn btn-primary switch-button"
              data-tip="Switch sides"
              onClick={this.handleSwitchSides}
            >
            </button>
            {
              diffViewEnabled &&
              <div>
                <button
                  className="btn btn-primary merge-right-button"
                  onClick={() => { this.content.current.diffView.current.mergeRight(); }}
                  data-tip="Merge right"
                >
                </button>
                <button
                  type="button"
                  className="btn btn-primary toggle-whitespace"
                  data-toggle="button"
                  aria-pressed="false"
                  autoComplete="off"
                  data-tip="Toggle whitespace"
                  onClick={this.handleToggleWhitespaces}
                >
                </button>
              </div>
            }
          </div>
        </div>
      </main>
    );
  }
}
