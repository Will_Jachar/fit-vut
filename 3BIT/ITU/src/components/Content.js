import React, { Component } from "react";

import DiffView from "./DiffView";
import FolderView from "./FolderView";
import FileView from "./FileView";

import dummyData from "../dummyData.json"

export default class Content extends Component {
  constructor(props) {
    super(props);

    this.diffView = React.createRef();
    this.leftContent = this.getContent(props.pathToLeftContent);
    this.rightContent = this.getContent(props.pathToRightContent);
  }

  getContent = (pathToContent) => {
    let currentLocation = dummyData;
    for (let layer of pathToContent) {
      currentLocation = currentLocation.content[layer];
    }

    if (!currentLocation) {
      currentLocation = dummyData.content;
    }

    return currentLocation;
  }

  componentWillUpdate(nextProps) {
    this.leftContent = this.getContent(nextProps.pathToLeftContent);
    this.rightContent = this.getContent(nextProps.pathToRightContent);
  }

  render() {
    let leftContentComponent = undefined;
    let rightContentComponent = undefined;

    if (this.leftContent.file && this.rightContent.file) {
      return (
        <div className="row content-row">
          <div className="col-12 content-col">
            <DiffView
              ref={this.diffView}
              leftContent={this.leftContent.content}
              rightContent={this.rightContent.content}
              ignoreWhitespaces={this.props.ignoreWhitespaces}
            />
          </div>
        </div>
      );
    } else {
      leftContentComponent = (
        this.leftContent.file ?
          <FileView
            fileType={this.leftContent.file_type}
            content={this.leftContent.content}
          /> :
          <FolderView
            folders={this.leftContent.content}
            handleAddToPath={this.props.handleAddToLeftPath}
          />
      );
      rightContentComponent = (
        this.rightContent.file ?
          <FileView
            fileType={this.rightContent.file_type}
            content={this.rightContent.content}
          /> :
          <FolderView
            folders={this.rightContent.content}
            handleAddToPath={this.props.handleAddToRightPath}
          />
      );

      return (
        <div className="row content-row">
          <div className="col-6 content-col">
            {leftContentComponent}
          </div>
          <div className="col-6 content-col">
            {rightContentComponent}
          </div>
        </div>
      );
    }
  }
}
