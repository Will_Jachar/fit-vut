import React, { Component } from "react";
import CodeMirror from "codemirror";
import "codemirror/addon/merge/merge.css";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
import "codemirror/theme/neat.css";
import "./static/css/diffView.css"

import "codemirror/addon/merge/merge"
import "codemirror/mode/xml/xml";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/python/python";
import "diff-match-patch-js-browser-and-nodejs/diff_match_patch_uncompressed.js"


class DiffView extends Component {
  leftContent = undefined;
  rightContent = undefined;

  updateContent = () => {
    document.getElementById("merge-view").innerHTML = "";

    CodeMirror.MergeView(document.getElementById("merge-view"), {
      value: this.leftContent,
      orig: this.rightContent,
      lineNumbers: true,
      mode: "python",
      highlightDifferences: true
    });

    document.querySelector(".CodeMirror-merge-scrolllock").click();
  };

  mergeLeft = () => {
    this.leftContent = this.rightContent;
    this.updateContent();
  };

  mergeRight = () => {
    this.rightContent = this.leftContent;
    this.updateContent();
  };

  componentDidMount = () => {
    this.leftContent = this.props.leftContent.join("\n");
    this.rightContent = this.props.rightContent.join("\n");
    this.updateContent();
  };

  componentDidUpdate = () => {
    this.leftContent = this.props.leftContent.join("\n");
    this.rightContent = this.props.rightContent.join("\n");
    this.updateContent();
  };

  render() {
    return (
      <div id="merge-view"></div>
    );
  };
};

export default DiffView;
