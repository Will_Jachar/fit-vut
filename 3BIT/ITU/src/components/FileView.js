import React from "react";
import { Controlled as CodeMirror } from "react-codemirror2";
import "codemirror/addon/dialog/dialog.css"
import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";
import "codemirror/theme/neat.css";

import "codemirror/addon/search/search"
import "codemirror/addon/search/searchcursor"
import "codemirror/addon/search/jump-to-line"
import "codemirror/addon/dialog/dialog.js"
import "codemirror/mode/xml/xml.js";
import "codemirror/mode/javascript/javascript";
import "codemirror/mode/python/python";
import "codemirror/mode/clike/clike"
import "codemirror/mode/css/css"
import "codemirror/mode/htmlmixed/htmlmixed"

const FileView = (props) => {
  let options = {
    mode: props.fileType,
    theme: "neat",
    lineNumbers: true
  };

  return (
    <CodeMirror
      value={props.content.join("\n")}
      options={options}
    />
  );
};

export default FileView;
