import React from "react";

import "./static/css/folderView.css"

const FolderView = (props) => (
  <div className="folder-view">
    {
      Object.keys(props.folders).map((name, index) => (
        <button
          className={ "folder-button " }
          key={index}
          onClick={() => props.handleAddToPath(name)}
        >
          <i className={props.folders[name].icon[0] + " " + props.folders[name].icon[2]}>
            <i className= { "ext-icon " +  props.folders[name].icon[1] }></i>
          </i><br />
          {name}
        </button>
      ))
    }
  </div>
);

export default FolderView;
