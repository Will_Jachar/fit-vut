import React from "react";

import PathElement from "./PathElement";
import "./static/css/path.css"

const Path = (props) => (
  <div className="path scrolling-wrapper">
    {
      props.pathElements.map((element, index) => (
        <PathElement
          key={index}
          index={index}
          name={element}
          handleRemoveFromPath={props.handleRemoveFromPath}
        />
      ))
    }
  </div>
);

export default Path;
