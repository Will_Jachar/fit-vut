import React from "react";
import "./static/css/pathElement.css"

const PathElement = (props) => (
  <div className="path-element">
    <button
      type="button"
      className="btn btn-primary btn-arrow-right"
      onClick={() => props.handleRemoveFromPath(props.index)}>
      {props.name}
    </button>
  </div>
);

export default PathElement;
