# FIT VUT - School projects

## [1-3]BIT Bachelor's degree ([Information Technology](https://www.fit.vut.cz/study/field/1/.en))
### 1BIT

- [AIT: English for IT](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/AIT)
- [IJC: The C Programming Language](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/IJC)
- [INC: Digital Systems Design](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/INC)
- [IOS: Operating Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/IOS)
- [ISJ: Scripting Languages](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/ISJ)
- [IUS: Introduction to Software Engineering](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/IUS)
- [IVS: Practical Aspects of Software Design](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/IVS)
- [IZP: Introduction to Programming Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1BIT/IZP)
### 2BIT

- [IAL: Algorithms](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IAL)
- [ICP: The C++ Programming Language](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/ICP)
- [IDS: Database Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IDS)
- [IFJ: Formal Languages and Compilers](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IFJ)
- [INP: Design of Computer Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/INP)
- [IPA: Advanced Assembly Languages](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IPA)
- [IPK: Computer Communications and Networks](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IPK)
- [IPP: Principles of Programming Languages](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/IPP)
- [ISS: Signals and Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/ISS)
- [ITW: Web Design](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2BIT/ITW)
### 3BIT

- [IIS: Information Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/3BIT/IIS)
- [IMP: Microprocessors and Embedded Systems](https://bitbucket.org/Will_Jachar/fit-vut/src/master/3BIT/IMP)
- [IMS: Modelling and Simulation](https://bitbucket.org/Will_Jachar/fit-vut/src/master/3BIT/IMS)
- [ISA: Network Applications and Network Administration](https://bitbucket.org/Will_Jachar/fit-vut/src/master/3BIT/ISA)
- [ITU: User Interface Programming](https://bitbucket.org/Will_Jachar/fit-vut/src/master/3BIT/ITU)

## [1-2]MIT Masters's degree ([Machine Learning](https://www.fit.vut.cz/study/field/39/.en))
### 1MIT

- [AVS: Computation Systems Architectures](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/AVS)
- [FLP: Functional and Logic Programming](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/FLP)
- [KNN: Convolutional Neural Networks](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/KNN)
- [PRL: Parallel and Distributed Algorithms](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/PRL)
- [SUI: Artificial Intelligence and Machine Learning](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/SUI)
- [SUR: Machine Learning and Recognition](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/SUR)
- [UPA: Data Storage and Preparation](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/UPA)
- [VIN: Computer Art](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/VIN)
- [VYF: Computational Photography](https://bitbucket.org/Will_Jachar/fit-vut/src/master/1MIT/VYF)
### 2MIT

- [BIS: Information System Security](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2MIT/BIS)
- [DIP: Master's Thesis](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2MIT/DIP)
- [SFC: Soft Computing](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2MIT/SFC)
- [WAP: Internet Applications](https://bitbucket.org/Will_Jachar/fit-vut/src/master/2MIT/WAP)
